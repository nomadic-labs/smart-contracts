#! /bin/bash

echo 'Step 1: list all addresses'
tezos-client rpc get /chains/main/blocks/head/context/contracts > contracts

echo 'Step 2: filter KT1s'
cat contracts | grep '"KT1' | cut -d '"' -f 2 > kt1s

echo 'Step 3: get the scripts'
N=$(cat kt1s | wc -l)
I=0
rm -f scripts/*
mkdir -p scripts
touch hashes
touch scripts/tmp
for kt1 in $(cat kt1s)
do
	I=$((I + 1))
	echo "$I / $N"
	if ( grep "$kt1" hashes )
	then
	    H=$(grep "$kt1" hashes | cut -d ':' -f 2)
	    if [ -f "scripts/$H.tz" ]
	    then
		echo "Skipped"
	    else
		echo "Known hash $H but script not found, requesting the script"
		tezos-client get contract code for "$kt1" > "scripts/$H.tz"
		echo "Checking the script hash"
		H2=$(sha256sum "scripts/$H.tz" | cut -d ' ' -f 1)
		if [[ "$H" == "$H2" ]]
		then
		    echo "Hash is correct"
		else
		    echo "Error: bad stored hash for contract $kt1; registered=$H actual=$H2"
		    echo 'Please remove the "hashes" file and rerun this bash script'
		    exit 1
		fi
	    fi
	else
            tezos-client get contract code for "$kt1" > scripts/tmp
            H=$(sha256sum scripts/tmp | cut -d ' ' -f 1)
	    echo "$kt1:$H" >> hashes
	    [ -f "scripts/$H.tz" ] || cp scripts/tmp "scripts/$H.tz"
	fi
done
rm scripts/tmp
