{ storage
    (pair (pair (address %admin)
                (pair (big_map %allowances
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (bool %destroyed)))
          (pair (pair (nat %last_token_id)
                      (big_map %lazy_mints (pair (nat %token_id_start) (nat %token_id_end)) address))
                (pair (big_map %ledger nat address)
                      (big_map %token_metadata (pair (nat %token_id_start) (nat %token_id_end)) bytes)))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %burn nat))
            (or (unit %destroy)
                (pair %mint
                   (list %mint_requests (pair (bytes %metadata) (nat %token_id)))
                   (address %owner))))
        (or (or (pair %power_mint
                   (list %mint_requests (pair (nat %count) (bytes %metadata)))
                   (address %owner))
                (list %select
                   (pair (nat %token_id)
                         (pair (address %recipient) (pair (nat %token_id_start) (nat %token_id_end))))))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat)))) (list nat))
                            (or unit (pair (list (pair bytes nat)) address)))
                        (or (or (pair (list (pair nat bytes)) address) (list (pair nat (pair address (pair nat nat)))))
                            (or (list (pair address (list (pair address (pair nat nat)))))
                                (list (or (pair address (pair address nat)) (pair address (pair address nat)))))))
                    (pair (pair address (pair (big_map (pair address (pair address nat)) unit) bool))
                          (pair (pair nat (big_map (pair nat nat) address))
                                (pair (big_map nat address) (big_map (pair nat nat) bytes))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     NIL (pair (pair address nat) nat) ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     ITER { DUP 4 ;
                            GET 5 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CDR ;
                            MEM ;
                            IF { DUP ;
                                 CAR ;
                                 DUP 5 ;
                                 GET 5 ;
                                 DUP 3 ;
                                 CDR ;
                                 GET ;
                                 IF_NONE { PUSH int 234 ; FAILWITH } {} ;
                                 COMPARE ;
                                 EQ }
                               { PUSH bool False } ;
                            IF { DUP ;
                                 CAR ;
                                 PUSH address "tz1Kpx6wtHMc2m346MqrBJkyGFKqPPGiNueV" ;
                                 COMPARE ;
                                 NEQ }
                               { PUSH bool False } ;
                            IF { SWAP ; PUSH nat 1 ; DIG 2 ; PAIR ; CONS }
                               { SWAP ; PUSH nat 0 ; DIG 2 ; PAIR ; CONS } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_ONLY_ADMIN_CAN_BURN" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP 3 ;
                            GET 5 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            MEM ;
                            IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                            DIG 2 ;
                            DUP ;
                            GET 5 ;
                            PUSH (option address) (Some "tz1Kpx6wtHMc2m346MqrBJkyGFKqPPGiNueV") ;
                            DIG 3 ;
                            UPDATE ;
                            UPDATE 5 ;
                            SWAP } ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_ONLY_ADMIN_CAN_BURN" ; FAILWITH } ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     PUSH bool True ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_ONLY_ADMIN_CAN_MINT" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     ITER { DUP ;
                            CDR ;
                            PUSH nat 1 ;
                            DUP 5 ;
                            GET 3 ;
                            CAR ;
                            ADD ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "FA2_TOKEN_ALREADY_EXISTS" ; FAILWITH } ;
                            DIG 2 ;
                            DUP ;
                            GET 6 ;
                            DUP 3 ;
                            CAR ;
                            SOME ;
                            DIG 3 ;
                            DUP ;
                            CDR ;
                            SWAP ;
                            DUP ;
                            DUG 5 ;
                            CDR ;
                            PAIR ;
                            UPDATE ;
                            UPDATE 6 ;
                            DUP ;
                            GET 5 ;
                            DUP 4 ;
                            CDR ;
                            SOME ;
                            DUP 4 ;
                            CDR ;
                            UPDATE ;
                            UPDATE 5 ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            CDR ;
                            DIG 3 ;
                            CDR ;
                            PAIR ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            SWAP } ;
                     DROP } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_ONLY_ADMIN_CAN_MINT" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     ITER { DUP 3 ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            DUP 6 ;
                            CDR ;
                            SOME ;
                            DUP 6 ;
                            CAR ;
                            DUP 9 ;
                            GET 3 ;
                            CAR ;
                            ADD ;
                            PUSH nat 1 ;
                            DIG 9 ;
                            GET 3 ;
                            CAR ;
                            ADD ;
                            PAIR ;
                            UPDATE ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            DUP ;
                            DUG 3 ;
                            DUP ;
                            GET 6 ;
                            DUP 3 ;
                            CDR ;
                            SOME ;
                            DUP 4 ;
                            CAR ;
                            DUP 7 ;
                            GET 3 ;
                            CAR ;
                            ADD ;
                            PUSH nat 1 ;
                            DIG 7 ;
                            GET 3 ;
                            CAR ;
                            ADD ;
                            PAIR ;
                            UPDATE ;
                            UPDATE 6 ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            UNPAIR ;
                            DIG 4 ;
                            CAR ;
                            ADD ;
                            PAIR ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            SWAP } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP 3 ;
                            GET 5 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            MEM ;
                            IF { PUSH string "FA2_TOKEN_ALREADY_EXISTS" ; FAILWITH } {} ;
                            DUP ;
                            GET 5 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 6 ;
                            COMPARE ;
                            GE ;
                            IF {} { PUSH string "FA2_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                            DUP ;
                            GET 5 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            COMPARE ;
                            GE ;
                            IF { DUP ; GET 6 ; SWAP ; DUP ; DUG 2 ; CAR ; COMPARE ; LE }
                               { PUSH bool False } ;
                            IF {} { PUSH string "FA2_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                            DUP 3 ;
                            GET 3 ;
                            CDR ;
                            SWAP ;
                            DUP ;
                            GET 6 ;
                            SWAP ;
                            DUP ;
                            DUG 3 ;
                            GET 5 ;
                            PAIR ;
                            MEM ;
                            IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                            SENDER ;
                            DUP 4 ;
                            GET 3 ;
                            CDR ;
                            DIG 2 ;
                            DUP ;
                            GET 6 ;
                            SWAP ;
                            DUP ;
                            DUG 4 ;
                            GET 5 ;
                            PAIR ;
                            GET ;
                            IF_NONE { PUSH int 190 ; FAILWITH } {} ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                            DIG 2 ;
                            DUP ;
                            GET 5 ;
                            DUP 3 ;
                            GET 3 ;
                            SOME ;
                            DIG 3 ;
                            CAR ;
                            UPDATE ;
                            UPDATE 5 ;
                            SWAP } ;
                     DROP } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        PUSH nat 1 ;
                                        COMPARE ;
                                        EQ ;
                                        IF { DUP 4 ; GET 5 ; SWAP ; DUP ; DUG 2 ; GET 3 ; MEM }
                                           { PUSH bool False } ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        SENDER ;
                                        COMPARE ;
                                        EQ ;
                                        IF { PUSH bool True }
                                           { DUP 4 ;
                                             CAR ;
                                             GET 3 ;
                                             SWAP ;
                                             DUP ;
                                             DUG 2 ;
                                             GET 3 ;
                                             SENDER ;
                                             DUP 5 ;
                                             CAR ;
                                             PAIR 3 ;
                                             MEM } ;
                                        IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        DUP 5 ;
                                        GET 5 ;
                                        DUP 3 ;
                                        GET 3 ;
                                        GET ;
                                        IF_NONE { PUSH int 224 ; FAILWITH } {} ;
                                        COMPARE ;
                                        EQ ;
                                        IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                        DIG 3 ;
                                        DUP ;
                                        GET 5 ;
                                        DUP 3 ;
                                        CAR ;
                                        SOME ;
                                        DIG 3 ;
                                        GET 3 ;
                                        UPDATE ;
                                        UPDATE 5 ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     IF { PUSH string "FA2_CONTRACT_IS_DESTROYED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DUP ;
                                GET 3 ;
                                PUSH address "tz1Kpx6wtHMc2m346MqrBJkyGFKqPPGiNueV" ;
                                COMPARE ;
                                NEQ ;
                                IF {} { PUSH string "FA2_INVALID_ADDRESS" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                NONE unit ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP } } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
