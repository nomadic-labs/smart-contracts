{ parameter
    (or (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (pair %mint (address %address) (pair (nat %amount) (nat %token_id))))
        (or (list %transfer
               (pair (address %from_)
                     (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
            (list %update_operators
               (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                   (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))) ;
  storage
    (pair (pair (nat %all_tokens) (big_map %ledger address nat))
          (pair (big_map %operators
                   (pair (address %owner) (pair (address %operator) (nat %token_id)))
                   unit)
                (big_map %tokens nat (map string bytes)))) ;
  code { CAST (pair (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                            (pair address (pair nat nat)))
                        (or (list (pair address (list (pair address (pair nat nat)))))
                            (list (or (pair address (pair address nat)) (pair address (pair address nat))))))
                    (pair (pair nat (big_map address nat))
                          (pair (big_map (pair address (pair address nat)) unit) (big_map nat (map string bytes))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DUP ;
                 CAR ;
                 MAP { DUP 3 ;
                       GET 4 ;
                       SWAP ;
                       DUP ;
                       DUG 2 ;
                       CDR ;
                       MEM ;
                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                       DUP 3 ;
                       CAR ;
                       CDR ;
                       SWAP ;
                       DUP ;
                       DUG 2 ;
                       CAR ;
                       MEM ;
                       IF { DUP 3 ;
                            CAR ;
                            CDR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            GET ;
                            { IF_NONE { PUSH int 425 ; FAILWITH } {} } ;
                            SWAP ;
                            PAIR }
                          { PUSH nat 0 ; SWAP ; PAIR } } ;
                 NIL operation ;
                 DIG 2 ;
                 CDR ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS }
               { DUP ;
                 GET 4 ;
                 PUSH nat 0 ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "single-asset: token-id <> 0" ; FAILWITH } ;
                 DUP ;
                 GET 4 ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 UNPAIR ;
                 CDR ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 DUP ;
                 PUSH nat 1 ;
                 DUP 6 ;
                 GET 4 ;
                 ADD ;
                 DUP ;
                 DUG 2 ;
                 COMPARE ;
                 LE ;
                 IF { DROP } { SWAP ; DROP } ;
                 PAIR ;
                 PAIR ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 MEM ;
                 IF { SWAP ;
                      UNPAIR ;
                      UNPAIR ;
                      SWAP ;
                      DUP ;
                      DUP 5 ;
                      CAR ;
                      DUP ;
                      DUG 2 ;
                      GET ;
                      { IF_NONE { PUSH int 499 ; FAILWITH } {} } ;
                      DIG 5 ;
                      GET 3 ;
                      ADD ;
                      SOME ;
                      SWAP ;
                      UPDATE ;
                      SWAP ;
                      PAIR ;
                      PAIR }
                    { SWAP ;
                      UNPAIR ;
                      UNPAIR ;
                      SWAP ;
                      DUP 4 ;
                      GET 3 ;
                      SOME ;
                      DIG 4 ;
                      CAR ;
                      UPDATE ;
                      SWAP ;
                      PAIR ;
                      PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { DUP ;
                 ITER { DUP ;
                        CDR ;
                        ITER { DUP ;
                               GET 3 ;
                               PUSH nat 0 ;
                               COMPARE ;
                               EQ ;
                               IF {} { PUSH string "single-asset: token-id <> 0" ; FAILWITH } ;
                               SENDER ;
                               DUP 3 ;
                               CAR ;
                               COMPARE ;
                               EQ ;
                               IF { PUSH bool True }
                                  { DUP 4 ;
                                    GET 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    GET 3 ;
                                    SENDER ;
                                    DUP 5 ;
                                    CAR ;
                                    PAIR 3 ;
                                    MEM } ;
                               IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                               DUP 4 ;
                               GET 4 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               GET 3 ;
                               MEM ;
                               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                               DUP ;
                               GET 4 ;
                               PUSH nat 0 ;
                               COMPARE ;
                               LT ;
                               IF { DUP ;
                                    GET 4 ;
                                    DUP 5 ;
                                    CAR ;
                                    CDR ;
                                    DUP 4 ;
                                    CAR ;
                                    GET ;
                                    { IF_NONE { PUSH int 403 ; FAILWITH } {} } ;
                                    COMPARE ;
                                    GE ;
                                    IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                    DUP 4 ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    DUP ;
                                    DUP 6 ;
                                    CAR ;
                                    DUP ;
                                    DUG 2 ;
                                    GET ;
                                    { IF_NONE { PUSH int 407 ; FAILWITH } { DROP } } ;
                                    DUP 5 ;
                                    GET 4 ;
                                    DIG 8 ;
                                    CAR ;
                                    CDR ;
                                    DUP 8 ;
                                    CAR ;
                                    GET ;
                                    { IF_NONE { PUSH int 407 ; FAILWITH } {} } ;
                                    SUB ;
                                    ISNAT ;
                                    { IF_NONE { PUSH int 407 ; FAILWITH } {} } ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    DUP ;
                                    DUG 4 ;
                                    CAR ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CAR ;
                                    MEM ;
                                    IF { DIG 3 ;
                                         UNPAIR ;
                                         UNPAIR ;
                                         SWAP ;
                                         DUP ;
                                         DUP 5 ;
                                         CAR ;
                                         DUP ;
                                         DUG 2 ;
                                         GET ;
                                         { IF_NONE { PUSH int 410 ; FAILWITH } {} } ;
                                         DIG 5 ;
                                         GET 4 ;
                                         ADD ;
                                         SOME ;
                                         SWAP ;
                                         UPDATE ;
                                         SWAP ;
                                         PAIR ;
                                         PAIR ;
                                         DUG 2 }
                                       { DIG 3 ;
                                         UNPAIR ;
                                         UNPAIR ;
                                         SWAP ;
                                         DUP 4 ;
                                         GET 4 ;
                                         SOME ;
                                         DIG 4 ;
                                         CAR ;
                                         UPDATE ;
                                         SWAP ;
                                         PAIR ;
                                         PAIR ;
                                         DUG 2 } }
                                  { DROP } } ;
                        DROP } ;
                 DROP }
               { DUP ;
                 ITER { IF_LEFT
                          { DUP ;
                            CAR ;
                            SENDER ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH int 468 ; FAILWITH } ;
                            DIG 2 ;
                            DUP ;
                            GET 3 ;
                            PUSH (option unit) (Some Unit) ;
                            DIG 3 ;
                            DUP ;
                            GET 4 ;
                            SWAP ;
                            DUP ;
                            GET 3 ;
                            SWAP ;
                            CAR ;
                            PAIR 3 ;
                            UPDATE ;
                            UPDATE 3 ;
                            SWAP }
                          { DUP ;
                            CAR ;
                            SENDER ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH int 474 ; FAILWITH } ;
                            DIG 2 ;
                            DUP ;
                            GET 3 ;
                            NONE unit ;
                            DIG 3 ;
                            DUP ;
                            GET 4 ;
                            SWAP ;
                            DUP ;
                            GET 3 ;
                            SWAP ;
                            CAR ;
                            PAIR 3 ;
                            UPDATE ;
                            UPDATE 3 ;
                            SWAP } } ;
                 DROP } ;
             NIL operation } ;
         PAIR } }
