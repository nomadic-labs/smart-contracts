{ storage
    (pair (pair (address %cryptostars)
                (big_map %deposits
                   (pair (address %depositer) (nat %deposit_id))
                   (pair (address %depositer)
                         (pair (address %rewarder)
                               (pair (mutez %amount)
                                     (pair (mutez %reward) (pair (timestamp %withdraw_timestamp) (nat %deposit_id))))))))
          (pair (big_map %remaining_rewards address mutez)
                (big_map %total_deposits address mutez))) ;
  parameter
    (or (or (pair %deposit
               (address %depositer)
               (pair (address %rewarder)
                     (pair (mutez %reward) (pair (timestamp %withdraw_timestamp) (nat %deposit_id)))))
            (address %deposit_reward))
        (or (pair %withdraw (address %depositer) (nat %deposit_id))
            (pair %withdraw_reward (address %rewarder) (mutez %amount)))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {}
                    { PUSH string "WrongCondition: self.data.cryptostars == sp.sender" ;
                      FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 GET 8 ;
                 SWAP ;
                 DUP ;
                 DUG 3 ;
                 CAR ;
                 PAIR ;
                 MEM ;
                 IF { PUSH string
                           "WrongCondition: ~ (self.data.deposits.contains(sp.set_type_expr(sp.record(depositer = params.depositer, deposit_id = params.deposit_id), sp.TRecord(deposit_id = sp.TNat, depositer = sp.TAddress).layout((\"depositer\", \"deposit_id\")))))" ;
                      FAILWITH }
                    {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 5 ;
                 COMPARE ;
                 LE ;
                 IF {}
                    { PUSH string
                           "WrongCondition: params.reward <= self.data.remaining_rewards.get(params.rewarder, default_value = sp.tez(0))" ;
                      FAILWITH } ;
                 SWAP ;
                 UNPAIR ;
                 UNPAIR ;
                 SWAP ;
                 DIG 3 ;
                 DUP ;
                 GET 8 ;
                 SWAP ;
                 DUP ;
                 GET 7 ;
                 SWAP ;
                 DUP ;
                 DUG 6 ;
                 GET 5 ;
                 AMOUNT ;
                 DIG 7 ;
                 DUP ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 9 ;
                 CAR ;
                 PAIR 6 ;
                 SOME ;
                 DIG 4 ;
                 DUP ;
                 GET 8 ;
                 SWAP ;
                 DUP ;
                 DUG 6 ;
                 CAR ;
                 PAIR ;
                 UPDATE ;
                 SWAP ;
                 PAIR ;
                 PAIR ;
                 DUP ;
                 DUG 2 ;
                 DUP ;
                 GET 3 ;
                 DUP 3 ;
                 GET 5 ;
                 DIG 4 ;
                 GET 3 ;
                 DUP 5 ;
                 GET 3 ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 SUB ;
                 SOME ;
                 DUP 4 ;
                 GET 3 ;
                 UPDATE ;
                 UPDATE 3 ;
                 DUP ;
                 DUG 2 ;
                 DUP ;
                 GET 4 ;
                 AMOUNT ;
                 DIG 4 ;
                 GET 4 ;
                 DUP 5 ;
                 CAR ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 ADD ;
                 SOME ;
                 DIG 3 ;
                 CAR ;
                 UPDATE ;
                 UPDATE 4 }
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {}
                    { PUSH string "WrongCondition: self.data.cryptostars == sp.sender" ;
                      FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 DUP ;
                 GET 3 ;
                 AMOUNT ;
                 DIG 4 ;
                 GET 3 ;
                 DUP 5 ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 ADD ;
                 SOME ;
                 DIG 3 ;
                 UPDATE ;
                 UPDATE 3 } ;
             NIL operation }
           { IF_LEFT
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {}
                    { PUSH string "WrongCondition: self.data.cryptostars == sp.sender" ;
                      FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 3 ;
                 CAR ;
                 PAIR ;
                 MEM ;
                 IF {}
                    { PUSH string
                           "WrongCondition: self.data.deposits.contains(sp.set_type_expr(sp.record(depositer = params.depositer, deposit_id = params.deposit_id), sp.TRecord(deposit_id = sp.TNat, depositer = sp.TAddress).layout((\"depositer\", \"deposit_id\"))))" ;
                      FAILWITH } ;
                 NOW ;
                 DUP 3 ;
                 CAR ;
                 CDR ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 4 ;
                 CAR ;
                 PAIR ;
                 GET ;
                 IF_NONE { PUSH int 86 ; FAILWITH } {} ;
                 GET 9 ;
                 COMPARE ;
                 LE ;
                 IF {}
                    { PUSH string
                           "WrongCondition: self.data.deposits[sp.set_type_expr(sp.record(depositer = params.depositer, deposit_id = params.deposit_id), sp.TRecord(deposit_id = sp.TNat, depositer = sp.TAddress).layout((\"depositer\", \"deposit_id\")))].withdraw_timestamp <= sp.now" ;
                      FAILWITH } ;
                 NIL operation ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 CONTRACT %receive_amount address ;
                 IF_NONE { PUSH int 88 ; FAILWITH } {} ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 5 ;
                 CAR ;
                 PAIR ;
                 GET ;
                 IF_NONE { PUSH int 89 ; FAILWITH } {} ;
                 GET 7 ;
                 DUP 5 ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 6 ;
                 CAR ;
                 PAIR ;
                 GET ;
                 IF_NONE { PUSH int 89 ; FAILWITH } {} ;
                 GET 5 ;
                 ADD ;
                 DUP 4 ;
                 CAR ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DUP 3 ;
                 DUP ;
                 GET 4 ;
                 DUP 5 ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 6 ;
                 CAR ;
                 PAIR ;
                 GET ;
                 IF_NONE { PUSH int 90 ; FAILWITH } {} ;
                 GET 5 ;
                 DIG 5 ;
                 GET 4 ;
                 DUP 6 ;
                 CAR ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 SUB ;
                 SOME ;
                 DUP 5 ;
                 CAR ;
                 UPDATE ;
                 UPDATE 4 ;
                 UNPAIR ;
                 UNPAIR ;
                 SWAP ;
                 NONE (pair address (pair address (pair mutez (pair mutez (pair timestamp nat))))) ;
                 DIG 5 ;
                 UPDATE ;
                 SWAP ;
                 PAIR ;
                 PAIR ;
                 SWAP }
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {}
                    { PUSH string "WrongCondition: self.data.cryptostars == sp.sender" ;
                      FAILWITH } ;
                 DUP ;
                 CDR ;
                 DUP 3 ;
                 GET 3 ;
                 DUP 3 ;
                 CAR ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 COMPARE ;
                 GE ;
                 IF {}
                    { PUSH string
                           "WrongCondition: self.data.remaining_rewards.get(params.rewarder, default_value = sp.tez(0)) >= params.amount" ;
                      FAILWITH } ;
                 NIL operation ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 CONTRACT %receive_amount address ;
                 IF_NONE { PUSH int 107 ; FAILWITH } {} ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 4 ;
                 CAR ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DUP 3 ;
                 DUP ;
                 GET 3 ;
                 DUP 4 ;
                 CDR ;
                 DIG 5 ;
                 GET 3 ;
                 DUP 6 ;
                 CAR ;
                 GET ;
                 IF_NONE { PUSH mutez 0 } {} ;
                 SUB ;
                 SOME ;
                 DIG 4 ;
                 CAR ;
                 UPDATE ;
                 UPDATE 3 ;
                 SWAP } } ;
         PAIR } }
