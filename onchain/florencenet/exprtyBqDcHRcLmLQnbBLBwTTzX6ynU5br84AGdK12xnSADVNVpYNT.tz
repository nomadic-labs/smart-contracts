{ storage
    (pair (pair %assets
             (pair (big_map %ledger (pair address nat) nat)
                   (big_map %operators
                      (pair (address %owner) (pair (address %operator) (nat %token_id)))
                      unit))
             (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                   (big_map %token_total_supply nat nat)))
          (pair (pair %config (address %admin) (bool %paused))
                (big_map %metadata string bytes))) ;
  parameter
    (or (or (pair %mint
               (pair (address %address) (nat %amount))
               (pair (map %metadata string bytes) (nat %tokenId)))
            (or (bool %pause) (address %set_admin)))
        (or (list %transfer
               (pair (address %from_)
                     (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
            (or (big_map %update_metadata string bytes)
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  code { CAST (pair (or (or (pair (pair address nat) (pair (map string bytes) nat)) (or bool address))
                        (or (list (pair address (list (pair address (pair nat nat)))))
                            (or (big_map string bytes)
                                (list (or (pair address (pair address nat)) (pair address (pair address nat)))))))
                    (pair (pair (pair (big_map (pair address nat) nat) (big_map (pair address (pair address nat)) unit))
                                (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat)))
                          (pair (pair address bool) (big_map string bytes)))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                 DUP ;
                 GET 4 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 DUP 3 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 MEM ;
                 IF { DIG 2 ;
                      DUP ;
                      CAR ;
                      UNPAIR ;
                      UNPAIR ;
                      DUP ;
                      DIG 5 ;
                      DUP ;
                      DUG 2 ;
                      GET ;
                      IF_NONE { PUSH int 154 ; FAILWITH } {} ;
                      DUP 7 ;
                      CAR ;
                      CDR ;
                      ADD ;
                      SOME ;
                      SWAP ;
                      UPDATE ;
                      PAIR ;
                      PAIR ;
                      UPDATE 1 ;
                      SWAP }
                    { DIG 2 ;
                      DUP ;
                      CAR ;
                      UNPAIR ;
                      UNPAIR ;
                      DUP 6 ;
                      CAR ;
                      CDR ;
                      SOME ;
                      DIG 5 ;
                      UPDATE ;
                      PAIR ;
                      PAIR ;
                      UPDATE 1 ;
                      SWAP } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 4 ;
                 MEM ;
                 IF { DROP }
                    { SWAP ;
                      DUP ;
                      CAR ;
                      DUP ;
                      GET 3 ;
                      DIG 3 ;
                      DUP ;
                      GET 3 ;
                      SWAP ;
                      DUP ;
                      DUG 5 ;
                      GET 4 ;
                      PAIR ;
                      SOME ;
                      DUP 5 ;
                      GET 4 ;
                      UPDATE ;
                      UPDATE 3 ;
                      UPDATE 1 ;
                      DUP ;
                      CAR ;
                      DUP ;
                      GET 4 ;
                      DUP 4 ;
                      CAR ;
                      CDR ;
                      SOME ;
                      DIG 4 ;
                      GET 4 ;
                      UPDATE ;
                      UPDATE 4 ;
                      UPDATE 1 } }
               { IF_LEFT { DROP } { SWAP ; DUP ; GET 3 ; DIG 2 ; UPDATE 1 ; UPDATE 3 } } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 CDR ;
                 IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                 DUP ;
                 ITER { DUP ;
                        CDR ;
                        ITER { DUP 4 ;
                               CAR ;
                               GET 3 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               GET 3 ;
                               MEM ;
                               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                               DUP 4 ;
                               GET 3 ;
                               CAR ;
                               SENDER ;
                               COMPARE ;
                               EQ ;
                               IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                               IF { PUSH bool True }
                                  { DUP 4 ;
                                    CAR ;
                                    CAR ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    GET 3 ;
                                    SENDER ;
                                    DUP 5 ;
                                    CAR ;
                                    PAIR 3 ;
                                    MEM } ;
                               IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                               DUP ;
                               GET 4 ;
                               PUSH nat 0 ;
                               COMPARE ;
                               LT ;
                               IF { DUP ;
                                    GET 3 ;
                                    DUP 3 ;
                                    CAR ;
                                    PAIR ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 3 ;
                                    CAR ;
                                    PAIR ;
                                    DUP 3 ;
                                    GET 4 ;
                                    DUP 7 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    DUP 4 ;
                                    GET ;
                                    IF_NONE { PUSH int 122 ; FAILWITH } {} ;
                                    COMPARE ;
                                    GE ;
                                    IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                    DUP 3 ;
                                    GET 4 ;
                                    DUP 7 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    DUP 4 ;
                                    GET ;
                                    IF_NONE { PUSH int 127 ; FAILWITH } {} ;
                                    SUB ;
                                    DIG 6 ;
                                    DUP ;
                                    CAR ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    DUP ;
                                    DIG 7 ;
                                    DUP ;
                                    DUG 2 ;
                                    GET ;
                                    IF_NONE { PUSH int 128 ; FAILWITH } { DROP } ;
                                    DIG 5 ;
                                    ISNAT ;
                                    IF_NONE { PUSH int 128 ; FAILWITH } {} ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR ;
                                    PAIR ;
                                    UPDATE 1 ;
                                    DUP ;
                                    DUG 5 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    MEM ;
                                    IF { DIG 4 ;
                                         DUP ;
                                         CAR ;
                                         UNPAIR ;
                                         UNPAIR ;
                                         DUP ;
                                         DIG 5 ;
                                         DUP ;
                                         DUG 2 ;
                                         GET ;
                                         IF_NONE { PUSH int 131 ; FAILWITH } {} ;
                                         DIG 6 ;
                                         GET 4 ;
                                         ADD ;
                                         SOME ;
                                         SWAP ;
                                         UPDATE ;
                                         PAIR ;
                                         PAIR ;
                                         UPDATE 1 ;
                                         DUG 2 }
                                       { DIG 4 ;
                                         DUP ;
                                         CAR ;
                                         UNPAIR ;
                                         UNPAIR ;
                                         DIG 5 ;
                                         GET 4 ;
                                         SOME ;
                                         DIG 5 ;
                                         UPDATE ;
                                         PAIR ;
                                         PAIR ;
                                         UPDATE 1 ;
                                         DUG 2 } }
                                  { DROP } } ;
                        DROP } ;
                 DROP }
               { IF_LEFT
                   { DROP }
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; GET 3 ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                CAR ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                UPDATE 1 ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; GET 3 ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                CAR ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                NONE unit ;
                                DIG 5 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                UPDATE 1 ;
                                SWAP } } ;
                     DROP } } } ;
         NIL operation ;
         PAIR } }
