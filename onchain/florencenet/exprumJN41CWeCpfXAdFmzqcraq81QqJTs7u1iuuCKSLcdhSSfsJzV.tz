{ parameter
    (or (list %transfer
           (pair (address %from_)
                 (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
        (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (or (pair %mint address (pair nat (map string bytes))) (address %setAdmin))))) ;
  storage
    (pair (big_map %ledger (pair address nat) nat)
          (pair (big_map %operators (pair address (pair address nat)) unit)
                (pair (big_map %metadata string bytes)
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (pair (address %contractOwnerAddress) (address %admin)))))) ;
  code { PUSH string "FA2_NOT_OWNER" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         DIG 2 ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { DIG 3 ;
             DROP ;
             ITER { DUP ;
                    DUG 2 ;
                    CDR ;
                    ITER { { DIP 2 { DUP } ; DIG 3 } ;
                           CAR ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           GET 3 ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           GET 3 ;
                           PAIR ;
                           SENDER ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           DIG 2 ;
                           { { DUP ; CAR ; DIP { CDR } } } ;
                           { DIP 3 { DUP } ; DIG 4 } ;
                           { DIP 3 { DUP } ; DIG 4 } ;
                           COMPARE ;
                           EQ ;
                           IF { DROP 4 ; PUSH unit Unit }
                              { DIG 3 ;
                                PAIR ;
                                DIG 2 ;
                                PAIR ;
                                GET ;
                                IF_NONE
                                  { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH }
                                  { DROP ; PUSH unit Unit } } ;
                           DROP ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           CAR ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           GET 3 ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           PAIR ;
                           GET ;
                           IF_NONE
                             { DROP 2 ; { DIP 2 { DUP } ; DIG 3 } ; FAILWITH }
                             { { DIP 2 { DUP } ; DIG 3 } ;
                               GET 4 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               COMPARE ;
                               GE ;
                               IF { DIG 3 ;
                                    DUP ;
                                    CAR ;
                                    { DIP 4 { DUP } ; DIG 5 } ;
                                    GET 4 ;
                                    DIG 3 ;
                                    SUB ;
                                    ABS ;
                                    { DIP 4 { DUP } ; DIG 5 } ;
                                    GET 3 ;
                                    DIG 4 ;
                                    PAIR ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    UPDATE 1 ;
                                    DUP ;
                                    DUP ;
                                    DUG 2 ;
                                    CAR ;
                                    { DIP 3 { DUP } ; DIG 4 } ;
                                    GET 4 ;
                                    DIG 3 ;
                                    CAR ;
                                    { DIP 4 { DUP } ; DIG 5 } ;
                                    GET 3 ;
                                    { DIP 5 { DUP } ; DIG 6 } ;
                                    CAR ;
                                    PAIR ;
                                    GET ;
                                    IF_NONE { PUSH nat 0 } {} ;
                                    ADD ;
                                    { DIP 3 { DUP } ; DIG 4 } ;
                                    GET 3 ;
                                    DIG 4 ;
                                    CAR ;
                                    PAIR ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    UPDATE 1 }
                                  { DROP 3 ; { DIP 2 { DUP } ; DIG 3 } ; FAILWITH } } } ;
                    SWAP ;
                    DROP } ;
             SWAP ;
             DROP ;
             NIL operation ;
             PAIR }
           { DIG 2 ;
             DROP ;
             IF_LEFT
               { DIG 2 ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 NIL operation ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CDR ;
                 AMOUNT ;
                 DIG 4 ;
                 CAR ;
                 MAP { PUSH nat 0 ;
                       PUSH nat 0 ;
                       { DIP 2 { DUP } ; DIG 3 } ;
                       CDR ;
                       COMPARE ;
                       NEQ ;
                       IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH }
                          { { DIP 6 { DUP } ; DIG 7 } ;
                            CAR ;
                            { DIP 2 { DUP } ; DIG 3 } ;
                            CDR ;
                            { DIP 3 { DUP } ; DIG 4 } ;
                            CAR ;
                            PAIR ;
                            GET ;
                            IF_NONE {} { SWAP ; DROP } } ;
                       SWAP ;
                       PAIR } ;
                 DIG 5 ;
                 DROP ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 PAIR }
               { IF_LEFT
                   { ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { { DIP 2 { DUP } ; DIG 3 } ; FAILWITH } ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                PUSH unit Unit ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                GET 4 ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                GET 3 ;
                                PAIR ;
                                DIG 4 ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                SOME ;
                                SWAP ;
                                UPDATE ;
                                UPDATE 3 }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { { DIP 2 { DUP } ; DIG 3 } ; FAILWITH } ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                GET 4 ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                GET 3 ;
                                PAIR ;
                                DIG 3 ;
                                CAR ;
                                PAIR ;
                                NONE unit ;
                                SWAP ;
                                UPDATE ;
                                UPDATE 3 } } ;
                     SWAP ;
                     DROP ;
                     NIL operation ;
                     PAIR }
                   { DIG 2 ;
                     DROP ;
                     IF_LEFT
                       { DUP ;
                         DUG 2 ;
                         CDR ;
                         CDR ;
                         PAIR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         DIG 2 ;
                         CAR ;
                         DIG 2 ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         SENDER ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 10 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "You are not allowed to mint a token." ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         CAR ;
                         PUSH nat 1 ;
                         { DIP 5 { DUP } ; DIG 6 } ;
                         DIG 5 ;
                         PAIR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 1 ;
                         DUP ;
                         GET 7 ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         DIG 3 ;
                         SWAP ;
                         PAIR ;
                         DIG 3 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 7 ;
                         NIL operation ;
                         PAIR }
                       { SENDER ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 9 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "You are not the contract owner." ; FAILWITH } ;
                         UPDATE 10 ;
                         NIL operation ;
                         PAIR } } } } } }
