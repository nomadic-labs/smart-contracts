{ parameter
    (or (or (pair %config
               (pair (int %period_days) (int %period_hours))
               (pair (bool %start) (mutez %starting_price)))
            (unit %place_bid))
        (unit %withdraw)) ;
  storage
    (pair (pair (pair (option %current_bidder address) (mutez %current_price))
                (pair (timestamp %end_time) (address %owner)))
          (mutez %starting_price)) ;
  code { LAMBDA
           string
           (pair (list operation)
                 (pair (pair (pair (option address) mutez) (pair timestamp address)) mutez))
           { FAILWITH } ;
         LAMBDA
           address
           (contract unit)
           { CONTRACT unit ;
             IF_NONE { PUSH string "a transaction contract is missing" ; FAILWITH } {} } ;
         DIG 2 ;
         DUP ;
         DUG 3 ;
         CDR ;
         DIG 3 ;
         CAR ;
         IF_LEFT
           { IF_LEFT
               { DIG 2 ;
                 DROP ;
                 DUP ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PUSH int 24 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 MUL ;
                 SWAP ;
                 CDR ;
                 ADD ;
                 PUSH int 3600 ;
                 SWAP ;
                 MUL ;
                 SOURCE ;
                 DIG 3 ;
                 DUP ;
                 DUG 4 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP 3 ;
                      PUSH string "You do not have an authorization to configure auction" ;
                      EXEC }
                    { PUSH bool False ;
                      DIG 2 ;
                      DUP ;
                      DUG 3 ;
                      CDR ;
                      CAR ;
                      COMPARE ;
                      EQ ;
                      IF { DROP 3 ; PUSH string "Set 'start' field to 'true' to start auction" ; EXEC }
                         { PUSH int 0 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           COMPARE ;
                           EQ ;
                           IF { DROP 3 ; PUSH string "Set a period to start auction" ; EXEC }
                              { PUSH mutez 0 ;
                                DIG 2 ;
                                DUP ;
                                DUG 3 ;
                                CDR ;
                                CDR ;
                                COMPARE ;
                                EQ ;
                                IF { DROP 3 ; PUSH string "Set an initial price to start auction" ; EXEC }
                                   { DIG 3 ;
                                     DROP ;
                                     DIG 2 ;
                                     DUP ;
                                     DUG 3 ;
                                     CDR ;
                                     DIG 3 ;
                                     DUP ;
                                     DUG 4 ;
                                     CAR ;
                                     CDR ;
                                     CDR ;
                                     DIG 2 ;
                                     NOW ;
                                     ADD ;
                                     PAIR ;
                                     DIG 3 ;
                                     CAR ;
                                     CAR ;
                                     PAIR ;
                                     PAIR ;
                                     DUP ;
                                     CDR ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     CAR ;
                                     CDR ;
                                     DIG 3 ;
                                     DUP ;
                                     DUG 4 ;
                                     CDR ;
                                     CDR ;
                                     DIG 3 ;
                                     CAR ;
                                     CAR ;
                                     CAR ;
                                     PAIR ;
                                     PAIR ;
                                     PAIR ;
                                     SWAP ;
                                     CDR ;
                                     CDR ;
                                     SWAP ;
                                     CAR ;
                                     PAIR ;
                                     NIL operation ;
                                     PAIR } } } } }
               { DROP ;
                 SOURCE ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 COMPARE ;
                 EQ ;
                 IF { DROP 2 ; PUSH string "Can't bid on owned artifact" ; EXEC }
                    { DUP ;
                      CAR ;
                      CDR ;
                      CAR ;
                      NOW ;
                      COMPARE ;
                      GT ;
                      IF { DROP 2 ; PUSH string "Auction is over" ; EXEC }
                         { DUP ;
                           CAR ;
                           CAR ;
                           CDR ;
                           AMOUNT ;
                           COMPARE ;
                           LE ;
                           IF { DROP 2 ; PUSH string "Bid is too low" ; EXEC }
                              { DIG 2 ;
                                DROP ;
                                DUP ;
                                CAR ;
                                CAR ;
                                CAR ;
                                IF_NONE
                                  { SWAP ;
                                    DROP ;
                                    DUP ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CAR ;
                                    CDR ;
                                    AMOUNT ;
                                    DIG 3 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    PAIR ;
                                    PAIR ;
                                    PAIR ;
                                    DUP ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CAR ;
                                    CDR ;
                                    DIG 2 ;
                                    CAR ;
                                    CAR ;
                                    CDR ;
                                    SOURCE ;
                                    SOME ;
                                    PAIR ;
                                    PAIR ;
                                    PAIR ;
                                    NIL operation ;
                                    PAIR }
                                  { DIG 2 ;
                                    SWAP ;
                                    EXEC ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CDR ;
                                    DIG 2 ;
                                    DUP ;
                                    DUG 3 ;
                                    CAR ;
                                    CDR ;
                                    AMOUNT ;
                                    DIG 4 ;
                                    DUP ;
                                    DUG 5 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    PAIR ;
                                    PAIR ;
                                    PAIR ;
                                    DUP ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CAR ;
                                    CDR ;
                                    DIG 2 ;
                                    CAR ;
                                    CAR ;
                                    CDR ;
                                    SOURCE ;
                                    SOME ;
                                    PAIR ;
                                    PAIR ;
                                    PAIR ;
                                    NIL operation ;
                                    DIG 2 ;
                                    DIG 3 ;
                                    CAR ;
                                    CAR ;
                                    CDR ;
                                    UNIT ;
                                    TRANSFER_TOKENS ;
                                    CONS ;
                                    PAIR } } } } } }
           { DROP ;
             DUP ;
             CAR ;
             CDR ;
             CAR ;
             NOW ;
             COMPARE ;
             LT ;
             IF { DROP 2 ; PUSH string "Auction not over yet" ; EXEC }
                { SOURCE ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  CAR ;
                  CDR ;
                  CDR ;
                  COMPARE ;
                  NEQ ;
                  IF { DROP 2 ;
                       PUSH string "You are not authorized to make this transaction" ;
                       EXEC }
                     { DIG 2 ;
                       DROP ;
                       DUP ;
                       CAR ;
                       CDR ;
                       CDR ;
                       DIG 2 ;
                       SWAP ;
                       EXEC ;
                       SWAP ;
                       DUP ;
                       DUG 2 ;
                       CDR ;
                       DIG 2 ;
                       DUP ;
                       DUG 3 ;
                       CAR ;
                       CDR ;
                       PUSH mutez 0 ;
                       DIG 4 ;
                       DUP ;
                       DUG 5 ;
                       CAR ;
                       CAR ;
                       CAR ;
                       PAIR ;
                       PAIR ;
                       PAIR ;
                       NIL operation ;
                       DIG 2 ;
                       DIG 3 ;
                       CAR ;
                       CAR ;
                       CDR ;
                       UNIT ;
                       TRANSFER_TOKENS ;
                       CONS ;
                       PAIR } } } } }
