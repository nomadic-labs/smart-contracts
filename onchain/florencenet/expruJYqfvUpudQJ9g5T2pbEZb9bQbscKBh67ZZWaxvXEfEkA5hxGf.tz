{ storage
    (pair (pair (pair (address %administrator) (nat %all_tokens))
                (pair (big_map %ledger (pair address nat) nat)
                      (pair (big_map %metadata string bytes) (bytes %onchain_bytestring))))
          (pair (pair (big_map %onchain_data string bytes)
                      (pair (big_map %onchain_data_types string string)
                            (big_map %operators
                               (pair (address %owner) (pair (address %operator) (nat %token_id)))
                               unit)))
                (pair (bool %paused)
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (big_map %total_supply nat nat))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (string %del_onchain_data))
            (or (pair %mint
                   (pair (address %address) (nat %amount))
                   (pair (map %metadata string bytes) (nat %token_id)))
                (or (address %set_administrator) (pair %set_metadata (string %k) (bytes %v)))))
        (or (or (pair %set_onchain_data (string %k) (pair (string %typeString) (bytes %v)))
                (pair %set_onchain_data_type (string %k) (string %typeString)))
            (or (bool %set_pause)
                (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat)))) string)
                            (or (pair (pair address nat) (pair (map string bytes) nat)) (or address (pair string bytes))))
                        (or (or (pair string (pair string bytes)) (pair string string))
                            (or bool
                                (or (list (pair address (list (pair address (pair nat nat)))))
                                    (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair (pair (pair address nat)
                                (pair (big_map (pair address nat) nat) (pair (big_map string bytes) bytes)))
                          (pair (pair (big_map string bytes)
                                      (pair (big_map string string) (big_map (pair address (pair address nat)) unit)))
                                (pair bool (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat)))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 7 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           GET 3 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 3 ;
                                CAR ;
                                GET 3 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 433 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     MEM ;
                     IF {} { PUSH string "FA2_ONCHAIN_KEY_UNDEFINED" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     NONE bytes ;
                     DUP 6 ;
                     UPDATE ;
                     SWAP ;
                     UNPAIR ;
                     NONE string ;
                     DIG 6 ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     GET 4 ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     UNPAIR ;
                     UNPAIR ;
                     CAR ;
                     DIG 4 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DUP ;
                     PUSH nat 1 ;
                     DUP 7 ;
                     GET 4 ;
                     ADD ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     LE ;
                     IF { DROP } { SWAP ; DROP } ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     GET 4 ;
                     SWAP ;
                     DUP ;
                     DUG 3 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     MEM ;
                     IF { SWAP ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          DUP ;
                          DIG 5 ;
                          DUP ;
                          GET 4 ;
                          SWAP ;
                          DUP ;
                          DUG 7 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 544 ; FAILWITH } {} ;
                          DUP 7 ;
                          CAR ;
                          CDR ;
                          ADD ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          SWAP }
                        { SWAP ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          DUP 5 ;
                          CAR ;
                          CDR ;
                          SOME ;
                          DIG 5 ;
                          DUP ;
                          GET 4 ;
                          SWAP ;
                          DUP ;
                          DUG 7 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          UPDATE ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          SWAP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 7 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     MEM ;
                     IF { DROP }
                        { SWAP ;
                          DUP ;
                          GET 7 ;
                          DIG 2 ;
                          DUP ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 4 ;
                          GET 4 ;
                          PAIR ;
                          SOME ;
                          DUP 4 ;
                          GET 4 ;
                          UPDATE ;
                          UPDATE 7 ;
                          DUP ;
                          GET 8 ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          SOME ;
                          DIG 3 ;
                          GET 4 ;
                          UPDATE ;
                          UPDATE 8 } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         DUP 6 ;
                         CDR ;
                         SOME ;
                         DIG 6 ;
                         CAR ;
                         UPDATE ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     DUP 5 ;
                     GET 4 ;
                     SOME ;
                     DUP 6 ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     UNPAIR ;
                     DUP 6 ;
                     GET 3 ;
                     SOME ;
                     DIG 6 ;
                     CAR ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     DUP 6 ;
                     CDR ;
                     SOME ;
                     DIG 6 ;
                     CAR ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 5 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { DUP 4 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       SENDER ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                       IF { PUSH bool True }
                                          { DUP 4 ;
                                            GET 3 ;
                                            GET 4 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       DUP 4 ;
                                       GET 7 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            DUP 5 ;
                                            CAR ;
                                            GET 3 ;
                                            DUP 3 ;
                                            GET 3 ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 411 ; FAILWITH } {} ;
                                            COMPARE ;
                                            GE ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            DUP 4 ;
                                            UNPAIR ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            DUP ;
                                            DUP 6 ;
                                            GET 3 ;
                                            DUP 8 ;
                                            CAR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 2 ;
                                            GET ;
                                            IF_NONE { PUSH int 415 ; FAILWITH } { DROP } ;
                                            DUP 6 ;
                                            GET 4 ;
                                            DIG 9 ;
                                            CAR ;
                                            GET 3 ;
                                            DUP 8 ;
                                            GET 3 ;
                                            DUP 10 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 415 ; FAILWITH } {} ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE { PUSH int 415 ; FAILWITH } {} ;
                                            SOME ;
                                            SWAP ;
                                            UPDATE ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 4 ;
                                            CAR ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 3 ;
                                            CAR ;
                                            PAIR ;
                                            MEM ;
                                            IF { DIG 3 ;
                                                 UNPAIR ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 DUP ;
                                                 DIG 5 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 7 ;
                                                 CAR ;
                                                 PAIR ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET ;
                                                 IF_NONE { PUSH int 418 ; FAILWITH } {} ;
                                                 DIG 6 ;
                                                 GET 4 ;
                                                 ADD ;
                                                 SOME ;
                                                 SWAP ;
                                                 UPDATE ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUG 2 }
                                               { DIG 3 ;
                                                 UNPAIR ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 DUP 5 ;
                                                 GET 4 ;
                                                 SOME ;
                                                 DIG 5 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 CAR ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUG 2 } }
                                          { DROP } } ;
                                DROP } ;
                         DROP }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    NONE unit ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP } } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
