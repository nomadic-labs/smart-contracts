{ parameter
    (or (or (unit %cancel)
            (pair %config
               (pair (int %period_days) (int %period_hours))
               (pair (bool %start) (mutez %starting_price))))
        (or (pair %place_bid (address %ad) (mutez %amount)) (unit %withdraw))) ;
  storage
    (pair (pair (address %current_bidder) (mutez %current_price))
          (pair (timestamp %end_time) (address %owner))) ;
  code { DUP ;
         CDR ;
         SWAP ;
         CAR ;
         IF_LEFT
           { IF_LEFT
               { DROP ;
                 SOURCE ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP ;
                      PUSH string "You are not authorized to make this transaction" ;
                      FAILWITH }
                    { SOURCE ;
                      CONTRACT unit ;
                      IF_NONE { PUSH string "No contract to match bid" ; FAILWITH } {} ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      CDR ;
                      PUSH mutez 0 ;
                      DIG 3 ;
                      DUP ;
                      DUG 4 ;
                      CAR ;
                      CAR ;
                      PAIR ;
                      PAIR ;
                      NIL operation ;
                      DIG 2 ;
                      DIG 3 ;
                      CAR ;
                      CDR ;
                      UNIT ;
                      TRANSFER_TOKENS ;
                      CONS ;
                      PAIR } }
               { DUP ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PUSH int 24 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 MUL ;
                 SWAP ;
                 CDR ;
                 ADD ;
                 PUSH int 3600 ;
                 SWAP ;
                 MUL ;
                 SOURCE ;
                 DIG 3 ;
                 DUP ;
                 DUG 4 ;
                 CDR ;
                 CDR ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP 3 ;
                      PUSH string "You do not have an authorization to configure auction" ;
                      FAILWITH }
                    { PUSH bool False ;
                      DIG 2 ;
                      DUP ;
                      DUG 3 ;
                      CDR ;
                      CAR ;
                      COMPARE ;
                      EQ ;
                      IF { DROP 3 ;
                           PUSH string "Set 'start' field to 'true' to start auction" ;
                           FAILWITH }
                         { PUSH int 0 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           COMPARE ;
                           EQ ;
                           IF { DROP 3 ; PUSH string "Set a period to start auction" ; FAILWITH }
                              { PUSH mutez 0 ;
                                DIG 2 ;
                                DUP ;
                                DUG 3 ;
                                CDR ;
                                CDR ;
                                COMPARE ;
                                EQ ;
                                IF { DROP 3 ; PUSH string "Set an initial price to start auction" ; FAILWITH }
                                   { DIG 2 ;
                                     DUP ;
                                     DUG 3 ;
                                     CDR ;
                                     CDR ;
                                     SWAP ;
                                     NOW ;
                                     ADD ;
                                     PAIR ;
                                     DIG 2 ;
                                     CAR ;
                                     PAIR ;
                                     DUP ;
                                     CDR ;
                                     DIG 2 ;
                                     CDR ;
                                     CDR ;
                                     DIG 2 ;
                                     CAR ;
                                     CAR ;
                                     PAIR ;
                                     PAIR ;
                                     NIL operation ;
                                     PAIR } } } } } }
           { IF_LEFT
               { DUP ;
                 CAR ;
                 DIG 2 ;
                 DUP ;
                 DUG 3 ;
                 CDR ;
                 CDR ;
                 COMPARE ;
                 EQ ;
                 IF { DROP 2 ; PUSH string "Cant bid on owned artifact" ; FAILWITH }
                    { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CDR ;
                      CAR ;
                      NOW ;
                      COMPARE ;
                      GT ;
                      IF { DROP 2 ; PUSH string "Auction is over" ; FAILWITH }
                         { SWAP ;
                           DUP ;
                           DUG 2 ;
                           CAR ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           COMPARE ;
                           LE ;
                           IF { DROP 2 ; PUSH string "Bid is too low" ; FAILWITH }
                              { SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                CAR ;
                                CONTRACT unit ;
                                IF_NONE { PUSH string "No contract matches the last bid" ; FAILWITH } {} ;
                                DIG 2 ;
                                DUP ;
                                DUG 3 ;
                                CDR ;
                                DIG 2 ;
                                DUP ;
                                DUG 3 ;
                                CDR ;
                                DIG 4 ;
                                DUP ;
                                DUG 5 ;
                                CAR ;
                                CAR ;
                                PAIR ;
                                CDR ;
                                DIG 3 ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                NIL operation ;
                                DIG 2 ;
                                DIG 3 ;
                                CAR ;
                                CDR ;
                                UNIT ;
                                TRANSFER_TOKENS ;
                                CONS ;
                                PAIR } } } }
               { DROP ;
                 DUP ;
                 CDR ;
                 CAR ;
                 NOW ;
                 COMPARE ;
                 LT ;
                 IF { DROP ; PUSH string "Auction not over yet" ; FAILWITH }
                    { SOURCE ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      CDR ;
                      CDR ;
                      COMPARE ;
                      NEQ ;
                      IF { DROP ;
                           PUSH string "You are not authorized to make this transaction" ;
                           FAILWITH }
                         { DUP ;
                           CDR ;
                           CDR ;
                           CONTRACT unit ;
                           IF_NONE { PUSH string "No contract to matches transaction" ; FAILWITH } {} ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           PUSH mutez 0 ;
                           DIG 3 ;
                           DUP ;
                           DUG 4 ;
                           CAR ;
                           CAR ;
                           PAIR ;
                           PAIR ;
                           NIL operation ;
                           DIG 2 ;
                           DIG 3 ;
                           CAR ;
                           CDR ;
                           UNIT ;
                           TRANSFER_TOKENS ;
                           CONS ;
                           PAIR } } } } } }
