{ parameter
    (or (or (or (or (pair %balance_of
                       (list %requests (pair (address %owner) (nat %token_id)))
                       (contract %callback
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                    (pair %burn (nat %token_id) (nat %amount)))
                (or (address %changeAdmin)
                    (list %mint (pair (nat %token_id) (pair (address %user) (nat %amount))))))
            (or (or (unit %pause)
                    (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
                (or (unit %unpause)
                    (list %updateMinters
                       (or (pair %add_minter (address %minter) (nat %token_id))
                           (pair %remove_minter (address %minter) (nat %token_id)))))))
        (list %update_operators
           (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
               (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))) ;
  storage
    (pair (pair (pair (address %admin)
                      (big_map %ledger address (pair (set %allowances address) (nat %balance))))
                (pair (big_map %metadata string bytes) (set %minters address)))
          (pair (pair (bool %paused)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))
                (nat %total_supply))) ;
  code { LAMBDA
           (pair address
                 (pair (pair (pair address (big_map address (pair (set address) nat)))
                             (pair (big_map string bytes) (set address)))
                       (pair (pair bool (big_map nat (pair nat (map string bytes)))) nat)))
           (pair (set address) nat)
           { { { DUP ; CAR ; DIP { CDR } } } ;
             SWAP ;
             CAR ;
             CAR ;
             CDR ;
             SWAP ;
             GET ;
             IF_NONE { PUSH nat 0 ; EMPTY_SET address ; PAIR } {} } ;
         SWAP ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DIG 2 ;
                         NIL (pair (pair address nat) nat) ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         ITER { DUP ;
                                DUG 2 ;
                                CDR ;
                                PUSH nat 0 ;
                                COMPARE ;
                                NEQ ;
                                IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                PAIR ;
                                { DIP 6 { DUP } ; DIG 7 } ;
                                SWAP ;
                                EXEC ;
                                CDR ;
                                DIG 2 ;
                                PAIR ;
                                CONS } ;
                         DIG 2 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         NIL operation ;
                         DIG 2 ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CAR ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         CAR ;
                         PUSH nat 0 ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SENDER ;
                         PAIR ;
                         DIG 3 ;
                         SWAP ;
                         EXEC ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         COMPARE ;
                         LT ;
                         IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SUB ;
                         ABS ;
                         SWAP ;
                         CAR ;
                         PAIR ;
                         SWAP ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         CDR ;
                         SUB ;
                         ABS ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         PAIR ;
                         DUP ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         DIG 4 ;
                         SOME ;
                         SENDER ;
                         UPDATE ;
                         DIG 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP ; PUSH string "FA2_NOT_ADMIN" ; FAILWITH }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              CAR ;
                              CDR ;
                              DIG 3 ;
                              CAR ;
                              CAR ;
                              CDR ;
                              DIG 3 ;
                              PAIR ;
                              PAIR ;
                              PAIR } ;
                         NIL operation ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CAR ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         CDR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                         ITER { DUP ;
                                DUG 2 ;
                                CAR ;
                                PUSH nat 0 ;
                                COMPARE ;
                                NEQ ;
                                IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                                DUP ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                SWAP ;
                                EXEC ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CDR ;
                                ADD ;
                                SWAP ;
                                CAR ;
                                PAIR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                CDR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                CDR ;
                                ADD ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                DIG 2 ;
                                CAR ;
                                PAIR ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                CDR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                CAR ;
                                CDR ;
                                DIG 4 ;
                                DIG 5 ;
                                CDR ;
                                CAR ;
                                SWAP ;
                                SOME ;
                                SWAP ;
                                UPDATE ;
                                DIG 3 ;
                                CAR ;
                                CAR ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                PAIR } ;
                         SWAP ;
                         DROP ;
                         NIL operation ;
                         PAIR } } }
               { IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         SWAP ;
                         DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "FA2_NOT_ADMIN" ; FAILWITH }
                            { DUP ;
                              CDR ;
                              CAR ;
                              CAR ;
                              IF { PUSH string "FA2_ALREADY_PAUSED" ; FAILWITH }
                                 { DUP ;
                                   CDR ;
                                   CDR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   CAR ;
                                   CDR ;
                                   PUSH bool True ;
                                   PAIR ;
                                   PAIR ;
                                   SWAP ;
                                   CAR ;
                                   PAIR } } ;
                         NIL operation ;
                         PAIR }
                       { ITER { SWAP ;
                                DUP ;
                                CDR ;
                                CAR ;
                                CAR ;
                                IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                                DUP ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                PAIR ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                SWAP ;
                                EXEC ;
                                DUP ;
                                CAR ;
                                SENDER ;
                                MEM ;
                                SENDER ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                OR ;
                                IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                SWAP ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                ITER { DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       CAR ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       NEQ ;
                                       IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       CDR ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CDR ;
                                       COMPARE ;
                                       LT ;
                                       IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       CDR ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CDR ;
                                       SUB ;
                                       ABS ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CAR ;
                                       PAIR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       CDR ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       DIG 3 ;
                                       { DIP 7 { DUP } ; DIG 8 } ;
                                       CAR ;
                                       SWAP ;
                                       SOME ;
                                       SWAP ;
                                       UPDATE ;
                                       DIG 3 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       PAIR ;
                                       PAIR ;
                                       PAIR ;
                                       DUP ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       PAIR ;
                                       { DIP 5 { DUP } ; DIG 6 } ;
                                       SWAP ;
                                       EXEC ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CDR ;
                                       CDR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       ADD ;
                                       SWAP ;
                                       CAR ;
                                       PAIR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       CDR ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       DIG 3 ;
                                       DIG 5 ;
                                       CAR ;
                                       SWAP ;
                                       SOME ;
                                       SWAP ;
                                       UPDATE ;
                                       DIG 3 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       PAIR ;
                                       PAIR ;
                                       PAIR } ;
                                SWAP ;
                                DROP ;
                                SWAP ;
                                DROP } ;
                         SWAP ;
                         DROP ;
                         NIL operation ;
                         PAIR } }
                   { DIG 2 ;
                     DROP ;
                     IF_LEFT
                       { DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "FA2_NOT_ADMIN" ; FAILWITH }
                            { DUP ;
                              CDR ;
                              CDR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              CAR ;
                              CDR ;
                              PUSH bool False ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              CAR ;
                              PAIR } ;
                         NIL operation ;
                         PAIR }
                       { ITER { SWAP ;
                                DUP ;
                                CAR ;
                                CAR ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                NEQ ;
                                IF { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } {} ;
                                SWAP ;
                                IF_LEFT
                                  { DUP ;
                                    CDR ;
                                    PUSH nat 0 ;
                                    COMPARE ;
                                    NEQ ;
                                    IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CDR ;
                                    { DIP 2 { DUP } ; DIG 3 } ;
                                    CAR ;
                                    CDR ;
                                    CDR ;
                                    DIG 2 ;
                                    CAR ;
                                    PUSH bool True ;
                                    SWAP ;
                                    UPDATE ;
                                    { DIP 2 { DUP } ; DIG 3 } ;
                                    CAR ;
                                    CDR ;
                                    CAR ;
                                    PAIR ;
                                    DIG 2 ;
                                    CAR ;
                                    CAR ;
                                    PAIR ;
                                    PAIR }
                                  { DUP ;
                                    CDR ;
                                    PUSH nat 0 ;
                                    COMPARE ;
                                    NEQ ;
                                    IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CDR ;
                                    { DIP 2 { DUP } ; DIG 3 } ;
                                    CAR ;
                                    CDR ;
                                    CDR ;
                                    DIG 2 ;
                                    CAR ;
                                    PUSH bool False ;
                                    SWAP ;
                                    UPDATE ;
                                    { DIP 2 { DUP } ; DIG 3 } ;
                                    CAR ;
                                    CDR ;
                                    CAR ;
                                    PAIR ;
                                    DIG 2 ;
                                    CAR ;
                                    CAR ;
                                    PAIR ;
                                    PAIR } } ;
                         NIL operation ;
                         PAIR } } } }
           { ITER { SWAP ;
                    DUP ;
                    CDR ;
                    CAR ;
                    CAR ;
                    IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                    SWAP ;
                    IF_LEFT
                      { DUP ;
                        CDR ;
                        CDR ;
                        PUSH nat 0 ;
                        COMPARE ;
                        NEQ ;
                        IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                        DUP ;
                        CAR ;
                        SENDER ;
                        COMPARE ;
                        NEQ ;
                        IF { PUSH string "FA2_NOT_OWNER" ; FAILWITH } {} ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        CAR ;
                        PAIR ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        SWAP ;
                        EXEC ;
                        { DIP 2 { DUP } ; DIG 3 } ;
                        CDR ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        CAR ;
                        CDR ;
                        { DIP 4 { DUP } ; DIG 5 } ;
                        CAR ;
                        CAR ;
                        CDR ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        CDR ;
                        DIG 4 ;
                        CAR ;
                        { DIP 5 { DUP } ; DIG 6 } ;
                        CDR ;
                        CAR ;
                        PUSH bool True ;
                        SWAP ;
                        UPDATE ;
                        PAIR ;
                        DIG 4 ;
                        CAR ;
                        SWAP ;
                        SOME ;
                        SWAP ;
                        UPDATE ;
                        DIG 3 ;
                        CAR ;
                        CAR ;
                        CAR ;
                        PAIR ;
                        PAIR ;
                        PAIR }
                      { DUP ;
                        CDR ;
                        CDR ;
                        PUSH nat 0 ;
                        COMPARE ;
                        NEQ ;
                        IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                        DUP ;
                        CAR ;
                        SENDER ;
                        COMPARE ;
                        NEQ ;
                        IF { PUSH string "FA2_NOT_OWNER" ; FAILWITH } {} ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        CAR ;
                        PAIR ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        SWAP ;
                        EXEC ;
                        { DIP 2 { DUP } ; DIG 3 } ;
                        CDR ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        CAR ;
                        CDR ;
                        { DIP 4 { DUP } ; DIG 5 } ;
                        CAR ;
                        CAR ;
                        CDR ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        CDR ;
                        DIG 4 ;
                        CAR ;
                        { DIP 5 { DUP } ; DIG 6 } ;
                        CDR ;
                        CAR ;
                        PUSH bool False ;
                        SWAP ;
                        UPDATE ;
                        PAIR ;
                        DIG 4 ;
                        CAR ;
                        SWAP ;
                        SOME ;
                        SWAP ;
                        UPDATE ;
                        DIG 3 ;
                        CAR ;
                        CAR ;
                        CAR ;
                        PAIR ;
                        PAIR ;
                        PAIR } } ;
             SWAP ;
             DROP ;
             NIL operation ;
             PAIR } } }
