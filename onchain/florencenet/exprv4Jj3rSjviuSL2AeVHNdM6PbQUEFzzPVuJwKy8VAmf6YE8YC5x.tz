{ storage
    (pair (pair (address %administrator)
                (pair (set %all_tokens nat) (big_map %initial_hodlers address nat)))
          (pair (pair (big_map %ledger (pair (address %owner) (nat %token_id)) nat)
                      (big_map %metadata string bytes))
                (pair (bool %paused)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  parameter
    (or (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (or (pair %mint (map %metadata string bytes) (nat %token_id))
                (address %set_administrator)))
        (or (bool %set_pause)
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { NIL (pair (pair address nat) nat) ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 ITER { SWAP ;
                        DUP 4 ;
                        GET 3 ;
                        CAR ;
                        DIG 2 ;
                        DUP ;
                        CDR ;
                        SWAP ;
                        DUP ;
                        DUG 4 ;
                        CAR ;
                        PAIR ;
                        GET ;
                        IF_NONE { PUSH nat 0 } {} ;
                        DIG 2 ;
                        PAIR ;
                        CONS } ;
                 NIL operation ;
                 DIG 2 ;
                 CDR ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "CRYPTOBOT_CONTRACT_IS_PAUSED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     MEM ;
                     IF { PUSH string "CRYPTOBOT_CANT_MINT_SAME_TOKEN_TWICE" ; FAILWITH } {} ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH (option nat) (Some 1) ;
                     DUP 6 ;
                     CDR ;
                     SENDER ;
                     PAIR ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     DUP ;
                     GET 6 ;
                     DIG 2 ;
                     UNPAIR ;
                     SWAP ;
                     PAIR ;
                     SOME ;
                     DIG 3 ;
                     CAR ;
                     GET 3 ;
                     SIZE ;
                     UPDATE ;
                     UPDATE 6 ;
                     DUP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH bool True ;
                     DIG 5 ;
                     CAR ;
                     GET 3 ;
                     SIZE ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                 UPDATE 5 }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "CRYPTOBOT_CONTRACT_IS_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        DUP 5 ;
                                        GET 3 ;
                                        CAR ;
                                        DUP 3 ;
                                        GET 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} ;
                                        COMPARE ;
                                        GE ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        SENDER ;
                                        COMPARE ;
                                        EQ ;
                                        IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        DUP 5 ;
                                        GET 4 ;
                                        DIG 8 ;
                                        GET 3 ;
                                        CAR ;
                                        DUP 7 ;
                                        GET 3 ;
                                        DUP 9 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 144 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH int 144 ; FAILWITH } {} ;
                                        SOME ;
                                        DUP 6 ;
                                        GET 3 ;
                                        DUP 8 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        PAIR ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        DUP ;
                                        DUG 4 ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        DUP 5 ;
                                        GET 4 ;
                                        DIG 8 ;
                                        GET 3 ;
                                        CAR ;
                                        DIG 6 ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 8 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} ;
                                        ADD ;
                                        SOME ;
                                        DIG 5 ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        PAIR ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { PUSH string "FA2_OPERATORS_UNSUPPORTED" ; FAILWITH } } ;
             NIL operation } ;
         PAIR } }
