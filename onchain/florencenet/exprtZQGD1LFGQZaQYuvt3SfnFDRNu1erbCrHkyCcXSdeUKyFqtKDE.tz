{ parameter
    (or (list %transfer
           (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))
        (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (list %update_operators
               (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                   (pair %remove_operator (address %owner) (address %operator) (nat %token_id)))))) ;
  storage
    (pair (big_map %options
             (address :owner)
             (map (nat :token_id) (pair (nat %balance) (set %operators address))))
          (pair (timestamp %expiration) (address %options_market))
          (big_map %metadata string bytes)) ;
  code { UNPAIR ;
         IF_LEFT
           { DIP { DUP ;
                   CAR @% ;
                   DIP { DUP ;
                         DIP { { CDR ; CAR } ;
                               UNPAIR ;
                               NOW ;
                               { COMPARE ;
                                 LT ;
                                 IF { DROP }
                                    { SENDER ;
                                      { COMPARE ; EQ ; IF {} { PUSH string "trading phase over" ; FAILWITH } } } } } } } ;
             ITER { DIP { DUP } ;
                    UNPAIR @% @% ;
                    DUP ;
                    DUG 3 ;
                    SWAP ;
                    DIP { GET ;
                          IF_NONE
                            { PUSH (map (nat :token_id) (pair nat (set address)))
                                   { Elt 0 (Pair 0 {}) ; Elt 1 (Pair 0 {}) } }
                            {} ;
                          RENAME @from_balances } ;
                    ITER { UNPAIR @% @% ;
                           SWAP ;
                           UNPAIR @to_id @to_amount ;
                           DUP ;
                           DUG 3 ;
                           DIP 3
                               { DUP 3 ;
                                 SENDER ;
                                 { COMPARE ;
                                   EQ ;
                                   IF { DROP }
                                      { DUP 5 ;
                                        { CDR @% ; CAR @% ; CDR @% } ;
                                        SENDER ;
                                        { COMPARE ;
                                          EQ ;
                                          IF { DROP }
                                             { DIP { DUP } ;
                                               GET ;
                                               { IF_NONE { PUSH string "no such token" ; FAILWITH } {} } ;
                                               CDR ;
                                               SENDER ;
                                               MEM ;
                                               IF {} { PUSH string "not allowed" ; FAILWITH } } } } } } ;
                           DIP 6 { DUP ; { CDR @% ; CAR @% ; CDR @% } ; DUP } ;
                           DIG 7 ;
                           DUP 6 ;
                           { COMPARE ;
                             NEQ ;
                             IF { DIG 3 ;
                                  DUP ;
                                  DIP { DUP 2 ;
                                        GET ;
                                        { IF_NONE { PUSH string "no such token" ; FAILWITH } {} } ;
                                        DUP 3 ;
                                        DIP { UNPAIR } ;
                                        SWAP ;
                                        SUB ;
                                        ISNAT ;
                                        { IF_NONE { { UNIT ; FAILWITH } } { RENAME @balance } } ;
                                        PAIR ;
                                        SOME ;
                                        DUP 2 } ;
                                  DUG 2 ;
                                  UPDATE ;
                                  DUG 3 }
                                {} } ;
                           DIG 6 ;
                           DUP 4 ;
                           { COMPARE ;
                             NEQ ;
                             IF { DUP 6 ;
                                  DUP 4 ;
                                  GET ;
                                  IF_NONE
                                    { PUSH (map (nat :token_id) (pair nat (set address)))
                                           { Elt 0 (Pair 0 {}) ; Elt 1 (Pair 0 {}) } }
                                    {} ;
                                  DUP ;
                                  DIP { DUP 2 ;
                                        GET ;
                                        { IF_NONE { PUSH string "no such token" ; FAILWITH } {} } ;
                                        SWAP ;
                                        DIP { UNPAIR ; SWAP ; DIP { ADD } ; SWAP ; PAIR ; SOME } } ;
                                  DUG 2 ;
                                  UPDATE ;
                                  SOME ;
                                  SWAP ;
                                  DIP 2 { DIG 2 } ;
                                  UPDATE ;
                                  DUG 2 }
                                { DROP 3 } } } ;
                    SOME ;
                    SWAP ;
                    UPDATE } ;
             SWAP ;
             { DUP ; CDR @%% ; DIP { CAR ; { DROP } } ; SWAP ; PAIR % %@ } ;
             NIL operation }
           { IF_LEFT
               { UNPAIR @% @% ;
                 DIP { NIL (pair (pair address nat) nat) ; DUP 3 ; CAR @% } ;
                 ITER { DUP ;
                        UNPAIR @% @request_id ;
                        DUP 4 ;
                        SWAP ;
                        GET ;
                        IF_NONE
                          { DROP ; PUSH nat 0 }
                          { SWAP ;
                            GET ;
                            { IF_NONE { PUSH string "no such token" ; FAILWITH } { CAR } } } ;
                        SWAP ;
                        PAIR ;
                        SWAP ;
                        DIP { CONS } } ;
                 DROP ;
                 PUSH mutez 0 ;
                 SWAP ;
                 TRANSFER_TOKENS ;
                 NIL operation ;
                 SWAP ;
                 CONS }
               { ITER { SWAP ;
                        DUP ;
                        DIP { CAR ;
                              DIP { IF_LEFT { PUSH bool True } { PUSH bool False } } ;
                              DUP ;
                              DIP { DIP { SWAP ;
                                          UNPAIR @% ;
                                          DUP ;
                                          SENDER ;
                                          { COMPARE ; EQ ; IF {} { PUSH string "not owner" ; FAILWITH } } } ;
                                    SWAP ;
                                    DUP ;
                                    DIP { GET ;
                                          IF_NONE
                                            { PUSH (map (nat :token_id) (pair nat (set address)))
                                                   { Elt 0 (Pair 0 {}) ; Elt 1 (Pair 0 {}) } }
                                            {} ;
                                          DUP ;
                                          DIP { SWAP ;
                                                UNPAIR @% @% ;
                                                DUP 2 ;
                                                DIP 2
                                                    { GET ;
                                                      IF_NONE { PUSH string "no such token" ; FAILWITH } {} ;
                                                      UNPAIR @balance @operators } ;
                                                DIG 2 ;
                                                DIP { DIP { DIP { SWAP } ; UPDATE } } ;
                                                SWAP ;
                                                DIP { PAIR ; SOME } ;
                                                SWAP } ;
                                          SWAP ;
                                          DIG 2 ;
                                          UPDATE ;
                                          SOME } ;
                                    SWAP } ;
                              SWAP ;
                              DIG 2 ;
                              UPDATE } ;
                        { DUP ; CDR @%% ; DIP { CAR ; { DROP } } ; SWAP ; PAIR % %@ } } ;
                 NIL operation } } ;
         PAIR } }
