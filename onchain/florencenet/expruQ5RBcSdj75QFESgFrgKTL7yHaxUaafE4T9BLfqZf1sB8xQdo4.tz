{ parameter
    (or (or (or (or (or (address %blacklist) (pair %buy_tickets address (pair nat string)))
                    (or (pair %buy_tickets_with_kolibri address (pair nat string))
                        (pair %create_ticket address string)))
                (or (or (pair %kolibri_ticket_price_oracle string (pair timestamp nat))
                        (pair %redeem_tickets address (pair string nat)))
                    (or (address %remove_pool) (unit %reset_kolibri_tx_status))))
            (or (or (or (address %update_admin) (address %update_kolibri_address))
                    (or (nat %update_kolibri_ticket_price) (string %update_nickname)))
                (or (or (mutez %update_nickname_fee) (pair %update_pool_addresses address string))
                    (or (address %update_storage_contract) (mutez %update_ticket_price)))))
        (or (string %update_ticket_types) (unit %withdraw_balance))) ;
  storage
    (pair (address %admin)
          (pair (big_map %pool_addresses address string)
                (pair (big_map %nicknames address (pair timestamp string))
                      (pair (big_map %blacklist address timestamp)
                            (pair (set %ticket_types string)
                                  (pair (mutez %ticket_price)
                                        (pair (int %ticket_validity)
                                              (pair (mutez %nickname_fee)
                                                    (pair (address %oracle)
                                                          (pair (address %kolibri)
                                                                (pair (nat %kolibri_ticket_price)
                                                                      (pair (bool %tx_to_kolibri) (address %storage_contract))))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET 7 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET ;
                                  IF_NONE
                                    { SWAP ;
                                      DUP ;
                                      GET 7 ;
                                      NOW ;
                                      DIG 3 ;
                                      SWAP ;
                                      SOME ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 7 }
                                    { DROP ;
                                      SWAP ;
                                      DUP ;
                                      DUP ;
                                      DUG 3 ;
                                      GET 7 ;
                                      DUP 3 ;
                                      NONE timestamp ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 7 ;
                                      DIG 2 ;
                                      GET 5 ;
                                      DIG 2 ;
                                      NONE (pair timestamp string) ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 5 } } ;
                             NIL operation ;
                             PAIR }
                           { DUP ;
                             CDR ;
                             UNPAIR ;
                             DUP 4 ;
                             GET 11 ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             MUL ;
                             AMOUNT ;
                             COMPARE ;
                             NEQ ;
                             PUSH mutez 0 ;
                             AMOUNT ;
                             COMPARE ;
                             NEQ ;
                             AND ;
                             IF { DROP 4 ; PUSH string "INCORRECT_AMOUNT" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  SENDER ;
                                  COMPARE ;
                                  NEQ ;
                                  PUSH mutez 0 ;
                                  AMOUNT ;
                                  COMPARE ;
                                  EQ ;
                                  AND ;
                                  IF { DROP 4 ; PUSH string "INVALID_SENDER" ; FAILWITH }
                                     { DUP 4 ;
                                       GET 23 ;
                                       NOT ;
                                       SELF_ADDRESS ;
                                       SENDER ;
                                       COMPARE ;
                                       EQ ;
                                       PUSH mutez 0 ;
                                       AMOUNT ;
                                       COMPARE ;
                                       EQ ;
                                       AND ;
                                       AND ;
                                       IF { DROP 4 ; PUSH string "INVALID_KOLIBRI_BUY" ; FAILWITH }
                                          { PUSH nat 0 ;
                                            SWAP ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DROP 3 ; PUSH string "ZERO_TICKET_AMOUNT" ; FAILWITH }
                                               { DUP 3 ;
                                                 GET 9 ;
                                                 SWAP ;
                                                 MEM ;
                                                 NOT ;
                                                 IF { DROP 2 ; PUSH string "INVALID_TICKET_TYPE" ; FAILWITH }
                                                    { UNPAIR ;
                                                      SWAP ;
                                                      UNPAIR ;
                                                      DUP 4 ;
                                                      GET 24 ;
                                                      CONTRACT %add_tickets
                                                        (pair (address %user) (pair (string %ticket_type) (nat %ticket_amount))) ;
                                                      IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                                      PUSH mutez 0 ;
                                                      DIG 4 ;
                                                      DIG 4 ;
                                                      DIG 4 ;
                                                      SWAP ;
                                                      PAIR ;
                                                      SWAP ;
                                                      PAIR ;
                                                      TRANSFER_TOKENS ;
                                                      SWAP ;
                                                      DUP ;
                                                      DUG 2 ;
                                                      GET 23 ;
                                                      SELF_ADDRESS ;
                                                      SENDER ;
                                                      COMPARE ;
                                                      EQ ;
                                                      PUSH mutez 0 ;
                                                      AMOUNT ;
                                                      COMPARE ;
                                                      EQ ;
                                                      AND ;
                                                      AND ;
                                                      IF { SWAP ; PUSH bool False ; UPDATE 23 ; NIL operation ; DIG 2 ; CONS ; PAIR }
                                                         { SWAP ; NIL operation ; DIG 2 ; CONS ; PAIR } } } } } } } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 23 ;
                             IF { DROP 2 ; PUSH string "ONGOING_KOLIBRI_TX" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  SENDER ;
                                  COMPARE ;
                                  EQ ;
                                  IF { DROP 2 ; PUSH string "NO_TX_FROM_SELF" ; FAILWITH }
                                     { DUP ;
                                       UNPAIR ;
                                       SWAP ;
                                       CAR ;
                                       SENDER ;
                                       DIG 2 ;
                                       COMPARE ;
                                       NEQ ;
                                       IF { DROP 3 ; PUSH string "PLAYER_IS_NOT_SENDER" ; FAILWITH }
                                          { SELF_ADDRESS ;
                                            CONTRACT %kolibri_ticket_price_oracle (pair string (pair timestamp nat)) ;
                                            IF_NONE { PUSH string "NO_CONTRACT" ; FAILWITH } {} ;
                                            DUP 4 ;
                                            GET 17 ;
                                            CONTRACT %get (pair string (contract (pair string (pair timestamp nat)))) ;
                                            IF_NONE { PUSH string "NO_ORACLE" ; FAILWITH } {} ;
                                            PUSH mutez 0 ;
                                            DIG 2 ;
                                            PUSH string "XTZ-USD" ;
                                            PAIR ;
                                            TRANSFER_TOKENS ;
                                            DUP 4 ;
                                            GET 19 ;
                                            CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                                            IF_NONE { PUSH string "UNKNOWN_KOLIBRI_CONTRACT" ; FAILWITH } {} ;
                                            SENDER ;
                                            DUP 6 ;
                                            CAR ;
                                            DUP 7 ;
                                            GET 21 ;
                                            DIG 5 ;
                                            MUL ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PUSH mutez 0 ;
                                            DIG 2 ;
                                            TRANSFER_TOKENS ;
                                            SELF %buy_tickets ;
                                            PUSH mutez 0 ;
                                            DIG 4 ;
                                            TRANSFER_TOKENS ;
                                            DIG 3 ;
                                            PUSH bool True ;
                                            UPDATE 23 ;
                                            NIL operation ;
                                            DIG 2 ;
                                            CONS ;
                                            DIG 2 ;
                                            CONS ;
                                            DIG 2 ;
                                            CONS ;
                                            PAIR } } } }
                           { UNPAIR ;
                             DUP 3 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             DUP 4 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             OR ;
                             IF { DUP 3 ;
                                  GET 24 ;
                                  CONTRACT %add_tickets
                                    (pair (address %user) (pair (string %ticket_type) (nat %ticket_amount))) ;
                                  IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                  PUSH mutez 0 ;
                                  DIG 2 ;
                                  DIG 3 ;
                                  PUSH nat 1 ;
                                  SWAP ;
                                  PAIR ;
                                  SWAP ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  SWAP ;
                                  NIL operation ;
                                  DIG 2 ;
                                  CONS ;
                                  PAIR }
                                { DROP 3 ; PUSH string "FORBIDDEN_OP" ; FAILWITH } } } }
                   { IF_LEFT
                       { IF_LEFT
                           { UNPAIR ;
                             SWAP ;
                             UNPAIR ;
                             DUP 4 ;
                             GET 19 ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 4 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { PUSH string "XTZ-USD" ;
                                  DIG 3 ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { DROP 3 ; PUSH string "WRONG_CURRENCY_PAIR" ; FAILWITH }
                                     { PUSH int 120 ;
                                       NOW ;
                                       SUB ;
                                       SWAP ;
                                       COMPARE ;
                                       LT ;
                                       IF { DROP 2 ; PUSH string "OLD_EXCHANGE_RATE" ; FAILWITH }
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            PUSH mutez 1 ;
                                            PUSH nat 1000000000000 ;
                                            DIG 4 ;
                                            GET 11 ;
                                            DIG 4 ;
                                            MUL ;
                                            MUL ;
                                            EDIV ;
                                            IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                            CAR ;
                                            UPDATE 21 } } } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "UNKNOWN_POOL" ; FAILWITH }
                                { UNPAIR ;
                                  SWAP ;
                                  UNPAIR ;
                                  DUP 4 ;
                                  GET 24 ;
                                  CONTRACT %sub_tickets
                                    (pair (address %user)
                                          (pair (string %ticket_type) (pair (nat %amount_to_sub) (int %ticket_expiry)))) ;
                                  IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                  PUSH mutez 0 ;
                                  DIG 4 ;
                                  DIG 3 ;
                                  DIG 4 ;
                                  DUP 6 ;
                                  GET 13 ;
                                  SWAP ;
                                  PAIR ;
                                  SWAP ;
                                  PAIR ;
                                  SWAP ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  SWAP ;
                                  NIL operation ;
                                  DIG 2 ;
                                  CONS ;
                                  PAIR } } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { SWAP ; DUP ; GET 3 ; DIG 2 ; NONE string ; SWAP ; UPDATE ; UPDATE 3 } ;
                             NIL operation ;
                             PAIR }
                           { DROP ;
                             DUP ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { DUP ; GET 23 ; NOT ; UPDATE 23 } ;
                             NIL operation ;
                             PAIR } } } }
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 1 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 19 } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 21 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 15 ;
                             AMOUNT ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "INCORRECT_FEE" ; FAILWITH }
                                { PUSH nat 15 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  SIZE ;
                                  COMPARE ;
                                  GT ;
                                  IF { DROP 2 ; PUSH string "NICKNAME_GREATER_THAN_30_CHAR" ; FAILWITH }
                                     { SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 5 ;
                                       SENDER ;
                                       GET ;
                                       IF_NONE
                                         { SWAP ;
                                           DUP ;
                                           GET 5 ;
                                           DIG 2 ;
                                           NOW ;
                                           PAIR ;
                                           SOME ;
                                           SENDER ;
                                           UPDATE ;
                                           UPDATE 5 }
                                         { NOW ;
                                           PUSH int 2592000 ;
                                           DIG 2 ;
                                           CAR ;
                                           ADD ;
                                           COMPARE ;
                                           GT ;
                                           IF { DROP 2 ; PUSH string "UPDATE_UNAVAILABLE" ; FAILWITH }
                                              { SWAP ;
                                                DUP ;
                                                GET 5 ;
                                                DIG 2 ;
                                                NOW ;
                                                PAIR ;
                                                SOME ;
                                                SENDER ;
                                                UPDATE ;
                                                UPDATE 5 } } } } ;
                             NIL operation ;
                             PAIR } } }
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 15 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { UNPAIR ;
                                  DIG 2 ;
                                  DUP ;
                                  DUP ;
                                  DUG 4 ;
                                  GET 3 ;
                                  DUP 3 ;
                                  GET ;
                                  IF_NONE
                                    { DIG 3 ; GET 3 ; DIG 3 ; DIG 3 ; SWAP ; SOME ; SWAP ; UPDATE }
                                    { DROP ; DIG 3 ; GET 3 ; DIG 3 ; SOME ; DIG 3 ; UPDATE } ;
                                  UPDATE 3 } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 24 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 11 } ;
                             NIL operation ;
                             PAIR } } } } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                    { SWAP ;
                      DUP ;
                      DUG 2 ;
                      GET 9 ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      MEM ;
                      IF { SWAP ; DUP ; GET 9 ; DIG 2 ; PUSH bool False ; SWAP ; UPDATE ; UPDATE 9 }
                         { SWAP ; DUP ; GET 9 ; DIG 2 ; PUSH bool True ; SWAP ; UPDATE ; UPDATE 9 } } ;
                 NIL operation ;
                 PAIR }
               { DROP ;
                 DUP ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                    { DUP ;
                      CAR ;
                      CONTRACT unit ;
                      IF_NONE { PUSH string "UNKNOWN_ADDRESS" ; FAILWITH } {} ;
                      SWAP ;
                      NIL operation ;
                      DIG 2 ;
                      BALANCE ;
                      UNIT ;
                      TRANSFER_TOKENS ;
                      CONS ;
                      PAIR } } } } }
