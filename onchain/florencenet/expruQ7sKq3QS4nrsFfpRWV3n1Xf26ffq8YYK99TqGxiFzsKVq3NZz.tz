{ storage
    (pair (pair (pair (set %administrator address) (set %all_tokens nat))
                (pair (unit %governable_storage)
                      (pair (address %governace_token) (big_map %ledger (pair address nat) nat))))
          (pair (pair (nat %message_count)
                      (pair (big_map %metadata string bytes)
                            (big_map %operators
                               (pair (address %owner) (pair (address %operator) (nat %token_id)))
                               unit)))
                (pair (bool %paused)
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (big_map %total_supply nat nat))))) ;
  parameter
    (or (or (or (address %add_administrator)
                (or (pair %balance_of
                       (list %requests (pair (address %owner) (nat %token_id)))
                       (contract %callback
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                    (pair %burn (address %address) (pair (nat %amount) (nat %token_id)))))
            (or (or (list %gov_token_transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (pair %mint
                       (pair (address %address) (nat %amount))
                       (pair (map %metadata string bytes) (nat %token_id))))
                (or (address %remove_administrator)
                    (pair %send_message
                       (list %addresses address)
                       (pair (string %body) (string %subject))))))
        (or (or (address %set_gov_token_admin)
                (or (unit %set_governable_storage) (address %set_governance_token)))
            (or (or (pair %set_metadata (string %k) (bytes %v)) (bool %set_pause))
                (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  code { CAST (pair (or (or (or address
                                (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                    (pair address (pair nat nat))))
                            (or (or (list (pair address (list (pair address (pair nat nat)))))
                                    (pair (pair address nat) (pair (map string bytes) nat)))
                                (or address (pair (list address) (pair string string)))))
                        (or (or address (or unit address))
                            (or (or (pair string bytes) bool)
                                (or (list (pair address (list (pair address (pair nat nat)))))
                                    (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair (pair (pair (set address) (set nat))
                                (pair unit (pair address (big_map (pair address nat) nat))))
                          (pair (pair nat (pair (big_map string bytes) (big_map (pair address (pair address nat)) unit)))
                                (pair bool (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat)))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH bool True ;
                     DIG 5 ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         CAR ;
                         MAP { DUP 3 ;
                               GET 7 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CDR ;
                               MEM ;
                               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                               DUP 3 ;
                               CAR ;
                               GET 6 ;
                               SWAP ;
                               DUP ;
                               CDR ;
                               SWAP ;
                               DUP ;
                               DUG 3 ;
                               CAR ;
                               PAIR ;
                               MEM ;
                               IF { DUP 3 ;
                                    CAR ;
                                    GET 6 ;
                                    SWAP ;
                                    DUP ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 3 ;
                                    CAR ;
                                    PAIR ;
                                    GET ;
                                    IF_NONE { PUSH int 412 ; FAILWITH } {} ;
                                    SWAP ;
                                    PAIR }
                                  { PUSH nat 0 ; SWAP ; PAIR } } ;
                         NIL operation ;
                         DIG 2 ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "Cannot burn another's tokens" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP ;
                         DIG 6 ;
                         DUP ;
                         GET 4 ;
                         SWAP ;
                         DUP ;
                         DUG 8 ;
                         CAR ;
                         PAIR ;
                         DUP ;
                         DUG 2 ;
                         GET ;
                         IF_NONE { PUSH int 524 ; FAILWITH } { DROP } ;
                         DUP 7 ;
                         GET 3 ;
                         DIG 8 ;
                         CAR ;
                         GET 6 ;
                         DIG 8 ;
                         DUP ;
                         GET 4 ;
                         SWAP ;
                         DUP ;
                         DUG 10 ;
                         CAR ;
                         PAIR ;
                         GET ;
                         IF_NONE { PUSH int 524 ; FAILWITH } {} ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { PUSH int 524 ; FAILWITH } {} ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         DUP ;
                         DUG 2 ;
                         DUP ;
                         GET 8 ;
                         DUP 3 ;
                         GET 3 ;
                         DIG 4 ;
                         GET 8 ;
                         DUP 5 ;
                         GET 4 ;
                         GET ;
                         IF_NONE { PUSH int 525 ; FAILWITH } {} ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { PUSH int 525 ; FAILWITH } {} ;
                         SOME ;
                         DIG 3 ;
                         GET 4 ;
                         UPDATE ;
                         UPDATE 8 ;
                         NIL operation } } }
               { IF_LEFT
                   { IF_LEFT
                       { NIL operation ;
                         DUP 3 ;
                         CAR ;
                         GET 5 ;
                         CONTRACT %transfer (list (pair address (list (pair address (pair nat nat))))) ;
                         IF_NONE { PUSH int 601 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         PUSH bool True ;
                         DUP 6 ;
                         GET 4 ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         GET 4 ;
                         SWAP ;
                         DUP ;
                         DUG 3 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         MEM ;
                         IF { SWAP ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              SWAP ;
                              DUP ;
                              DIG 6 ;
                              DUP ;
                              GET 4 ;
                              SWAP ;
                              DUP ;
                              DUG 8 ;
                              CAR ;
                              CAR ;
                              PAIR ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 547 ; FAILWITH } {} ;
                              DUP 8 ;
                              CAR ;
                              CDR ;
                              ADD ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              SWAP ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              SWAP }
                            { SWAP ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              SWAP ;
                              DUP 6 ;
                              CAR ;
                              CDR ;
                              SOME ;
                              DIG 6 ;
                              DUP ;
                              GET 4 ;
                              SWAP ;
                              DUP ;
                              DUG 8 ;
                              CAR ;
                              CAR ;
                              PAIR ;
                              UPDATE ;
                              SWAP ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              SWAP } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 4 ;
                         MEM ;
                         IF { DROP }
                            { SWAP ;
                              DUP ;
                              GET 7 ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              SWAP ;
                              DUP ;
                              DUG 4 ;
                              GET 4 ;
                              PAIR ;
                              SOME ;
                              DUP 4 ;
                              GET 4 ;
                              UPDATE ;
                              UPDATE 7 ;
                              DUP ;
                              GET 8 ;
                              DUP 3 ;
                              CAR ;
                              CDR ;
                              SOME ;
                              DIG 3 ;
                              GET 4 ;
                              UPDATE ;
                              UPDATE 8 } ;
                         NIL operation } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         UNPAIR ;
                         PUSH bool False ;
                         DIG 5 ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { DUP ;
                         CAR ;
                         ITER { DUP 3 ;
                                UNPAIR ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                PUSH bool True ;
                                DIG 7 ;
                                GET 3 ;
                                CAR ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                PAIR ;
                                DUP ;
                                CAR ;
                                GET 6 ;
                                SWAP ;
                                DUP ;
                                DUG 4 ;
                                GET 3 ;
                                CAR ;
                                DUP 3 ;
                                PAIR ;
                                MEM ;
                                IF { DUP 3 ;
                                     UNPAIR ;
                                     UNPAIR ;
                                     SWAP ;
                                     UNPAIR ;
                                     SWAP ;
                                     UNPAIR ;
                                     SWAP ;
                                     DUP ;
                                     DIG 8 ;
                                     GET 3 ;
                                     CAR ;
                                     DIG 7 ;
                                     PAIR ;
                                     DUP ;
                                     DUG 2 ;
                                     GET ;
                                     IF_NONE { PUSH int 547 ; FAILWITH } {} ;
                                     PUSH nat 1 ;
                                     ADD ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     SWAP ;
                                     PAIR ;
                                     SWAP ;
                                     PAIR ;
                                     SWAP ;
                                     PAIR ;
                                     PAIR ;
                                     SWAP }
                                   { DUP 3 ;
                                     UNPAIR ;
                                     UNPAIR ;
                                     SWAP ;
                                     UNPAIR ;
                                     SWAP ;
                                     UNPAIR ;
                                     SWAP ;
                                     PUSH (option nat) (Some 1) ;
                                     DIG 8 ;
                                     GET 3 ;
                                     CAR ;
                                     DIG 7 ;
                                     PAIR ;
                                     UPDATE ;
                                     SWAP ;
                                     PAIR ;
                                     SWAP ;
                                     PAIR ;
                                     SWAP ;
                                     PAIR ;
                                     PAIR ;
                                     SWAP } ;
                                SWAP ;
                                DUP ;
                                GET 7 ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                GET 3 ;
                                CAR ;
                                MEM ;
                                IF {}
                                   { SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     DUP ;
                                     GET 7 ;
                                     PUSH (map string bytes) { Elt "decimals" 0x30 } ;
                                     DUP 4 ;
                                     GET 4 ;
                                     PACK ;
                                     PUSH nat 6 ;
                                     DUP 6 ;
                                     GET 4 ;
                                     PACK ;
                                     SIZE ;
                                     SUB ;
                                     ISNAT ;
                                     IF_NONE { PUSH int 565 ; FAILWITH } {} ;
                                     PUSH nat 6 ;
                                     SLICE ;
                                     IF_NONE { PUSH int 566 ; FAILWITH } {} ;
                                     SOME ;
                                     PUSH string "symbol" ;
                                     UPDATE ;
                                     DUP 4 ;
                                     GET 3 ;
                                     PACK ;
                                     PUSH nat 6 ;
                                     DUP 6 ;
                                     GET 3 ;
                                     PACK ;
                                     SIZE ;
                                     SUB ;
                                     ISNAT ;
                                     IF_NONE { PUSH int 565 ; FAILWITH } {} ;
                                     PUSH nat 6 ;
                                     SLICE ;
                                     IF_NONE { PUSH int 566 ; FAILWITH } {} ;
                                     SOME ;
                                     PUSH string "name" ;
                                     UPDATE ;
                                     DUP 5 ;
                                     GET 3 ;
                                     CAR ;
                                     PAIR ;
                                     SOME ;
                                     DIG 4 ;
                                     GET 3 ;
                                     CAR ;
                                     UPDATE ;
                                     UPDATE 7 ;
                                     DUP ;
                                     DUG 2 ;
                                     DUP ;
                                     GET 8 ;
                                     PUSH (option nat) (Some 1) ;
                                     DIG 4 ;
                                     GET 3 ;
                                     CAR ;
                                     UPDATE ;
                                     UPDATE 8 ;
                                     SWAP } } ;
                         DROP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } ;
                     NIL operation } } }
           { IF_LEFT
               { IF_LEFT
                   { NIL operation ;
                     DUP 3 ;
                     CAR ;
                     GET 5 ;
                     CONTRACT %transfer address ;
                     IF_NONE { PUSH int 612 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { SWAP ; UNPAIR ; UNPAIR ; SWAP ; CDR ; DIG 3 ; PAIR ; SWAP ; PAIR ; PAIR }
                       { SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         DIG 4 ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } ;
                     NIL operation } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         DUP 6 ;
                         CDR ;
                         SOME ;
                         DIG 6 ;
                         CAR ;
                         UPDATE ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 5 } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { DUP 4 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       SENDER ;
                                       MEM ;
                                       IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                       IF { PUSH bool True }
                                          { DUP 4 ;
                                            GET 3 ;
                                            GET 4 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       DUP 4 ;
                                       GET 7 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            DUP 5 ;
                                            CAR ;
                                            GET 6 ;
                                            DUP 3 ;
                                            GET 3 ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 390 ; FAILWITH } {} ;
                                            COMPARE ;
                                            GE ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            DUP 4 ;
                                            UNPAIR ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            DUP ;
                                            DUP 7 ;
                                            GET 3 ;
                                            DUP 9 ;
                                            CAR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 2 ;
                                            GET ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } { DROP } ;
                                            DUP 7 ;
                                            GET 4 ;
                                            DIG 10 ;
                                            CAR ;
                                            GET 6 ;
                                            DUP 9 ;
                                            GET 3 ;
                                            DUP 11 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } {} ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } {} ;
                                            SOME ;
                                            SWAP ;
                                            UPDATE ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 4 ;
                                            CAR ;
                                            GET 6 ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 3 ;
                                            CAR ;
                                            PAIR ;
                                            MEM ;
                                            IF { DIG 3 ;
                                                 UNPAIR ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 DUP ;
                                                 DIG 6 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 8 ;
                                                 CAR ;
                                                 PAIR ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET ;
                                                 IF_NONE { PUSH int 397 ; FAILWITH } {} ;
                                                 DIG 7 ;
                                                 GET 4 ;
                                                 ADD ;
                                                 SOME ;
                                                 SWAP ;
                                                 UPDATE ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUG 2 }
                                               { DIG 3 ;
                                                 UNPAIR ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 DUP 6 ;
                                                 GET 4 ;
                                                 SOME ;
                                                 DIG 6 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 CAR ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUG 2 } }
                                          { DROP } } ;
                                DROP } ;
                         DROP }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; MEM } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; MEM } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    NONE unit ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP } } ;
                         DROP } } ;
                 NIL operation } } ;
         PAIR } }
