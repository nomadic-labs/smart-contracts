{ parameter
    (or (or (pair %add_item
               (pair (pair (nat %available_quantity) (map %data string string))
                     (pair (nat %item_id) (string %name)))
               (pair (option %no_update_after timestamp) (nat %total_quantity)))
            (pair %assign_item_proxy (pair address nat) nat))
        (or (nat %freeze_item)
            (pair %update_item
               (pair (pair (nat %available_quantity) (map %data string string))
                     (pair (nat %item_id) (string %name)))
               (pair (option %no_update_after timestamp) (nat %total_quantity))))) ;
  storage
    (pair (pair (address %owner) (string %version))
          (big_map %warehouse
             nat
             (pair (pair (pair (nat %available_quantity) (map %data string string))
                         (pair (nat %item_id) (string %name)))
                   (pair (option %no_update_after timestamp) (nat %total_quantity))))) ;
  code { { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 GET ;
                 IF_NONE
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     CAR ;
                     CDR ;
                     CAR ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     CAR ;
                     PAIR }
                   { DROP 2 ; PUSH string "ITEM_ID_ALREADY_EXISTS" ; FAILWITH } ;
                 NIL operation ;
                 PAIR }
               { NIL operation ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CDR ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 CDR ;
                 GET ;
                 IF_NONE
                   { SWAP ; DROP ; PUSH string "ITEM_DOESNT_EXIST" ; FAILWITH }
                   { DUP ;
                     CDR ;
                     CDR ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CDR ;
                     COMPARE ;
                     GT ;
                     PUSH nat 0 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     CAR ;
                     CAR ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     IF { DIG 2 ; DROP 2 ; PUSH string "NO_AVAILABLE_ITEM" ; FAILWITH }
                        { SWAP ;
                          DROP ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          CAR ;
                          CONTRACT %assign_item
                            (pair (pair (map %data string string) (nat %instance_number)) (nat %item_id)) ;
                          IF_NONE { PUSH string "CONTRACT_NOT_FOUND" ; FAILWITH } {} ;
                          PUSH mutez 0 ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          CAR ;
                          CDR ;
                          { DIP 4 { DUP } ; DIG 5 } ;
                          CDR ;
                          EMPTY_MAP string string ;
                          PAIR ;
                          PAIR ;
                          TRANSFER_TOKENS ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          CAR ;
                          CDR ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          CAR ;
                          CAR ;
                          CDR ;
                          PUSH nat 1 ;
                          DIG 5 ;
                          CAR ;
                          CAR ;
                          CAR ;
                          SUB ;
                          ABS ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          CDR ;
                          SWAP ;
                          DIG 3 ;
                          CAR ;
                          CDR ;
                          SWAP ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          DIG 2 ;
                          CAR ;
                          PAIR ;
                          NIL operation ;
                          DIG 2 ;
                          CONS ;
                          PAIR } } } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 SWAP ;
                 GET ;
                 IF_NONE
                   { PUSH string "ITEM_ID_DOESNT_EXIST" ; FAILWITH }
                   { DUP ;
                     CDR ;
                     CAR ;
                     IF_NONE
                       { PUSH unit Unit }
                       { NOW ;
                         COMPARE ;
                         GE ;
                         IF { PUSH string "ITEM_IS_FROZEN" ; FAILWITH } { PUSH unit Unit } } ;
                     DROP ;
                     DUP ;
                     CDR ;
                     CDR ;
                     NOW ;
                     SOME ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     PAIR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     SWAP ;
                     SOME ;
                     DIG 2 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     CAR ;
                     PAIR } ;
                 NIL operation ;
                 PAIR }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 GET ;
                 IF_NONE
                   { DROP ; PUSH string "ITEM_ID_DOESNT_EXIST" ; FAILWITH }
                   { CDR ;
                     CAR ;
                     IF_NONE
                       { PUSH unit Unit }
                       { NOW ;
                         COMPARE ;
                         GE ;
                         IF { PUSH string "ITEM_IS_FROZEN" ; FAILWITH } { PUSH unit Unit } } ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     SOME ;
                     DIG 2 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     CAR ;
                     PAIR } ;
                 NIL operation ;
                 PAIR } } } }
