{ parameter
    (or (or (or (or (list %addAllowedChars (pair string bytes))
                    (list %batchUpdateName
                       (pair (address %nft_address) (pair (nat %token_id) (bytes %new_name)))))
                (or (list %removeAllowedChars string) (address %setDogaAddress)))
            (or (or (address %setMultisigContract)
                    (pair %setParams (nat %name_minsize) (pair (nat %name_maxsize) (nat %doga_price))))
                (or (bool %setPause) (address %setReserveAddress))))
        (pair %updateName (address %nft_address) (pair (nat %token_id) (string %new_name)))) ;
  storage
    (pair (bool %paused)
          (pair (address %multisig)
                (pair (address %doga_address)
                      (pair (address %reserve_address)
                            (pair (map %allowed_chars string bytes)
                                  (pair (big_map %token_names nat bytes)
                                        (pair (nat %name_minsize) (pair (nat %name_maxsize) (nat %doga_price))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "addAllowedChars" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address (list (pair string bytes))) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %addAllowedChars (list (pair string bytes)) ;
                                      IF_NONE
                                        { DROP ; PUSH string "no addAllowedChars entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 9 ;
                              SWAP ;
                              ITER { DUP ; DUG 2 ; CDR ; SOME ; DIG 2 ; CAR ; UPDATE } ;
                              UPDATE 9 ;
                              NIL operation ;
                              PAIR } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "batchUpdateName" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address (list (pair address (pair nat bytes)))) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %batchUpdateName
                                        (list (pair (address %nft_address) (pair (nat %token_id) (bytes %new_name)))) ;
                                      IF_NONE
                                        { DROP ; PUSH string "no batchUpdateName entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { SWAP ;
                              NIL operation ;
                              PAIR ;
                              SWAP ;
                              ITER { DUP ;
                                     DUG 2 ;
                                     CAR ;
                                     CONTRACT %updateMetadataWithFunction
                                       (pair (lambda %metadata_updater (map string bytes) (map string bytes)) (nat %token_id)) ;
                                     IF_NONE
                                       { PUSH string "NFT contract must have an update metadata with function entrypoint" ;
                                         FAILWITH }
                                       { PUSH mutez 0 ;
                                         DUP 4 ;
                                         GET 3 ;
                                         DUP 5 ;
                                         LAMBDA
                                           (pair (pair address (pair nat bytes)) (map string bytes))
                                           (map string bytes)
                                           { UNPAIR ; GET 4 ; SOME ; PUSH string "name" ; UPDATE } ;
                                         SWAP ;
                                         APPLY ;
                                         PAIR ;
                                         TRANSFER_TOKENS } ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     CDR ;
                                     DUP 3 ;
                                     CDR ;
                                     GET 11 ;
                                     DUP 5 ;
                                     GET 4 ;
                                     SOME ;
                                     DIG 5 ;
                                     GET 3 ;
                                     UPDATE ;
                                     UPDATE 11 ;
                                     DIG 2 ;
                                     CAR ;
                                     DIG 2 ;
                                     CONS ;
                                     PAIR } } } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "removeAllowedChars" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address (list string)) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %removeAllowedChars (list string) ;
                                      IF_NONE
                                        { DROP ; PUSH string "no removeAllowedChars entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 9 ;
                              SWAP ;
                              ITER { SWAP ; NONE bytes ; DIG 2 ; UPDATE } ;
                              UPDATE 9 ;
                              NIL operation ;
                              PAIR } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "setDogaAddress" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address address) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %setDogaAddress address ;
                                      IF_NONE
                                        { DROP ; PUSH string "no setDogaAddress entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { UPDATE 5 ; NIL operation ; PAIR } } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "updateMultisig" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address address) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %updateMultisig address ;
                                      IF_NONE
                                        { DROP ; PUSH string "no updateMultisig entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { UPDATE 3 ; NIL operation ; PAIR } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "setParams" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address (pair nat (pair nat nat))) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %setParams
                                        (pair (nat %name_minsize) (pair (nat %name_maxsize) (nat %doga_price))) ;
                                      IF_NONE
                                        { DROP ; PUSH string "no setParams entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { DUP ;
                              DUG 2 ;
                              CAR ;
                              UPDATE 13 ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              UPDATE 15 ;
                              SWAP ;
                              GET 4 ;
                              UPDATE 16 ;
                              NIL operation ;
                              PAIR } } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "setPause" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address bool) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %setPause bool ;
                                      IF_NONE
                                        { DROP ; PUSH string "no setPause entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { UPDATE 1 ; NIL operation ; PAIR } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { SELF_ADDRESS ;
                              DIG 2 ;
                              DUP ;
                              GET 3 ;
                              CONTRACT %callMultisig
                                (pair (pair %entrypoint_signature
                                         (string %name)
                                         (pair (bytes %params) (address %source_contract)))
                                      (lambda %callback unit (list operation))) ;
                              IF_NONE
                                { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                                { SELF_ADDRESS ;
                                  DUP 5 ;
                                  PACK ;
                                  SHA256 ;
                                  PUSH string "setReserveAddress" ;
                                  PAIR 3 ;
                                  SWAP ;
                                  PUSH mutez 0 ;
                                  DIG 5 ;
                                  DIG 5 ;
                                  PAIR ;
                                  LAMBDA
                                    (pair (pair address address) unit)
                                    (list operation)
                                    { CAR ;
                                      UNPAIR ;
                                      CONTRACT %setReserveAddress address ;
                                      IF_NONE
                                        { DROP ; PUSH string "no setReserveAddress entrypoint" ; FAILWITH }
                                        { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                                  SWAP ;
                                  APPLY ;
                                  DIG 3 ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  NIL operation ;
                                  SWAP ;
                                  CONS } ;
                              PAIR }
                            { UPDATE 7 ; NIL operation ; PAIR } } } } }
           { PUSH bool True ;
             DUP 3 ;
             CAR ;
             COMPARE ;
             EQ ;
             IF { DROP 2 ; PUSH string "Contract is paused" ; FAILWITH }
                { DUP ;
                  GET 3 ;
                  SENDER ;
                  PAIR ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  CAR ;
                  SWAP ;
                  VIEW "balance_of_view" nat ;
                  IF_NONE
                    { DROP 2 ; PUSH string "View returned an error" ; FAILWITH }
                    { PUSH nat 1 ;
                      SWAP ;
                      COMPARE ;
                      NEQ ;
                      IF { DROP 2 ; PUSH string "You do not own this token" ; FAILWITH }
                         { SWAP ;
                           DUP ;
                           DUG 2 ;
                           GET 16 ;
                           DUP 3 ;
                           GET 7 ;
                           SENDER ;
                           DUP 5 ;
                           GET 5 ;
                           PAIR 4 ;
                           UNPAIR 4 ;
                           CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                           IF_NONE
                             { PUSH string "FA1.2 contract must have a transfer entrypoint" ; FAILWITH }
                             {} ;
                           PUSH mutez 0 ;
                           DIG 4 ;
                           DIG 4 ;
                           DIG 4 ;
                           PAIR 3 ;
                           TRANSFER_TOKENS ;
                           DUP 3 ;
                           GET 15 ;
                           DUP 3 ;
                           GET 4 ;
                           SIZE ;
                           COMPARE ;
                           GT ;
                           IF { PUSH string "Name is too long" ; FAILWITH }
                              { DUP 3 ;
                                GET 13 ;
                                DUP 3 ;
                                GET 4 ;
                                SIZE ;
                                COMPARE ;
                                LT ;
                                IF { PUSH string "Name is too short" ; FAILWITH }
                                   { PUSH nat 0 ;
                                     PUSH bytes 0x ;
                                     PAIR ;
                                     LEFT bytes ;
                                     LOOP_LEFT
                                       { UNPAIR ;
                                         DUP 4 ;
                                         GET 4 ;
                                         SIZE ;
                                         PUSH nat 1 ;
                                         DUP 4 ;
                                         ADD ;
                                         COMPARE ;
                                         EQ ;
                                         IF { DUP 5 ;
                                              GET 9 ;
                                              DUP 5 ;
                                              GET 4 ;
                                              PUSH nat 1 ;
                                              DIG 4 ;
                                              SLICE ;
                                              IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                                              GET ;
                                              IF_NONE { PUSH string "Character not allowed" ; FAILWITH } {} ;
                                              SWAP ;
                                              CONCAT ;
                                              RIGHT (pair bytes nat) }
                                            { DUP 5 ;
                                              GET 9 ;
                                              DUP 5 ;
                                              GET 4 ;
                                              PUSH nat 1 ;
                                              DUP 5 ;
                                              SLICE ;
                                              IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                                              GET ;
                                              IF_NONE { PUSH string "Character not allowed" ; FAILWITH } {} ;
                                              SWAP ;
                                              CONCAT ;
                                              PUSH nat 1 ;
                                              DIG 2 ;
                                              ADD ;
                                              SWAP ;
                                              PAIR ;
                                              LEFT bytes } } } } ;
                           DUP 4 ;
                           GET 11 ;
                           DUP 4 ;
                           GET 3 ;
                           GET ;
                           IF_NONE
                             { UNIT }
                             { SWAP ;
                               DUP ;
                               DUG 2 ;
                               SWAP ;
                               COMPARE ;
                               EQ ;
                               IF { PUSH string "Please select a different name than the current one" ;
                                    FAILWITH }
                                  { UNIT } } ;
                           DROP ;
                           DUP 3 ;
                           CAR ;
                           CONTRACT %updateMetadataWithFunction
                             (pair (lambda %metadata_updater (map string bytes) (map string bytes)) (nat %token_id)) ;
                           IF_NONE
                             { PUSH string "NFT contract must have an update metadata with function entrypoint" ;
                               FAILWITH }
                             { PUSH mutez 0 ;
                               DUP 5 ;
                               GET 3 ;
                               DUP 4 ;
                               LAMBDA
                                 (pair bytes (map string bytes))
                                 (map string bytes)
                                 { UNPAIR ;
                                   SWAP ;
                                   DUP ;
                                   PUSH string "attributes" ;
                                   GET ;
                                   IF_NONE { PUSH string "Attributes does not exist" ; FAILWITH } {} ;
                                   UNPACK (map string (pair (option string) string)) ;
                                   IF_NONE { PUSH string "Could not unpack attributes" ; FAILWITH } {} ;
                                   PUSH string "Status" ;
                                   GET ;
                                   IF_NONE
                                     { PUSH string "Status attribute does not exist" ; FAILWITH }
                                     { PUSH string "Box" ;
                                       SWAP ;
                                       CDR ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH string "Token has not been revealed" ; FAILWITH } {} } ;
                                   SWAP ;
                                   SOME ;
                                   PUSH string "name" ;
                                   UPDATE } ;
                               SWAP ;
                               APPLY ;
                               PAIR ;
                               TRANSFER_TOKENS } ;
                           DIG 4 ;
                           DUP ;
                           GET 11 ;
                           DIG 3 ;
                           SOME ;
                           DIG 5 ;
                           GET 3 ;
                           UPDATE ;
                           UPDATE 11 ;
                           NIL operation ;
                           DIG 3 ;
                           CONS ;
                           DIG 2 ;
                           CONS ;
                           PAIR } } } } } }
