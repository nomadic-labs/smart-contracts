{ parameter
    (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
            (or %assets
               (or (pair %balance_of
                      (list %requests (pair (address %owner) (nat %token_id)))
                      (contract %callback
                         (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                   (list %transfer
                      (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))))
               (list %update_operators
                  (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                      (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))))
        (or (or %postcards
               (or (list %mail_postcard
                      (pair (address %postman)
                            (key %pk)
                            (signature %sig)
                            (nat %counter)
                            (address %address)
                            (nat %token_id)
                            (bytes %token_info_uri)))
                   (list %mint_postcard (pair (address %owner) (nat %token_id) (bytes %token_info_uri))))
               (list %stamp_postcard
                  (pair (address %postman)
                        (key %pk)
                        (signature %sig)
                        (nat %counter)
                        (nat %token_id)
                        (bytes %token_info_uri))))
            (or %trustee (address %add_trustee) (address %remove_trustee)))) ;
  storage
    (pair (pair (pair (pair %admin (pair (address %admin) (bool %paused)) (option %pending_admin address))
                      (pair %assets
                         (pair (big_map %ledger (pair address nat) nat)
                               (big_map %operators (pair address address nat) unit))
                         (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                         (big_map %token_total_supply nat nat)))
                (big_map %metadata string bytes)
                (big_map %postcards
                   nat
                   (pair (pair (nat %counter) (address %postman)) (bool %stamped))))
          (pair %trustee (nat %max_trustee) (set %trustees address))) ;
  code { PUSH string "FA2_TOKEN_UNDEFINED" ;
         LAMBDA
           (pair (pair address bool) (option address))
           unit
           { CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair (pair address nat) (big_map (pair address nat) nat))
           nat
           { UNPAIR ; GET ; IF_NONE { PUSH nat 0 } {} } ;
         PUSH bytes 0x7c ;
         LAMBDA
           (pair key address)
           unit
           { UNPAIR ;
             HASH_KEY ;
             IMPLICIT_ACCOUNT ;
             ADDRESS ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "PUBLIC_KEY_AND_ADDRESS_MISMATCH" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair (pair key signature) bytes)
           unit
           { UNPAIR ;
             UNPAIR ;
             PUSH bool False ;
             DUG 3 ;
             CHECK_SIGNATURE ;
             COMPARE ;
             EQ ;
             IF { PUSH string "INVALID_SIGNATURE" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair (pair address nat) (pair nat address) bool)
           unit
           { UNPAIR ;
             UNPAIR ;
             DUP 3 ;
             CAR ;
             CAR ;
             DIG 2 ;
             COMPARE ;
             NEQ ;
             DIG 2 ;
             CAR ;
             CDR ;
             DIG 2 ;
             COMPARE ;
             NEQ ;
             OR ;
             IF { PUSH string "POSTCARD_INFO_MISMATCH" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair bytes (pair nat nat))
           bytes
           { UNPAIR ;
             SWAP ;
             UNPAIR ;
             PACK ;
             SWAP ;
             PACK ;
             SELF_ADDRESS ;
             PACK ;
             NIL bytes ;
             DIG 2 ;
             CONS ;
             DUP 4 ;
             CONS ;
             DIG 2 ;
             CONS ;
             DIG 2 ;
             CONS ;
             SWAP ;
             CONS ;
             PUSH bytes 0x54657a6f73205369676e6564204d6573736167653a ;
             CONS ;
             CONCAT } ;
         DUP 5 ;
         APPLY ;
         DIG 4 ;
         DROP ;
         LAMBDA
           (pair (pair nat bytes) (big_map nat (pair nat (map string bytes))))
           (big_map nat (pair nat (map string bytes)))
           { UNPAIR ;
             UNPAIR ;
             DUP 3 ;
             DUP 2 ;
             MEM ;
             NOT ;
             IF { PUSH string "TOKEN_METADATA_NOT_FOUND" ; FAILWITH } {} ;
             DIG 2 ;
             EMPTY_MAP string bytes ;
             DIG 3 ;
             SOME ;
             PUSH string "" ;
             UPDATE ;
             DUP 3 ;
             PAIR ;
             SOME ;
             DIG 2 ;
             UPDATE } ;
         DIG 8 ;
         UNPAIR ;
         IF_LEFT
           { DIG 2 ;
             DIG 3 ;
             DIG 4 ;
             DIG 5 ;
             DIG 6 ;
             DROP 5 ;
             IF_LEFT
               { DIG 2 ;
                 DIG 4 ;
                 DROP 2 ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DIG 3 ;
                         DROP 2 ;
                         DUP ;
                         CDR ;
                         IF_NONE
                           { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             SWAP ;
                             DUP 2 ;
                             COMPARE ;
                             EQ ;
                             IF { NONE address ; DIG 2 ; CAR ; CDR ; DIG 2 ; PAIR ; PAIR }
                                { DROP 2 ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } }
                       { DUP 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP 2 ;
                         CDR ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR } }
                   { DUP 2 ; DIG 4 ; SWAP ; EXEC ; DROP ; SOME ; SWAP ; CAR ; PAIR } ;
                 NIL operation ;
                 DUP 3 ;
                 CDR ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DIG 4 }
               { DIG 3 ;
                 DROP ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CDR ;
                 IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DUP ;
                         CAR ;
                         MAP { DUP 3 ;
                               CDR ;
                               CAR ;
                               DUP 2 ;
                               CDR ;
                               MEM ;
                               NOT ;
                               IF { DROP ; DUP 5 ; FAILWITH }
                                  { DUP 3 ;
                                    CAR ;
                                    CAR ;
                                    DUP 2 ;
                                    CDR ;
                                    DUP 3 ;
                                    CAR ;
                                    PAIR ;
                                    PAIR ;
                                    DUP 6 ;
                                    SWAP ;
                                    EXEC ;
                                    SWAP ;
                                    PAIR } } ;
                         DIG 4 ;
                         DIG 5 ;
                         DROP 2 ;
                         SWAP ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS }
                       { DUP 2 ;
                         CAR ;
                         CAR ;
                         SWAP ;
                         ITER { SWAP ;
                                DUP 2 ;
                                CDR ;
                                ITER { SWAP ;
                                       DUP 4 ;
                                       CDR ;
                                       CAR ;
                                       DUP 3 ;
                                       GET 3 ;
                                       MEM ;
                                       NOT ;
                                       IF { DROP 2 ; DUP 5 ; FAILWITH }
                                          { SENDER ;
                                            DUP 4 ;
                                            CAR ;
                                            DUP 2 ;
                                            DUP 2 ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DROP 2 }
                                               { DUP 6 ;
                                                 CAR ;
                                                 CDR ;
                                                 DUP 5 ;
                                                 GET 3 ;
                                                 DIG 3 ;
                                                 PAIR ;
                                                 DIG 2 ;
                                                 PAIR ;
                                                 MEM ;
                                                 IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } ;
                                            DUP 2 ;
                                            GET 3 ;
                                            DUP 4 ;
                                            CAR ;
                                            PAIR ;
                                            DUP 2 ;
                                            DUP 2 ;
                                            PAIR ;
                                            DUP 8 ;
                                            SWAP ;
                                            EXEC ;
                                            DUP 4 ;
                                            GET 4 ;
                                            SWAP ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE
                                              { DROP 2 ; PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                              { PUSH nat 0 ;
                                                DUP 2 ;
                                                COMPARE ;
                                                EQ ;
                                                IF { DROP ; NONE nat ; SWAP ; UPDATE }
                                                   { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
                                            DUP 2 ;
                                            GET 3 ;
                                            DUP 3 ;
                                            CAR ;
                                            PAIR ;
                                            DUP 2 ;
                                            DUP 2 ;
                                            PAIR ;
                                            DUP 8 ;
                                            SWAP ;
                                            EXEC ;
                                            DIG 3 ;
                                            GET 4 ;
                                            ADD ;
                                            PUSH nat 0 ;
                                            DUP 2 ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DROP ; NONE nat ; SWAP ; UPDATE }
                                               { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } } ;
                                SWAP ;
                                DROP } ;
                         DIG 3 ;
                         DIG 4 ;
                         DROP 2 ;
                         DUP 2 ;
                         CDR ;
                         DIG 2 ;
                         CAR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         PAIR ;
                         NIL operation } }
                   { DIG 3 ;
                     DIG 4 ;
                     DROP 2 ;
                     SENDER ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     DIG 2 ;
                     ITER { SWAP ;
                            DUP 3 ;
                            DUP 3 ;
                            IF_LEFT {} {} ;
                            CAR ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                            SWAP ;
                            IF_LEFT
                              { SWAP ;
                                UNIT ;
                                SOME ;
                                DUP 3 ;
                                GET 4 ;
                                DUP 4 ;
                                GET 3 ;
                                PAIR ;
                                DIG 3 ;
                                CAR ;
                                PAIR ;
                                UPDATE }
                              { SWAP ;
                                DUP 2 ;
                                GET 4 ;
                                DUP 3 ;
                                GET 3 ;
                                PAIR ;
                                DIG 2 ;
                                CAR ;
                                PAIR ;
                                NONE unit ;
                                SWAP ;
                                UPDATE } } ;
                     SWAP ;
                     DROP ;
                     DUP 2 ;
                     CDR ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     NIL operation } ;
                 DUP 3 ;
                 CDR ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CAR } ;
             PAIR ;
             PAIR ;
             PAIR ;
             SWAP }
           { DIG 7 ;
             DIG 9 ;
             DROP 2 ;
             IF_LEFT
               { DIG 7 ;
                 DROP ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DUP 3 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 DIG 2 ;
                 IF_LEFT
                   { IF_LEFT
                       { DUG 2 ;
                         PAIR ;
                         SWAP ;
                         ITER { SWAP ;
                                UNPAIR ;
                                DUP 3 ;
                                CAR ;
                                DUP 4 ;
                                GET 3 ;
                                PAIR ;
                                DUP 10 ;
                                SWAP ;
                                EXEC ;
                                DROP ;
                                DUP 3 ;
                                GET 7 ;
                                DUP 4 ;
                                GET 11 ;
                                PAIR ;
                                DUP 7 ;
                                SWAP ;
                                EXEC ;
                                DUP 4 ;
                                GET 5 ;
                                DUP 5 ;
                                GET 3 ;
                                PAIR ;
                                PAIR ;
                                DUP 9 ;
                                SWAP ;
                                EXEC ;
                                DROP ;
                                DUP 3 ;
                                GET 11 ;
                                DUP 2 ;
                                DUP 2 ;
                                GET ;
                                IF_NONE
                                  { DROP 2 ; PUSH string "INVALID_POSTCARD_INFO" ; FAILWITH }
                                  { DUP ;
                                    DUP 6 ;
                                    GET 7 ;
                                    DUP 7 ;
                                    CAR ;
                                    PAIR ;
                                    PAIR ;
                                    DUP 10 ;
                                    SWAP ;
                                    EXEC ;
                                    DROP ;
                                    DUP ;
                                    CDR ;
                                    NOT ;
                                    IF { DROP 3 ; PUSH string "TOKEN_HAS_NOT_STAMPED_YET" ; FAILWITH }
                                       { PUSH nat 15 ;
                                         DUP 2 ;
                                         CAR ;
                                         CAR ;
                                         COMPARE ;
                                         EQ ;
                                         IF { DROP 3 ; PUSH string "COUNTER_HAS_REACHED_THE_LIMIT" ; FAILWITH }
                                            { PUSH bool False ;
                                              DUP 6 ;
                                              GET 9 ;
                                              PUSH nat 1 ;
                                              DIG 3 ;
                                              CAR ;
                                              CAR ;
                                              ADD ;
                                              PAIR ;
                                              PAIR ;
                                              DIG 2 ;
                                              SWAP ;
                                              SOME ;
                                              DIG 2 ;
                                              UPDATE } } } ;
                                DUP 2 ;
                                CAR ;
                                CAR ;
                                DUP 4 ;
                                GET 11 ;
                                DUP 5 ;
                                GET 9 ;
                                PAIR ;
                                GET ;
                                IF_NONE
                                  { PUSH nat 1 }
                                  { PUSH nat 0 ;
                                    SWAP ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH nat 1 } { PUSH string "DUPLICATED_OWNER" ; FAILWITH } } ;
                                DUP 3 ;
                                CDR ;
                                CDR ;
                                DUP 5 ;
                                GET 11 ;
                                DUP 2 ;
                                DUP 2 ;
                                GET ;
                                IF_NONE
                                  { DROP 2 ; PUSH string "TOKEN NOT FOUND" ; FAILWITH }
                                  { PUSH nat 1 ; ADD ; DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } ;
                                DUP 4 ;
                                CDR ;
                                CAR ;
                                DUP 6 ;
                                GET 12 ;
                                DUP 7 ;
                                GET 11 ;
                                PAIR ;
                                PAIR ;
                                DUP 8 ;
                                SWAP ;
                                EXEC ;
                                DUP 5 ;
                                CDR ;
                                DUP 6 ;
                                CAR ;
                                CDR ;
                                DIG 6 ;
                                CAR ;
                                CAR ;
                                DIG 5 ;
                                SOME ;
                                DUP 8 ;
                                GET 11 ;
                                DIG 8 ;
                                GET 9 ;
                                PAIR ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                DUP ;
                                CDR ;
                                CDR ;
                                DIG 2 ;
                                PAIR ;
                                SWAP ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                DUP 2 ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                PAIR } ;
                         DIG 2 ;
                         DIG 3 ;
                         DIG 4 ;
                         DIG 5 ;
                         DIG 6 ;
                         DROP 5 ;
                         UNPAIR }
                       { DIG 4 ;
                         DIG 5 ;
                         DIG 6 ;
                         DIG 7 ;
                         DIG 8 ;
                         DROP 5 ;
                         DUG 2 ;
                         PAIR ;
                         SWAP ;
                         ITER { SWAP ;
                                UNPAIR ;
                                DUP 3 ;
                                GET 3 ;
                                DUP 3 ;
                                CDR ;
                                CAR ;
                                DUP 2 ;
                                GET ;
                                IF_NONE
                                  { DUP 3 ;
                                    CDR ;
                                    DUP 4 ;
                                    CAR ;
                                    CDR ;
                                    DUP 5 ;
                                    CAR ;
                                    CAR ;
                                    PUSH nat 1 ;
                                    DUP 5 ;
                                    DUP 9 ;
                                    CAR ;
                                    PAIR ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR ;
                                    PAIR ;
                                    DUP ;
                                    CDR ;
                                    CDR ;
                                    DUP 5 ;
                                    CDR ;
                                    CAR ;
                                    EMPTY_MAP string bytes ;
                                    DUP 8 ;
                                    GET 4 ;
                                    SOME ;
                                    PUSH string "" ;
                                    UPDATE ;
                                    DUP 5 ;
                                    PAIR ;
                                    DUP 5 ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR ;
                                    SWAP ;
                                    CAR ;
                                    PAIR ;
                                    DIG 3 ;
                                    CDR ;
                                    CDR ;
                                    PUSH nat 1 ;
                                    DUP 4 ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    DUP 2 ;
                                    CDR ;
                                    CAR ;
                                    PAIR ;
                                    SWAP ;
                                    CAR ;
                                    PAIR ;
                                    DIG 2 ;
                                    PUSH bool False ;
                                    DIG 4 ;
                                    CAR ;
                                    PUSH nat 1 ;
                                    PAIR ;
                                    PAIR ;
                                    DIG 3 ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR }
                                  { DROP 5 ; PUSH string "TOKEN_HAS_ALREADY_EXISTED" ; FAILWITH } } ;
                         UNPAIR } }
                   { DUG 2 ;
                     PAIR ;
                     SWAP ;
                     ITER { SWAP ;
                            UNPAIR ;
                            DUP 3 ;
                            CAR ;
                            DUP 4 ;
                            GET 3 ;
                            PAIR ;
                            DUP 10 ;
                            SWAP ;
                            EXEC ;
                            DROP ;
                            DUP 3 ;
                            GET 7 ;
                            DUP 4 ;
                            GET 9 ;
                            PAIR ;
                            DUP 7 ;
                            SWAP ;
                            EXEC ;
                            DUP 4 ;
                            GET 5 ;
                            DUP 5 ;
                            GET 3 ;
                            PAIR ;
                            PAIR ;
                            DUP 9 ;
                            SWAP ;
                            EXEC ;
                            DROP ;
                            DUP 3 ;
                            GET 9 ;
                            DUP 2 ;
                            DUP 2 ;
                            GET ;
                            IF_NONE
                              { DROP 2 ; PUSH string "INVALID_POSTCARD_INFO" ; FAILWITH }
                              { DUP ;
                                DUP 6 ;
                                GET 7 ;
                                DUP 7 ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                DUP 10 ;
                                SWAP ;
                                EXEC ;
                                DROP ;
                                DUP ;
                                CDR ;
                                IF { DROP 3 ; PUSH string "TOKEN_HAS_ALREADY_STAMPED" ; FAILWITH }
                                   { DIG 2 ; PUSH bool True ; DIG 2 ; CAR ; PAIR ; SOME ; DIG 2 ; UPDATE } } ;
                            DUP 2 ;
                            CDR ;
                            CAR ;
                            DUP 4 ;
                            GET 10 ;
                            DIG 4 ;
                            GET 9 ;
                            PAIR ;
                            PAIR ;
                            DUP 5 ;
                            SWAP ;
                            EXEC ;
                            DUP 3 ;
                            CDR ;
                            CDR ;
                            SWAP ;
                            PAIR ;
                            DIG 2 ;
                            CAR ;
                            PAIR ;
                            SWAP ;
                            PAIR } ;
                     DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DIG 5 ;
                     DIG 6 ;
                     DROP 5 ;
                     UNPAIR } ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 PAIR ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 DUP ;
                 CDR ;
                 DUP 2 ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 5 ;
                 DIG 6 ;
                 DROP 5 ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP 2 ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { DUP 2 ;
                     CAR ;
                     DUP 3 ;
                     CDR ;
                     SIZE ;
                     COMPARE ;
                     GE ;
                     IF { PUSH string "OVER_MAX_TRUSTEE_AMOUNT" ; FAILWITH } {} ;
                     DUP 2 ;
                     CDR ;
                     DUP 2 ;
                     MEM ;
                     IF { PUSH string "TRUSTEE_ALREADY_EXIST" ; FAILWITH } {} ;
                     DUP 2 ;
                     CDR ;
                     SWAP ;
                     PUSH bool True ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     CAR }
                   { DUP 2 ;
                     CDR ;
                     DUP 2 ;
                     MEM ;
                     NOT ;
                     IF { PUSH string "NOT_TRUSTEE" ; FAILWITH } {} ;
                     DUP 2 ;
                     CDR ;
                     SWAP ;
                     PUSH bool False ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     CAR } ;
                 PAIR ;
                 SWAP ;
                 CAR } ;
             PAIR ;
             NIL operation } ;
         PAIR } }
