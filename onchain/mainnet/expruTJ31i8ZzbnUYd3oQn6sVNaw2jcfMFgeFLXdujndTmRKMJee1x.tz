{ storage
    (pair (pair (pair (address %administrator) (nat %all_tokens))
                (pair (big_map %ledger nat address) (big_map %metadata string bytes)))
          (pair (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (option %protocol_address address))
                (pair (bool %protocol_for_life)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %mint
                   (pair (option %to_ address) (pair (nat %token_id) (map %token_info string bytes)))))
            (or (pair %mutez_transfer (mutez %amount) (address %destination))
                (address %set_administrator)))
        (or (or (option %set_protocol_address address)
                (pair %token_metadata
                   (list %token_ids nat)
                   (lambda %handler (list (pair (nat %token_id) (map %token_info string bytes))) unit)))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           CAR ;
                           GET 3 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP ;
                           CAR ;
                           DUP 4 ;
                           CAR ;
                           GET 3 ;
                           DUP 3 ;
                           CDR ;
                           GET ;
                           IF_NONE { PUSH int 398 ; FAILWITH } {} ;
                           COMPARE ;
                           EQ ;
                           IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                           PUSH nat 1 ;
                           SWAP ;
                           PAIR } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP 3 ;
                            CAR ;
                            GET 3 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 3 ;
                            MEM ;
                            IF { PUSH string "FA2_CANNOT_MINT_SAME_TOKEN_TWICE" ; FAILWITH } {} ;
                            DIG 2 ;
                            DUP ;
                            GET 6 ;
                            DIG 2 ;
                            DUP ;
                            GET 4 ;
                            SWAP ;
                            DUP ;
                            DUG 4 ;
                            GET 3 ;
                            PAIR ;
                            SOME ;
                            DUP 4 ;
                            GET 3 ;
                            UPDATE ;
                            UPDATE 6 ;
                            DUG 2 ;
                            DUP ;
                            CAR ;
                            IF_NONE
                              { DIG 2 ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                SENDER ;
                                SOME ;
                                DIG 5 ;
                                GET 3 ;
                                UPDATE ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP }
                              { DROP ;
                                DIG 2 ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                DUP 5 ;
                                CAR ;
                                IF_NONE { PUSH int 486 ; FAILWITH } {} ;
                                SOME ;
                                DIG 5 ;
                                GET 3 ;
                                UPDATE ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP } ;
                            SWAP ;
                            UNPAIR ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            PUSH nat 1 ;
                            ADD ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            PAIR ;
                            SWAP } ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH int 329 ; FAILWITH } ;
                     DUP ;
                     CDR ;
                     CONTRACT unit ;
                     IF_NONE { PUSH int 332 ; FAILWITH } {} ;
                     NIL operation ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation } } }
           { IF_LEFT
               { IF_LEFT
                   { PUSH bool False ;
                     DUP 3 ;
                     GET 5 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH int 450 ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PUSH bool True ;
                     UPDATE 5 }
                   { DUP ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     MAP { DUP 4 ; GET 6 ; SWAP ; GET ; IF_NONE { PUSH int 504 ; FAILWITH } {} } ;
                     DIG 2 ;
                     DROP ;
                     EXEC ;
                     DROP } }
               { IF_LEFT
                   { DUP ;
                     ITER { DUP 3 ;
                            GET 3 ;
                            CDR ;
                            SENDER ;
                            SOME ;
                            COMPARE ;
                            EQ ;
                            IF {}
                               { DUP ;
                                 CAR ;
                                 SENDER ;
                                 COMPARE ;
                                 EQ ;
                                 IF {} { PUSH int 385 ; FAILWITH } } ;
                            DUP ;
                            CDR ;
                            ITER { SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CAR ;
                                   DUP 5 ;
                                   CAR ;
                                   GET 3 ;
                                   DUP 3 ;
                                   GET 3 ;
                                   GET ;
                                   IF_NONE { PUSH int 388 ; FAILWITH } {} ;
                                   COMPARE ;
                                   EQ ;
                                   IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                   DIG 3 ;
                                   UNPAIR ;
                                   UNPAIR ;
                                   SWAP ;
                                   UNPAIR ;
                                   DUP 5 ;
                                   CAR ;
                                   SOME ;
                                   DIG 5 ;
                                   GET 3 ;
                                   UPDATE ;
                                   PAIR ;
                                   SWAP ;
                                   PAIR ;
                                   PAIR ;
                                   DUG 2 } ;
                            DROP } ;
                     DROP }
                   { PUSH string "FA2_OPERATORS_UNSUPPORTED" ; FAILWITH } } ;
             NIL operation } ;
         PAIR } }
