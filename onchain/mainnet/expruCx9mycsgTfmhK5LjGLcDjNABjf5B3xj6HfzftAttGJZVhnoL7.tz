{ parameter
    (or (or (or (address %add_admin) (string %admin_set_battledata_fix))
            (or (pair %admin_set_hero_attributes
                   (map %attributes string int)
                   (pair %uid address nat))
                (pair %admin_set_hero_owner (address %owner) (pair %uid address nat))))
        (or (or (pair %admin_set_player_data
                   (pair (address %address) (nat %experience_total))
                   (pair (nat %experience_unspent) (map %meta string bytes)))
                (unit %default))
            (or (address %del_admin)
                (or (pair %send (mutez %amount) (address %receiver))
                    (address %set_datastore_proxy_address))))) ;
  storage
    (pair (pair (set %admins address) (address %datastore_address))
          (pair (address %datastore_proxy_address) (big_map %metadata string bytes))) ;
  code { LAMBDA
           (pair unit (pair (pair (set address) address) (pair address (big_map string bytes))))
           (pair unit (pair (pair (set address) address) (pair address (big_map string bytes))))
           { CDR ;
             DUP ;
             CAR ;
             CAR ;
             SENDER ;
             MEM ;
             IF {} { PUSH string "Only admin can call this entrypoint" ; FAILWITH } ;
             UNIT ;
             PAIR } ;
         SWAP ;
         LAMBDA
           (pair string
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           (pair (pair (pair (pair (pair string timestamp) (pair (pair address nat) nat))
                             (pair (pair (pair address nat) nat) (pair nat (pair (option timestamp) bool))))
                       (pair (pair (pair (option (pair address nat)) mutez) (pair string bool))
                             (pair (pair (option timestamp) bool)
                                   (pair (pair address nat)
                                         (pair (pair (option timestamp) (list (pair nat (pair (pair address nat) timestamp))))
                                               (option (pair address nat)))))))
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           { UNPAIR 3 ;
             SWAP ;
             DUP 3 ;
             CAR ;
             CDR ;
             DIG 2 ;
             VIEW "get_battle"
                  (pair (pair (pair (pair (string %bid) (timestamp %challenge_time))
                                    (pair (pair %challenged address nat) (nat %challenged_damage)))
                              (pair (pair (pair %challenger address nat) (nat %challenger_damage))
                                    (pair (nat %experience_gained)
                                          (pair (option %finish_time timestamp) (bool %finished)))))
                        (pair (pair (pair (option %looser (pair address nat)) (mutez %loot))
                                    (pair (string %mode) (bool %resolved)))
                              (pair (pair (option %start_time timestamp) (bool %started))
                                    (pair (pair %turn address nat)
                                          (pair (pair %turns
                                                   (option %latest timestamp)
                                                   (list %turns
                                                      (pair (nat %damage) (pair (pair %hero address nat) (timestamp %timestamp)))))
                                                (option %victor (pair address nat))))))) ;
             IF_NONE { PUSH string "Invalid view get_battle" ; FAILWITH } {} ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair (pair address nat)
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           (pair (pair (pair (pair (map string int) bool)
                             (pair (pair bool (pair string string))
                                   (pair (list (pair (pair (set address) (map string (map string int))) (pair string (pair address nat))))
                                         (map string bytes))))
                       (pair (pair address bool) (pair timestamp (pair address nat))))
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           { UNPAIR 3 ;
             SWAP ;
             DUP 3 ;
             GET 3 ;
             DIG 2 ;
             VIEW "get_hero"
                  (pair (pair (pair (map %attrs string int) (bool %battling))
                              (pair (pair %character (bool %battle_ready) (pair (string %name) (string %story)))
                                    (pair (list %heroItems
                                             (pair (pair (set %collections address) (map %mods string (map string int)))
                                                   (pair (string %name) (pair (address %token_address) (nat %token_id)))))
                                          (map %meta string bytes))))
                        (pair (pair (address %owner) (bool %suited))
                              (pair (timestamp %summoned) (pair (address %token_address) (nat %token_id))))) ;
             IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair address
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           (pair (pair (pair address nat) (pair nat (map string bytes)))
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           { UNPAIR 3 ;
             SWAP ;
             DUP 3 ;
             GET 3 ;
             DIG 2 ;
             VIEW "get_player_data"
                  (pair (pair (address %address) (nat %experience_total))
                        (pair (nat %experience_unspent) (map %meta string bytes))) ;
             IF_NONE { PUSH string "Invalid view get_player_data" ; FAILWITH } {} ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair (pair (pair (pair (pair string timestamp) (pair address address))
                             (pair (pair nat (option timestamp))
                                   (pair bool (pair (map (pair address nat) (pair (map string int) address)) mutez))))
                       (pair (pair (pair (option address) (map string bytes))
                                   (pair string (pair bool (option timestamp))))
                             (pair (pair bool bool)
                                   (pair address
                                         (pair (pair (option timestamp)
                                                     (list (pair (pair string (pair address nat))
                                                                 (pair (option
                                                                          (pair (pair (set address) (map string (map string int))) (pair string (pair address nat))))
                                                                       (pair (map string int) timestamp)))))
                                               (option address))))))
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           (pair unit
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           { UNPAIR 3 ;
             SWAP ;
             DUP 3 ;
             GET 3 ;
             CONTRACT %set_battle
               (pair (pair (pair (pair (string %bid) (timestamp %challenge_time))
                                 (pair (address %challenged) (address %challenger)))
                           (pair (pair (nat %experience_gained) (option %finish_time timestamp))
                                 (pair (bool %finished)
                                       (pair (map %heroes (pair address nat) (pair (map %attrs string int) (address %player)))
                                             (mutez %loot)))))
                     (pair (pair (pair (option %loser address) (map %meta string bytes))
                                 (pair (string %mode) (pair (bool %resolved) (option %start_time timestamp))))
                           (pair (pair (bool %started) (bool %tournament_battle))
                                 (pair (address %turn)
                                       (pair (pair %turns
                                                (option %latest timestamp)
                                                (list %turns
                                                   (pair (pair (string %action) (pair %hero address nat))
                                                         (pair (option %item_
                                                                  (pair (pair (set %collections address) (map %mods string (map string int)))
                                                                        (pair (string %name) (pair (address %token_address) (nat %token_id)))))
                                                               (pair (map %mods string int) (timestamp %timestamp))))))
                                             (option %victor address)))))) ;
             IF_NONE { PUSH int 96 ; FAILWITH } {} ;
             PUSH mutez 0 ;
             DIG 3 ;
             TRANSFER_TOKENS ;
             CONS ;
             UNIT ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair (pair (pair (pair (map string int) bool)
                             (pair (pair bool (pair string string))
                                   (pair (list (pair (pair (set address) (map string (map string int))) (pair string (pair address nat))))
                                         (map string bytes))))
                       (pair (pair address bool) (pair timestamp (pair address nat))))
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           (pair unit
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           { UNPAIR 3 ;
             SWAP ;
             DUP 3 ;
             GET 3 ;
             CONTRACT %set_hero
               (pair (pair (pair (map %attrs string int) (bool %battling))
                           (pair (pair %character (bool %battle_ready) (pair (string %name) (string %story)))
                                 (pair (list %heroItems
                                          (pair (pair (set %collections address) (map %mods string (map string int)))
                                                (pair (string %name) (pair (address %token_address) (nat %token_id)))))
                                       (map %meta string bytes))))
                     (pair (pair (address %owner) (bool %suited))
                           (pair (timestamp %summoned) (pair (address %token_address) (nat %token_id))))) ;
             IF_NONE { PUSH int 70 ; FAILWITH } {} ;
             PUSH mutez 0 ;
             DIG 3 ;
             TRANSFER_TOKENS ;
             CONS ;
             UNIT ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair (pair (pair address nat) (pair nat (map string bytes)))
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           (pair unit
                 (pair (list operation)
                       (pair (pair (set address) address) (pair address (big_map string bytes)))))
           { UNPAIR 3 ;
             SWAP ;
             DUP 3 ;
             GET 3 ;
             CONTRACT %set_player_data
               (pair (pair (address %address) (nat %experience_total))
                     (pair (nat %experience_unspent) (map %meta string bytes))) ;
             IF_NONE { PUSH int 88 ; FAILWITH } {} ;
             PUSH mutez 0 ;
             DIG 3 ;
             TRANSFER_TOKENS ;
             CONS ;
             UNIT ;
             PAIR 3 } ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH bool True ;
                     DIG 4 ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 5 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     DUG 4 ;
                     DUG 4 ;
                     DIG 3 ;
                     DIG 4 ;
                     NIL operation ;
                     DIG 5 ;
                     DIG 2 ;
                     SWAP ;
                     DUG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     UNPAIR 3 ;
                     DUP 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     CAR ;
                     SWAP ;
                     DIG 4 ;
                     DIG 4 ;
                     DIG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     UNPAIR 3 ;
                     DIG 5 ;
                     DIG 5 ;
                     DIG 4 ;
                     DIG 4 ;
                     DIG 5 ;
                     DIG 5 ;
                     DIG 5 ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     GET 3 ;
                     SWAP ;
                     DIG 5 ;
                     DIG 5 ;
                     DIG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     UNPAIR 3 ;
                     DIG 5 ;
                     DIG 3 ;
                     DIG 3 ;
                     DIG 5 ;
                     DIG 5 ;
                     DIG 5 ;
                     EMPTY_MAP (pair address nat) (pair (map string int) address) ;
                     DIG 2 ;
                     DUP ;
                     GET 3 ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 4 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     SOME ;
                     DUP 5 ;
                     CAR ;
                     GET 3 ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 3 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     SOME ;
                     DUP 5 ;
                     CAR ;
                     CAR ;
                     GET 3 ;
                     UPDATE ;
                     DIG 6 ;
                     PUSH (pair (pair (option timestamp)
                                      (list (pair (pair string (pair address nat))
                                                  (pair (option
                                                           (pair (pair (set address) (map string (map string int))) (pair string (pair address nat))))
                                                        (pair (map string int) timestamp)))))
                                (option address))
                          (Pair (Pair (Some 1571659294) {}) None) ;
                     DUP 5 ;
                     GET 3 ;
                     CAR ;
                     PAIR ;
                     PUSH bool False ;
                     DUP 7 ;
                     GET 5 ;
                     CDR ;
                     PAIR ;
                     PAIR ;
                     DIG 5 ;
                     DUP ;
                     GET 5 ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 7 ;
                     GET 3 ;
                     GET 4 ;
                     PAIR ;
                     DUP 7 ;
                     GET 3 ;
                     GET 3 ;
                     PAIR ;
                     PUSH (pair (option address) (map string bytes)) (Pair None {}) ;
                     PAIR ;
                     PAIR ;
                     DUP 6 ;
                     GET 3 ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     DUP 6 ;
                     CAR ;
                     GET 8 ;
                     PAIR ;
                     DIG 5 ;
                     DUP ;
                     CAR ;
                     GET 7 ;
                     SWAP ;
                     DUP ;
                     DUG 7 ;
                     CAR ;
                     GET 5 ;
                     PAIR ;
                     PAIR ;
                     DIG 4 ;
                     GET 3 ;
                     CAR ;
                     DIG 4 ;
                     GET 3 ;
                     CAR ;
                     PAIR ;
                     DIG 4 ;
                     DUP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     DUG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     CDR ;
                     UNPAIR } }
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     DIG 3 ;
                     DIG 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     NIL operation ;
                     DIG 4 ;
                     DUP 3 ;
                     CDR ;
                     SWAP ;
                     DIG 4 ;
                     DIG 3 ;
                     DIG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     UNPAIR 3 ;
                     DIG 4 ;
                     DIG 3 ;
                     DUG 4 ;
                     DUG 4 ;
                     UNPAIR ;
                     UNPAIR ;
                     CDR ;
                     DIG 4 ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     PAIR 3 ;
                     EXEC ;
                     CDR ;
                     UNPAIR }
                   { DIG 2 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     DIG 3 ;
                     DIG 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     NIL operation ;
                     DIG 4 ;
                     DUP 3 ;
                     CDR ;
                     SWAP ;
                     DIG 4 ;
                     DIG 3 ;
                     DIG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     UNPAIR 3 ;
                     DIG 4 ;
                     DIG 3 ;
                     DUG 4 ;
                     DUG 4 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 4 ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR 3 ;
                     EXEC ;
                     CDR ;
                     UNPAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     DIG 3 ;
                     DIG 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     NIL operation ;
                     DIG 4 ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     DIG 4 ;
                     DIG 3 ;
                     DIG 3 ;
                     PAIR 3 ;
                     EXEC ;
                     UNPAIR 3 ;
                     DIG 4 ;
                     DIG 3 ;
                     DUG 4 ;
                     DUG 4 ;
                     DIG 2 ;
                     SWAP ;
                     GET 4 ;
                     UPDATE 4 ;
                     PAIR 3 ;
                     EXEC ;
                     CDR ;
                     UNPAIR }
                   { DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH bool False ;
                     DIG 4 ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 48 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         UPDATE 3 ;
                         NIL operation } } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
