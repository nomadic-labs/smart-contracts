{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %mint
                   (pair (nat %token_id) (nat %amount) (address %address) (map %metadata string bytes))))
            (or (address %set_administrator)
                (list %transfer
                   (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))))
        (list %update_operators
           (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
               (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))) ;
  storage
    (pair (pair (pair (address %administrator) (big_map %ledger (pair nat address) nat))
                (nat %max_amount)
                (nat %max_tokens))
          (pair (big_map %operators (pair nat address) (set address)) (nat %token_count))
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
          (map %total_supply nat nat)) ;
  code { PUSH string "FA2_TOKEN_UNDEFINED" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         DIG 2 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DIG 2 ;
                 DROP ;
                 IF_LEFT
                   { DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           CAR ;
                           CAR ;
                           CDR ;
                           DUP 2 ;
                           CAR ;
                           DUP 3 ;
                           CDR ;
                           PAIR ;
                           GET ;
                           IF_NONE
                             { DUP 3 ;
                               CDR ;
                               CAR ;
                               CDR ;
                               DUP 2 ;
                               CDR ;
                               COMPARE ;
                               GE ;
                               IF { DROP ; DUP 3 ; FAILWITH } { PUSH nat 0 ; SWAP ; PAIR } }
                             { SWAP ; PAIR } } ;
                     DIG 3 ;
                     DROP ;
                     SWAP ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS }
                   { DIG 2 ;
                     DROP ;
                     ITER { SWAP ;
                            SENDER ;
                            PUSH nat 0 ;
                            DUP 4 ;
                            GET 3 ;
                            COMPARE ;
                            EQ ;
                            IF { DIG 2 ; DROP 2 }
                               { DUP 2 ;
                                 CAR ;
                                 CAR ;
                                 CAR ;
                                 SWAP ;
                                 COMPARE ;
                                 NEQ ;
                                 IF { DROP 2 ; PUSH string "SENDER_NOT_ADMIN" ; FAILWITH }
                                    { DUP ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      DUP 3 ;
                                      CAR ;
                                      COMPARE ;
                                      LT ;
                                      IF { DROP 2 ; PUSH string "TOKEN_EXISTS" ; FAILWITH }
                                         { DUP ;
                                           CDR ;
                                           CAR ;
                                           CDR ;
                                           DUP 3 ;
                                           CAR ;
                                           COMPARE ;
                                           GT ;
                                           IF { DROP 2 ; PUSH string "TOKEN_ID_TOO_HIGH" ; FAILWITH }
                                              { DUP ;
                                                CAR ;
                                                CDR ;
                                                CDR ;
                                                PUSH nat 1 ;
                                                DUP 4 ;
                                                CAR ;
                                                ADD ;
                                                COMPARE ;
                                                GT ;
                                                IF { DROP 2 ; PUSH string "TOKEN_LIMIT_EXHAUSTED" ; FAILWITH }
                                                   { DUP ;
                                                     CAR ;
                                                     CDR ;
                                                     CAR ;
                                                     DUP 3 ;
                                                     GET 3 ;
                                                     COMPARE ;
                                                     GT ;
                                                     IF { SWAP ;
                                                          DROP ;
                                                          CAR ;
                                                          CDR ;
                                                          CAR ;
                                                          PUSH string "AMOUNT_EXCEEDS" ;
                                                          PAIR ;
                                                          FAILWITH }
                                                        { PUSH nat 1 ;
                                                          DUP 2 ;
                                                          CDR ;
                                                          CAR ;
                                                          CDR ;
                                                          ADD ;
                                                          DUP 2 ;
                                                          CDR ;
                                                          CDR ;
                                                          SWAP ;
                                                          DUP 3 ;
                                                          CDR ;
                                                          CAR ;
                                                          CAR ;
                                                          PAIR ;
                                                          PAIR ;
                                                          DUP 2 ;
                                                          CAR ;
                                                          PAIR ;
                                                          DUP ;
                                                          CDR ;
                                                          DUP 2 ;
                                                          CAR ;
                                                          CDR ;
                                                          DUP 4 ;
                                                          CAR ;
                                                          CAR ;
                                                          CDR ;
                                                          DUP 6 ;
                                                          GET 3 ;
                                                          SOME ;
                                                          DUP 7 ;
                                                          GET 5 ;
                                                          DUP 8 ;
                                                          CAR ;
                                                          PAIR ;
                                                          UPDATE ;
                                                          DIG 3 ;
                                                          CAR ;
                                                          CAR ;
                                                          CAR ;
                                                          PAIR ;
                                                          PAIR ;
                                                          PAIR ;
                                                          DUP ;
                                                          CDR ;
                                                          CDR ;
                                                          CDR ;
                                                          DUP 3 ;
                                                          CDR ;
                                                          CDR ;
                                                          CAR ;
                                                          DUP 5 ;
                                                          GET 6 ;
                                                          DUP 6 ;
                                                          CAR ;
                                                          PAIR ;
                                                          DUP 6 ;
                                                          CAR ;
                                                          SWAP ;
                                                          SOME ;
                                                          SWAP ;
                                                          UPDATE ;
                                                          PAIR ;
                                                          DUP 2 ;
                                                          CDR ;
                                                          CAR ;
                                                          PAIR ;
                                                          SWAP ;
                                                          CAR ;
                                                          PAIR ;
                                                          SWAP ;
                                                          CDR ;
                                                          CDR ;
                                                          CDR ;
                                                          DUP 3 ;
                                                          GET 3 ;
                                                          SOME ;
                                                          DIG 3 ;
                                                          CAR ;
                                                          UPDATE ;
                                                          DUP 2 ;
                                                          CDR ;
                                                          CDR ;
                                                          CAR ;
                                                          PAIR ;
                                                          DUP 2 ;
                                                          CDR ;
                                                          CAR ;
                                                          PAIR ;
                                                          SWAP ;
                                                          CAR ;
                                                          PAIR } } } } } } } ;
                     NIL operation } ;
                 PAIR }
               { IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DROP 2 ;
                     DUP 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ; PUSH string "SENDER_NOT_ADMIN" ; FAILWITH }
                        { DUP 2 ;
                          CDR ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          DIG 3 ;
                          CAR ;
                          CAR ;
                          CDR ;
                          DIG 3 ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          NIL operation ;
                          PAIR } }
                   { DUP 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DUP 3 ;
                     CDR ;
                     CAR ;
                     CAR ;
                     DUP 4 ;
                     DIG 3 ;
                     ITER { SWAP ;
                            DUP 2 ;
                            CDR ;
                            ITER { SWAP ;
                                   DUP 2 ;
                                   CAR ;
                                   DUP 4 ;
                                   CAR ;
                                   COMPARE ;
                                   EQ ;
                                   PUSH nat 0 ;
                                   DUP 4 ;
                                   GET 4 ;
                                   COMPARE ;
                                   EQ ;
                                   OR ;
                                   IF { SWAP ; DROP }
                                      { DUP 5 ;
                                        DUP 4 ;
                                        CAR ;
                                        DUP 4 ;
                                        GET 3 ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE
                                          { CDR ;
                                            CDR ;
                                            CAR ;
                                            SWAP ;
                                            GET 3 ;
                                            GET ;
                                            IF_NONE { DUP 6 ; FAILWITH } { DROP ; DUP 5 ; FAILWITH } }
                                          { DUP 3 ;
                                            GET 4 ;
                                            DUP 2 ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE
                                              { DROP 3 ; DUP 5 ; FAILWITH }
                                              { DUP 6 ;
                                                DUP 5 ;
                                                GET 3 ;
                                                SENDER ;
                                                PAIR ;
                                                DUP 7 ;
                                                CAR ;
                                                DUP 11 ;
                                                CAR ;
                                                CAR ;
                                                CAR ;
                                                DIG 2 ;
                                                UNPAIR ;
                                                DIG 2 ;
                                                DUP 2 ;
                                                COMPARE ;
                                                EQ ;
                                                DUP 4 ;
                                                DUP 3 ;
                                                COMPARE ;
                                                EQ ;
                                                OR ;
                                                IF { DROP 4 }
                                                   { DUG 3 ;
                                                     PAIR ;
                                                     GET ;
                                                     IF_NONE { DROP ; PUSH bool False } { SWAP ; MEM } ;
                                                     IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } ;
                                                DUP 7 ;
                                                DUP 8 ;
                                                DUP 6 ;
                                                CAR ;
                                                DUP 7 ;
                                                GET 3 ;
                                                PAIR ;
                                                GET ;
                                                IF_NONE { PUSH nat 0 } {} ;
                                                DUP 6 ;
                                                GET 4 ;
                                                ADD ;
                                                SOME ;
                                                DUP 6 ;
                                                CAR ;
                                                DUP 7 ;
                                                GET 3 ;
                                                PAIR ;
                                                UPDATE ;
                                                DUP 4 ;
                                                CDR ;
                                                DUP 5 ;
                                                CAR ;
                                                CDR ;
                                                DUP 7 ;
                                                GET 4 ;
                                                DIG 5 ;
                                                COMPARE ;
                                                EQ ;
                                                IF { DIG 3 ;
                                                     DROP ;
                                                     DIG 2 ;
                                                     NONE nat ;
                                                     DUP 7 ;
                                                     CAR ;
                                                     DUP 7 ;
                                                     GET 3 ;
                                                     PAIR ;
                                                     UPDATE }
                                                   { DIG 2 ; DIG 3 ; SOME ; DUP 7 ; CAR ; DUP 7 ; GET 3 ; PAIR ; UPDATE } ;
                                                DIG 3 ;
                                                CAR ;
                                                CAR ;
                                                CAR ;
                                                PAIR ;
                                                PAIR ;
                                                PAIR ;
                                                DUP ;
                                                CDR ;
                                                CDR ;
                                                DUP 2 ;
                                                CDR ;
                                                CAR ;
                                                CDR ;
                                                DUP 6 ;
                                                NONE (set address) ;
                                                DUP 7 ;
                                                CAR ;
                                                DIG 6 ;
                                                GET 3 ;
                                                PAIR ;
                                                UPDATE ;
                                                PAIR ;
                                                PAIR ;
                                                SWAP ;
                                                CAR ;
                                                PAIR } } } } ;
                            SWAP ;
                            DROP } ;
                     SWAP ;
                     DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DIG 5 ;
                     DROP 5 ;
                     NIL operation ;
                     PAIR } } }
           { DIG 2 ;
             DIG 3 ;
             DROP 2 ;
             SENDER ;
             DUP 3 ;
             CDR ;
             CAR ;
             CAR ;
             DIG 2 ;
             ITER { SWAP ;
                    DUP 3 ;
                    DUP 3 ;
                    IF_LEFT {} {} ;
                    CAR ;
                    COMPARE ;
                    EQ ;
                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                    SENDER ;
                    DIG 2 ;
                    IF_LEFT
                      { DUP 3 ;
                        DUP 3 ;
                        DUP 3 ;
                        GET 4 ;
                        PAIR ;
                        GET ;
                        IF_NONE
                          { EMPTY_SET address ; DUP 2 ; GET 3 ; PUSH bool True ; SWAP ; UPDATE }
                          { DUP 2 ; GET 3 ; PUSH bool True ; SWAP ; UPDATE } ;
                        SOME ;
                        SWAP ;
                        GET 4 }
                      { DUP 3 ;
                        DUP 3 ;
                        DUP 3 ;
                        GET 4 ;
                        PAIR ;
                        GET ;
                        IF_NONE
                          { NONE (set address) }
                          { DUP 2 ; GET 3 ; PUSH bool False ; SWAP ; UPDATE ; SOME } ;
                        SWAP ;
                        GET 4 } ;
                    DIG 3 ;
                    DIG 2 ;
                    DIG 3 ;
                    DIG 3 ;
                    PAIR ;
                    UPDATE } ;
             SWAP ;
             DROP ;
             DUP 2 ;
             CDR ;
             CDR ;
             DUP 3 ;
             CDR ;
             CAR ;
             CDR ;
             DIG 2 ;
             PAIR ;
             PAIR ;
             SWAP ;
             CAR ;
             PAIR ;
             NIL operation ;
             PAIR } } }
