{ view "getNextTokenId"
       unit
       nat
       { CDR ;
         CDR ;
         CDR ;
         CDR ;
         CDR ;
         CDR ;
         CDR ;
         CDR ;
         UNPAIR ;
         SWAP ;
         DROP ;
         UNIT ;
         DUP 2 ;
         SWAP ;
         DROP ;
         DIP { DROP } } ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))
            (or (list %update_creator_operators
                   (or (pair (address %operator) (nat %token_id))
                       (pair (address %operator) (nat %token_id))))
                (list %transfer
                   (pair address (list (pair (address %to) (pair (nat %token_id) (nat %amount))))))))
        (or (or (pair %mint (address %iowner) (nat %iamount))
                (pair %burn (nat %itokenid) (pair (address %iowner) (nat %iamount))))
            (or (or (bytes %setMetadataUri)
                    (pair %setTokenMetadata (nat %iTokenId) (map %iExtras string bytes)))
                (or (unit %pause) (unit %unpause))))) ;
  storage
    (pair (address %owner)
          (bool %pause_)
          (big_map %ledger (pair address nat) nat)
          (big_map %creator_ledger nat (pair (address %c_creator) (set %c_operators address)))
          (big_map %supply_ledger nat nat)
          (big_map %operator (pair address nat address) unit)
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
          (nat %nextTokenId)
          (big_map %metadata string bytes)) ;
  code { LAMBDA
           bool
           bool
           { PUSH unit Unit ;
             DUP 2 ;
             IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
             PUSH bool True ;
             SWAP ;
             DROP ;
             DUG 1 ;
             DROP } ;
         NIL operation ;
         DIG 2 ;
         UNPAIR ;
         DIP { UNPAIR 9 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { UNPAIR ;
                     NIL operation ;
                     NIL operation ;
                     DUP 14 ;
                     ITER { CONS } ;
                     DUP 4 ;
                     AMOUNT ;
                     DUP 5 ;
                     MAP { DUP 10 ;
                           DUP 2 ;
                           CDR ;
                           DUP 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 10 ;
                                DUP 2 ;
                                CDR ;
                                DUP 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE
                                  { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                                  {} }
                              { PUSH nat 0 } ;
                           DUP 2 ;
                           PAIR ;
                           SWAP ;
                           DROP } ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     ITER { CONS } ;
                     DIP { DIG 11 ; DROP } ;
                     DUG 11 ;
                     DROP 2 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR }
                   { DUP 12 ;
                     DUP 4 ;
                     EXEC ;
                     NOT ;
                     IF { PUSH string "r0" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                NOT ;
                                IF { PUSH string "CALLER NOT OWNER" ; FAILWITH } {} ;
                                DUP 9 ;
                                DUP 2 ;
                                CAR ;
                                DUP 3 ;
                                CDR ;
                                CDR ;
                                PAIR ;
                                DUP 3 ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                MEM ;
                                IF { PUSH string "operator" ; PUSH string "KEY_EXISTS" ; PAIR ; FAILWITH }
                                   { DUP 9 ;
                                     PUSH unit Unit ;
                                     SOME ;
                                     DUP 3 ;
                                     CAR ;
                                     DUP 4 ;
                                     CDR ;
                                     CDR ;
                                     PAIR ;
                                     DUP 4 ;
                                     CDR ;
                                     CAR ;
                                     PAIR ;
                                     UPDATE ;
                                     DIP { DIG 8 ; DROP } ;
                                     DUG 8 } ;
                                DROP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                NOT ;
                                IF { PUSH string "CALLER NOT OWNER" ; FAILWITH } {} ;
                                DUP 9 ;
                                NONE unit ;
                                DUP 3 ;
                                CAR ;
                                DUP 4 ;
                                CDR ;
                                CDR ;
                                PAIR ;
                                DUP 4 ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                UPDATE ;
                                DIP { DIG 8 ; DROP } ;
                                DUG 8 ;
                                DROP } ;
                            DROP } ;
                     DROP ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR } }
               { IF_LEFT
                   { DUP 12 ;
                     DUP 4 ;
                     EXEC ;
                     NOT ;
                     IF { PUSH string "r9" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            IF_LEFT
                              { DUP 7 ;
                                DUP 2 ;
                                CDR ;
                                GET ;
                                DUP ;
                                IF_NONE
                                  { PUSH string "TOKEN_DNE" ; FAILWITH }
                                  { SENDER ;
                                    DUP 2 ;
                                    CAR ;
                                    COMPARE ;
                                    EQ ;
                                    IF { DUP 9 ;
                                         DUP 10 ;
                                         DUP 5 ;
                                         CDR ;
                                         GET ;
                                         IF_NONE
                                           { PUSH string "creator_ledger" ;
                                             PUSH string "ASSET_NOT_FOUND" ;
                                             PAIR ;
                                             FAILWITH }
                                           {} ;
                                         UNPAIR ;
                                         SWAP ;
                                         DROP ;
                                         DUP 3 ;
                                         CDR ;
                                         PUSH bool True ;
                                         DUP 7 ;
                                         CAR ;
                                         UPDATE ;
                                         SWAP ;
                                         PAIR ;
                                         SOME ;
                                         DUP 5 ;
                                         CDR ;
                                         UPDATE ;
                                         DIP { DIG 8 ; DROP } ;
                                         DUG 8 }
                                       { PUSH string "CALLER NOT CREATOR" ; FAILWITH } ;
                                    DROP } ;
                                DROP 2 }
                              { DUP 7 ;
                                DUP 2 ;
                                CDR ;
                                GET ;
                                DUP ;
                                IF_NONE
                                  { PUSH string "TOKEN_DNE" ; FAILWITH }
                                  { SENDER ;
                                    DUP 2 ;
                                    CAR ;
                                    COMPARE ;
                                    EQ ;
                                    IF { DUP 9 ;
                                         DUP 10 ;
                                         DUP 5 ;
                                         CDR ;
                                         GET ;
                                         IF_NONE
                                           { PUSH string "creator_ledger" ;
                                             PUSH string "ASSET_NOT_FOUND" ;
                                             PAIR ;
                                             FAILWITH }
                                           {} ;
                                         UNPAIR ;
                                         SWAP ;
                                         DROP ;
                                         DUP 3 ;
                                         CDR ;
                                         PUSH bool False ;
                                         DUP 7 ;
                                         CAR ;
                                         UPDATE ;
                                         SWAP ;
                                         PAIR ;
                                         SOME ;
                                         DUP 5 ;
                                         CDR ;
                                         UPDATE ;
                                         DIP { DIG 8 ; DROP } ;
                                         DUG 8 }
                                       { PUSH string "CALLER NOT CREATOR" ; FAILWITH } ;
                                    DROP } ;
                                DROP 2 } ;
                            DROP } ;
                     DROP ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR }
                   { DUP 12 ;
                     DUP 4 ;
                     EXEC ;
                     NOT ;
                     IF { PUSH string "r2" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CAR ;
                            DUP 2 ;
                            CDR ;
                            DUP ;
                            ITER { DUP 3 ;
                                   SENDER ;
                                   COMPARE ;
                                   NEQ ;
                                   IF { DUP 11 ;
                                        DUP 4 ;
                                        DUP 3 ;
                                        CDR ;
                                        CAR ;
                                        PAIR ;
                                        SENDER ;
                                        PAIR ;
                                        MEM ;
                                        NOT ;
                                        IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} }
                                      {} ;
                                   DUP ;
                                   CDR ;
                                   CAR ;
                                   DUP 9 ;
                                   DUP 2 ;
                                   DUP 6 ;
                                   PAIR ;
                                   GET ;
                                   IF_NONE
                                     { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                                     {} ;
                                   DUP ;
                                   DUP 4 ;
                                   CDR ;
                                   CDR ;
                                   COMPARE ;
                                   GT ;
                                   IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                      { DUP ;
                                        DUP 4 ;
                                        CDR ;
                                        CDR ;
                                        COMPARE ;
                                        EQ ;
                                        IF { DUP 10 ;
                                             NONE nat ;
                                             DUP 4 ;
                                             DUP 8 ;
                                             PAIR ;
                                             UPDATE ;
                                             DIP { DIG 9 ; DROP } ;
                                             DUG 9 }
                                           { DUP 10 ;
                                             DUP 3 ;
                                             DUP 7 ;
                                             PAIR ;
                                             GET ;
                                             IF_NONE
                                               { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                                               {} ;
                                             DUP 11 ;
                                             PUSH int 0 ;
                                             DUP 6 ;
                                             CDR ;
                                             CDR ;
                                             INT ;
                                             DUP 4 ;
                                             SUB ;
                                             COMPARE ;
                                             GE ;
                                             IF { DUP 5 ; CDR ; CDR ; INT ; DUP 3 ; SUB ; ABS }
                                                { PUSH string "NAT_NEG_ASSIGN" ; FAILWITH } ;
                                             SOME ;
                                             DUP 5 ;
                                             DUP 9 ;
                                             PAIR ;
                                             UPDATE ;
                                             DIP { DIG 10 ; DROP } ;
                                             DUG 10 ;
                                             DROP } } ;
                                   DUP 10 ;
                                   DUP 3 ;
                                   DUP 5 ;
                                   CAR ;
                                   PAIR ;
                                   MEM ;
                                   IF { DUP 10 ;
                                        DUP 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE
                                          { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                                          {} ;
                                        DUP 11 ;
                                        DUP 5 ;
                                        CDR ;
                                        CDR ;
                                        DUP 3 ;
                                        ADD ;
                                        SOME ;
                                        DUP 5 ;
                                        DUP 7 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 10 ; DROP } ;
                                        DUG 10 ;
                                        DROP }
                                      { DUP 10 ;
                                        DUP 4 ;
                                        CDR ;
                                        CDR ;
                                        PUSH nat 0 ;
                                        ADD ;
                                        SOME ;
                                        DUP 4 ;
                                        DUP 6 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 9 ; DROP } ;
                                        DUG 9 } ;
                                   DROP 3 } ;
                            DROP 3 } ;
                     DROP ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { UNPAIR ;
                     SWAP ;
                     DUP 13 ;
                     DUP 5 ;
                     EXEC ;
                     NOT ;
                     IF { PUSH string "r3" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                     PUSH nat 0 ;
                     DUP 2 ;
                     COMPARE ;
                     GT ;
                     NOT ;
                     IF { PUSH string "r8" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                     DUP 5 ;
                     DUP 11 ;
                     DUP 4 ;
                     PAIR ;
                     MEM ;
                     IF { PUSH string "ledger" ; PUSH string "KEY_EXISTS" ; PAIR ; FAILWITH }
                        { DUP 5 ;
                          DUP 2 ;
                          SOME ;
                          DUP 12 ;
                          DUP 5 ;
                          PAIR ;
                          UPDATE ;
                          DIP { DIG 4 ; DROP } ;
                          DUG 4 } ;
                     DUP 6 ;
                     DUP 11 ;
                     MEM ;
                     IF { PUSH string "creator_ledger" ; PUSH string "KEY_EXISTS" ; PAIR ; FAILWITH }
                        { DUP 6 ;
                          EMPTY_SET address ;
                          SENDER ;
                          PAIR ;
                          SOME ;
                          DUP 12 ;
                          UPDATE ;
                          DIP { DIG 5 ; DROP } ;
                          DUG 5 } ;
                     DUP 7 ;
                     DUP 11 ;
                     MEM ;
                     IF { PUSH string "supply_ledger" ; PUSH string "KEY_EXISTS" ; PAIR ; FAILWITH }
                        { DUP 7 ; DUP 2 ; SOME ; DUP 12 ; UPDATE ; DIP { DIG 6 ; DROP } ; DUG 6 } ;
                     PUSH nat 1 ;
                     DUP 11 ;
                     ADD ;
                     DIP { DIG 9 ; DROP } ;
                     DUG 9 ;
                     DROP 2 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                     DUP 14 ;
                     DUP 6 ;
                     EXEC ;
                     NOT ;
                     IF { PUSH string "r4" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                     DUP 6 ;
                     DUP 4 ;
                     DUP 4 ;
                     PAIR ;
                     GET ;
                     IF_NONE
                       { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                       {} ;
                     DUP 2 ;
                     DUP 2 ;
                     COMPARE ;
                     GT ;
                     IF { DUP 7 ;
                          DUP 5 ;
                          DUP 5 ;
                          PAIR ;
                          GET ;
                          IF_NONE
                            { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                            {} ;
                          DUP 8 ;
                          PUSH int 0 ;
                          DUP 5 ;
                          INT ;
                          DUP 4 ;
                          SUB ;
                          COMPARE ;
                          GE ;
                          IF { DUP 4 ; INT ; DUP 3 ; SUB ; ABS }
                             { PUSH string "NAT_NEG_ASSIGN" ; FAILWITH } ;
                          SOME ;
                          DUP 7 ;
                          DUP 7 ;
                          PAIR ;
                          UPDATE ;
                          DIP { DIG 7 ; DROP } ;
                          DUG 7 ;
                          DROP }
                        { DUP 2 ;
                          DUP 2 ;
                          COMPARE ;
                          EQ ;
                          IF { DUP 7 ;
                               NONE nat ;
                               DUP 6 ;
                               DUP 6 ;
                               PAIR ;
                               UPDATE ;
                               DIP { DIG 6 ; DROP } ;
                               DUG 6 }
                             { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } } ;
                     DUP 9 ;
                     DUP 5 ;
                     GET ;
                     IF_NONE
                       { PUSH string "supply_ledger" ;
                         PUSH string "ASSET_NOT_FOUND" ;
                         PAIR ;
                         FAILWITH }
                       {} ;
                     DUP 3 ;
                     DUP 2 ;
                     COMPARE ;
                     GT ;
                     IF { DUP 10 ;
                          DUP 6 ;
                          GET ;
                          IF_NONE
                            { PUSH string "supply_ledger" ;
                              PUSH string "ASSET_NOT_FOUND" ;
                              PAIR ;
                              FAILWITH }
                            {} ;
                          DUP 11 ;
                          PUSH int 0 ;
                          DUP 6 ;
                          INT ;
                          DUP 4 ;
                          SUB ;
                          COMPARE ;
                          GE ;
                          IF { DUP 5 ; INT ; DUP 3 ; SUB ; ABS }
                             { PUSH string "NAT_NEG_ASSIGN" ; FAILWITH } ;
                          SOME ;
                          DUP 8 ;
                          UPDATE ;
                          DIP { DIG 10 ; DROP } ;
                          DUG 10 ;
                          DROP }
                        { DUP 3 ;
                          DUP 2 ;
                          COMPARE ;
                          EQ ;
                          IF { DUP 10 ;
                               NONE nat ;
                               DUP 7 ;
                               UPDATE ;
                               DIP { DIG 9 ; DROP } ;
                               DUG 9 ;
                               DUP 12 ;
                               NONE (pair nat (map string bytes)) ;
                               DUP 7 ;
                               UPDATE ;
                               DIP { DIG 11 ; DROP } ;
                               DUG 11 ;
                               DUP 9 ;
                               NONE (pair address (set address)) ;
                               DUP 7 ;
                               UPDATE ;
                               DIP { DIG 8 ; DROP } ;
                               DUG 8 }
                             {} } ;
                     DROP 5 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR } }
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 10 ;
                         DUP 2 ;
                         SOME ;
                         PUSH string "" ;
                         UPDATE ;
                         DIP { DIG 9 ; DROP } ;
                         DUG 9 ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR }
                       { UNPAIR ;
                         SWAP ;
                         DUP 13 ;
                         DUP 5 ;
                         EXEC ;
                         NOT ;
                         IF { PUSH string "r7" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                         DUP 6 ;
                         DUP 3 ;
                         GET ;
                         DUP ;
                         IF_NONE
                           { PUSH string "TOKEN_DNE" ; FAILWITH }
                           { SENDER ;
                             DUP 2 ;
                             CAR ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH bool True }
                                { DUP ;
                                  CDR ;
                                  SENDER ;
                                  MEM ;
                                  IF { PUSH bool True } { PUSH bool False } } ;
                             IF { DUP 11 ;
                                  DUP 4 ;
                                  DUP 6 ;
                                  PAIR ;
                                  SOME ;
                                  DUP 6 ;
                                  UPDATE ;
                                  DIP { DIG 10 ; DROP } ;
                                  DUG 10 }
                                { PUSH string "INVALID_PERMISSIONS" ; FAILWITH } ;
                             DROP } ;
                         DROP 3 ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR } }
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 11 ;
                         DUP 3 ;
                         EXEC ;
                         NOT ;
                         IF { PUSH string "r5" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                         PUSH bool True ;
                         DIP { DIG 1 ; DROP } ;
                         DUG 1 ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR }
                       { DROP ;
                         DUP ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 2 ;
                         NOT ;
                         IF { PUSH string "CONTRACT_UNPAUSE" ; FAILWITH } {} ;
                         PUSH bool False ;
                         DIP { DIG 1 ; DROP } ;
                         DUG 1 ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR } } } } ;
         DIP { DROP } } }
