{ parameter
    (or (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (nat %changestate))
        (or (pair %mint (map %metadata string bytes) (address %to_))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  storage
    (pair (pair (pair (address %administrator) (big_map %ledger nat address))
                (pair (nat %maxKey) (big_map %metadata string bytes)))
          (pair (pair (nat %next_token_id)
                      (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (nat %state)
                      (pair (string %strv)
                            (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { NIL operation ;
                 DUP ;
                 DUP 3 ;
                 CDR ;
                 PUSH mutez 0 ;
                 DUP 5 ;
                 CAR ;
                 MAP { DUP 7 ;
                       GET 3 ;
                       CAR ;
                       SWAP ;
                       DUP ;
                       DUG 2 ;
                       CDR ;
                       COMPARE ;
                       LT ;
                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                       DUP ;
                       CAR ;
                       DUP 8 ;
                       CAR ;
                       CAR ;
                       CDR ;
                       DUP 3 ;
                       CDR ;
                       GET ;
                       IF_NONE { PUSH int 148 ; FAILWITH } {} ;
                       COMPARE ;
                       EQ ;
                       IF { PUSH nat 1 } { PUSH nat 0 } ;
                       SWAP ;
                       PAIR } ;
                 DIG 4 ;
                 DROP ;
                 DIG 4 ;
                 DROP ;
                 TRANSFER_TOKENS ;
                 CONS }
               { UPDATE 5 ; NIL operation } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 CAR ;
                 DIG 2 ;
                 DUP ;
                 GET 8 ;
                 DUP 4 ;
                 CAR ;
                 DUP 4 ;
                 PAIR ;
                 SOME ;
                 DUP 4 ;
                 UPDATE ;
                 UPDATE 8 ;
                 UNPAIR ;
                 UNPAIR ;
                 UNPAIR ;
                 SWAP ;
                 DIG 5 ;
                 CDR ;
                 SOME ;
                 DIG 5 ;
                 UPDATE ;
                 SWAP ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 UNPAIR ;
                 UNPAIR ;
                 PUSH nat 1 ;
                 ADD ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     PUSH nat 1 ;
                     DUP ;
                     DUP 3 ;
                     COMPARE ;
                     GT ;
                     LOOP { DIG 3 ;
                            DUP ;
                            GET 7 ;
                            PUSH string "X" ;
                            SWAP ;
                            CONCAT ;
                            UPDATE 7 ;
                            DUG 3 ;
                            PUSH nat 1 ;
                            ADD ;
                            DUP ;
                            DUP 3 ;
                            COMPARE ;
                            GT } ;
                     DROP 2 ;
                     PUSH nat 0 ;
                     DUP 3 ;
                     GET 5 ;
                     COMPARE ;
                     EQ ;
                     IF { SWAP ; PUSH string "X" ; UPDATE 7 ; SWAP } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   GET 3 ;
                                   CAR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   COMPARE ;
                                   LT ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   SENDER ;
                                   DUP 3 ;
                                   CAR ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        PUSH nat 1 ;
                                        COMPARE ;
                                        EQ ;
                                        IF { SWAP ;
                                             DUP ;
                                             DUG 2 ;
                                             CAR ;
                                             DUP 5 ;
                                             CAR ;
                                             CAR ;
                                             CDR ;
                                             DUP 3 ;
                                             GET 3 ;
                                             GET ;
                                             IF_NONE { PUSH int 98 ; FAILWITH } {} ;
                                             COMPARE ;
                                             EQ }
                                           { PUSH bool False } ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DIG 3 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 5 ;
                                        CAR ;
                                        SOME ;
                                        DIG 5 ;
                                        GET 3 ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                NONE unit ;
                                DIG 5 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
