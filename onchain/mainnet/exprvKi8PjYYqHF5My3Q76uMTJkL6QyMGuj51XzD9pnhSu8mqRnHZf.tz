{ storage
    (pair (address %owner)
          (pair (option %owner_candidate address)
                (pair (option %sales_contract address)
                      (pair (option %transfer_manager address)
                            (pair (big_map %sales
                                     (pair address (pair nat (pair address (pair int bytes))))
                                     (pair (list %sale_origin_fees (pair (address %part_account) (nat %part_value)))
                                           (pair (list %sale_payouts (pair (address %part_account) (nat %part_value)))
                                                 (pair (nat %sale_amount)
                                                       (pair (nat %sale_asset_qty)
                                                             (pair (option %sale_start timestamp)
                                                                   (pair (option %sale_end timestamp)
                                                                         (pair (nat %sale_max_fees_base_boint)
                                                                               (pair (option %sale_data_type bytes) (option %sale_data bytes))))))))))
                                  (pair (big_map %bundle_sales
                                           (pair bytes (pair address (pair int bytes)))
                                           (pair (list %bundle_sale_origin_fees (pair (address %part_account) (nat %part_value)))
                                                 (pair (list %bundle_sale_payouts (pair (address %part_account) (nat %part_value)))
                                                       (pair (nat %bundle_sale_amount)
                                                             (pair (option %bundle_sale_start timestamp)
                                                                   (pair (option %bundle_sale_end timestamp)
                                                                         (pair (nat %bundle_sale_qty)
                                                                               (pair (nat %bundle_sale_max_fees_base_boint)
                                                                                     (pair (option %bundle_sale_data_type bytes) (option %bundle_sale_data bytes))))))))))
                                        (big_map %metadata string bytes))))))) ;
  parameter
    (or (or (or (or (address %declare_ownership) (unit %claim_ownership))
                (or (address %set_sales_contract) (address %set_transfer_manager)))
            (or (or (pair %set_sale
                       (address %ss_asset_contract)
                       (pair (nat %ss_asset_token_id)
                             (pair (address %ss_seller)
                                   (pair (int %ss_sale_type)
                                         (pair (bytes %ss_sale_asset)
                                               (pair %ss_sale
                                                  (list %sale_origin_fees (pair (address %part_account) (nat %part_value)))
                                                  (pair (list %sale_payouts (pair (address %part_account) (nat %part_value)))
                                                        (pair (nat %sale_amount)
                                                              (pair (nat %sale_asset_qty)
                                                                    (pair (option %sale_start timestamp)
                                                                          (pair (option %sale_end timestamp)
                                                                                (pair (nat %sale_max_fees_base_boint)
                                                                                      (pair (option %sale_data_type bytes) (option %sale_data bytes))))))))))))))
                    (pair %set_bundle_sale
                       (bytes %sbs_bundle)
                       (pair (address %sbs_seller)
                             (pair (int %sbs_sale_type)
                                   (pair (bytes %sbs_sale_asset)
                                         (pair %sbs_sale
                                            (list %bundle_sale_origin_fees (pair (address %part_account) (nat %part_value)))
                                            (pair (list %bundle_sale_payouts (pair (address %part_account) (nat %part_value)))
                                                  (pair (nat %bundle_sale_amount)
                                                        (pair (option %bundle_sale_start timestamp)
                                                              (pair (option %bundle_sale_end timestamp)
                                                                    (pair (nat %bundle_sale_qty)
                                                                          (pair (nat %bundle_sale_max_fees_base_boint)
                                                                                (pair (option %bundle_sale_data_type bytes) (option %bundle_sale_data bytes))))))))))))))
                (or (pair %remove_sale
                       (address %rs_asset_contract)
                       (pair (nat %rs_asset_token_id)
                             (pair (address %rs_seller) (pair (int %rs_sale_type) (bytes %rs_sale_asset)))))
                    (pair %remove_bundle_sale
                       (bytes %rbs_bundle)
                       (pair (address %rbs_seller) (pair (int %rbs_sale_type) (bytes %rbs_sale_asset)))))))
        (unit %default)) ;
  code { UNPAIR ;
         DIP { UNPAIR 7 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         SOME ;
                         DIP { DIG 2 ; DROP } ;
                         DUG 2 ;
                         DROP ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR }
                       { DROP ;
                         DUP 2 ;
                         IF_NONE { PUSH string "NotFound" ; FAILWITH } {} ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 2 ;
                         IF_NONE { PUSH string "NotFound" ; FAILWITH } {} ;
                         SWAP ;
                         DROP ;
                         NONE address ;
                         DIP { DIG 1 ; DROP } ;
                         DUG 1 ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR } }
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         SOME ;
                         DIP { DIG 3 ; DROP } ;
                         DUG 3 ;
                         DROP ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR }
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         SOME ;
                         DIP { DIG 4 ; DROP } ;
                         DUG 4 ;
                         DROP ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR } } }
               { IF_LEFT
                   { IF_LEFT
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 9 ;
                         IF_NONE { PUSH string "MISSING_SALES_CONTRACT" ; FAILWITH } {} ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 11 ;
                         DUP 3 ;
                         DUP 5 ;
                         PAIR ;
                         DUP 6 ;
                         PAIR ;
                         DUP 7 ;
                         PAIR ;
                         DUP 8 ;
                         PAIR ;
                         MEM ;
                         IF { DUP 11 ;
                              DUP 2 ;
                              SOME ;
                              DUP 4 ;
                              DUP 6 ;
                              PAIR ;
                              DUP 7 ;
                              PAIR ;
                              DUP 8 ;
                              PAIR ;
                              DUP 9 ;
                              PAIR ;
                              UPDATE ;
                              DIP { DIG 10 ; DROP } ;
                              DUG 10 }
                            { DUP 11 ;
                              DUP 3 ;
                              DUP 5 ;
                              PAIR ;
                              DUP 6 ;
                              PAIR ;
                              DUP 7 ;
                              PAIR ;
                              DUP 8 ;
                              PAIR ;
                              MEM ;
                              IF { PUSH string "sales" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                                 { DUP 11 ;
                                   DUP 2 ;
                                   SOME ;
                                   DUP 4 ;
                                   DUP 6 ;
                                   PAIR ;
                                   DUP 7 ;
                                   PAIR ;
                                   DUP 8 ;
                                   PAIR ;
                                   DUP 9 ;
                                   PAIR ;
                                   UPDATE ;
                                   DIP { DIG 10 ; DROP } ;
                                   DUG 10 } } ;
                         DROP 6 ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR }
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 8 ;
                         IF_NONE { PUSH string "MISSING_SALES_CONTRACT" ; FAILWITH } {} ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 11 ;
                         DUP 3 ;
                         DUP 5 ;
                         PAIR ;
                         DUP 6 ;
                         PAIR ;
                         DUP 7 ;
                         PAIR ;
                         MEM ;
                         IF { DUP 11 ;
                              DUP 2 ;
                              SOME ;
                              DUP 4 ;
                              DUP 6 ;
                              PAIR ;
                              DUP 7 ;
                              PAIR ;
                              DUP 8 ;
                              PAIR ;
                              UPDATE ;
                              DIP { DIG 10 ; DROP } ;
                              DUG 10 }
                            { DUP 11 ;
                              DUP 3 ;
                              DUP 5 ;
                              PAIR ;
                              DUP 6 ;
                              PAIR ;
                              DUP 7 ;
                              PAIR ;
                              MEM ;
                              IF { PUSH string "bundle_sales" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                                 { DUP 11 ;
                                   DUP 2 ;
                                   SOME ;
                                   DUP 4 ;
                                   DUP 6 ;
                                   PAIR ;
                                   DUP 7 ;
                                   PAIR ;
                                   DUP 8 ;
                                   PAIR ;
                                   UPDATE ;
                                   DIP { DIG 10 ; DROP } ;
                                   DUG 10 } } ;
                         DROP 5 ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR } }
                   { IF_LEFT
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 8 ;
                         IF_NONE { PUSH string "MISSING_SALES_CONTRACT" ; FAILWITH } {} ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 10 ;
                         NONE (pair (list (pair address nat))
                                    (pair (list (pair address nat))
                                          (pair nat
                                                (pair nat
                                                      (pair (option timestamp)
                                                            (pair (option timestamp) (pair nat (pair (option bytes) (option bytes))))))))) ;
                         DUP 3 ;
                         DUP 5 ;
                         PAIR ;
                         DUP 6 ;
                         PAIR ;
                         DUP 7 ;
                         PAIR ;
                         DUP 8 ;
                         PAIR ;
                         UPDATE ;
                         DIP { DIG 9 ; DROP } ;
                         DUG 9 ;
                         DROP 5 ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR }
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 7 ;
                         IF_NONE { PUSH string "MISSING_SALES_CONTRACT" ; FAILWITH } {} ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 10 ;
                         NONE (pair (list (pair address nat))
                                    (pair (list (pair address nat))
                                          (pair nat
                                                (pair (option timestamp)
                                                      (pair (option timestamp) (pair nat (pair nat (pair (option bytes) (option bytes))))))))) ;
                         DUP 3 ;
                         DUP 5 ;
                         PAIR ;
                         DUP 6 ;
                         PAIR ;
                         DUP 7 ;
                         PAIR ;
                         UPDATE ;
                         DIP { DIG 9 ; DROP } ;
                         DUG 9 ;
                         DROP 4 ;
                         PAIR 7 ;
                         NIL operation ;
                         PAIR } } } }
           { DROP ; PAIR 7 ; NIL operation ; PAIR } } ;
  view "sale_exists"
       (pair address (pair nat (pair address (pair int bytes))))
       bool
       { UNPAIR ;
         DIP { CDR ; CDR ; CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNPAIR 5 ;
         UNIT ;
         DUP 7 ;
         DUP 7 ;
         DUP 7 ;
         PAIR ;
         DUP 6 ;
         PAIR ;
         DUP 5 ;
         PAIR ;
         DUP 4 ;
         PAIR ;
         MEM ;
         SWAP ;
         DROP ;
         DIP { DROP 6 } } ;
  view "bundle_sale_exists"
       (pair bytes (pair address (pair int bytes)))
       bool
       { UNPAIR ;
         DIP { CDR ; CDR ; CDR ; CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNPAIR 4 ;
         UNIT ;
         DUP 6 ;
         DUP 6 ;
         DUP 6 ;
         PAIR ;
         DUP 5 ;
         PAIR ;
         DUP 4 ;
         PAIR ;
         MEM ;
         SWAP ;
         DROP ;
         DIP { DROP 5 } } ;
  view "get_sale"
       (pair address (pair nat (pair address (pair int bytes))))
       (option
          (pair (list %sale_origin_fees (pair (address %part_account) (nat %part_value)))
                (pair (list %sale_payouts (pair (address %part_account) (nat %part_value)))
                      (pair (nat %sale_amount)
                            (pair (nat %sale_asset_qty)
                                  (pair (option %sale_start timestamp)
                                        (pair (option %sale_end timestamp)
                                              (pair (nat %sale_max_fees_base_boint)
                                                    (pair (option %sale_data_type bytes) (option %sale_data bytes))))))))))
       { UNPAIR ;
         DIP { CDR ; CDR ; CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNPAIR 5 ;
         UNIT ;
         DUP 7 ;
         DUP 7 ;
         DUP 7 ;
         PAIR ;
         DUP 6 ;
         PAIR ;
         DUP 5 ;
         PAIR ;
         DUP 4 ;
         PAIR ;
         MEM ;
         IF { DUP 7 ;
              DUP 7 ;
              DUP 7 ;
              PAIR ;
              DUP 6 ;
              PAIR ;
              DUP 5 ;
              PAIR ;
              DUP 4 ;
              PAIR ;
              GET ;
              IF_NONE
                { PUSH string "sales" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                {} ;
              SOME ;
              SWAP ;
              DROP }
            { NONE (pair (list (pair address nat))
                         (pair (list (pair address nat))
                               (pair nat
                                     (pair nat
                                           (pair (option timestamp)
                                                 (pair (option timestamp) (pair nat (pair (option bytes) (option bytes))))))))) ;
              SWAP ;
              DROP } ;
         DIP { DROP 6 } } ;
  view "get_bundle_sale"
       (pair bytes (pair address (pair int bytes)))
       (option
          (pair (list %bundle_sale_origin_fees (pair (address %part_account) (nat %part_value)))
                (pair (list %bundle_sale_payouts (pair (address %part_account) (nat %part_value)))
                      (pair (nat %bundle_sale_amount)
                            (pair (option %bundle_sale_start timestamp)
                                  (pair (option %bundle_sale_end timestamp)
                                        (pair (nat %bundle_sale_qty)
                                              (pair (nat %bundle_sale_max_fees_base_boint)
                                                    (pair (option %bundle_sale_data_type bytes) (option %bundle_sale_data bytes))))))))))
       { UNPAIR ;
         DIP { CDR ; CDR ; CDR ; CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNPAIR 4 ;
         UNIT ;
         DUP 6 ;
         DUP 6 ;
         DUP 6 ;
         PAIR ;
         DUP 5 ;
         PAIR ;
         DUP 4 ;
         PAIR ;
         MEM ;
         IF { DUP 6 ;
              DUP 6 ;
              DUP 6 ;
              PAIR ;
              DUP 5 ;
              PAIR ;
              DUP 4 ;
              PAIR ;
              GET ;
              IF_NONE
                { PUSH string "bundle_sales" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                {} ;
              SOME ;
              SWAP ;
              DROP }
            { NONE (pair (list (pair address nat))
                         (pair (list (pair address nat))
                               (pair nat
                                     (pair (option timestamp)
                                           (pair (option timestamp) (pair nat (pair nat (pair (option bytes) (option bytes))))))))) ;
              SWAP ;
              DROP } ;
         DIP { DROP 5 } } }
