{ parameter
    (or (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (or (list %burn (pair (nat %amount) (pair (address %from_) (nat %token_id))))
                (pair %mint (map %metadata string bytes) (address %to_))))
        (or (or (address %set_administrator)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (list %update_token_metadata (pair (map %metadata string bytes) (nat %token_id)))))) ;
  storage
    (pair (pair (address %administrator)
                (pair (nat %last_token_id) (big_map %ledger nat address)))
          (pair (big_map %metadata string bytes)
                (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { NIL operation ;
                 DUP ;
                 DUP 3 ;
                 CDR ;
                 PUSH mutez 0 ;
                 DUP 5 ;
                 CAR ;
                 MAP { DUP 7 ;
                       GET 6 ;
                       SWAP ;
                       DUP ;
                       DUG 2 ;
                       CDR ;
                       MEM ;
                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                       DUP ;
                       CAR ;
                       DUP 8 ;
                       CAR ;
                       GET 4 ;
                       DUP 3 ;
                       CDR ;
                       GET ;
                       IF_NONE { PUSH int 375 ; FAILWITH } {} ;
                       COMPARE ;
                       EQ ;
                       IF { PUSH nat 1 } { PUSH nat 0 } ;
                       SWAP ;
                       PAIR } ;
                 DIG 4 ;
                 DROP ;
                 DIG 4 ;
                 DROP ;
                 TRANSFER_TOKENS ;
                 CONS }
               { IF_LEFT
                   { DUP ;
                     ITER { DUP 3 ;
                            GET 6 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 4 ;
                            MEM ;
                            IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                            DUP ;
                            GET 3 ;
                            SENDER ;
                            COMPARE ;
                            EQ ;
                            IF { PUSH bool True }
                               { DUP 3 ;
                                 GET 5 ;
                                 SWAP ;
                                 DUP ;
                                 DUG 2 ;
                                 GET 4 ;
                                 SENDER ;
                                 DUP 4 ;
                                 GET 3 ;
                                 PAIR 3 ;
                                 MEM } ;
                            IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                            DUP ;
                            CAR ;
                            PUSH nat 0 ;
                            COMPARE ;
                            LT ;
                            IF { DUP ;
                                 CAR ;
                                 PUSH nat 1 ;
                                 COMPARE ;
                                 EQ ;
                                 IF { DUP ;
                                      GET 3 ;
                                      DUP 4 ;
                                      CAR ;
                                      GET 4 ;
                                      DUP 3 ;
                                      GET 4 ;
                                      GET ;
                                      IF_NONE { PUSH int 657 ; FAILWITH } {} ;
                                      COMPARE ;
                                      EQ }
                                    { PUSH bool False } ;
                                 IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                 DIG 2 ;
                                 UNPAIR ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 SWAP ;
                                 NONE address ;
                                 DUP 6 ;
                                 GET 4 ;
                                 UPDATE ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 PAIR ;
                                 DUP ;
                                 GET 6 ;
                                 NONE (pair nat (map string bytes)) ;
                                 DIG 3 ;
                                 GET 4 ;
                                 UPDATE ;
                                 UPDATE 6 ;
                                 SWAP }
                               { DROP } } ;
                     DROP }
                   { PUSH nat 844 ;
                     DUP 3 ;
                     CAR ;
                     GET 3 ;
                     COMPARE ;
                     LE ;
                     IF {} { PUSH string "NONE_LEFT" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     DIG 2 ;
                     DUP ;
                     GET 6 ;
                     DUP 4 ;
                     CAR ;
                     DUP 4 ;
                     PAIR ;
                     SOME ;
                     DUP 4 ;
                     UPDATE ;
                     UPDATE 6 ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DIG 5 ;
                     CDR ;
                     SOME ;
                     DIG 5 ;
                     UPDATE ;
                     SWAP ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR }
                   { DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   GET 6 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 5 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        PUSH nat 1 ;
                                        COMPARE ;
                                        EQ ;
                                        IF { SWAP ;
                                             DUP ;
                                             DUG 2 ;
                                             CAR ;
                                             DUP 5 ;
                                             CAR ;
                                             GET 4 ;
                                             DUP 3 ;
                                             GET 3 ;
                                             GET ;
                                             IF_NONE { PUSH int 404 ; FAILWITH } {} ;
                                             COMPARE ;
                                             EQ }
                                           { PUSH bool False } ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DIG 3 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 5 ;
                                        CAR ;
                                        SOME ;
                                        DIG 5 ;
                                        GET 3 ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP } }
               { IF_LEFT
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 5 ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 5 ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 5 ;
                                NONE unit ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 5 ;
                                SWAP } } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DIG 2 ;
                            DUP ;
                            GET 6 ;
                            DIG 2 ;
                            DUP ;
                            CAR ;
                            SWAP ;
                            DUP ;
                            DUG 4 ;
                            CDR ;
                            PAIR ;
                            SOME ;
                            DIG 3 ;
                            CDR ;
                            UPDATE ;
                            UPDATE 6 ;
                            SWAP } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
