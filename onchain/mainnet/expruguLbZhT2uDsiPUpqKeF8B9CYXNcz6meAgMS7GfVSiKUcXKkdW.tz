{ parameter
    (or (or (or (pair %approve address nat)
                (pair %getAllowance (pair address address) (contract nat)))
            (or (pair %getBalance address (contract nat))
                (pair %getTotalSupply unit (contract nat))))
        (or (pair %mintOrBurn (int %quantity) (address %target))
            (pair %transfer address (pair address nat)))) ;
  storage
    (pair (pair (pair (address %admin) (big_map %ledger address (pair nat (map address nat))))
                (pair (big_map %metadata string bytes)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))
          (nat %total_supply)) ;
  code { PUSH string "NotEnoughAllowance" ;
         PUSH string "BurnAmountExceedingFunds" ;
         LAMBDA
           (pair (map address nat) address)
           nat
           { UNPAIR ; SWAP ; GET ; IF_NONE { PUSH nat 0 } {} } ;
         LAMBDA
           (pair (big_map address (pair nat (map address nat))) address)
           (pair nat (map address nat))
           { UNPAIR ;
             SWAP ;
             GET ;
             IF_NONE { EMPTY_MAP address nat ; PUSH nat 0 ; PAIR } {} } ;
         LAMBDA
           (pair (big_map address (pair nat (map address nat)))
                 (pair address (pair nat (map address nat))))
           (big_map address (pair nat (map address nat)))
           { UNPAIR 4 ; DIG 3 ; DIG 3 ; PAIR ; SOME ; DIG 2 ; UPDATE } ;
         LAMBDA
           (pair (pair (pair address (big_map address (pair nat (map address nat))))
                       (pair (big_map string bytes) (big_map nat (pair nat (map string bytes)))))
                 nat)
           (big_map address (pair nat (map address nat)))
           { CAR ; CAR ; CDR } ;
         LAMBDA
           (pair (pair (pair (pair address (big_map address (pair nat (map address nat))))
                             (pair (big_map string bytes) (big_map nat (pair nat (map string bytes)))))
                       nat)
                 (big_map address (pair nat (map address nat))))
           (pair (pair (pair address (big_map address (pair nat (map address nat))))
                       (pair (big_map string bytes) (big_map nat (pair nat (map string bytes)))))
                 nat)
           { UNPAIR ;
             DUP ;
             CDR ;
             DUP 2 ;
             CAR ;
             CDR ;
             DIG 3 ;
             DIG 3 ;
             CAR ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             PAIR } ;
         DIG 7 ;
         UNPAIR ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         NEQ ;
         IF { PUSH string "TezNotAllowed" ; FAILWITH } {} ;
         IF_LEFT
           { DIG 7 ;
             DIG 8 ;
             DROP 2 ;
             IF_LEFT
               { IF_LEFT
                   { UNPAIR ;
                     DUP 3 ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     DUG 2 ;
                     SENDER ;
                     DIG 3 ;
                     DUP 2 ;
                     DUP 2 ;
                     PAIR ;
                     DIG 8 ;
                     SWAP ;
                     EXEC ;
                     UNPAIR ;
                     DUP 5 ;
                     DUP 3 ;
                     PAIR ;
                     DIG 10 ;
                     SWAP ;
                     EXEC ;
                     PUSH string "UnsafeAllowanceChange" ;
                     PUSH nat 0 ;
                     DUP 9 ;
                     COMPARE ;
                     EQ ;
                     PUSH nat 0 ;
                     DIG 3 ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     IF { DROP } { FAILWITH } ;
                     SWAP ;
                     DIG 5 ;
                     DIG 5 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     DIG 3 ;
                     DIG 3 ;
                     PAIR 4 ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     SWAP ;
                     PAIR ;
                     EXEC ;
                     NIL operation }
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DROP 3 ;
                     UNPAIR ;
                     UNPAIR ;
                     DUP 4 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     PAIR ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     CDR ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     SWAP ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS } }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 6 ;
                 DROP 4 ;
                 IF_LEFT
                   { UNPAIR ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     SWAP ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     CAR ;
                     TRANSFER_TOKENS }
                   { DIG 2 ; DROP ; CDR ; PUSH mutez 0 ; DUP 3 ; CDR ; TRANSFER_TOKENS } ;
                 SWAP ;
                 NIL operation ;
                 DIG 2 ;
                 CONS } }
           { IF_LEFT
               { DIG 2 ;
                 DIG 6 ;
                 DIG 8 ;
                 DROP 3 ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { PUSH string "OnlyAdminAllowed" ; FAILWITH } {} ;
                 DUP 2 ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DUP 2 ;
                 CDR ;
                 DUP 2 ;
                 PAIR ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 UNPAIR ;
                 DUP 4 ;
                 CAR ;
                 ADD ;
                 ISNAT ;
                 IF_NONE { DUP 6 ; FAILWITH } {} ;
                 DUP 4 ;
                 CDR ;
                 DIG 3 ;
                 PAIR 4 ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 SWAP ;
                 CAR ;
                 DUP 3 ;
                 CDR ;
                 ADD ;
                 ISNAT ;
                 IF_NONE { DIG 2 ; FAILWITH } { DIG 3 ; DROP } ;
                 DUP 3 ;
                 CAR ;
                 CDR ;
                 DIG 2 ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 PAIR }
               { DIG 7 ;
                 DROP ;
                 UNPAIR ;
                 SWAP ;
                 UNPAIR ;
                 DUP 4 ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 DUP 3 ;
                 DIG 4 ;
                 SENDER ;
                 DIG 3 ;
                 DUP 3 ;
                 DUP 2 ;
                 PAIR ;
                 DUP 11 ;
                 SWAP ;
                 EXEC ;
                 UNPAIR ;
                 DUP 5 ;
                 DUP 5 ;
                 COMPARE ;
                 EQ ;
                 IF { DIG 12 ; DROP ; DUP }
                    { DUP 4 ; DUP 3 ; PAIR ; DIG 13 ; SWAP ; EXEC } ;
                 PUSH string "NotEnoughBalance" ;
                 DUP 8 ;
                 DUP 4 ;
                 COMPARE ;
                 GE ;
                 IF { DROP } { FAILWITH } ;
                 DUP 7 ;
                 DUP 2 ;
                 COMPARE ;
                 LT ;
                 IF { DUP 7 ; PAIR ; DUP 14 ; PAIR ; FAILWITH } { DROP } ;
                 DUP 6 ;
                 DUP 2 ;
                 SUB ;
                 ISNAT ;
                 IF_NONE { DUP 6 ; PAIR ; DUP 13 ; PAIR ; FAILWITH } { SWAP ; DROP } ;
                 DUP 5 ;
                 DUP 5 ;
                 COMPARE ;
                 EQ ;
                 IF { DIG 3 ; DIG 5 ; DIG 12 ; DROP 3 ; SWAP }
                    { DUP 2 ;
                      DUP 5 ;
                      GET ;
                      IF_NONE
                        { SWAP ; DIG 3 ; DIG 5 ; DROP 3 ; DIG 9 ; FAILWITH }
                        { DIG 6 ;
                          SWAP ;
                          SUB ;
                          ISNAT ;
                          IF_NONE { DIG 11 ; FAILWITH } { DIG 12 ; DROP } ;
                          PUSH nat 0 ;
                          DUP 2 ;
                          COMPARE ;
                          GT ;
                          IF { DIG 2 ; SWAP ; SOME ; DIG 4 ; UPDATE }
                             { DROP ; SWAP ; DIG 3 ; NONE nat ; SWAP ; UPDATE } } } ;
                 SWAP ;
                 DIG 3 ;
                 DIG 3 ;
                 PAIR 4 ;
                 DUP 6 ;
                 SWAP ;
                 EXEC ;
                 DUP 2 ;
                 DUP 2 ;
                 PAIR ;
                 DIG 7 ;
                 SWAP ;
                 EXEC ;
                 UNPAIR ;
                 DIG 4 ;
                 ADD ;
                 DIG 3 ;
                 DIG 3 ;
                 PAIR 4 ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 SWAP ;
                 PAIR ;
                 EXEC } ;
             NIL operation } ;
         PAIR } }
