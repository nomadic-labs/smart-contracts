{ parameter
    (or (or (or (address %add_manager)
                (or (pair %get_tickets
                       (list address)
                       (contract (list (pair (address %address) (nat %amount)))))
                    (pair %mint (set %artifacts nat) (pair (bytes %metadata) (signature %signature)))))
            (or (list %mint_callback
                   (pair (string %category)
                         (pair (map %ints string int)
                               (pair (bool %on_sale)
                                     (pair (address %owner)
                                           (pair (map %sets string (set nat))
                                                 (pair (map %strings string string) (pair (nat %token_id) (nat %uid)))))))))
                (or (address %remove_manager) (address %set_administrator))))
        (or (or (address %set_minter) (or (bool %set_pause) (nat %set_tezotop_count)))
            (or (pair %update_builder_details
                   (address %burn_address)
                   (pair (nat %max_artifacts)
                         (pair (nat %max_multiplier)
                               (pair (nat %min_artifacts) (pair (address %minter) (list %resource_list string))))))
                (or (map %update_ledger address nat) (key %update_signer))))) ;
  storage
    (pair (address %administrator)
          (pair (address %burn_address)
                (pair (option %current_data
                         (pair (address %address)
                               (pair (set %artifacts nat) (pair (address %contract) (bytes %metadata)))))
                      (pair (set %managers address)
                            (pair (nat %max_artifacts)
                                  (pair (nat %max_multiplier)
                                        (pair (big_map %metadata string bytes)
                                              (pair (nat %min_artifacts)
                                                    (pair (address %minter)
                                                          (pair (address %nft_registry)
                                                                (pair (bool %paused)
                                                                      (pair (list %resource_list string)
                                                                            (pair (key %signer) (pair (nat %tezotops) (big_map %ticket_ledger address nat))))))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 7 ;
                     PUSH bool True ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 7 ;
                     NIL operation }
                   { IF_LEFT
                       { NIL (pair address nat) ;
                         PUSH nat 0 ;
                         DUP 3 ;
                         CAR ;
                         ITER { SWAP ;
                                DROP ;
                                DUP 4 ;
                                GET 28 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                MEM ;
                                IF { DUP 4 ;
                                     GET 28 ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     GET ;
                                     IF_NONE { PUSH int 109 ; FAILWITH } {} ;
                                     SWAP }
                                   { PUSH nat 0 ; SWAP } ;
                                DUG 2 ;
                                DUP ;
                                DUG 3 ;
                                DIG 2 ;
                                PAIR ;
                                CONS ;
                                SWAP } ;
                         DROP ;
                         NIL operation ;
                         DIG 2 ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 21 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 15 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SIZE ;
                         COMPARE ;
                         GE ;
                         IF { SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 9 ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CAR ;
                              SIZE ;
                              COMPARE ;
                              LE }
                            { PUSH bool False } ;
                         IF {} { PUSH string "INVALID_ARTIFACT_COUNT" ; FAILWITH } ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SELF_ADDRESS ;
                         DUP 4 ;
                         CAR ;
                         SENDER ;
                         PAIR 4 ;
                         SOME ;
                         UPDATE 5 ;
                         SWAP ;
                         DUP ;
                         GET 3 ;
                         SELF_ADDRESS ;
                         DUP 3 ;
                         CAR ;
                         SENDER ;
                         PAIR 4 ;
                         PACK ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 4 ;
                         DUP 4 ;
                         GET 25 ;
                         CHECK_SIGNATURE ;
                         IF {} { PUSH string "INVALID_SIGN" ; FAILWITH } ;
                         PUSH nat 0 ;
                         DUP 3 ;
                         GET 28 ;
                         SENDER ;
                         GET ;
                         IF_NONE { PUSH int 137 ; FAILWITH } {} ;
                         COMPARE ;
                         GT ;
                         IF {} { PUSH string "INSUFFICIENT_TICKETS" ; FAILWITH } ;
                         NIL operation ;
                         DUP 3 ;
                         GET 19 ;
                         CONTRACT %get_attributes
                           (pair (list nat)
                                 (contract
                                    (list (pair string
                                                (pair (map string int)
                                                      (pair bool
                                                            (pair address (pair (map string (set nat)) (pair (map string string) (pair nat nat)))))))))) ;
                         IF_NONE { PUSH int 138 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         SELF %mint_callback ;
                         NIL nat ;
                         DIG 5 ;
                         CAR ;
                         ITER { CONS } ;
                         NIL nat ;
                         SWAP ;
                         ITER { CONS } ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 19 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_NFT_REGISTRY" ; FAILWITH } ;
                     EMPTY_MAP string int ;
                     NIL (pair address (pair nat nat)) ;
                     PUSH nat 2527 ;
                     DUP 5 ;
                     GET 27 ;
                     COMPARE ;
                     LT ;
                     IF { DUP 4 ;
                          GET 23 ;
                          ITER { DIG 2 ; PUSH (option int) (Some 1) ; DIG 2 ; UPDATE ; SWAP } }
                        { DUP 4 ;
                          GET 23 ;
                          ITER { DIG 2 ; PUSH (option int) (Some 0) ; DIG 2 ; UPDATE ; SWAP } } ;
                     DUP 3 ;
                     ITER { DUP ;
                            CAR ;
                            PUSH string "artifact" ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "NOT_AN_ARTIFACT" ; FAILWITH } ;
                            DUP 5 ;
                            GET 5 ;
                            IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                            CAR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 7 ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "NOT_OWNED_BY_USER" ; FAILWITH } ;
                            DUP 5 ;
                            GET 5 ;
                            IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                            GET 3 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 13 ;
                            MEM ;
                            IF {} { PUSH string "ARTIFACT_MISMATCH" ; FAILWITH } ;
                            SWAP ;
                            PUSH nat 1 ;
                            DUP 3 ;
                            GET 13 ;
                            DUP 7 ;
                            GET 3 ;
                            PAIR 3 ;
                            CONS ;
                            SWAP ;
                            DUP 5 ;
                            GET 23 ;
                            ITER { SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   MEM ;
                                   IF { DIG 3 ;
                                        DUP ;
                                        DUP 3 ;
                                        DUP ;
                                        DUG 2 ;
                                        GET ;
                                        IF_NONE { PUSH int 171 ; FAILWITH } {} ;
                                        DUP 5 ;
                                        GET 3 ;
                                        DIG 4 ;
                                        GET ;
                                        IF_NONE { PUSH int 171 ; FAILWITH } {} ;
                                        ADD ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DUP 4 ;
                     GET 23 ;
                     ITER { DUP 5 ;
                            GET 11 ;
                            DUP 4 ;
                            DIG 2 ;
                            GET ;
                            IF_NONE { PUSH int 173 ; FAILWITH } {} ;
                            ISNAT ;
                            IF_NONE { PUSH int 173 ; FAILWITH } {} ;
                            COMPARE ;
                            LE ;
                            IF {} { PUSH string "MULTIPLIER_EXCEEDS_LIMIT" ; FAILWITH } } ;
                     NIL operation ;
                     DUP 5 ;
                     GET 19 ;
                     CONTRACT %transfer (list (pair address (list (pair address (pair nat nat))))) ;
                     IF_NONE { PUSH int 176 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     DUP 5 ;
                     DUP 9 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     DUP ;
                     DUP 6 ;
                     GET 17 ;
                     CONTRACT %mint
                       (pair address
                             (pair (pair string (pair (map string int) (pair (map string (set nat)) (map string string))))
                                   (pair nat bytes))) ;
                     IF_NONE { PUSH int 180 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DUP 8 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     GET 6 ;
                     PUSH nat 1 ;
                     EMPTY_MAP string string ;
                     EMPTY_MAP string (set nat) ;
                     DUP 10 ;
                     PUSH string "tezotop" ;
                     PAIR 4 ;
                     DUP 11 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     PAIR 4 ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     SWAP ;
                     DUP ;
                     GET 27 ;
                     PUSH nat 1 ;
                     ADD ;
                     UPDATE 27 ;
                     DUP ;
                     DUG 2 ;
                     DUP ;
                     GET 28 ;
                     PUSH nat 1 ;
                     DIG 4 ;
                     DUP ;
                     GET 28 ;
                     SWAP ;
                     DUP ;
                     DUG 6 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH int 195 ; FAILWITH } {} ;
                     SUB ;
                     ISNAT ;
                     IF_NONE { PUSH int 195 ; FAILWITH } {} ;
                     SOME ;
                     DIG 4 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     UPDATE ;
                     UPDATE 28 ;
                     NONE (pair address (pair (set nat) (pair address bytes))) ;
                     UPDATE 5 ;
                     SWAP }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 7 ;
                         PUSH bool False ;
                         DIG 3 ;
                         UPDATE ;
                         UPDATE 7 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 1 } ;
                     NIL operation } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 17 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 21 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 27 } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     UPDATE 9 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 7 ;
                     UPDATE 15 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     UPDATE 11 ;
                     SWAP ;
                     GET 10 ;
                     UPDATE 23 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         SENDER ;
                         MEM ;
                         IF { PUSH bool True }
                            { SWAP ; DUP ; DUG 2 ; CAR ; SENDER ; COMPARE ; EQ } ;
                         IF {} { PUSH string "NOT_ADMIN_OR_MANAGER" ; FAILWITH } ;
                         DUP ;
                         ITER { CAR ;
                                DUP 3 ;
                                GET 28 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                MEM ;
                                IF { DIG 2 ;
                                     DUP ;
                                     GET 28 ;
                                     DUP ;
                                     DUP 4 ;
                                     DUP ;
                                     DUG 2 ;
                                     GET ;
                                     IF_NONE { PUSH int 98 ; FAILWITH } {} ;
                                     DUP 6 ;
                                     DIG 5 ;
                                     GET ;
                                     IF_NONE { PUSH int 98 ; FAILWITH } {} ;
                                     ADD ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     UPDATE 28 ;
                                     SWAP }
                                   { DIG 2 ;
                                     DUP ;
                                     GET 28 ;
                                     DUP 4 ;
                                     DUP 4 ;
                                     GET ;
                                     IF_NONE { PUSH int 100 ; FAILWITH } {} ;
                                     SOME ;
                                     DIG 3 ;
                                     UPDATE ;
                                     UPDATE 28 ;
                                     SWAP } } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 25 } } } ;
             NIL operation } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
