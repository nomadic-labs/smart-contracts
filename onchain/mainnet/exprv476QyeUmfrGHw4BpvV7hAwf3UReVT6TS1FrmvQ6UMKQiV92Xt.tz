{ parameter
    (or (or (or (unit %addAddressToWhitelist) (bytes %mint))
            (or (unit %ownerWithdraw) (address %setMintAddress)))
        (or (or (mutez %setMintCost) (address %setOwnerAddress))
            (bool %setWhiteListMintAvailable))) ;
  storage
    (pair (pair (pair %mintProperties
                   (pair (nat %collection_id) (nat %editions))
                   (pair (address %mintAddress) (mutez %mintCost)))
                (address %ownerAddress))
          (pair (pair %statisticProperties (nat %totalNftMinted) (nat %whitelistCounter))
                (pair %whiteListProperties
                   (pair (set %whiteList address) (bool %whiteListMintAvailable))
                   (nat %whiteListSize)))) ;
  code { { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     CDR ;
                     CDR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     CAR ;
                     SIZE ;
                     COMPARE ;
                     LE ;
                     IF {} { PUSH string "Whitelist is full." ; FAILWITH } ;
                     DUP ;
                     CDR ;
                     CDR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF { PUSH string "Your address is already added to the whitelist." ; FAILWITH }
                        {} ;
                     DUP ;
                     CDR ;
                     CDR ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     CDR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     CDR ;
                     CAR ;
                     CAR ;
                     PUSH bool True ;
                     SENDER ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     PUSH nat 1 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     CAR ;
                     CDR ;
                     ADD ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     CDR ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          CDR ;
                          CAR ;
                          CAR ;
                          SENDER ;
                          MEM ;
                          IF { PUSH unit Unit }
                             { PUSH string "Only whitelist user are able to mint" ; FAILWITH } }
                        { PUSH unit Unit } ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     CDR ;
                     AMOUNT ;
                     COMPARE ;
                     GE ;
                     IF {}
                        { PUSH string "The amount you pay is not enough to mint a NFT." ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CONTRACT %mint_artist
                       (pair (nat %collection_id)
                             (pair (nat %editions) (pair (bytes %metadata_cid) (address %target)))) ;
                     IF_NONE
                       { PUSH string "not a correct contract for mint a NFT" ; FAILWITH }
                       {} ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     SENDER ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     CAR ;
                     CDR ;
                     PUSH nat 1 ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CDR ;
                     CAR ;
                     CAR ;
                     ADD ;
                     PAIR ;
                     PAIR ;
                     DIG 2 ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR } }
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Access denied." ; FAILWITH } ;
                     SENDER ;
                     CONTRACT unit ;
                     IF_NONE { PUSH string "Address does not exist." ; FAILWITH } {} ;
                     BALANCE ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Access denied." ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     CAR ;
                     CAR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     DIG 4 ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Access denied." ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     CAR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Access denied." ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "Access denied." ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CDR ;
                 DUP ;
                 CDR ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CAR ;
                 PAIR ;
                 SWAP ;
                 CAR ;
                 PAIR ;
                 NIL operation ;
                 PAIR } } } }
