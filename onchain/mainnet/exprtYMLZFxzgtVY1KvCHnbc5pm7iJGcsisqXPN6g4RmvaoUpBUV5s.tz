{ parameter
    (or (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (or (pair %mint (map %metadata string bytes) (address %to_))
                (address %set_administrator)))
        (or (or (big_map %set_metadata string bytes)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes)))))) ;
  storage
    (pair (pair (address %administrator)
                (pair (nat %last_token_id) (big_map %ledger nat address)))
          (pair (big_map %metadata string bytes)
                (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { NIL operation ;
                 DUP ;
                 DUP 3 ;
                 CDR ;
                 PUSH mutez 0 ;
                 DUP 5 ;
                 CAR ;
                 MAP { DUP 7 ;
                       GET 6 ;
                       SWAP ;
                       DUP ;
                       DUG 2 ;
                       CDR ;
                       MEM ;
                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                       DUP ;
                       CAR ;
                       DUP 8 ;
                       CAR ;
                       GET 4 ;
                       DUP 3 ;
                       CDR ;
                       GET ;
                       IF_NONE { PUSH int 374 ; FAILWITH } {} ;
                       COMPARE ;
                       EQ ;
                       IF { PUSH nat 1 } { PUSH nat 0 } ;
                       SWAP ;
                       PAIR } ;
                 DIG 4 ;
                 DROP ;
                 DIG 4 ;
                 DROP ;
                 TRANSFER_TOKENS ;
                 CONS }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     DIG 2 ;
                     DUP ;
                     GET 6 ;
                     DUP 4 ;
                     CAR ;
                     DUP 4 ;
                     PAIR ;
                     SOME ;
                     DUP 4 ;
                     UPDATE ;
                     UPDATE 6 ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DIG 5 ;
                     CDR ;
                     SOME ;
                     DIG 5 ;
                     UPDATE ;
                     SWAP ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 3 }
                   { DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   GET 6 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 5 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        PUSH nat 1 ;
                                        COMPARE ;
                                        EQ ;
                                        IF { SWAP ;
                                             DUP ;
                                             DUG 2 ;
                                             CAR ;
                                             DUP 5 ;
                                             CAR ;
                                             GET 4 ;
                                             DUP 3 ;
                                             GET 3 ;
                                             GET ;
                                             IF_NONE { PUSH int 403 ; FAILWITH } {} ;
                                             COMPARE ;
                                             EQ }
                                           { PUSH bool False } ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DIG 3 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 5 ;
                                        CAR ;
                                        SOME ;
                                        DIG 5 ;
                                        GET 3 ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP } }
               { IF_LEFT
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 5 ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 5 ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 5 ;
                                NONE unit ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 5 ;
                                SWAP } } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_ONLY_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DIG 2 ;
                            DUP ;
                            GET 6 ;
                            DIG 2 ;
                            DUP ;
                            SOME ;
                            SWAP ;
                            CAR ;
                            UPDATE ;
                            UPDATE 6 ;
                            SWAP } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
