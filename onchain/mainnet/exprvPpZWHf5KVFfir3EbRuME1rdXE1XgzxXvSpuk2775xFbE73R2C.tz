{ storage
    (pair (pair (pair (address %administrator) (nat %all_tokens))
                (pair (bool %isMinter1Locked)
                      (pair (bool %isMinter2Locked) (big_map %ledger (pair address nat) nat))))
          (pair (pair (big_map %metadata string bytes) (pair (address %minter1) (address %minter2)))
                (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (pair (bool %paused)
                            (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (or (pair %create_token (nat %token_id) (map %token_info string bytes))
                    (unit %lockMinter1)))
            (or (or (unit %lockMinter2) (address %set_administrator))
                (or (pair %set_metadata (string %k) (bytes %v)) (address %set_minter1))))
        (or (or (address %set_minter2)
                (or (bool %set_pause)
                    (or %tokens
                       (list %burn_tokens (pair (address %owner) (pair (nat %token_id) (nat %amount))))
                       (list %mint_tokens (pair (address %owner) (pair (nat %token_id) (nat %amount)))))))
            (or (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (unit %unlockMinter1))
                (or (unit %unlockMinter2)
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (or (pair nat (map string bytes)) unit))
                            (or (or unit address) (or (pair string bytes) address)))
                        (or (or address
                                (or bool (or (list (pair address (pair nat nat))) (list (pair address (pair nat nat))))))
                            (or (or (list (pair address (list (pair address (pair nat nat))))) unit)
                                (or unit (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair (pair (pair address nat) (pair bool (pair bool (big_map (pair address nat) nat))))
                          (pair (pair (big_map string bytes) (pair address address))
                                (pair (big_map (pair address (pair address nat)) unit)
                                      (pair bool (big_map nat (pair nat (map string bytes)))))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 7 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 8 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           GET 6 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 3 ;
                                CAR ;
                                GET 6 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 412 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         COMPARE ;
                         LT ;
                         IF { PUSH string "Token already created" ; FAILWITH } {} ;
                         DUP ;
                         CAR ;
                         DUP 3 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CAR ;
                         PUSH nat 1 ;
                         DUP 5 ;
                         CAR ;
                         ADD ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         DUP ;
                         GET 8 ;
                         DIG 2 ;
                         DUP ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 4 ;
                         CAR ;
                         PAIR ;
                         SOME ;
                         DIG 3 ;
                         CAR ;
                         UPDATE ;
                         UPDATE 8 }
                       { DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         PUSH bool True ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } ;
                     NIL operation } }
               { IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         PUSH bool True ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         PAIR } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         DUP 5 ;
                         CDR ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         DIG 4 ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 4 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     DIG 4 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 7 }
                       { PUSH bool False ;
                         DUP 3 ;
                         GET 3 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP ; SWAP ; DUP ; DUG 2 ; CAR ; GET 3 ; NOT }
                            { DUP 3 ;
                              GET 3 ;
                              GET 4 ;
                              SENDER ;
                              COMPARE ;
                              EQ ;
                              IF { DROP ; SWAP ; DUP ; DUG 2 ; CAR ; GET 5 ; NOT } {} } ;
                         DUP ;
                         IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         IF_LEFT
                           { DUP ;
                             ITER { DUP 5 ;
                                    CAR ;
                                    CAR ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    GET 3 ;
                                    COMPARE ;
                                    LT ;
                                    IF {} { PUSH string "Token does not exists" ; FAILWITH } ;
                                    DUP 5 ;
                                    CAR ;
                                    GET 6 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 3 ;
                                    CAR ;
                                    PAIR ;
                                    MEM ;
                                    IF {} { PUSH string "Cannot burn more than balance" ; FAILWITH } ;
                                    DUP 5 ;
                                    CAR ;
                                    GET 6 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 3 ;
                                    CAR ;
                                    PAIR ;
                                    GET ;
                                    IF_NONE { PUSH int 620 ; FAILWITH } {} ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    GET 4 ;
                                    COMPARE ;
                                    LE ;
                                    IF {} { PUSH string "Cannot burn more than balance" ; FAILWITH } ;
                                    DUP 5 ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    DUP ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 8 ;
                                    CAR ;
                                    PAIR ;
                                    DUP ;
                                    DUG 2 ;
                                    GET ;
                                    IF_NONE { PUSH int 621 ; FAILWITH } { DROP } ;
                                    DUP 7 ;
                                    GET 4 ;
                                    DIG 11 ;
                                    CAR ;
                                    GET 6 ;
                                    DIG 8 ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR ;
                                    GET ;
                                    IF_NONE { PUSH int 621 ; FAILWITH } {} ;
                                    SUB ;
                                    ABS ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    DUG 3 } ;
                             DROP 3 }
                           { DUP ;
                             ITER { DUP 5 ;
                                    CAR ;
                                    CAR ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    GET 3 ;
                                    COMPARE ;
                                    LT ;
                                    IF {} { PUSH string "Token does not exists" ; FAILWITH } ;
                                    DUP 5 ;
                                    CAR ;
                                    GET 6 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 3 ;
                                    CAR ;
                                    PAIR ;
                                    MEM ;
                                    IF { DIG 4 ;
                                         UNPAIR ;
                                         UNPAIR ;
                                         SWAP ;
                                         UNPAIR ;
                                         SWAP ;
                                         UNPAIR ;
                                         SWAP ;
                                         DUP ;
                                         DIG 6 ;
                                         DUP ;
                                         GET 3 ;
                                         SWAP ;
                                         DUP ;
                                         DUG 8 ;
                                         CAR ;
                                         PAIR ;
                                         DUP ;
                                         DUG 2 ;
                                         GET ;
                                         IF_NONE { PUSH int 602 ; FAILWITH } {} ;
                                         DIG 7 ;
                                         GET 4 ;
                                         ADD ;
                                         SOME ;
                                         SWAP ;
                                         UPDATE ;
                                         SWAP ;
                                         PAIR ;
                                         SWAP ;
                                         PAIR ;
                                         SWAP ;
                                         PAIR ;
                                         PAIR ;
                                         DUG 3 }
                                       { DIG 4 ;
                                         UNPAIR ;
                                         UNPAIR ;
                                         SWAP ;
                                         UNPAIR ;
                                         SWAP ;
                                         UNPAIR ;
                                         SWAP ;
                                         DUP 6 ;
                                         GET 4 ;
                                         SOME ;
                                         DIG 6 ;
                                         DUP ;
                                         GET 3 ;
                                         SWAP ;
                                         CAR ;
                                         PAIR ;
                                         UPDATE ;
                                         SWAP ;
                                         PAIR ;
                                         SWAP ;
                                         PAIR ;
                                         SWAP ;
                                         PAIR ;
                                         PAIR ;
                                         DUG 3 } } ;
                             DROP 3 } } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { DUP 4 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       SENDER ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                       IF { PUSH bool True }
                                          { DUP 4 ;
                                            GET 5 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       DUP 4 ;
                                       GET 8 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            DUP 5 ;
                                            CAR ;
                                            GET 6 ;
                                            DUP 3 ;
                                            GET 3 ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 390 ; FAILWITH } {} ;
                                            COMPARE ;
                                            GE ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            DUP 4 ;
                                            UNPAIR ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            DUP ;
                                            DUP 7 ;
                                            GET 3 ;
                                            DUP 9 ;
                                            CAR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 2 ;
                                            GET ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } { DROP } ;
                                            DUP 7 ;
                                            GET 4 ;
                                            DIG 10 ;
                                            CAR ;
                                            GET 6 ;
                                            DUP 9 ;
                                            GET 3 ;
                                            DUP 11 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } {} ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } {} ;
                                            SOME ;
                                            SWAP ;
                                            UPDATE ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 4 ;
                                            CAR ;
                                            GET 6 ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 3 ;
                                            CAR ;
                                            PAIR ;
                                            MEM ;
                                            IF { DIG 3 ;
                                                 UNPAIR ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 DUP ;
                                                 DIG 6 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 8 ;
                                                 CAR ;
                                                 PAIR ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET ;
                                                 IF_NONE { PUSH int 397 ; FAILWITH } {} ;
                                                 DIG 7 ;
                                                 GET 4 ;
                                                 ADD ;
                                                 SOME ;
                                                 SWAP ;
                                                 UPDATE ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUG 2 }
                                               { DIG 3 ;
                                                 UNPAIR ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 UNPAIR ;
                                                 SWAP ;
                                                 DUP 6 ;
                                                 GET 4 ;
                                                 SOME ;
                                                 DIG 6 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 CAR ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 SWAP ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUG 2 } }
                                          { DROP } } ;
                                DROP } ;
                         DROP }
                       { DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         PUSH bool False ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } }
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         PUSH bool False ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 5 ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 3 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    UPDATE 5 ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 5 ;
                                    NONE unit ;
                                    DIG 3 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    UPDATE 5 ;
                                    SWAP } } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } ;
  view "balance_of_view"
       (pair (address %owner) (nat %token_id))
       (pair (nat %balance) (pair %request (address %owner) (nat %token_id)))
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 7 ;
         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 8 ;
         SWAP ;
         DUP ;
         DUG 2 ;
         CDR ;
         MEM ;
         IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
         SWAP ;
         DUP ;
         DUG 2 ;
         CAR ;
         GET 6 ;
         SWAP ;
         DUP ;
         CDR ;
         SWAP ;
         DUP ;
         DUG 3 ;
         CAR ;
         PAIR ;
         MEM ;
         IF { DUP ;
              CDR ;
              SWAP ;
              DUP ;
              DUG 2 ;
              CAR ;
              PAIR ;
              DIG 2 ;
              CAR ;
              GET 6 ;
              DIG 2 ;
              GET ;
              IF_NONE { PUSH int 450 ; FAILWITH } {} ;
              PAIR }
            { SWAP ; DROP ; PUSH nat 0 ; PAIR } } ;
  view "get_balance_view"
       (pair (address %owner) (nat %token_id))
       nat
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 8 ;
         SWAP ;
         DUP ;
         DUG 2 ;
         CDR ;
         MEM ;
         IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
         SWAP ;
         CAR ;
         GET 6 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 470 ; FAILWITH } {} } }
