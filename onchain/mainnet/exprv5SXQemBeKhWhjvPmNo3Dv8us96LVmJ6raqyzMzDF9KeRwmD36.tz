{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (pair %mint (nat %token_id) (pair (map %token_metadata string bytes) (nat %amount_))))
            (or (bool %setPause)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))))
        (or (or (address %updateMultisig)
                (or %updateProxy (address %add_proxy) (address %remove_proxy)))
            (list %update_operators
               (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                   (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))) ;
  storage
    (pair (pair (pair (bool %is_updatable) (big_map %ledger (pair address nat) nat))
                (pair (big_map %metadata string bytes) (address %multisig)))
          (pair (pair (big_map %operators (pair address (pair address nat)) unit) (bool %paused))
                (pair (set %proxy address)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  code { PUSH string "FA2_TOKEN_UNDEFINED" ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CDR ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DUP 3 ;
                     CAR ;
                     MAP { DUP 3 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           GET ;
                           IF_NONE { DUP 6 ; FAILWITH } { DROP } ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           DUP 3 ;
                           CAR ;
                           PAIR ;
                           GET ;
                           IF_NONE { PUSH nat 0 ; SWAP ; PAIR } { SWAP ; PAIR } } ;
                     SWAP ;
                     DIG 2 ;
                     DIG 5 ;
                     DROP 3 ;
                     SWAP ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { DIG 2 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     IF { DROP 2 ; PUSH nat 8 ; FAILWITH }
                        { PUSH bool False ;
                          DUP 3 ;
                          CDR ;
                          CDR ;
                          CAR ;
                          SENDER ;
                          MEM ;
                          COMPARE ;
                          EQ ;
                          IF { DROP 2 ; PUSH nat 2 ; FAILWITH }
                             { UNPAIR 3 ;
                               DUP 4 ;
                               CAR ;
                               CAR ;
                               CDR ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               SOURCE ;
                               PAIR ;
                               GET ;
                               IF_NONE {} { DROP ; PUSH string "token already exists" ; FAILWITH } ;
                               DUP 4 ;
                               CAR ;
                               CAR ;
                               CDR ;
                               DIG 3 ;
                               SOME ;
                               DUP 3 ;
                               SOURCE ;
                               PAIR ;
                               UPDATE ;
                               DUP 3 ;
                               PUSH string "" ;
                               GET ;
                               IF_NONE { PUSH string "option is None" ; FAILWITH } {} ;
                               DUP ;
                               PUSH nat 6 ;
                               DIG 2 ;
                               SIZE ;
                               SUB ;
                               ABS ;
                               PUSH nat 6 ;
                               SLICE ;
                               IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                               DUP 5 ;
                               CDR ;
                               DUP 6 ;
                               CAR ;
                               CDR ;
                               DIG 3 ;
                               DUP 7 ;
                               CAR ;
                               CAR ;
                               CAR ;
                               PAIR ;
                               PAIR ;
                               PAIR ;
                               DIG 4 ;
                               CDR ;
                               CDR ;
                               CDR ;
                               DIG 4 ;
                               DIG 3 ;
                               SOME ;
                               PUSH string "" ;
                               UPDATE ;
                               DUP 4 ;
                               PAIR ;
                               SOME ;
                               DIG 3 ;
                               UPDATE ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CDR ;
                               CDR ;
                               CAR ;
                               PAIR ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CDR ;
                               CAR ;
                               PAIR ;
                               SWAP ;
                               CAR ;
                               PAIR } } ;
                     NIL operation ;
                     PAIR } }
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     SELF_ADDRESS ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DIG 2 ;
                          DUP ;
                          CAR ;
                          CDR ;
                          CDR ;
                          CONTRACT %callMultisig
                            (pair (pair %entrypoint_signature
                                     (pair (string %name) (bytes %params))
                                     (address %source_contract))
                                  (lambda %callback unit (list operation))) ;
                          IF_NONE
                            { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                            { SELF_ADDRESS ;
                              DUP 5 ;
                              PACK ;
                              SHA256 ;
                              PUSH string "setPause" ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PUSH mutez 0 ;
                              DIG 5 ;
                              DIG 5 ;
                              PAIR ;
                              LAMBDA
                                (pair (pair address bool) unit)
                                (list operation)
                                { CAR ;
                                  UNPAIR ;
                                  CONTRACT %setPause bool ;
                                  IF_NONE
                                    { DROP ; PUSH string "no setPause entrypoint" ; FAILWITH }
                                    { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                              SWAP ;
                              APPLY ;
                              DIG 3 ;
                              PAIR ;
                              TRANSFER_TOKENS ;
                              NIL operation ;
                              SWAP ;
                              CONS } ;
                          PAIR }
                        { DROP ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          CDR ;
                          SWAP ;
                          DUP 3 ;
                          CDR ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          PAIR ;
                          SWAP ;
                          CAR ;
                          PAIR ;
                          NIL operation ;
                          PAIR } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     IF { DROP 3 ; PUSH nat 8 ; FAILWITH }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          CAR ;
                          CDR ;
                          DUP 3 ;
                          CDR ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          LAMBDA
                            (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                            unit
                            { UNPAIR ;
                              UNPAIR ;
                              DIG 2 ;
                              UNPAIR ;
                              DUP 4 ;
                              DUP 4 ;
                              COMPARE ;
                              EQ ;
                              IF { DROP 4 ; UNIT }
                                 { DIG 3 ;
                                   PAIR ;
                                   DIG 2 ;
                                   PAIR ;
                                   MEM ;
                                   IF { UNIT } { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } } ;
                          DUG 2 ;
                          UNPAIR ;
                          SWAP ;
                          DIG 2 ;
                          ITER { DUP ;
                                 DUG 2 ;
                                 CDR ;
                                 ITER { SWAP ;
                                        DUP ;
                                        DUP 3 ;
                                        GET 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE
                                          { DROP 2 ; DUP 5 ; FAILWITH }
                                          { DUP 3 ;
                                            GET 4 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            COMPARE ;
                                            LT ;
                                            IF { DROP 3 ; PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                               { SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 DUP 4 ;
                                                 GET 3 ;
                                                 DUP 5 ;
                                                 CAR ;
                                                 PAIR ;
                                                 GET ;
                                                 IF_NONE { DUP 3 ; GET 4 } { DUP 4 ; GET 4 ; ADD } ;
                                                 DUP 4 ;
                                                 GET 4 ;
                                                 DIG 2 ;
                                                 SUB ;
                                                 ABS ;
                                                 DUP 6 ;
                                                 DUP 5 ;
                                                 GET 3 ;
                                                 PAIR ;
                                                 SENDER ;
                                                 DUP 7 ;
                                                 CAR ;
                                                 PAIR ;
                                                 PAIR ;
                                                 DUP 8 ;
                                                 SWAP ;
                                                 EXEC ;
                                                 DROP ;
                                                 PUSH nat 0 ;
                                                 DUP 5 ;
                                                 GET 4 ;
                                                 COMPARE ;
                                                 EQ ;
                                                 IF { SWAP ; DIG 3 ; DROP 3 }
                                                    { DUG 2 ;
                                                      SOME ;
                                                      DUP 4 ;
                                                      GET 3 ;
                                                      DUP 5 ;
                                                      CAR ;
                                                      PAIR ;
                                                      UPDATE ;
                                                      SWAP ;
                                                      SOME ;
                                                      DIG 2 ;
                                                      GET 3 ;
                                                      DUP 4 ;
                                                      CAR ;
                                                      PAIR ;
                                                      UPDATE } } } } ;
                                 SWAP ;
                                 DROP } ;
                          SWAP ;
                          DIG 2 ;
                          DIG 4 ;
                          DROP 3 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          DIG 2 ;
                          DIG 3 ;
                          CAR ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          NIL operation ;
                          PAIR } } } }
           { DIG 2 ;
             DROP ;
             IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          SWAP ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          CAR ;
                          PAIR ;
                          DIG 2 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          PAIR ;
                          NIL operation ;
                          PAIR }
                        { SELF_ADDRESS ;
                          DIG 2 ;
                          DUP ;
                          CAR ;
                          CDR ;
                          CDR ;
                          CONTRACT %callMultisig
                            (pair (pair %entrypoint_signature
                                     (pair (string %name) (bytes %params))
                                     (address %source_contract))
                                  (lambda %callback unit (list operation))) ;
                          IF_NONE
                            { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                            { SELF_ADDRESS ;
                              DUP 5 ;
                              PACK ;
                              SHA256 ;
                              PUSH string "update_multisig" ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PUSH mutez 0 ;
                              DIG 5 ;
                              DIG 5 ;
                              PAIR ;
                              LAMBDA
                                (pair (pair address address) unit)
                                (list operation)
                                { CAR ;
                                  UNPAIR ;
                                  CONTRACT %updateMultisig address ;
                                  IF_NONE
                                    { DROP ; PUSH string "no updateMultisig entrypoint" ; FAILWITH }
                                    { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                              SWAP ;
                              APPLY ;
                              DIG 3 ;
                              PAIR ;
                              TRANSFER_TOKENS ;
                              NIL operation ;
                              SWAP ;
                              CONS } ;
                          PAIR } }
                   { SELF_ADDRESS ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DIG 2 ;
                          DUP ;
                          CAR ;
                          CDR ;
                          CDR ;
                          CONTRACT %callMultisig
                            (pair (pair %entrypoint_signature
                                     (pair (string %name) (bytes %params))
                                     (address %source_contract))
                                  (lambda %callback unit (list operation))) ;
                          IF_NONE
                            { SWAP ; DIG 2 ; DROP 2 ; PUSH string "no call entrypoint" ; FAILWITH }
                            { SELF_ADDRESS ;
                              DUP 5 ;
                              PACK ;
                              SHA256 ;
                              PUSH string "updateProxy" ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PUSH mutez 0 ;
                              DIG 5 ;
                              DIG 5 ;
                              PAIR ;
                              LAMBDA
                                (pair (pair address (or address address)) unit)
                                (list operation)
                                { CAR ;
                                  UNPAIR ;
                                  CONTRACT %updateProxy (or (address %add_proxy) (address %remove_proxy)) ;
                                  IF_NONE
                                    { DROP ; PUSH string "no updateProxy entrypoint" ; FAILWITH }
                                    { NIL operation ; SWAP ; PUSH mutez 0 ; DIG 3 ; TRANSFER_TOKENS ; CONS } } ;
                              SWAP ;
                              APPLY ;
                              DIG 3 ;
                              PAIR ;
                              TRANSFER_TOKENS ;
                              NIL operation ;
                              SWAP ;
                              CONS } ;
                          PAIR }
                        { DROP ;
                          IF_LEFT
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              CDR ;
                              CAR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              MEM ;
                              IF { DROP 2 ; PUSH nat 6 ; FAILWITH }
                                 { SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   CDR ;
                                   CDR ;
                                   DUP 3 ;
                                   CDR ;
                                   CDR ;
                                   CAR ;
                                   DIG 2 ;
                                   PUSH bool True ;
                                   SWAP ;
                                   UPDATE ;
                                   PAIR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   CAR ;
                                   PAIR ;
                                   SWAP ;
                                   CAR ;
                                   PAIR ;
                                   NIL operation ;
                                   PAIR } }
                            { PUSH bool False ;
                              DUP 3 ;
                              CDR ;
                              CDR ;
                              CAR ;
                              DUP 3 ;
                              MEM ;
                              COMPARE ;
                              EQ ;
                              IF { DROP 2 ; PUSH nat 7 ; FAILWITH }
                                 { SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   CDR ;
                                   CDR ;
                                   DUP 3 ;
                                   CDR ;
                                   CDR ;
                                   CAR ;
                                   DIG 2 ;
                                   PUSH bool False ;
                                   SWAP ;
                                   UPDATE ;
                                   PAIR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   CAR ;
                                   PAIR ;
                                   SWAP ;
                                   CAR ;
                                   PAIR ;
                                   NIL operation ;
                                   PAIR } } } } }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 SENDER ;
                 DUG 2 ;
                 ITER { SWAP ;
                        DUP 3 ;
                        DUP 3 ;
                        IF_LEFT {} {} ;
                        CAR ;
                        COMPARE ;
                        EQ ;
                        IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                        SWAP ;
                        IF_LEFT
                          { SWAP ;
                            UNIT ;
                            SOME ;
                            DUP 3 ;
                            GET 4 ;
                            DUP 4 ;
                            GET 3 ;
                            PAIR ;
                            DIG 3 ;
                            CAR ;
                            PAIR ;
                            UPDATE }
                          { DUP ;
                            DUG 2 ;
                            GET 4 ;
                            DUP 3 ;
                            GET 3 ;
                            PAIR ;
                            DIG 2 ;
                            CAR ;
                            PAIR ;
                            NONE unit ;
                            SWAP ;
                            UPDATE } } ;
                 SWAP ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CDR ;
                 DUP 3 ;
                 CDR ;
                 CAR ;
                 CDR ;
                 DIG 2 ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 CAR ;
                 PAIR ;
                 NIL operation ;
                 PAIR } } } }
