{ parameter
    (or (or (list %check_balance
               (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))
            (set %claim nat))
        (or (unit %confirm_ownership)
            (or %owner_action
               (or (unit %renounce_ownership) (bool %set_pause))
               (or (pair %set_settings (int %interval) (nat %claim_per_interval))
                   (address %transfer_ownership))))) ;
  storage
    (pair (big_map %metadata string bytes)
          (pair %roles (address %owner) (option %pending_owner address))
          (pair %settings (int %interval) (nat %claim_per_interval))
          (bool %paused)
          (address %stake_collection)
          (pair %claim_token (address %address) (nat %token_id))
          (timestamp %genesis)
          (big_map %claims nat nat)) ;
  code { NIL operation ;
         PUSH string "INVALID_ENTRYPOINT" ;
         LAMBDA
           (pair unit
                 (big_map string bytes)
                 (pair address (option address))
                 (pair int nat)
                 bool
                 address
                 (pair address nat)
                 timestamp
                 (big_map nat nat))
           nat
           { CDR ;
             DUP ;
             GET 5 ;
             CAR ;
             DUP 2 ;
             GET 13 ;
             NOW ;
             SUB ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR ;
             ISNAT ;
             IF_NONE { DROP ; PUSH nat 0 } { SWAP ; GET 5 ; CDR ; SWAP ; MUL } } ;
         DIG 3 ;
         UNPAIR ;
         PUSH string "DONT_SEND_TEZ" ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         EQ ;
         IF { DROP } { FAILWITH } ;
         IF_LEFT
           { IF_LEFT
               { DIG 2 ;
                 DIG 3 ;
                 DROP 2 ;
                 ITER { PUSH nat 0 ;
                        SWAP ;
                        CDR ;
                        COMPARE ;
                        GT ;
                        IF {} { PUSH string "INVALID_FA2_BALANCE" ; FAILWITH } } }
               { DIG 4 ;
                 DROP ;
                 SWAP ;
                 PUSH string "PAUSED" ;
                 PUSH bool False ;
                 DUP 3 ;
                 GET 7 ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 PUSH nat 0 ;
                 DUP 3 ;
                 SIZE ;
                 COMPARE ;
                 EQ ;
                 IF { PUSH string "MISSING_TOKEN_IDS" ; FAILWITH } {} ;
                 PUSH nat 0 ;
                 NIL (pair address nat) ;
                 DUP 3 ;
                 UNIT ;
                 PAIR ;
                 DUP 6 ;
                 SWAP ;
                 EXEC ;
                 DIG 4 ;
                 ITER { DUP ;
                        SENDER ;
                        PAIR ;
                        DIG 3 ;
                        SWAP ;
                        CONS ;
                        DUG 2 ;
                        DUP 5 ;
                        DUP 2 ;
                        DUP 2 ;
                        GET 14 ;
                        SWAP ;
                        GET ;
                        IF_NONE { NONE nat } { SOME } ;
                        IF_NONE { PUSH nat 0 } {} ;
                        SWAP ;
                        UNIT ;
                        PAIR ;
                        DUP 8 ;
                        SWAP ;
                        EXEC ;
                        SUB ;
                        ISNAT ;
                        IF_NONE { PUSH nat 0 } {} ;
                        DIG 4 ;
                        ADD ;
                        DUG 3 ;
                        DUP 5 ;
                        DIG 5 ;
                        GET 14 ;
                        DUP 4 ;
                        DIG 3 ;
                        SWAP ;
                        SOME ;
                        SWAP ;
                        UPDATE ;
                        UPDATE 14 ;
                        DUG 3 } ;
                 DIG 4 ;
                 DROP 2 ;
                 NIL operation ;
                 PUSH nat 0 ;
                 DUP 4 ;
                 COMPARE ;
                 GT ;
                 IF { DIG 2 ;
                      SENDER ;
                      DUP 5 ;
                      GET 11 ;
                      DUP ;
                      CAR ;
                      CONTRACT %mint (pair (address %owner) (nat %token_id) (nat %amount)) ;
                      IF_NONE
                        { DROP 3 ; PUSH string "FA_MINT_ENTRYPOINT_UNDEFINED" ; FAILWITH }
                        { PUSH mutez 0 ; DIG 4 ; DIG 3 ; CDR ; DIG 4 ; PAIR 3 ; TRANSFER_TOKENS } ;
                      CONS }
                    { DIG 2 ; DROP } ;
                 DUP 3 ;
                 GET 9 ;
                 CONTRACT %balance_of
                   (pair (list %requests (pair (address %owner) (nat %token_id)))
                         (contract %callback
                            (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance))))) ;
                 IF_NONE { DUP 4 ; FAILWITH } {} ;
                 SELF_ADDRESS ;
                 CONTRACT %check_balance
                   (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance))) ;
                 IF_NONE { DIG 4 ; FAILWITH } { DIG 5 ; DROP } ;
                 DIG 3 ;
                 PAIR ;
                 DUG 2 ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 SWAP } }
           { DIG 2 ;
             DIG 3 ;
             DROP 2 ;
             IF_LEFT
               { DROP ;
                 PUSH string "DO_NOT_SEND_TEZ" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 PUSH string "NO_PENDING_OWNER" ;
                 DUP 2 ;
                 GET 3 ;
                 CDR ;
                 IF_NONE { FAILWITH } { SWAP ; DROP } ;
                 PUSH string "INVALID_PENDING_OWNER_ACCESS" ;
                 DUP 2 ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 DUP 2 ;
                 DIG 2 ;
                 GET 3 ;
                 DIG 2 ;
                 UPDATE 1 ;
                 UPDATE 3 ;
                 DUP ;
                 GET 3 ;
                 NONE address ;
                 UPDATE 2 ;
                 UPDATE 3 }
               { PUSH string "INVALID_OWNER_ACCESS" ;
                 DUP 3 ;
                 SENDER ;
                 SWAP ;
                 GET 3 ;
                 CAR ;
                 SWAP ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         GET 3 ;
                         PUSH address 0x00000000000000000000000000000000000000000000 ;
                         UPDATE 1 ;
                         UPDATE 3 }
                       { UPDATE 7 } }
                   { IF_LEFT
                       { UPDATE 5 }
                       { SWAP ; DUP ; GET 3 ; DIG 2 ; SOME ; UPDATE 2 ; UPDATE 3 } } } } ;
         SWAP ;
         PAIR } ;
  view "owner" unit address { CDR ; GET 3 ; CAR } ;
  view "is_owner"
       address
       bool
       { UNPAIR ; SWAP ; GET 3 ; CAR ; SWAP ; COMPARE ; EQ } ;
  view "get_claimed_amount"
       nat
       nat
       { UNPAIR ;
         SWAP ;
         GET 14 ;
         SWAP ;
         GET ;
         IF_NONE { NONE nat } { SOME } ;
         IF_NONE { PUSH nat 0 } {} } ;
  view "get_accum_claim_per_token"
       unit
       nat
       { CDR ;
         DUP ;
         GET 5 ;
         CAR ;
         DUP 2 ;
         GET 13 ;
         NOW ;
         SUB ;
         EDIV ;
         IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
         CAR ;
         ISNAT ;
         IF_NONE { DROP ; PUSH nat 0 } { SWAP ; GET 5 ; CDR ; SWAP ; MUL } } ;
  view "get_pending_claim"
       nat
       nat
       { UNPAIR ;
         DUP 2 ;
         GET 14 ;
         SWAP ;
         GET ;
         IF_NONE { NONE nat } { SOME } ;
         IF_NONE { PUSH nat 0 } {} ;
         DUP 2 ;
         GET 5 ;
         CAR ;
         DUP 3 ;
         GET 13 ;
         NOW ;
         SUB ;
         EDIV ;
         IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
         CAR ;
         ISNAT ;
         IF_NONE { SWAP ; DROP ; PUSH nat 0 } { DIG 2 ; GET 5 ; CDR ; SWAP ; MUL } ;
         SUB ;
         ISNAT ;
         IF_NONE { PUSH nat 0 } {} } }
