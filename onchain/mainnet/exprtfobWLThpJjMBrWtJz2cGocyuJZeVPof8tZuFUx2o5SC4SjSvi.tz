{ storage
    (pair (pair (address %core_team_multisig)
                (pair (nat %counter) (big_map %metadata string bytes)))
          (pair (nat %minimum_votes)
                (pair (big_map %proposals
                         nat
                         (pair (address %issuer)
                               (pair (timestamp %timestamp)
                                     (pair (bytes %title)
                                           (pair (bytes %description)
                                                 (pair (map %options nat bytes) (pair (nat %voting_period) (nat %minimum_votes))))))))
                      (big_map %votes (pair nat address) nat)))) ;
  parameter
    (or (pair %create_proposal
           (bytes %title)
           (pair (bytes %description) (pair (map %options nat bytes) (nat %voting_period))))
        (or (nat %set_minimum_votes) (pair %vote_proposal (nat %proposal_id) (nat %option)))) ;
  code { UNPAIR ;
         IF_LEFT
           { SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CAR ;
             SENDER ;
             VIEW "is_user" bool ;
             IF_NONE { PUSH int 65 ; FAILWITH } {} ;
             IF {} { PUSH string "CTV_NOT_CORE_TEAM_USER" ; FAILWITH } ;
             DUP ;
             GET 5 ;
             SIZE ;
             PUSH nat 1 ;
             COMPARE ;
             LT ;
             IF {} { PUSH string "CTV_WRONG_OPTIONS" ; FAILWITH } ;
             DUP ;
             GET 6 ;
             PUSH nat 1 ;
             COMPARE ;
             LT ;
             IF {} { PUSH string "CTV_WRONG_VOTING_PERIOD" ; FAILWITH } ;
             SWAP ;
             DUP ;
             DUG 2 ;
             DUP ;
             GET 5 ;
             DUP 4 ;
             GET 3 ;
             DIG 3 ;
             DUP ;
             GET 6 ;
             SWAP ;
             DUP ;
             GET 5 ;
             SWAP ;
             DUP ;
             GET 3 ;
             SWAP ;
             DUP ;
             DUG 7 ;
             CAR ;
             NOW ;
             SENDER ;
             PAIR 7 ;
             DIG 3 ;
             DROP ;
             SOME ;
             DIG 3 ;
             CAR ;
             GET 3 ;
             UPDATE ;
             UPDATE 5 ;
             UNPAIR ;
             UNPAIR ;
             SWAP ;
             UNPAIR ;
             PUSH nat 1 ;
             ADD ;
             PAIR ;
             SWAP ;
             PAIR ;
             PAIR }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "CTV_NOT_MULTISIG" ; FAILWITH } ;
                 DUP ;
                 PUSH nat 1 ;
                 COMPARE ;
                 LT ;
                 IF {} { PUSH string "CTV_WRONG_MINIMUM_VOTES" ; FAILWITH } ;
                 UPDATE 3 }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 VIEW "is_user" bool ;
                 IF_NONE { PUSH int 65 ; FAILWITH } {} ;
                 IF {} { PUSH string "CTV_NOT_CORE_TEAM_USER" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 5 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET ;
                 IF_NONE { PUSH string "CTV_INEXISTENT_PROPOSAL" ; FAILWITH } {} ;
                 DUP ;
                 GET 11 ;
                 INT ;
                 PUSH int 86400 ;
                 SWAP ;
                 MUL ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 ADD ;
                 NOW ;
                 COMPARE ;
                 LT ;
                 IF {} { PUSH string "CTV_CLOSED_PROPOSAL" ; FAILWITH } ;
                 GET 9 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 MEM ;
                 IF {} { PUSH string "CTV_WRONG_OPTION" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 GET 6 ;
                 DUP 3 ;
                 CDR ;
                 SOME ;
                 SENDER ;
                 DIG 4 ;
                 CAR ;
                 PAIR ;
                 UPDATE ;
                 UPDATE 6 } } ;
         NIL operation ;
         PAIR } ;
  view "get_proposal"
       nat
       (pair (address %issuer)
             (pair (timestamp %timestamp)
                   (pair (bytes %title)
                         (pair (bytes %description)
                               (pair (map %options nat bytes) (pair (nat %voting_period) (nat %minimum_votes)))))))
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 5 ;
         SWAP ;
         DUP ;
         DUG 2 ;
         MEM ;
         IF {} { PUSH string "CTV_INEXISTENT_PROPOSAL" ; FAILWITH } ;
         SWAP ;
         GET 5 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 172 ; FAILWITH } {} } ;
  view "get_proposal_count" unit nat { CDR ; CAR ; GET 3 } ;
  view "get_vote"
       (pair (nat %proposal_id) (address %user))
       nat
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 6 ;
         SWAP ;
         DUP ;
         CDR ;
         SWAP ;
         DUP ;
         DUG 3 ;
         CAR ;
         PAIR ;
         MEM ;
         IF {} { PUSH string "CTV_NO_USER_VOTE" ; FAILWITH } ;
         SWAP ;
         GET 6 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 189 ; FAILWITH } {} } ;
  view "has_voted"
       (pair (nat %proposal_id) (address %user))
       bool
       { UNPAIR ; SWAP ; GET 6 ; SWAP ; MEM } }
