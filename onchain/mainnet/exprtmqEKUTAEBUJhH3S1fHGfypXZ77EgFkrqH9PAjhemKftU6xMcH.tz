{ parameter
    (or (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
                (or %assets
                   (or (pair %balance_of
                          (list %requests (pair (address %owner) (nat %token_id)))
                          (contract %callback
                             (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                       (list %transfer
                          (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))))
                   (list %update_operators
                      (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                          (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))))
            (or (nat %buy) (unit %disableSale)))
        (or (or (pair %mint (pair nat string) bytes) (pair %pushLayer string string))
            (unit %withdraw))) ;
  storage
    (pair (pair %assets
             (pair (big_map %ledger (pair address nat) nat)
                   (big_map %operators (pair address (pair address nat)) unit))
             (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                   (big_map %token_total_supply nat nat)))
          (big_map %metadata string bytes)
          (option %pauseable_admin
             (pair (pair (address %admin) (bool %paused)) (option %pending_admin address)))
          (big_map %layers string string)
          (bool %sale_active)
          (address %sink)
          (nat %next_token_id)) ;
  code { LAMBDA
           (option (pair (pair address bool) (option address)))
           unit
           { IF_NONE
               { UNIT }
               { CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } } ;
         PUSH string "FA2_TOKEN_UNDEFINED" ;
         LAMBDA
           (pair (pair address nat) (big_map (pair address nat) nat))
           nat
           { UNPAIR ; GET ; IF_NONE { PUSH nat 0 } {} } ;
         DUP ;
         LAMBDA
           (pair (lambda (pair (pair address nat) (big_map (pair address nat) nat)) nat)
                 (pair (pair address nat) (pair nat (big_map (pair address nat) nat))))
           (big_map (pair address nat) nat)
           { UNPAIR ;
             SWAP ;
             UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DIG 3 ;
             DIG 3 ;
             PAIR ;
             DUP 3 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             PAIR ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DIG 2 ;
             ADD ;
             PUSH nat 0 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             COMPARE ;
             EQ ;
             IF { DROP ; NONE nat ; SWAP ; UPDATE }
                { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
         SWAP ;
         APPLY ;
         LAMBDA
           (pair bool string)
           unit
           { UNPAIR ; NOT ; IF { FAILWITH } { DROP ; UNIT } } ;
         LAMBDA
           (pair string (map string bytes))
           bytes
           { UNPAIR ; GET ; IF_NONE { PUSH string "NO_KEY" ; FAILWITH } {} } ;
         LAMBDA
           nat
           bytes
           { PUSH nat 0 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             COMPARE ;
             EQ ;
             IF { DROP ; PUSH bytes 0x30 }
                { PUSH bytes 0x30313233343536373839 ;
                  PUSH bytes 0x ;
                  DIG 2 ;
                  PAIR ;
                  PAIR ;
                  LEFT bytes ;
                  LOOP_LEFT
                    { UNPAIR ;
                      UNPAIR ;
                      PUSH nat 0 ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      COMPARE ;
                      GT ;
                      IF { PUSH nat 10 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           EDIV ;
                           IF_NONE { PUSH string "MOD by 0" ; FAILWITH } {} ;
                           CDR ;
                           DUP 4 ;
                           DIG 3 ;
                           DIG 4 ;
                           PUSH nat 1 ;
                           DIG 4 ;
                           SLICE ;
                           IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                           CONCAT ;
                           PUSH nat 10 ;
                           DIG 3 ;
                           EDIV ;
                           IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                           CAR ;
                           PAIR ;
                           PAIR ;
                           LEFT bytes }
                         { DIG 2 ; DROP 2 ; RIGHT (pair (pair nat bytes) bytes) } } } } ;
         DIG 7 ;
         UNPAIR ;
         IF_LEFT
           { DIG 3 ;
             DROP ;
             IF_LEFT
               { DIG 2 ;
                 DIG 3 ;
                 DROP 2 ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DROP 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     IF_LEFT
                       { IF_LEFT
                           { DIG 3 ;
                             DROP 2 ;
                             IF_NONE
                               { PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { DUP ;
                                 CDR ;
                                 IF_NONE
                                   { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                                   { SENDER ;
                                     COMPARE ;
                                     EQ ;
                                     IF { NONE address ; SWAP ; CAR ; CDR ; SENDER ; PAIR ; PAIR ; SOME }
                                        { DROP ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             DIG 4 ;
                             SWAP ;
                             EXEC ;
                             DROP ;
                             SWAP ;
                             IF_NONE
                               { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { DUP ; CDR ; DUG 2 ; CAR ; CAR ; PAIR ; PAIR ; SOME } ;
                             NIL operation ;
                             PAIR } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         IF_NONE
                           { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                           { SWAP ; SOME ; SWAP ; CAR ; PAIR ; SOME } ;
                         NIL operation ;
                         PAIR } ;
                     UNPAIR ;
                     DUG 2 ;
                     UPDATE 5 ;
                     SWAP ;
                     PAIR }
                   { DIG 5 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SWAP ;
                     IF_LEFT
                       { IF_LEFT
                           { DIG 3 ;
                             DROP ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             CDR ;
                             CAR ;
                             DUP 3 ;
                             CAR ;
                             CAR ;
                             DIG 2 ;
                             DUP ;
                             CAR ;
                             MAP { DUP 4 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   MEM ;
                                   NOT ;
                                   IF { DROP ; DUP 7 ; FAILWITH }
                                      { DUP 3 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CDR ;
                                        DUP 3 ;
                                        CAR ;
                                        PAIR ;
                                        PAIR ;
                                        DUP 8 ;
                                        SWAP ;
                                        EXEC ;
                                        SWAP ;
                                        PAIR } } ;
                             DIG 2 ;
                             DIG 3 ;
                             DIG 6 ;
                             DIG 7 ;
                             DROP 4 ;
                             SWAP ;
                             CDR ;
                             PUSH mutez 0 ;
                             DIG 2 ;
                             TRANSFER_TOKENS ;
                             SWAP ;
                             NIL operation ;
                             DIG 2 ;
                             CONS ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             LAMBDA
                               (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                               unit
                               { UNPAIR ;
                                 UNPAIR ;
                                 DIG 2 ;
                                 UNPAIR ;
                                 DUP 4 ;
                                 DUP 4 ;
                                 COMPARE ;
                                 EQ ;
                                 IF { DROP 4 ; UNIT }
                                    { DIG 3 ;
                                      PAIR ;
                                      DIG 2 ;
                                      PAIR ;
                                      MEM ;
                                      IF { UNIT } { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } } ;
                             DIG 2 ;
                             DUP 3 ;
                             CAR ;
                             CAR ;
                             SWAP ;
                             ITER { DUP ;
                                    DUG 2 ;
                                    CDR ;
                                    ITER { SWAP ;
                                           DUP 5 ;
                                           CDR ;
                                           CAR ;
                                           DUP 3 ;
                                           GET 3 ;
                                           MEM ;
                                           NOT ;
                                           IF { DROP 2 ; DUP 8 ; FAILWITH }
                                              { DUP 5 ;
                                                CAR ;
                                                CDR ;
                                                DUP 3 ;
                                                GET 3 ;
                                                PAIR ;
                                                SENDER ;
                                                DUP 5 ;
                                                CAR ;
                                                PAIR ;
                                                PAIR ;
                                                DUP 5 ;
                                                SWAP ;
                                                EXEC ;
                                                DROP ;
                                                SWAP ;
                                                DUP ;
                                                DUG 2 ;
                                                GET 4 ;
                                                PAIR ;
                                                SWAP ;
                                                DUP ;
                                                DUG 2 ;
                                                GET 3 ;
                                                DUP 4 ;
                                                CAR ;
                                                DIG 2 ;
                                                UNPAIR ;
                                                DIG 3 ;
                                                DIG 3 ;
                                                PAIR ;
                                                DUP 3 ;
                                                SWAP ;
                                                DUP ;
                                                DUG 2 ;
                                                PAIR ;
                                                DUP 12 ;
                                                SWAP ;
                                                EXEC ;
                                                DIG 2 ;
                                                SWAP ;
                                                SUB ;
                                                ISNAT ;
                                                IF_NONE
                                                  { DROP 2 ; PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                                  { PUSH nat 0 ;
                                                    SWAP ;
                                                    DUP ;
                                                    DUG 2 ;
                                                    COMPARE ;
                                                    EQ ;
                                                    IF { DROP ; NONE nat ; SWAP ; UPDATE }
                                                       { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
                                                SWAP ;
                                                DUP ;
                                                DUG 2 ;
                                                GET 4 ;
                                                PAIR ;
                                                SWAP ;
                                                DUP ;
                                                DUG 2 ;
                                                GET 3 ;
                                                DIG 2 ;
                                                CAR ;
                                                PAIR ;
                                                PAIR ;
                                                DUP 7 ;
                                                SWAP ;
                                                EXEC } } ;
                                    SWAP ;
                                    DROP } ;
                             SWAP ;
                             DIG 2 ;
                             DIG 5 ;
                             DIG 6 ;
                             DIG 7 ;
                             DROP 5 ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             CDR ;
                             DIG 2 ;
                             CAR ;
                             CDR ;
                             DIG 2 ;
                             PAIR ;
                             PAIR ;
                             NIL operation ;
                             PAIR } }
                       { DIG 3 ;
                         DIG 4 ;
                         DIG 5 ;
                         DROP 3 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         SWAP ;
                         SENDER ;
                         DUG 2 ;
                         ITER { SWAP ;
                                DUP 3 ;
                                DUP 3 ;
                                IF_LEFT {} {} ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                SWAP ;
                                IF_LEFT
                                  { SWAP ;
                                    UNIT ;
                                    SOME ;
                                    DUP 3 ;
                                    GET 4 ;
                                    DUP 4 ;
                                    GET 3 ;
                                    PAIR ;
                                    DIG 3 ;
                                    CAR ;
                                    PAIR ;
                                    UPDATE }
                                  { DUP ;
                                    DUG 2 ;
                                    GET 4 ;
                                    DUP 3 ;
                                    GET 3 ;
                                    PAIR ;
                                    DIG 2 ;
                                    CAR ;
                                    PAIR ;
                                    NONE unit ;
                                    SWAP ;
                                    UPDATE } } ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } ;
                     UNPAIR ;
                     DUG 2 ;
                     UPDATE 1 ;
                     SWAP ;
                     PAIR } }
               { DIG 5 ;
                 DROP ;
                 IF_LEFT
                   { DIG 6 ;
                     DROP 2 ;
                     PUSH string "SALE_OVER" ;
                     PUSH bool True ;
                     DUP 3 ;
                     GET 9 ;
                     COMPARE ;
                     EQ ;
                     PAIR ;
                     DUP 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH bytes 0x556e69717565 ;
                     PUSH string "Unique" ;
                     PUSH string "WRONG_PRICE" ;
                     PUSH nat 10 ;
                     PUSH mutez 1000000 ;
                     MUL ;
                     AMOUNT ;
                     COMPARE ;
                     EQ ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP 3 ;
                     GET 12 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DUP 4 ;
                     CAR ;
                     EMPTY_MAP string bytes ;
                     DIG 2 ;
                     SOME ;
                     PUSH string "token_id" ;
                     UPDATE ;
                     DIG 2 ;
                     PACK ;
                     SOME ;
                     PUSH string "rarity_pack" ;
                     UPDATE ;
                     DIG 2 ;
                     SOME ;
                     PUSH string "rarity" ;
                     UPDATE ;
                     PUSH bool False ;
                     PACK ;
                     SOME ;
                     PUSH string "minted" ;
                     UPDATE ;
                     DUP 3 ;
                     GET 12 ;
                     PAIR ;
                     DUP ;
                     CAR ;
                     DUP 3 ;
                     CDR ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE
                       { DUP 3 ;
                         CDR ;
                         CDR ;
                         DUP 4 ;
                         CDR ;
                         CAR ;
                         DIG 3 ;
                         DUP 4 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         PAIR ;
                         DUP 3 ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         CDR ;
                         CDR ;
                         PUSH nat 0 ;
                         DIG 3 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR }
                       { DROP 4 ; PUSH string "FA2_DUP_TOKEN_ID" ; FAILWITH } ;
                     NIL (pair address nat nat) ;
                     PUSH nat 1 ;
                     DUP 4 ;
                     GET 12 ;
                     SENDER ;
                     PAIR 3 ;
                     CONS ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     ITER { DUP ;
                            DUG 2 ;
                            GET 4 ;
                            PAIR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 3 ;
                            DIG 2 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            DUP 5 ;
                            SWAP ;
                            EXEC } ;
                     DIG 4 ;
                     DROP ;
                     DUP 3 ;
                     CDR ;
                     CDR ;
                     DIG 2 ;
                     ITER { SWAP ;
                            DUP ;
                            DUP 3 ;
                            GET 3 ;
                            GET ;
                            IF_NONE
                              { DROP 2 ; DUP 4 ; FAILWITH }
                              { DUP 3 ; GET 4 ; ADD ; SOME ; DIG 2 ; GET 3 ; UPDATE } } ;
                     DIG 4 ;
                     DROP ;
                     DUP 3 ;
                     CDR ;
                     DIG 3 ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     SWAP ;
                     UPDATE 1 ;
                     PUSH nat 1 ;
                     DIG 2 ;
                     GET 12 ;
                     ADD ;
                     UPDATE 12 ;
                     NIL operation ;
                     PAIR }
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DIG 5 ;
                     DROP 5 ;
                     DUP ;
                     GET 5 ;
                     DIG 2 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH bool False ;
                     UPDATE 9 ;
                     NIL operation ;
                     PAIR } } }
           { DIG 5 ;
             DIG 6 ;
             DIG 7 ;
             DROP 3 ;
             IF_LEFT
               { IF_LEFT
                   { UNPAIR ;
                     UNPAIR ;
                     DIG 3 ;
                     DIG 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     DIG 8 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP 3 ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     DUP 3 ;
                     CAR ;
                     DUP 5 ;
                     SWAP ;
                     CDR ;
                     CAR ;
                     SWAP ;
                     GET ;
                     IF_NONE { PUSH string "NO_KEY" ; FAILWITH } { CDR } ;
                     DUP ;
                     PUSH string "minted" ;
                     PAIR ;
                     DUP 8 ;
                     SWAP ;
                     EXEC ;
                     UNPACK bool ;
                     IF_NONE { PUSH string "FATAL_MINTED" ; FAILWITH } {} ;
                     PUSH string "TOKEN_MINTED" ;
                     PUSH bool False ;
                     DIG 2 ;
                     COMPARE ;
                     EQ ;
                     PAIR ;
                     DIG 8 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP ;
                     PUSH string "rarity_pack" ;
                     PAIR ;
                     DIG 7 ;
                     SWAP ;
                     EXEC ;
                     UNPACK string ;
                     IF_NONE { PUSH string "FATAL_RARITY" ; FAILWITH } {} ;
                     DUP 4 ;
                     PUSH nat 6 ;
                     PUSH nat 0 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 8 ;
                     PUSH nat 6 ;
                     PUSH nat 0 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP } ;
                     DUP 5 ;
                     PUSH nat 6 ;
                     PUSH nat 6 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 9 ;
                     PUSH nat 6 ;
                     PUSH nat 6 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP } ;
                     DUP 6 ;
                     PUSH nat 6 ;
                     PUSH nat 12 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 10 ;
                     PUSH nat 6 ;
                     PUSH nat 12 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP } ;
                     DUP 7 ;
                     PUSH nat 6 ;
                     PUSH nat 18 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 11 ;
                     PUSH nat 6 ;
                     PUSH nat 18 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP 2 } ;
                     DUP 7 ;
                     PUSH nat 6 ;
                     PUSH nat 24 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 11 ;
                     PUSH nat 6 ;
                     PUSH nat 24 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP 2 } ;
                     DUP 7 ;
                     PUSH nat 6 ;
                     PUSH nat 30 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 11 ;
                     PUSH nat 6 ;
                     PUSH nat 30 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP 2 } ;
                     DUP 7 ;
                     PUSH nat 8 ;
                     PUSH nat 56 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
                     DUP 11 ;
                     PUSH nat 8 ;
                     PUSH nat 56 ;
                     SLICE ;
                     IF_NONE { PUSH string "SLICE" ; FAILWITH } { DROP } ;
                     PUSH string "Common" ;
                     DUP 6 ;
                     COMPARE ;
                     EQ ;
                     IF { DIG 4 ;
                          DROP ;
                          DIG 6 ;
                          PUSH nat 12 ;
                          PUSH nat 0 ;
                          SLICE ;
                          IF_NONE { PUSH string "SLICE" ; FAILWITH } {} }
                        { PUSH string "Rare" ;
                          DUP 6 ;
                          COMPARE ;
                          EQ ;
                          IF { DIG 4 ;
                               DROP ;
                               DIG 6 ;
                               PUSH nat 24 ;
                               PUSH nat 0 ;
                               SLICE ;
                               IF_NONE { PUSH string "SLICE" ; FAILWITH } {} }
                             { PUSH string "Unique" ;
                               DIG 5 ;
                               COMPARE ;
                               EQ ;
                               IF { DIG 6 ;
                                    PUSH nat 36 ;
                                    PUSH nat 0 ;
                                    SLICE ;
                                    IF_NONE { PUSH string "SLICE" ; FAILWITH } {} }
                                  { DIG 6 ; DROP ; PUSH string "WRONG_TYPE" ; FAILWITH } } } ;
                     DUP 8 ;
                     CAR ;
                     DIG 6 ;
                     DUP 8 ;
                     PUSH bytes 0x4b455923 ;
                     CONCAT ;
                     SOME ;
                     PUSH string "symbol" ;
                     UPDATE ;
                     DIG 7 ;
                     PUSH bytes 0x7344414f204b65792023 ;
                     CONCAT ;
                     SOME ;
                     PUSH string "name" ;
                     UPDATE ;
                     PUSH bytes 0x30 ;
                     SOME ;
                     PUSH string "decimals" ;
                     UPDATE ;
                     DIG 6 ;
                     SOME ;
                     PUSH string "grad_start" ;
                     UPDATE ;
                     DIG 5 ;
                     SOME ;
                     PUSH string "grad_end" ;
                     UPDATE ;
                     DIG 4 ;
                     SOME ;
                     PUSH string "border" ;
                     UPDATE ;
                     DIG 3 ;
                     SOME ;
                     PUSH string "secret_bytes" ;
                     UPDATE ;
                     DIG 5 ;
                     PACK ;
                     SOME ;
                     PUSH string "rng_packed" ;
                     UPDATE ;
                     PUSH bool True ;
                     PACK ;
                     SOME ;
                     PUSH string "minted" ;
                     UPDATE ;
                     DUP 3 ;
                     PUSH bytes 0x68747470733a2f2f7364616f6172742e78797a2f6b6579732f ;
                     CONCAT ;
                     SOME ;
                     PUSH string "artifactUri" ;
                     UPDATE ;
                     DUP 3 ;
                     PUSH bytes 0x68747470733a2f2f7364616f6172742e78797a2f6b6579732f ;
                     CONCAT ;
                     SOME ;
                     PUSH string "displayUri" ;
                     UPDATE ;
                     DIG 2 ;
                     PUSH bytes 0x68747470733a2f2f7364616f6172742e78797a2f6b6579732f ;
                     CONCAT ;
                     SOME ;
                     PUSH string "thumbnailUri" ;
                     UPDATE ;
                     DUP 4 ;
                     PAIR ;
                     DIG 3 ;
                     DUP 3 ;
                     CDR ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE
                       { DROP 3 ; PUSH string "NO_TOKEN" ; FAILWITH }
                       { DROP ;
                         DUP 3 ;
                         CDR ;
                         CDR ;
                         DUP 4 ;
                         CDR ;
                         CAR ;
                         DIG 3 ;
                         SOME ;
                         DIG 3 ;
                         UPDATE ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR } ;
                     UPDATE 1 ;
                     NIL operation ;
                     PAIR }
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DROP 3 ;
                     UNPAIR ;
                     DUP 3 ;
                     GET 5 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DIG 2 ;
                     DUP ;
                     GET 7 ;
                     DIG 3 ;
                     SOME ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 7 ;
                     NIL operation ;
                     PAIR } }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 5 ;
                 DROP 5 ;
                 DUP ;
                 GET 11 ;
                 CONTRACT unit ;
                 IF_NONE { PUSH string "0" ; FAILWITH } {} ;
                 SWAP ;
                 NIL operation ;
                 DIG 2 ;
                 BALANCE ;
                 UNIT ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 PAIR } } } ;
  view "getTokenImg"
       nat
       string
       { LAMBDA
           (pair string (map string bytes))
           bytes
           { UNPAIR ; GET ; IF_NONE { PUSH string "NO_KEY" ; FAILWITH } {} } ;
         LAMBDA
           (pair string (big_map string string))
           string
           { UNPAIR ; GET ; IF_NONE { PUSH string "NO_LAYER" ; FAILWITH } {} } ;
         DIG 2 ;
         UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         CAR ;
         CDR ;
         CAR ;
         SWAP ;
         GET ;
         IF_NONE { PUSH string "NO_KEY" ; FAILWITH } { CDR } ;
         DUP ;
         PUSH string "rarity_pack" ;
         PAIR ;
         DUP 5 ;
         SWAP ;
         EXEC ;
         UNPACK string ;
         IF_NONE { PUSH string "FATAL_RARITY" ; FAILWITH } { DROP } ;
         PUSH string "rng_packed" ;
         PAIR ;
         DIG 3 ;
         SWAP ;
         EXEC ;
         UNPACK string ;
         IF_NONE { PUSH string "FATAL_RNG" ; FAILWITH } {} ;
         DUP ;
         PUSH nat 6 ;
         PUSH nat 0 ;
         SLICE ;
         IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
         SWAP ;
         DUP ;
         DUG 2 ;
         PUSH nat 6 ;
         PUSH nat 6 ;
         SLICE ;
         IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
         LAMBDA (pair string string) string { UNPAIR ; CONCAT } ;
         DUP 5 ;
         GET 7 ;
         PUSH string "section1" ;
         PAIR ;
         DUP 7 ;
         SWAP ;
         EXEC ;
         DUP 4 ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DUP 6 ;
         GET 7 ;
         PUSH string "section2" ;
         PAIR ;
         DUP 8 ;
         SWAP ;
         EXEC ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DUP 3 ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DUP 6 ;
         GET 7 ;
         PUSH string "section3" ;
         PAIR ;
         DUP 8 ;
         SWAP ;
         EXEC ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DIG 4 ;
         PUSH nat 6 ;
         PUSH nat 12 ;
         SLICE ;
         IF_NONE { PUSH string "SLICE" ; FAILWITH } {} ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DUP 5 ;
         GET 7 ;
         PUSH string "section4" ;
         PAIR ;
         DUP 7 ;
         SWAP ;
         EXEC ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DIG 3 ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DUP 4 ;
         GET 7 ;
         PUSH string "section5" ;
         PAIR ;
         DUP 6 ;
         SWAP ;
         EXEC ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DIG 2 ;
         SWAP ;
         PAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         SWAP ;
         EXEC ;
         DIG 2 ;
         GET 7 ;
         PUSH string "section6" ;
         PAIR ;
         DIG 3 ;
         SWAP ;
         EXEC ;
         SWAP ;
         PAIR ;
         EXEC } ;
  view "getTokenMetadata"
       nat
       (map string bytes)
       { UNPAIR ;
         SWAP ;
         CAR ;
         CDR ;
         CAR ;
         SWAP ;
         GET ;
         IF_NONE { PUSH string "NO_KEY" ; FAILWITH } { CDR } } }
