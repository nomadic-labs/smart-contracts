{ parameter
    (or (or (nat %claim) (address %set_admin))
        (or (pair %start_vesting
               (or %asset (or (address %fa12) (pair %fa2 (address %token) (nat %id))) (unit %tez))
               (pair (nat %amount) (pair (address %receiver) (timestamp %deadline))))
            (nat %stop_vesting))) ;
  storage
    (pair (address %admin)
          (pair (nat %vestings_counter)
                (pair (big_map %vestings
                         nat
                         (pair (or %asset (or (address %fa12) (pair %fa2 (address %token) (nat %id))) (unit %tez))
                               (pair (address %receiver)
                                     (pair (nat %treasury)
                                           (pair (timestamp %deadline)
                                                 (pair (nat %collected) (pair (nat %distr_speed_f) (timestamp %last_claimed))))))))
                      (big_map %receiver_to_vests address (set nat))))) ;
  code { LAMBDA
           (pair (option
                    (pair (or (or address (pair address nat)) unit)
                          (pair address (pair nat (pair timestamp (pair nat (pair nat timestamp)))))))
                 string)
           (pair (or (or address (pair address nat)) unit)
                 (pair address (pair nat (pair timestamp (pair nat (pair nat timestamp))))))
           { UNPAIR ; IF_NONE { FAILWITH } { SWAP ; DROP } } ;
         LAMBDA
           (pair (pair address address) (pair nat (or (or address (pair address nat)) unit)))
           operation
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             SWAP ;
             IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     PUSH string "wrong-token-entrypoint" ;
                     DIG 2 ;
                     CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                     IF_NONE { FAILWITH } { SWAP ; DROP } ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     DIG 4 ;
                     DIG 4 ;
                     DUG 2 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     TRANSFER_TOKENS }
                   { UNPAIR ;
                     DIG 2 ;
                     PAIR ;
                     DIG 3 ;
                     DIG 3 ;
                     DIG 2 ;
                     UNPAIR ;
                     PUSH string "wrong-token-entrypoint" ;
                     DIG 2 ;
                     CONTRACT %transfer
                       (list (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                     IF_NONE { FAILWITH } { SWAP ; DROP } ;
                     PUSH mutez 0 ;
                     DIG 5 ;
                     DIG 3 ;
                     PAIR ;
                     DIG 4 ;
                     DIG 4 ;
                     DIG 2 ;
                     UNPAIR ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     NIL (pair address (pair nat nat)) ;
                     DIG 2 ;
                     DIG 3 ;
                     DIG 5 ;
                     PAIR 3 ;
                     CONS ;
                     DIG 2 ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS } }
               { DIG 2 ;
                 DROP 2 ;
                 SWAP ;
                 CONTRACT unit ;
                 IF_NONE { PUSH string "bad address for get_contract" ; FAILWITH } {} ;
                 PUSH mutez 1 ;
                 DIG 2 ;
                 MUL ;
                 UNIT ;
                 TRANSFER_TOKENS } } ;
         LAMBDA
           (pair (pair timestamp nat) nat)
           nat
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             PUSH nat 1000000000000000000 ;
             DIG 3 ;
             DIG 3 ;
             NOW ;
             SUB ;
             ABS ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             COMPARE ;
             LT ;
             IF { SWAP ; DROP } { DROP } } ;
         DIG 3 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { PUSH string "unknown-vesting-entry" ;
                 DUP 3 ;
                 GET 5 ;
                 DUP 3 ;
                 GET ;
                 PAIR ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 PUSH string "overcollected-treasury" ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 9 ;
                 DUP 3 ;
                 GET 5 ;
                 SUB ;
                 ISNAT ;
                 IF_NONE { FAILWITH } { SWAP ; DROP } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 11 ;
                 DUP 3 ;
                 GET 12 ;
                 PAIR ;
                 PAIR ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 PUSH string "nothing-to-claim" ;
                 PUSH nat 0 ;
                 DUP 3 ;
                 COMPARE ;
                 GT ;
                 IF { DROP } { FAILWITH } ;
                 NIL operation ;
                 DUP 3 ;
                 CAR ;
                 DUP 3 ;
                 PAIR ;
                 DUP 4 ;
                 GET 3 ;
                 SELF_ADDRESS ;
                 PAIR ;
                 PAIR ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 CONS ;
                 DUP 3 ;
                 DIG 2 ;
                 DIG 3 ;
                 GET 9 ;
                 ADD ;
                 UPDATE 9 ;
                 NOW ;
                 UPDATE 12 ;
                 DIG 3 ;
                 DUP ;
                 GET 5 ;
                 DIG 2 ;
                 DIG 4 ;
                 SWAP ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 5 ;
                 SWAP ;
                 PAIR }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DROP 3 ;
                 PUSH string "not-admin" ;
                 DUP 3 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 UPDATE 1 ;
                 NIL operation ;
                 PAIR } }
           { IF_LEFT
               { DIG 2 ;
                 DIG 4 ;
                 DROP 2 ;
                 PUSH string "not-admin" ;
                 DUP 3 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 PUSH string "zero-vest-amount" ;
                 PUSH nat 0 ;
                 DUP 3 ;
                 GET 3 ;
                 COMPARE ;
                 GT ;
                 IF { DROP } { FAILWITH } ;
                 DUP ;
                 CAR ;
                 IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         PUSH string "mutez-not-zero" ;
                         PUSH mutez 0 ;
                         AMOUNT ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         DUP ;
                         GET 3 ;
                         NIL operation ;
                         DUP 3 ;
                         CAR ;
                         DUP 4 ;
                         GET 3 ;
                         PAIR ;
                         SELF_ADDRESS ;
                         SENDER ;
                         PAIR ;
                         PAIR ;
                         DIG 5 ;
                         SWAP ;
                         EXEC ;
                         CONS ;
                         PAIR }
                       { DROP ;
                         PUSH string "mutez-not-zero" ;
                         PUSH mutez 0 ;
                         AMOUNT ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         DUP ;
                         GET 3 ;
                         NIL operation ;
                         DUP 3 ;
                         CAR ;
                         DUP 4 ;
                         GET 3 ;
                         PAIR ;
                         SELF_ADDRESS ;
                         SENDER ;
                         PAIR ;
                         PAIR ;
                         DIG 5 ;
                         SWAP ;
                         EXEC ;
                         CONS ;
                         PAIR } }
                   { DIG 3 ;
                     DROP 2 ;
                     DUP ;
                     GET 3 ;
                     PUSH string "mutez-not-equal-amount" ;
                     PUSH mutez 1 ;
                     AMOUNT ;
                     EDIV ;
                     IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                     CAR ;
                     DUP 3 ;
                     COMPARE ;
                     EQ ;
                     IF { DROP } { FAILWITH } ;
                     NIL operation ;
                     PAIR } ;
                 UNPAIR ;
                 DUP 4 ;
                 GET 3 ;
                 PUSH string "deadline-in-past" ;
                 NOW ;
                 DUP 6 ;
                 GET 6 ;
                 SUB ;
                 ISNAT ;
                 IF_NONE { FAILWITH } { SWAP ; DROP } ;
                 PUSH nat 1000000000000000000 ;
                 DUP 5 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 NOW ;
                 SWAP ;
                 PUSH nat 0 ;
                 DUP 7 ;
                 GET 6 ;
                 DIG 6 ;
                 DUP 8 ;
                 GET 5 ;
                 DUP 9 ;
                 CAR ;
                 PAIR 7 ;
                 DIG 4 ;
                 DUP ;
                 DUP ;
                 DUG 6 ;
                 GET 6 ;
                 EMPTY_SET nat ;
                 DIG 7 ;
                 GET 6 ;
                 DUP 8 ;
                 GET 5 ;
                 GET ;
                 IF_NONE {} { SWAP ; DROP } ;
                 DUP 5 ;
                 PUSH bool True ;
                 SWAP ;
                 UPDATE ;
                 DIG 6 ;
                 GET 5 ;
                 SWAP ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 6 ;
                 DUP ;
                 GET 5 ;
                 DIG 2 ;
                 DUP 4 ;
                 SWAP ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 5 ;
                 PUSH nat 1 ;
                 DIG 2 ;
                 ADD ;
                 UPDATE 3 ;
                 SWAP ;
                 PAIR }
               { PUSH string "not-admin" ;
                 DUP 3 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF { DROP } { FAILWITH } ;
                 PUSH string "unknown-vesting-entry" ;
                 DUP 3 ;
                 GET 5 ;
                 DUP 3 ;
                 GET ;
                 PAIR ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 PUSH string "nothing-to-stop" ;
                 NOW ;
                 DUP 3 ;
                 GET 7 ;
                 COMPARE ;
                 GT ;
                 IF { DROP } { FAILWITH } ;
                 PUSH string "overcollected-treasury" ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 9 ;
                 DUP 3 ;
                 GET 5 ;
                 SUB ;
                 ISNAT ;
                 IF_NONE { FAILWITH } { SWAP ; DROP } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 11 ;
                 DUP 3 ;
                 GET 12 ;
                 PAIR ;
                 PAIR ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 9 ;
                 ADD ;
                 PUSH string "overcollected-treasury" ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 DUP 4 ;
                 GET 5 ;
                 SUB ;
                 ISNAT ;
                 IF_NONE { FAILWITH } { SWAP ; DROP } ;
                 PUSH string "nothing-to-stop" ;
                 PUSH nat 0 ;
                 DUP 3 ;
                 COMPARE ;
                 GT ;
                 IF { DROP } { FAILWITH } ;
                 NIL operation ;
                 DUP 4 ;
                 CAR ;
                 DIG 2 ;
                 PAIR ;
                 DUP 6 ;
                 CAR ;
                 SELF_ADDRESS ;
                 PAIR ;
                 PAIR ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 CONS ;
                 DUG 2 ;
                 UPDATE 5 ;
                 NOW ;
                 UPDATE 7 ;
                 DIG 3 ;
                 DUP ;
                 GET 5 ;
                 DIG 2 ;
                 DIG 4 ;
                 SWAP ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 5 ;
                 SWAP ;
                 PAIR } } } }
