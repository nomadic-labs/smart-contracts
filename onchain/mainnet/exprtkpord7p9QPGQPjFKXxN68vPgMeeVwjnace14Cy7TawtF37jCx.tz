{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (nat %burn))
            (or (pair %mint
                   (map %link_to_metadata string string)
                   (string %token_uri)
                   (address %owner))
                (list %transfer
                   (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))))
        (or (address %update_Admin)
            (list %update_operators
               (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                   (pair %remove_operator (address %owner) (address %operator) (nat %token_id)))))) ;
  storage
    (pair (pair (pair (set %admin address) (big_map %ledger nat address))
                (pair (big_map %metadata string bytes) (nat %next_token_id)))
          (pair (pair (big_map %nft_signature_storage string nat)
                      (big_map %operators (pair address (pair address nat)) unit))
                (pair (big_map %reverse_ledger address (list nat))
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  code { PUSH string "FA2_TOKEN_UNDEFINED" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         LAMBDA
           (pair address (set address))
           unit
           { UNPAIR ;
             MEM ;
             IF {} { PUSH string "NOT_AUTHORIZED" ; FAILWITH } ;
             UNIT } ;
         DIG 3 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DIG 2 ;
                 DIG 3 ;
                 DROP 2 ;
                 IF_LEFT
                   { DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           CAR ;
                           CAR ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           GET ;
                           IF_NONE
                             { DROP ; DUP 3 ; FAILWITH }
                             { SWAP ;
                               DUP ;
                               DUG 2 ;
                               CAR ;
                               SWAP ;
                               COMPARE ;
                               EQ ;
                               IF { PUSH nat 1 } { PUSH nat 0 } ;
                               SWAP ;
                               PAIR } } ;
                     DIG 3 ;
                     DROP ;
                     SWAP ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { DIG 2 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE
                       { PUSH string "UNKNOWN_TOKEN" ; FAILWITH }
                       { CDR ;
                         PUSH string "" ;
                         GET ;
                         IF_NONE { PUSH string "ipfs not found" ; FAILWITH } { UNPACK string } } ;
                     IF_NONE { PUSH string "UNPACK_FAILED" ; FAILWITH } {} ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE
                       { PUSH string "UNKNOWN_TOKEN" ; FAILWITH }
                       { SENDER ;
                         SWAP ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "NOT_TOKEN_OWNER" ; FAILWITH }
                            { DUP 3 ; CAR ; CAR ; CDR ; DUP 3 ; NONE address ; SWAP ; UPDATE } } ;
                     DUP 4 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     SENDER ;
                     GET ;
                     IF_NONE
                       { PUSH string "NOT_A_USER" ; FAILWITH }
                       { DUP 5 ;
                         CDR ;
                         CDR ;
                         CAR ;
                         NIL nat ;
                         DIG 2 ;
                         ITER { SWAP ;
                                DUP 6 ;
                                DUP 3 ;
                                COMPARE ;
                                EQ ;
                                IF { SWAP ; DROP } { SWAP ; CONS } } ;
                         SOME ;
                         SENDER ;
                         PAIR 3 ;
                         UNPAIR 3 ;
                         UPDATE } ;
                     DUP 5 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     DUP 5 ;
                     SELF_ADDRESS ;
                     PAIR ;
                     SENDER ;
                     PAIR ;
                     PAIR ;
                     DUP 6 ;
                     CDR ;
                     CAR ;
                     CAR ;
                     DIG 4 ;
                     NONE nat ;
                     SWAP ;
                     UPDATE ;
                     DUP 6 ;
                     CDR ;
                     DUP 7 ;
                     CAR ;
                     CDR ;
                     DIG 5 ;
                     DUP 8 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     CDR ;
                     DIG 4 ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     DUP 4 ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DIG 4 ;
                     CDR ;
                     CDR ;
                     CDR ;
                     DIG 4 ;
                     NONE (pair nat (map string bytes)) ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     DIG 3 ;
                     UNPAIR ;
                     NONE unit ;
                     SWAP ;
                     UPDATE ;
                     DUP 3 ;
                     CDR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } }
               { IF_LEFT
                   { DIG 3 ;
                     DIG 4 ;
                     DROP 2 ;
                     DUP ;
                     CAR ;
                     MAP { CDR ; PACK } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     DIG 2 ;
                     GET 3 ;
                     DUP 4 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     DUP 5 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     PAIR ;
                     DIG 6 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP 5 ;
                     CDR ;
                     CAR ;
                     CAR ;
                     DUP 3 ;
                     MEM ;
                     IF { PUSH string "NFT_EXISTED" ; FAILWITH } {} ;
                     DUP 5 ;
                     CDR ;
                     DUP 6 ;
                     CAR ;
                     CDR ;
                     DUP 7 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DUP 6 ;
                     DUP 5 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     DUP 8 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     CDR ;
                     DUP 7 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     DUP 6 ;
                     GET ;
                     IF_NONE
                       { DUP 7 ;
                         CDR ;
                         CDR ;
                         CAR ;
                         NIL nat ;
                         DUP 5 ;
                         CONS ;
                         DIG 6 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE }
                       { DUP 8 ; CDR ; CDR ; CAR ; SWAP ; DUP 5 ; CONS ; SOME ; DIG 6 ; UPDATE } ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     DUP 7 ;
                     CDR ;
                     CAR ;
                     CAR ;
                     DUP 5 ;
                     DIG 6 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DIG 3 ;
                     CDR ;
                     CDR ;
                     CDR ;
                     DIG 3 ;
                     DUP 4 ;
                     PAIR ;
                     DUP 4 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     PUSH nat 1 ;
                     DIG 3 ;
                     ADD ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     NIL operation ;
                     PAIR }
                   { DIG 2 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     CAR ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     PAIR ;
                     SWAP ;
                     ITER { DUP ;
                            DUG 2 ;
                            CDR ;
                            ITER { SWAP ;
                                   UNPAIR ;
                                   PUSH nat 0 ;
                                   DUP 4 ;
                                   GET 4 ;
                                   COMPARE ;
                                   EQ ;
                                   IF { DIG 2 ; DROP ; PAIR }
                                      { PUSH nat 1 ;
                                        DUP 4 ;
                                        GET 4 ;
                                        COMPARE ;
                                        NEQ ;
                                        IF { DROP 3 ; DUP 3 ; FAILWITH }
                                           { DUP ;
                                             DUP 4 ;
                                             GET 3 ;
                                             GET ;
                                             IF_NONE
                                               { DROP 3 ; DUP 4 ; FAILWITH }
                                               { DUP 5 ;
                                                 CAR ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 COMPARE ;
                                                 NEQ ;
                                                 IF { DROP 4 ; DUP 3 ; FAILWITH }
                                                    { DUP 6 ;
                                                      CDR ;
                                                      CAR ;
                                                      CDR ;
                                                      DUP 5 ;
                                                      GET 3 ;
                                                      PAIR ;
                                                      SENDER ;
                                                      DUG 2 ;
                                                      UNPAIR ;
                                                      DUP 4 ;
                                                      DUP 4 ;
                                                      COMPARE ;
                                                      EQ ;
                                                      IF { DROP 4 ; UNIT }
                                                         { DIG 3 ;
                                                           PAIR ;
                                                           DIG 2 ;
                                                           PAIR ;
                                                           MEM ;
                                                           IF { UNIT } { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } ;
                                                      DROP ;
                                                      SWAP ;
                                                      DUP ;
                                                      DUG 2 ;
                                                      DUP 5 ;
                                                      CAR ;
                                                      GET ;
                                                      IF_NONE
                                                        { SWAP ; DROP ; DUP 5 ; FAILWITH }
                                                        { DIG 2 ;
                                                          NIL nat ;
                                                          DIG 2 ;
                                                          ITER { SWAP ;
                                                                 DUP 5 ;
                                                                 GET 3 ;
                                                                 DUP 3 ;
                                                                 COMPARE ;
                                                                 EQ ;
                                                                 IF { SWAP ; DROP } { SWAP ; CONS } } ;
                                                          SOME ;
                                                          DUP 5 ;
                                                          CAR ;
                                                          PAIR 3 ;
                                                          UNPAIR 3 ;
                                                          UPDATE } ;
                                                      DUP ;
                                                      DUP 4 ;
                                                      CAR ;
                                                      GET ;
                                                      IF_NONE
                                                        { NIL nat ;
                                                          DUP 4 ;
                                                          GET 3 ;
                                                          CONS ;
                                                          DUP 4 ;
                                                          CAR ;
                                                          SWAP ;
                                                          SOME ;
                                                          SWAP ;
                                                          UPDATE }
                                                        { DUP 4 ; GET 3 ; CONS ; SOME ; DUP 4 ; CAR ; UPDATE } ;
                                                      SWAP ;
                                                      DUP 3 ;
                                                      CAR ;
                                                      SOME ;
                                                      DIG 3 ;
                                                      GET 3 ;
                                                      UPDATE ;
                                                      PAIR } } } } } ;
                            SWAP ;
                            DROP } ;
                     DIG 2 ;
                     DIG 3 ;
                     DROP 2 ;
                     UNPAIR ;
                     DUP 3 ;
                     CDR ;
                     DUP 4 ;
                     CAR ;
                     CDR ;
                     DIG 2 ;
                     DIG 4 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     CDR ;
                     CDR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } } }
           { DIG 3 ;
             DIG 4 ;
             DROP 2 ;
             IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 PAIR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 DUP 3 ;
                 CAR ;
                 CDR ;
                 DUP 4 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 DIG 4 ;
                 PUSH bool True ;
                 SWAP ;
                 UPDATE ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 NIL operation ;
                 PAIR }
               { DIG 2 ;
                 DROP ;
                 SENDER ;
                 DUP 3 ;
                 CDR ;
                 CAR ;
                 CDR ;
                 DIG 2 ;
                 ITER { SWAP ;
                        DUP 3 ;
                        DUP 3 ;
                        IF_LEFT {} {} ;
                        CAR ;
                        COMPARE ;
                        EQ ;
                        IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                        SWAP ;
                        IF_LEFT
                          { SWAP ;
                            UNIT ;
                            SOME ;
                            DUP 3 ;
                            GET 4 ;
                            DUP 4 ;
                            GET 3 ;
                            PAIR ;
                            DIG 3 ;
                            CAR ;
                            PAIR ;
                            UPDATE }
                          { DUP ;
                            DUG 2 ;
                            GET 4 ;
                            DUP 3 ;
                            GET 3 ;
                            PAIR ;
                            DIG 2 ;
                            CAR ;
                            PAIR ;
                            NONE unit ;
                            SWAP ;
                            UPDATE } } ;
                 SWAP ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CDR ;
                 SWAP ;
                 DUP 3 ;
                 CDR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 CAR ;
                 PAIR ;
                 NIL operation ;
                 PAIR } } } ;
  view "reverseLedgerView"
       (pair (nat %token_id) (address %sender))
       unit
       { UNPAIR ;
         SWAP ;
         CDR ;
         CDR ;
         CAR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         CDR ;
         GET ;
         IF_NONE
           { DROP ; PUSH string "reverse ledger owner does not exist" ; FAILWITH }
           { ITER { PUSH string "you already own the token" ;
                    DUP 3 ;
                    CAR ;
                    DIG 2 ;
                    COMPARE ;
                    EQ ;
                    IF { DROP } { FAILWITH } } ;
             DROP ;
             UNIT } } ;
  view "ledgerView"
       nat
       address
       { UNPAIR ;
         SWAP ;
         CAR ;
         CAR ;
         CDR ;
         SWAP ;
         GET ;
         IF_NONE { PUSH string "ledger owner does not exist" ; FAILWITH } {} } ;
  view "typeView"
       nat
       string
       { UNPAIR ;
         SWAP ;
         CDR ;
         CDR ;
         CDR ;
         SWAP ;
         GET ;
         IF_NONE
           { PUSH string "NFT_DOES_NOT_EXIST" ; FAILWITH }
           { CDR ;
             PUSH string "sales_type" ;
             GET ;
             IF_NONE
               { PUSH string "SALES_TYPE_NOT_FOUND" ; FAILWITH }
               { UNPACK string ;
                 IF_NONE { PUSH string "TYPE_UNPACK_FAILED" ; FAILWITH } {} } } } ;
  view "ownerAddressView"
       nat
       address
       { UNPAIR ;
         SWAP ;
         CDR ;
         CDR ;
         CDR ;
         SWAP ;
         GET ;
         IF_NONE
           { PUSH string "NFT_DOES_NOT_EXIST" ; FAILWITH }
           { CDR ;
             PUSH string "artist_address" ;
             GET ;
             IF_NONE
               { PUSH string "ARTIST_NOT_FOUND" ; FAILWITH }
               { UNPACK address ;
                 IF_NONE { PUSH string "ADDRESS_UNPACK_FAILED" ; FAILWITH } {} } } } }
