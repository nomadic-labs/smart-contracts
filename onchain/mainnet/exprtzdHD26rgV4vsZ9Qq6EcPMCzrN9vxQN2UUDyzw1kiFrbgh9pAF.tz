{ storage
    (pair (pair (pair (address %admin_contract_address) (address %administrator))
                (pair (address %generator_address)
                      (pair (nat %last_token_id) (big_map %ledger nat address))))
          (pair (pair (big_map %listed_tokens nat string)
                      (pair (address %market_contract_address) (big_map %metadata string bytes)))
                (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (pair (big_map %token_data
                               nat
                               (pair (pair (nat %edition_number) (string %generation_status))
                                     (pair (nat %project_id)
                                           (pair (string %project_name) (map %questionnaire string string)))))
                            (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (or (pair %create_sale
                       (nat %token_id)
                       (pair (nat %project_id) (pair (nat %market_fee) (address %buyer))))
                    (pair %list_token (nat %token_id) (address %owner))))
            (or (or (pair %mint
                       (nat %project_id)
                       (pair (string %project_name)
                             (pair (address %owner_address)
                                   (pair (string %generation_status)
                                         (pair (nat %edition_number) (map %metadata string bytes))))))
                    (pair %save_token_questionnaire
                       (nat %project_id)
                       (pair (nat %token_id)
                             (pair (map %questionnaire string string) (address %owner_address)))))
                (or (address %set_administrator) (big_map %set_metadata string bytes))))
        (or (or (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (pair %unlist_token (nat %token_id) (address %owner)))
                (or (address %update_admin_contract_address) (address %update_generator_address)))
            (or (or (address %update_market_contract_address)
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))
                (or (pair %update_token_metadata (nat %token_id) (map %metadata string bytes))
                    (pair %withdraw_mutez (mutez %amount) (address %destination)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { NIL operation ;
                     DUP ;
                     DUP 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DUP 5 ;
                     CAR ;
                     MAP { DUP 7 ;
                           GET 8 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP ;
                           CAR ;
                           DUP 8 ;
                           CAR ;
                           GET 6 ;
                           DUP 3 ;
                           CDR ;
                           GET ;
                           IF_NONE { PUSH int 370 ; FAILWITH } {} ;
                           COMPARE ;
                           EQ ;
                           IF { PUSH nat 1 } { PUSH nat 0 } ;
                           SWAP ;
                           PAIR } ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { SENDER ;
                         DUP 3 ;
                         GET 3 ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_MARKET" ; FAILWITH } ;
                         DUP ;
                         GET 6 ;
                         DUP 3 ;
                         CAR ;
                         GET 6 ;
                         DUP 3 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 159 ; FAILWITH } {} ;
                         COMPARE ;
                         NEQ ;
                         IF {} { PUSH string "TOKEN_OWNER" ; FAILWITH } ;
                         DUP ;
                         GET 3 ;
                         DUP 3 ;
                         GET 7 ;
                         DUP 3 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 160 ; FAILWITH } {} ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_PROJECT" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 161 ; FAILWITH } {} ;
                         DIG 2 ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         NONE string ;
                         DUP 7 ;
                         CAR ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 7 ;
                         GET 6 ;
                         SOME ;
                         DUP 8 ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         DUG 2 ;
                         NIL operation ;
                         DUP ;
                         DUP 5 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CONTRACT %share_royalties
                           (pair (nat %project_id)
                                 (pair (address %owner_token) (pair (nat %market_fee) (address %buyer)))) ;
                         IF_NONE { PUSH int 164 ; FAILWITH } {} ;
                         AMOUNT ;
                         DIG 5 ;
                         DUP ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 7 ;
                         GET 5 ;
                         DUP 7 ;
                         DUP 9 ;
                         GET 3 ;
                         PAIR 4 ;
                         DIG 4 ;
                         DROP ;
                         DIG 4 ;
                         DROP ;
                         DIG 4 ;
                         DROP ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SENDER ;
                         DUP 3 ;
                         GET 3 ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_MARKET" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         DUP 3 ;
                         CAR ;
                         GET 6 ;
                         DUP 3 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 130 ; FAILWITH } {} ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 8 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         MEM ;
                         IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                         PUSH bool False ;
                         DUP 3 ;
                         GET 3 ;
                         CAR ;
                         DUP 3 ;
                         CAR ;
                         MEM ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "TOKEN_ALREADY_LISTED" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         PUSH (option string) (Some "LISTED") ;
                         DIG 5 ;
                         CAR ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         NIL operation } } }
               { IF_LEFT
                   { IF_LEFT
                       { SENDER ;
                         DUP 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         DUP ;
                         GET 8 ;
                         DUP 3 ;
                         GET 10 ;
                         DUP 5 ;
                         CAR ;
                         GET 5 ;
                         PAIR ;
                         SOME ;
                         DIG 4 ;
                         CAR ;
                         GET 5 ;
                         UPDATE ;
                         UPDATE 8 ;
                         DUP ;
                         DUG 2 ;
                         DUP ;
                         GET 7 ;
                         PUSH (map string string) { Elt "" "" } ;
                         DUP 4 ;
                         GET 3 ;
                         PAIR ;
                         DUP 4 ;
                         CAR ;
                         PAIR ;
                         DIG 3 ;
                         DUP ;
                         GET 7 ;
                         SWAP ;
                         DUP ;
                         DUG 5 ;
                         GET 9 ;
                         PAIR ;
                         PAIR ;
                         SOME ;
                         DIG 4 ;
                         CAR ;
                         GET 5 ;
                         UPDATE ;
                         UPDATE 7 ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DIG 5 ;
                         GET 5 ;
                         SOME ;
                         DIG 6 ;
                         CAR ;
                         GET 5 ;
                         UPDATE ;
                         SWAP ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR }
                       { SENDER ;
                         DUP 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         MEM ;
                         IF {} { PUSH string "FA2_TOKEN_NOT_FOUND" ; FAILWITH } ;
                         DUP ;
                         CAR ;
                         DUP 3 ;
                         GET 7 ;
                         DUP 3 ;
                         GET 3 ;
                         GET ;
                         IF_NONE { PUSH int 101 ; FAILWITH } {} ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_PROJECT" ; FAILWITH } ;
                         DUP ;
                         GET 6 ;
                         DUP 3 ;
                         CAR ;
                         GET 6 ;
                         DUP 3 ;
                         GET 3 ;
                         GET ;
                         IF_NONE { PUSH int 102 ; FAILWITH } {} ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         MEM ;
                         IF { PUSH string "TOKEN_LISTED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         GET ;
                         IF_NONE { PUSH int 104 ; FAILWITH } {} ;
                         GET 6 ;
                         PUSH string "" ;
                         MEM ;
                         IF {} { PUSH string "FA2_ALREADY_ANSWERED" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 7 ;
                         DUP ;
                         DUP 4 ;
                         GET 3 ;
                         DUP ;
                         DUG 2 ;
                         GET ;
                         IF_NONE { PUSH int 105 ; FAILWITH } {} ;
                         DIG 4 ;
                         GET 5 ;
                         UPDATE 6 ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 7 } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CAR ;
                         DIG 3 ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CAR ;
                         DIG 4 ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { DUP 4 ;
                                       GET 8 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       PUSH bool False ;
                                       DUP 5 ;
                                       GET 3 ;
                                       CAR ;
                                       DUP 3 ;
                                       GET 3 ;
                                       MEM ;
                                       COMPARE ;
                                       EQ ;
                                       IF {} { PUSH string "TOKEN_LISTED" ; FAILWITH } ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CAR ;
                                       SENDER ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH bool True }
                                          { DUP 4 ;
                                            GET 5 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            PUSH nat 1 ;
                                            COMPARE ;
                                            EQ ;
                                            IF { SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 CAR ;
                                                 DUP 5 ;
                                                 CAR ;
                                                 GET 6 ;
                                                 DUP 3 ;
                                                 GET 3 ;
                                                 GET ;
                                                 IF_NONE { PUSH int 193 ; FAILWITH } {} ;
                                                 COMPARE ;
                                                 EQ }
                                               { PUSH bool False } ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            DIG 3 ;
                                            UNPAIR ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            DUP 6 ;
                                            CAR ;
                                            SOME ;
                                            DIG 6 ;
                                            GET 3 ;
                                            UPDATE ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            PAIR ;
                                            DUG 2 }
                                          { DROP } } ;
                                DROP } ;
                         DROP }
                       { SENDER ;
                         DUP 3 ;
                         GET 3 ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_MARKET" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         DUP 3 ;
                         CAR ;
                         GET 6 ;
                         DUP 3 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 144 ; FAILWITH } {} ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         MEM ;
                         IF {} { PUSH string "TOKEN_ALREADY_UNLISTED" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         NONE string ;
                         DIG 5 ;
                         CAR ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } } ;
                 NIL operation }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         DIG 4 ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 5 ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 3 ;
                                    UPDATE ;
                                    UPDATE 5 ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 5 ;
                                    NONE unit ;
                                    DIG 3 ;
                                    UPDATE ;
                                    UPDATE 5 ;
                                    SWAP } } ;
                         DROP } ;
                     NIL operation }
                   { IF_LEFT
                       { SENDER ;
                         DUP 3 ;
                         CAR ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_GENERATOR" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 8 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         MEM ;
                         IF {} { PUSH string "FA2_TOKEN_NOT_EXISTS" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 8 ;
                         DUP ;
                         DUP 4 ;
                         CAR ;
                         DUP ;
                         DUG 2 ;
                         GET ;
                         IF_NONE { PUSH int 118 ; FAILWITH } {} ;
                         DUP 5 ;
                         CDR ;
                         UPDATE 2 ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 8 ;
                         DUP ;
                         GET 7 ;
                         DUP ;
                         DIG 3 ;
                         CAR ;
                         DUP ;
                         DUG 2 ;
                         GET ;
                         IF_NONE { PUSH int 119 ; FAILWITH } {} ;
                         UNPAIR ;
                         CAR ;
                         PUSH string "GENERATED" ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 7 ;
                         NIL operation }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 546 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS } } } } ;
         PAIR } ;
  view "get_balance_of"
       (list (pair (address %owner) (nat %token_id)))
       (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))
       { UNPAIR ;
         DUP ;
         MAP { DUP 3 ;
               GET 8 ;
               SWAP ;
               DUP ;
               DUG 2 ;
               CDR ;
               MEM ;
               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
               DUP ;
               CAR ;
               DUP 4 ;
               CAR ;
               GET 6 ;
               DUP 3 ;
               CDR ;
               GET ;
               IF_NONE { PUSH int 370 ; FAILWITH } {} ;
               COMPARE ;
               EQ ;
               IF { PUSH nat 1 } { PUSH nat 0 } ;
               SWAP ;
               PAIR } ;
         SWAP ;
         DROP ;
         SWAP ;
         DROP } }
