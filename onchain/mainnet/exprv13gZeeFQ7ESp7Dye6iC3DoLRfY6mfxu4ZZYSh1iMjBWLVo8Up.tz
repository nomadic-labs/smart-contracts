{ storage
    (pair (pair (nat %counter) (big_map %metadata string bytes))
          (pair (big_map %polls
                   nat
                   (pair (bytes %question)
                         (pair (bytes %description)
                               (pair (map %options nat bytes)
                                     (pair (or %vote_weight_method (unit %equal) (or (unit %linear) (unit %quadratic)))
                                           (pair (nat %vote_period)
                                                 (pair (address %issuer)
                                                       (pair (timestamp %timestamp) (pair (nat %level) (map %votes_count nat nat))))))))))
                (pair (address %token)
                      (big_map %votes (pair nat address) (pair (nat %option) (nat %weight)))))) ;
  parameter
    (or (pair %create_poll
           (bytes %question)
           (pair (bytes %description)
                 (pair (map %options nat bytes)
                       (pair (or %vote_weight_method (unit %equal) (or (unit %linear) (unit %quadratic)))
                             (nat %vote_period)))))
        (pair %vote (nat %poll_id) (pair (nat %option) (option %max_checkpoints nat)))) ;
  code { LAMBDA
           nat
           nat
           { PUSH nat 2 ;
             DUP 2 ;
             EDIV ;
             IF_NONE { PUSH int 170 ; FAILWITH } { CAR } ;
             PUSH nat 0 ;
             DUP 2 ;
             COMPARE ;
             NEQ ;
             IF { PUSH nat 2 ;
                  DUP 2 ;
                  DUP 4 ;
                  EDIV ;
                  IF_NONE { PUSH int 173 ; FAILWITH } { CAR } ;
                  DUP 3 ;
                  ADD ;
                  EDIV ;
                  IF_NONE { PUSH int 173 ; FAILWITH } { CAR } ;
                  DUP 2 ;
                  DUP 2 ;
                  COMPARE ;
                  LT ;
                  LOOP { SWAP ;
                         DROP ;
                         PUSH nat 2 ;
                         DUP 2 ;
                         DUP 4 ;
                         EDIV ;
                         IF_NONE { PUSH int 177 ; FAILWITH } { CAR } ;
                         DUP 3 ;
                         ADD ;
                         EDIV ;
                         IF_NONE { PUSH int 177 ; FAILWITH } { CAR } ;
                         DUP 2 ;
                         DUP 2 ;
                         COMPARE ;
                         LT } ;
                  DROP ;
                  SWAP ;
                  DROP }
                { DROP } } ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { PUSH nat 0 ;
             DUP 3 ;
             GET 5 ;
             PUSH nat 0 ;
             SENDER ;
             PAIR ;
             VIEW "get_balance" nat ;
             IF_NONE { PUSH int 132 ; FAILWITH } {} ;
             COMPARE ;
             GT ;
             IF {} { PUSH string "POLL_NOT_DAO_MEMBER" ; FAILWITH } ;
             PUSH nat 1 ;
             DUP 2 ;
             GET 5 ;
             SIZE ;
             COMPARE ;
             GT ;
             IF {} { PUSH string "POLL_WRONG_OPTIONS" ; FAILWITH } ;
             PUSH nat 1 ;
             DUP 2 ;
             GET 8 ;
             COMPARE ;
             GE ;
             IF { PUSH nat 30 ; DUP 2 ; GET 8 ; COMPARE ; LE } { PUSH bool False } ;
             IF {} { PUSH string "POLL_WRONG_VOTE_PERIOD" ; FAILWITH } ;
             EMPTY_MAP nat nat ;
             DUP 2 ;
             GET 5 ;
             ITER { CAR ; SWAP ; PUSH (option nat) (Some 0) ; DIG 2 ; UPDATE } ;
             DUP 3 ;
             DUP ;
             GET 3 ;
             DUP 3 ;
             LEVEL ;
             NOW ;
             SENDER ;
             DUP 8 ;
             GET 8 ;
             DUP 9 ;
             GET 7 ;
             DUP 10 ;
             GET 5 ;
             DUP 11 ;
             GET 3 ;
             DUP 12 ;
             CAR ;
             PAIR 9 ;
             DIG 3 ;
             DROP ;
             DIG 3 ;
             DROP ;
             DIG 4 ;
             DROP ;
             SOME ;
             DIG 3 ;
             CAR ;
             CAR ;
             UPDATE ;
             UPDATE 3 ;
             UNPAIR ;
             UNPAIR ;
             PUSH nat 1 ;
             ADD ;
             PAIR ;
             PAIR }
           { DUP 2 ;
             GET 3 ;
             DUP 2 ;
             CAR ;
             GET ;
             IF_NONE { PUSH string "POLL_NONEXISTENT_POLL" ; FAILWITH } {} ;
             DUP ;
             GET 5 ;
             DUP 3 ;
             GET 3 ;
             MEM ;
             IF {} { PUSH string "POLL_WRONG_OPTION" ; FAILWITH } ;
             PUSH int 86400 ;
             DUP 2 ;
             GET 9 ;
             INT ;
             MUL ;
             DUP 2 ;
             GET 13 ;
             ADD ;
             NOW ;
             COMPARE ;
             LT ;
             IF {} { PUSH string "POLL_CLOSED_POLL" ; FAILWITH } ;
             DUP 3 ;
             GET 5 ;
             DUP 3 ;
             GET 4 ;
             DUP 3 ;
             GET 15 ;
             SENDER ;
             PAIR 3 ;
             VIEW "get_prior_balance" nat ;
             IF_NONE { PUSH int 157 ; FAILWITH } {} ;
             PUSH nat 0 ;
             DUP 2 ;
             COMPARE ;
             GT ;
             IF {} { PUSH string "POLL_INSUFICIENT_BALANCE" ; FAILWITH } ;
             PUSH nat 0 ;
             DUP 3 ;
             GET 7 ;
             IF_LEFT
               { DROP 3 ; DIG 3 ; DROP ; PUSH nat 1 }
               { SWAP ;
                 DROP ;
                 IF_LEFT
                   { DROP ; DIG 4 ; DROP }
                   { DROP ;
                     DIG 4 ;
                     PUSH nat 10000 ;
                     DIG 2 ;
                     EDIV ;
                     IF_NONE { PUSH int 268 ; FAILWITH } { CAR } ;
                     EXEC ;
                     PUSH nat 100 ;
                     MUL } } ;
             SENDER ;
             DUP 4 ;
             CAR ;
             PAIR ;
             DIG 2 ;
             GET 16 ;
             DUP 5 ;
             GET 6 ;
             DUP 3 ;
             MEM ;
             IF { DUP 5 ;
                  GET 6 ;
                  DUP 3 ;
                  GET ;
                  IF_NONE { PUSH int 279 ; FAILWITH } {} ;
                  CAR ;
                  DUP 2 ;
                  DUP 5 ;
                  DIG 3 ;
                  DUP 4 ;
                  GET ;
                  IF_NONE { PUSH int 280 ; FAILWITH } {} ;
                  SUB ;
                  ISNAT ;
                  IF_NONE { PUSH int 280 ; FAILWITH } {} ;
                  SOME ;
                  DIG 2 ;
                  UPDATE }
                {} ;
             DIG 4 ;
             DUP ;
             GET 6 ;
             DUP 5 ;
             DUP 7 ;
             GET 3 ;
             PAIR ;
             SOME ;
             DIG 4 ;
             UPDATE ;
             UPDATE 6 ;
             DUG 3 ;
             DUP ;
             DUP 4 ;
             GET 3 ;
             DUP ;
             DUG 2 ;
             GET ;
             IF_NONE { PUSH int 289 ; FAILWITH } {} ;
             DIG 3 ;
             ADD ;
             SOME ;
             SWAP ;
             UPDATE ;
             DIG 2 ;
             DUP ;
             GET 3 ;
             DUP ;
             DIG 4 ;
             CAR ;
             DUP ;
             DUG 2 ;
             GET ;
             IF_NONE { PUSH int 290 ; FAILWITH } {} ;
             DIG 4 ;
             UPDATE 16 ;
             SOME ;
             SWAP ;
             UPDATE ;
             UPDATE 3 } ;
         NIL operation ;
         PAIR } ;
  view "get_poll"
       nat
       (pair (bytes %question)
             (pair (bytes %description)
                   (pair (map %options nat bytes)
                         (pair (or %vote_weight_method (unit %equal) (or (unit %linear) (unit %quadratic)))
                               (pair (nat %vote_period)
                                     (pair (address %issuer)
                                           (pair (timestamp %timestamp) (pair (nat %level) (map %votes_count nat nat)))))))))
       { UNPAIR ;
         DUP 2 ;
         GET 3 ;
         DUP 2 ;
         MEM ;
         IF {} { PUSH string "POLL_NONEXISTENT_POLL" ; FAILWITH } ;
         SWAP ;
         GET 3 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 312 ; FAILWITH } {} } ;
  view "get_poll_count" unit nat { CDR ; CAR ; CAR } ;
  view "get_vote"
       (pair (nat %poll_id) (address %user))
       (pair (nat %option) (nat %weight))
       { UNPAIR ;
         DUP 2 ;
         GET 6 ;
         DUP 2 ;
         CDR ;
         DUP 3 ;
         CAR ;
         PAIR ;
         MEM ;
         IF {} { PUSH string "POLL_NO_USER_VOTE" ; FAILWITH } ;
         SWAP ;
         GET 6 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 329 ; FAILWITH } {} } ;
  view "has_voted"
       (pair (nat %poll_id) (address %user))
       bool
       { UNPAIR ; SWAP ; GET 6 ; SWAP ; MEM } }
