{ parameter
    (or (or (or (pair %mint (set %artifacts nat) (pair (bytes %metadata) (signature %signature)))
                (list %mint_callback
                   (pair (string %category)
                         (pair (map %ints string int)
                               (pair (bool %on_sale)
                                     (pair (address %owner)
                                           (pair (map %sets string (set nat))
                                                 (pair (map %strings string string) (pair (nat %token_id) (nat %uid))))))))))
            (or (address %set_administrator) (address %set_ledger)))
        (or (or (address %set_minter) (bool %set_pause))
            (or (nat %set_tezotop_count)
                (or (pair %update_builder_details
                       (address %burn_address)
                       (pair (nat %max_artifacts)
                             (pair (nat %max_multiplier)
                                   (pair (nat %min_artifacts) (pair (address %minter) (map %resources string nat))))))
                    (key %update_signer))))) ;
  storage
    (pair (address %administrator)
          (pair (address %burn_address)
                (pair (option %current_data
                         (pair (address %address)
                               (pair (set %artifacts nat) (pair (address %contract) (bytes %metadata)))))
                      (pair (nat %max_artifacts)
                            (pair (nat %max_multiplier)
                                  (pair (big_map %metadata string bytes)
                                        (pair (nat %min_artifacts)
                                              (pair (address %minter)
                                                    (pair (address %nft_registry)
                                                          (pair (bool %paused)
                                                                (pair (map %resources string nat)
                                                                      (pair (key %signer) (pair (nat %tezotops) (address %ticket_ledger)))))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 19 ;
                     IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 13 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SIZE ;
                     COMPARE ;
                     GE ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 7 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          SIZE ;
                          COMPARE ;
                          LE }
                        { PUSH bool False } ;
                     IF {} { PUSH string "INVALID_ARTIFACT_COUNT" ; FAILWITH } ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SELF_ADDRESS ;
                     DUP 4 ;
                     CAR ;
                     SENDER ;
                     PAIR 4 ;
                     SOME ;
                     UPDATE 5 ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     SELF_ADDRESS ;
                     DUP 3 ;
                     CAR ;
                     SENDER ;
                     PAIR 4 ;
                     PACK ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     DUP 4 ;
                     GET 23 ;
                     CHECK_SIGNATURE ;
                     IF {} { PUSH string "INVALID_SIGN" ; FAILWITH } ;
                     NIL operation ;
                     DUP 3 ;
                     GET 17 ;
                     CONTRACT %get_attributes
                       (pair (list nat)
                             (contract
                                (list (pair string
                                            (pair (map string int)
                                                  (pair bool
                                                        (pair address (pair (map string (set nat)) (pair (map string string) (pair nat nat)))))))))) ;
                     IF_NONE { PUSH int 101 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     SELF %mint_callback ;
                     NIL nat ;
                     DIG 5 ;
                     CAR ;
                     ITER { CONS } ;
                     NIL nat ;
                     SWAP ;
                     ITER { CONS } ;
                     PAIR ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 17 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_NFT_REGISTRY" ; FAILWITH } ;
                     EMPTY_MAP string int ;
                     NIL (pair address (pair nat nat)) ;
                     PUSH nat 2527 ;
                     DUP 5 ;
                     GET 25 ;
                     COMPARE ;
                     EQ ;
                     IF { DIG 3 ;
                          DUP ;
                          GET 21 ;
                          PUSH (option nat) (Some 0) ;
                          PUSH string "uno" ;
                          UPDATE ;
                          UPDATE 21 ;
                          DUG 3 }
                        {} ;
                     DUP 4 ;
                     GET 21 ;
                     ITER { CAR ;
                            DIG 2 ;
                            DUP 5 ;
                            GET 21 ;
                            DUP 3 ;
                            GET ;
                            IF_NONE { PUSH int 123 ; FAILWITH } {} ;
                            INT ;
                            SOME ;
                            DIG 2 ;
                            UPDATE ;
                            SWAP } ;
                     DUP 3 ;
                     ITER { DUP ;
                            CAR ;
                            PUSH string "artifact" ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "NOT_AN_ARTIFACT" ; FAILWITH } ;
                            DUP 5 ;
                            GET 5 ;
                            IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                            CAR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 7 ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "NOT_OWNED_BY_USER" ; FAILWITH } ;
                            DUP 5 ;
                            GET 5 ;
                            IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                            GET 3 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 13 ;
                            MEM ;
                            IF {} { PUSH string "ARTIFACT_MISMATCH" ; FAILWITH } ;
                            SWAP ;
                            PUSH nat 1 ;
                            DUP 3 ;
                            GET 13 ;
                            DUP 7 ;
                            GET 3 ;
                            PAIR 3 ;
                            CONS ;
                            SWAP ;
                            DUP 5 ;
                            GET 21 ;
                            ITER { CAR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   MEM ;
                                   IF { DIG 3 ;
                                        DUP ;
                                        DUP 3 ;
                                        DUP ;
                                        DUG 2 ;
                                        GET ;
                                        IF_NONE { PUSH int 133 ; FAILWITH } {} ;
                                        DUP 5 ;
                                        GET 3 ;
                                        DIG 4 ;
                                        GET ;
                                        IF_NONE { PUSH int 133 ; FAILWITH } {} ;
                                        ADD ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DUP 4 ;
                     GET 21 ;
                     ITER { CAR ;
                            DUP 5 ;
                            GET 9 ;
                            DUP 4 ;
                            DIG 2 ;
                            GET ;
                            IF_NONE { PUSH int 135 ; FAILWITH } {} ;
                            ISNAT ;
                            IF_NONE { PUSH int 135 ; FAILWITH } {} ;
                            COMPARE ;
                            LE ;
                            IF {} { PUSH string "MULTIPLIER_EXCEEDS_LIMIT" ; FAILWITH } } ;
                     NIL operation ;
                     DUP 5 ;
                     GET 17 ;
                     CONTRACT %transfer (list (pair address (list (pair address (pair nat nat))))) ;
                     IF_NONE { PUSH int 138 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     DUP 5 ;
                     DUP 9 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     DUP ;
                     DUP 6 ;
                     GET 15 ;
                     CONTRACT %mint
                       (pair address
                             (pair (pair string (pair (map string int) (pair (map string (set nat)) (map string string))))
                                   (pair nat bytes))) ;
                     IF_NONE { PUSH int 142 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DUP 8 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     GET 6 ;
                     PUSH nat 1 ;
                     EMPTY_MAP string string ;
                     EMPTY_MAP string (set nat) ;
                     DUP 10 ;
                     PUSH string "tezotop" ;
                     PAIR 4 ;
                     DUP 11 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     PAIR 4 ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     SWAP ;
                     DUP ;
                     GET 25 ;
                     PUSH nat 1 ;
                     ADD ;
                     UPDATE 25 ;
                     DUP ;
                     DUG 2 ;
                     GET 26 ;
                     CONTRACT %deduct_ledger (map address nat) ;
                     IF_NONE { PUSH int 158 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     EMPTY_MAP address nat ;
                     PUSH (option nat) (Some 1) ;
                     DUP 6 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                     CAR ;
                     UPDATE ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     SWAP ;
                     NONE (pair address (pair (set nat) (pair address bytes))) ;
                     UPDATE 5 ;
                     SWAP } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 1 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 26 } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 15 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 19 } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 25 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         UPDATE 7 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         UPDATE 13 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         UPDATE 9 ;
                         SWAP ;
                         GET 10 ;
                         UPDATE 21 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 23 } } } ;
             NIL operation } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
