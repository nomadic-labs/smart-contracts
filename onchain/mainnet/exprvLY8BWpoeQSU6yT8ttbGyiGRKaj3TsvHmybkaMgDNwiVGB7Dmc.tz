{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (pair %mint (address %address) (pair (nat %amount) (nat %token_id))))
            (or (address %set_administrator) (pair %set_metadata (string %k) (bytes %v))))
        (or (or (bool %set_pause)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (pair %update_token_metadata (map %metadata string bytes) (nat %token_id))))) ;
  storage
    (pair (pair (address %administrator) (pair (nat %all_tokens) (big_map %ledger address nat)))
          (pair (pair (big_map %metadata string bytes)
                      (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (bool %paused)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (pair address (pair nat nat)))
                            (or address (pair string bytes)))
                        (or (or bool (list (pair address (list (pair address (pair nat nat))))))
                            (or (list (or (pair address (pair address nat)) (pair address (pair address nat))))
                                (pair (map string bytes) nat))))
                    (pair (pair address (pair nat (big_map address nat)))
                          (pair (pair (big_map string bytes) (big_map (pair address (pair address nat)) unit))
                                (pair bool (big_map nat (pair nat (map string bytes))))))) ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { { DIP 2 { DUP } ; DIG 3 } ;
                           GET 6 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           CAR ;
                           GET 4 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CAR ;
                           MEM ;
                           IF { { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                GET ;
                                IF_NONE { PUSH int 425 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     GET 4 ;
                     PUSH nat 0 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "single-asset: token-id <> 0" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 4 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     MEM ;
                     IF { SWAP ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          SWAP ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          SWAP ;
                          DUP ;
                          { DIP 5 { DUP } ; DIG 6 } ;
                          CAR ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 543 ; FAILWITH } {} ;
                          { DIP 6 { DUP } ; DIG 7 } ;
                          GET 3 ;
                          ADD ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          SWAP }
                        { SWAP ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          SWAP ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          SWAP ;
                          { DIP 4 { DUP } ; DIG 5 } ;
                          GET 3 ;
                          SOME ;
                          { DIP 5 { DUP } ; DIG 6 } ;
                          CAR ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          SWAP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     COMPARE ;
                     LT ;
                     IF { DROP }
                        { DUP ;
                          GET 4 ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          CAR ;
                          GET 3 ;
                          COMPARE ;
                          EQ ;
                          IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                          SWAP ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          { { DUP ; CAR ; DIP { CDR } } } ;
                          SWAP ;
                          CDR ;
                          PUSH nat 1 ;
                          DIG 4 ;
                          GET 4 ;
                          ADD ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR } ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     SWAP ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CDR ;
                     SOME ;
                     DIG 5 ;
                     CAR ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 5 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP ;
                                   GET 3 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   EQ ;
                                   IF {} { PUSH string "single-asset: token-id <> 0" ; FAILWITH } ;
                                   { DIP 3 { DUP } ; DIG 4 } ;
                                   CAR ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { SENDER ; { DIP 2 { DUP } ; DIG 3 } ; CAR ; COMPARE ; EQ } ;
                                   IF { PUSH bool True }
                                      { { DIP 3 { DUP } ; DIG 4 } ;
                                        GET 3 ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        { DIP 4 { DUP } ; DIG 5 } ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   { DIP 3 { DUP } ; DIG 4 } ;
                                   GET 6 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        { DIP 4 { DUP } ; DIG 5 } ;
                                        CAR ;
                                        GET 4 ;
                                        { DIP 3 { DUP } ; DIG 4 } ;
                                        CAR ;
                                        GET ;
                                        IF_NONE { PUSH int 404 ; FAILWITH } {} ;
                                        COMPARE ;
                                        GE ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        { DIP 3 { DUP } ; DIG 4 } ;
                                        { { DUP ; CAR ; DIP { CDR } } } ;
                                        { { DUP ; CAR ; DIP { CDR } } } ;
                                        SWAP ;
                                        { { DUP ; CAR ; DIP { CDR } } } ;
                                        SWAP ;
                                        DUP ;
                                        { DIP 6 { DUP } ; DIG 7 } ;
                                        CAR ;
                                        DUP ;
                                        DUG 2 ;
                                        GET ;
                                        IF_NONE { PUSH int 407 ; FAILWITH } { DROP } ;
                                        { DIP 5 { DUP } ; DIG 6 } ;
                                        GET 4 ;
                                        DIG 9 ;
                                        CAR ;
                                        GET 4 ;
                                        { DIP 8 { DUP } ; DIG 9 } ;
                                        CAR ;
                                        GET ;
                                        IF_NONE { PUSH int 408 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH int 407 ; FAILWITH } {} ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 4 ;
                                        CAR ;
                                        GET 4 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        MEM ;
                                        IF { DIG 3 ;
                                             { { DUP ; CAR ; DIP { CDR } } } ;
                                             { { DUP ; CAR ; DIP { CDR } } } ;
                                             SWAP ;
                                             { { DUP ; CAR ; DIP { CDR } } } ;
                                             SWAP ;
                                             DUP ;
                                             { DIP 5 { DUP } ; DIG 6 } ;
                                             CAR ;
                                             DUP ;
                                             DUG 2 ;
                                             GET ;
                                             IF_NONE { PUSH int 410 ; FAILWITH } {} ;
                                             DIG 6 ;
                                             GET 4 ;
                                             ADD ;
                                             SOME ;
                                             SWAP ;
                                             UPDATE ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 }
                                           { DIG 3 ;
                                             { { DUP ; CAR ; DIP { CDR } } } ;
                                             { { DUP ; CAR ; DIP { CDR } } } ;
                                             SWAP ;
                                             { { DUP ; CAR ; DIP { CDR } } } ;
                                             SWAP ;
                                             { DIP 4 { DUP } ; DIG 5 } ;
                                             GET 4 ;
                                             SOME ;
                                             DIG 5 ;
                                             CAR ;
                                             UPDATE ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 } }
                                      { DROP } } ;
                            DROP } ;
                     DROP } }
               { IF_LEFT
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True }
                                   { { DIP 2 { DUP } ; DIG 3 } ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                SWAP ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True }
                                   { { DIP 2 { DUP } ; DIG 3 } ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                SWAP ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                SWAP ;
                                NONE unit ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 6 ;
                     DIG 2 ;
                     DUP ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 4 ;
                     CDR ;
                     PAIR ;
                     SOME ;
                     DIG 3 ;
                     CDR ;
                     UPDATE ;
                     UPDATE 6 } } ;
             NIL operation } ;
         PAIR } }
