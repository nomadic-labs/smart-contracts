{ parameter
    (or (or (or (address %declare_ownership) (unit %claim_ownership))
            (or (address %add_coowner) (address %remove_coowner)))
        (or (or (or (address %add_minter) (address %remove_minter))
                (or (unit %pause) (unit %unpause)))
            (or (or (address %set_timbre_collection) (list %burn_list nat))
                (or (nat %drop)
                    (pair %transfer_tokens (address %dest_address) (list %token_ids nat)))))) ;
  storage
    (pair (address %owner)
          (pair (address %timbre_collection)
                (pair (set %coowners address)
                      (pair (option %owner_candidate address)
                            (pair (set %minters address) (pair (bool %paused) (big_map %metadata string bytes))))))) ;
  code { LAMBDA
           bool
           bool
           { PUSH unit Unit ;
             DUP 2 ;
             IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
             PUSH bool True ;
             SWAP ;
             DROP ;
             DUG 1 ;
             DROP } ;
         NIL operation ;
         DIG 2 ;
         UNPAIR ;
         DIP { UNPAIR 7 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { PUSH bool True }
                        { DUP 4 ; SENDER ; MEM ; IF { PUSH bool True } { PUSH bool False } } ;
                     NOT ;
                     IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                     DUP ;
                     SOME ;
                     DIP { DIG 4 ; DROP } ;
                     DUG 4 ;
                     DROP ;
                     PAIR 7 ;
                     DIG 1 ;
                     PAIR }
                   { DROP ;
                     DUP 4 ;
                     IF_NONE
                       { PUSH bool False }
                       { SENDER ; DUP 2 ; COMPARE ; EQ ; SWAP ; DROP } ;
                     NOT ;
                     IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                     SENDER ;
                     SWAP ;
                     DROP ;
                     NONE address ;
                     DIP { DIG 3 ; DROP } ;
                     DUG 3 ;
                     PAIR 7 ;
                     DIG 1 ;
                     PAIR } }
               { IF_LEFT
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                     DUP 7 ;
                     IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                     DUP 4 ;
                     DUP 2 ;
                     MEM ;
                     IF { PUSH string "coowners" ; PUSH string "KEY_EXISTS" ; PAIR ; FAILWITH }
                        { DUP 4 ;
                          PUSH bool True ;
                          DUP 3 ;
                          UPDATE ;
                          DIP { DIG 3 ; DROP } ;
                          DUG 3 } ;
                     DROP ;
                     PAIR 7 ;
                     DIG 1 ;
                     PAIR }
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                     DUP 7 ;
                     IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                     DUP 4 ;
                     PUSH bool False ;
                     DUP 3 ;
                     UPDATE ;
                     DIP { DIG 3 ; DROP } ;
                     DUG 3 ;
                     DROP ;
                     PAIR 7 ;
                     DIG 1 ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 7 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         DUP 6 ;
                         DUP 2 ;
                         MEM ;
                         IF { PUSH string "minters" ; PUSH string "KEY_EXISTS" ; PAIR ; FAILWITH }
                            { DUP 6 ;
                              PUSH bool True ;
                              DUP 3 ;
                              UPDATE ;
                              DIP { DIG 5 ; DROP } ;
                              DUG 5 } ;
                         DROP ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR }
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 7 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         DUP 6 ;
                         PUSH bool False ;
                         DUP 3 ;
                         UPDATE ;
                         DIP { DIG 5 ; DROP } ;
                         DUG 5 ;
                         DROP ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR } }
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 9 ;
                         DUP 7 ;
                         EXEC ;
                         NOT ;
                         IF { PUSH string "pausable_r1" ;
                              PUSH string "INVALID_CONDITION" ;
                              PAIR ;
                              FAILWITH }
                            {} ;
                         PUSH bool True ;
                         DIP { DIG 5 ; DROP } ;
                         DUG 5 ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR }
                       { DROP ;
                         DUP ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 6 ;
                         NOT ;
                         IF { PUSH string "CONTRACT_NOT_PAUSED" ; FAILWITH } {} ;
                         PUSH bool False ;
                         DIP { DIG 5 ; DROP } ;
                         DUG 5 ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR } } }
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 7 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 2 ; DROP } ;
                         DUG 2 ;
                         DROP ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR }
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH bool True }
                            { DUP 6 ; SENDER ; MEM ; IF { PUSH bool True } { PUSH bool False } } ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 7 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { NIL operation ;
                                NIL operation ;
                                DUP 12 ;
                                ITER { CONS } ;
                                DUP 6 ;
                                CONTRACT %burn nat ;
                                IF_NONE
                                  { PUSH string "burn" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                                  {} ;
                                PUSH mutez 0 ;
                                DUP 5 ;
                                TRANSFER_TOKENS ;
                                CONS ;
                                ITER { CONS } ;
                                DIP { DIG 9 ; DROP } ;
                                DUG 9 ;
                                DROP } ;
                         DROP ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR } }
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH bool True }
                            { DUP 6 ; SENDER ; MEM ; IF { PUSH bool True } { PUSH bool False } } ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 7 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         NIL operation ;
                         NIL operation ;
                         DUP 11 ;
                         ITER { CONS } ;
                         DUP 5 ;
                         CONTRACT %drop nat ;
                         IF_NONE
                           { PUSH string "drop" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                           {} ;
                         PUSH mutez 0 ;
                         DUP 5 ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         ITER { CONS } ;
                         DIP { DIG 8 ; DROP } ;
                         DUG 8 ;
                         DROP ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR }
                       { UNPAIR ;
                         SWAP ;
                         DUP 3 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH bool True }
                            { DUP 7 ; SENDER ; MEM ; IF { PUSH bool True } { PUSH bool False } } ;
                         NOT ;
                         IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                         DUP 8 ;
                         IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                         NIL (pair (address %to_) (pair (nat %token_id) (nat %amount))) ;
                         DUP 2 ;
                         ITER { PUSH nat 1 ;
                                DUP 2 ;
                                PAIR ;
                                DUP 5 ;
                                PAIR ;
                                DIG 2 ;
                                DUP 2 ;
                                CONS ;
                                DUG 2 ;
                                DROP 2 } ;
                         DUP ;
                         SELF_ADDRESS ;
                         PAIR ;
                         NIL (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))) ;
                         DUP 2 ;
                         CONS ;
                         NIL operation ;
                         NIL operation ;
                         DUP 15 ;
                         ITER { CONS } ;
                         DUP 9 ;
                         CONTRACT %transfer (list (pair address (list (pair address (pair nat nat))))) ;
                         IF_NONE
                           { PUSH string "transfer" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                           {} ;
                         PUSH mutez 0 ;
                         DUP 5 ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         ITER { CONS } ;
                         DIP { DIG 12 ; DROP } ;
                         DUG 12 ;
                         DROP 5 ;
                         PAIR 7 ;
                         DIG 1 ;
                         PAIR } } } } ;
         DIP { DROP } } }
