{ storage
    (pair (pair (nat %counter) (big_map %metadata string bytes))
          (pair (big_map %polls
                   nat
                   (pair (address %issuer)
                         (pair (timestamp %timestamp)
                               (pair (bytes %question) (pair (map %options nat bytes) (nat %voting_period))))))
                (pair (big_map %results (pair nat nat) nat) (big_map %votes (pair nat address) nat)))) ;
  parameter
    (or (pair %create_poll
           (bytes %question)
           (pair (map %options nat bytes) (nat %voting_period)))
        (pair %vote (nat %poll_id) (nat %option))) ;
  code { UNPAIR ;
         IF_LEFT
           { DUP ;
             GET 3 ;
             SIZE ;
             PUSH nat 1 ;
             COMPARE ;
             LT ;
             IF {} { PUSH string "POLL_WRONG_OPTIONS" ; FAILWITH } ;
             DUP ;
             GET 4 ;
             PUSH nat 1 ;
             SWAP ;
             COMPARE ;
             GE ;
             IF { DUP ; GET 4 ; PUSH nat 5 ; SWAP ; COMPARE ; LE } { PUSH bool False } ;
             IF {} { PUSH string "POLL_WRONG_VOTING_PERIOD" ; FAILWITH } ;
             SWAP ;
             DUP ;
             DUG 2 ;
             DUP ;
             GET 3 ;
             DIG 2 ;
             DUP ;
             GET 4 ;
             SWAP ;
             DUP ;
             GET 3 ;
             SWAP ;
             DUP ;
             DUG 5 ;
             CAR ;
             NOW ;
             SENDER ;
             PAIR 5 ;
             DIG 3 ;
             DROP ;
             SOME ;
             DIG 3 ;
             CAR ;
             CAR ;
             UPDATE ;
             UPDATE 3 ;
             UNPAIR ;
             UNPAIR ;
             PUSH nat 1 ;
             ADD ;
             PAIR ;
             PAIR }
           { SWAP ;
             DUP ;
             DUG 2 ;
             GET 3 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             GET ;
             IF_NONE { PUSH string "INEXISTENT_POLL" ; FAILWITH } {} ;
             DUP ;
             GET 8 ;
             INT ;
             PUSH int 86400 ;
             SWAP ;
             MUL ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 3 ;
             ADD ;
             NOW ;
             COMPARE ;
             LT ;
             IF {} { PUSH string "CLOSED_POLL" ; FAILWITH } ;
             GET 7 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CDR ;
             MEM ;
             IF {} { PUSH string "POLL_WRONG_OPTION" ; FAILWITH } ;
             DUP ;
             CAR ;
             SENDER ;
             SWAP ;
             PAIR ;
             DUP 3 ;
             GET 6 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             MEM ;
             IF { DUP 3 ;
                  GET 6 ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  GET ;
                  IF_NONE { PUSH int 108 ; FAILWITH } {} ;
                  DUP 4 ;
                  DUP ;
                  GET 5 ;
                  PUSH nat 1 ;
                  DIG 6 ;
                  GET 5 ;
                  DUP 5 ;
                  DUP 8 ;
                  CAR ;
                  PAIR ;
                  GET ;
                  IF_NONE { PUSH int 109 ; FAILWITH } {} ;
                  SUB ;
                  ISNAT ;
                  IF_NONE { PUSH int 109 ; FAILWITH } {} ;
                  SOME ;
                  DIG 3 ;
                  DUP 6 ;
                  CAR ;
                  PAIR ;
                  UPDATE ;
                  UPDATE 5 ;
                  DUG 2 }
                {} ;
             DIG 2 ;
             DUP ;
             GET 6 ;
             DUP 4 ;
             CDR ;
             SOME ;
             DIG 3 ;
             UPDATE ;
             UPDATE 6 ;
             DUP ;
             DUG 2 ;
             DUP ;
             GET 5 ;
             DIG 3 ;
             GET 5 ;
             DUP 4 ;
             GET ;
             IF_NONE { PUSH nat 0 } {} ;
             PUSH nat 1 ;
             ADD ;
             SOME ;
             DIG 3 ;
             UPDATE ;
             UPDATE 5 } ;
         NIL operation ;
         PAIR } ;
  view "get_poll"
       nat
       (pair (address %issuer)
             (pair (timestamp %timestamp)
                   (pair (bytes %question) (pair (map %options nat bytes) (nat %voting_period)))))
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 3 ;
         SWAP ;
         DUP ;
         DUG 2 ;
         MEM ;
         IF {} { PUSH string "INEXISTENT_POLL" ; FAILWITH } ;
         SWAP ;
         GET 3 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 140 ; FAILWITH } {} } ;
  view "get_poll_count" unit nat { CDR ; CAR ; CAR } ;
  view "get_vote"
       (pair (nat %poll_id) (address %user))
       nat
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         GET 6 ;
         SWAP ;
         DUP ;
         CDR ;
         SWAP ;
         DUP ;
         DUG 3 ;
         CAR ;
         PAIR ;
         MEM ;
         IF {} { PUSH string "NO_USER_VOTE" ; FAILWITH } ;
         SWAP ;
         GET 6 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 157 ; FAILWITH } {} } ;
  view "has_voted"
       (pair (nat %poll_id) (address %user))
       bool
       { UNPAIR ; SWAP ; GET 6 ; SWAP ; MEM } }
