{ parameter
    (or (list %transfer
           (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))
        (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                       (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))
                (pair %materialize
                   (map %token_info string bytes)
                   (key %author)
                   (address %beneficiary)
                   (address %recipient)
                   (nat %max_editions)
                   (timestamp %expiration)
                   (signature %admin_signature))))) ;
  storage
    (pair (pair (pair (pair (key %admin) (big_map %ledger (pair address nat) nat))
                      (big_map %metadata string bytes)
                      (nat %next_token_id))
                (pair (big_map %operators (pair address address) (set nat))
                      (address %platform_address))
                (nat %platform_fee)
                (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))
          (big_map %used_hashes
             bytes
             (pair (pair (nat %editions_minted) (nat %max_editions)) (nat %token_id)))) ;
  code { PUSH string "INVALID_DESTINATION" ;
         LAMBDA
           address
           unit
           { PUSH string "The sender can only manage operators for his own token" ;
             SENDER ;
             DIG 2 ;
             COMPARE ;
             EQ ;
             IF { DROP ; UNIT } { FAILWITH } } ;
         LAMBDA
           (pair (pair (big_map (pair address nat) nat) address) nat)
           nat
           { UNPAIR ; UNPAIR ; DUG 2 ; PAIR ; GET ; IF_NONE { PUSH nat 0 } {} } ;
         LAMBDA
           (pair (pair (big_map (pair address nat) nat) address) nat nat)
           (big_map (pair address nat) nat)
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DUG 2 ;
             SOME ;
             DIG 2 ;
             DIG 3 ;
             PAIR ;
             UPDATE } ;
         LAMBDA
           (pair (pair (pair (pair (pair key (big_map (pair address nat) nat)) (big_map string bytes) nat)
                             (pair (big_map (pair address address) (set nat)) address)
                             nat
                             (big_map nat (pair nat (map string bytes))))
                       (big_map bytes (pair (pair nat nat) nat)))
                 nat)
           unit
           { UNPAIR ;
             CAR ;
             CDR ;
             CDR ;
             CDR ;
             SWAP ;
             GET ;
             IF_NONE { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } { DROP } ;
             UNIT } ;
         DIG 5 ;
         UNPAIR ;
         IF_LEFT
           { DIG 5 ;
             DIG 6 ;
             DROP 2 ;
             DUP 2 ;
             CAR ;
             CAR ;
             CAR ;
             CDR ;
             SWAP ;
             ITER { UNPAIR ;
                    DUG 2 ;
                    ITER { UNPAIR 3 ;
                           DUP 2 ;
                           DUP 7 ;
                           PAIR ;
                           DUP 8 ;
                           SWAP ;
                           EXEC ;
                           DROP ;
                           SENDER ;
                           DUP 6 ;
                           DUP 2 ;
                           COMPARE ;
                           NEQ ;
                           IF { DUP 7 ;
                                CAR ;
                                CDR ;
                                CAR ;
                                CAR ;
                                SWAP ;
                                DUP 7 ;
                                PAIR ;
                                GET ;
                                IF_NONE { EMPTY_SET nat } {} ;
                                DUP 3 ;
                                MEM ;
                                NOT ;
                                IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} }
                              { DROP } ;
                           DUP 2 ;
                           DUP 6 ;
                           DUP 6 ;
                           PAIR ;
                           PAIR ;
                           DUP 10 ;
                           SWAP ;
                           EXEC ;
                           PUSH string "FA2_INSUFFICIENT_BALANCE" ;
                           DUP 5 ;
                           DUP 3 ;
                           COMPARE ;
                           GE ;
                           IF { DROP } { FAILWITH } ;
                           DUP 4 ;
                           SWAP ;
                           SUB ;
                           ABS ;
                           DUP 3 ;
                           PAIR ;
                           DUP 6 ;
                           DIG 5 ;
                           PAIR ;
                           PAIR ;
                           DUP 8 ;
                           SWAP ;
                           EXEC ;
                           DIG 3 ;
                           DIG 3 ;
                           DUP ;
                           DUP 5 ;
                           DUP 5 ;
                           PAIR ;
                           PAIR ;
                           DUP 10 ;
                           SWAP ;
                           EXEC ;
                           DIG 2 ;
                           ADD ;
                           SWAP ;
                           PAIR ;
                           DUG 2 ;
                           PAIR ;
                           PAIR ;
                           DUP 5 ;
                           SWAP ;
                           EXEC } ;
                    SWAP ;
                    DROP } ;
             DIG 2 ;
             DIG 3 ;
             DIG 4 ;
             DROP 3 ;
             DUP 2 ;
             DIG 2 ;
             CAR ;
             DUP ;
             CAR ;
             DUP ;
             CAR ;
             DIG 4 ;
             UPDATE 2 ;
             UPDATE 1 ;
             UPDATE 1 ;
             UPDATE 1 ;
             NIL operation }
           { DIG 3 ;
             DROP ;
             IF_LEFT
               { DIG 4 ;
                 DIG 5 ;
                 DROP 2 ;
                 UNPAIR ;
                 MAP { DUP ;
                       UNPAIR ;
                       DUP 2 ;
                       DUP 6 ;
                       PAIR ;
                       DUP 7 ;
                       SWAP ;
                       EXEC ;
                       DROP ;
                       DUP 5 ;
                       CAR ;
                       CAR ;
                       CAR ;
                       CDR ;
                       PAIR ;
                       PAIR ;
                       DUP 6 ;
                       SWAP ;
                       EXEC ;
                       SWAP ;
                       PAIR } ;
                 DIG 3 ;
                 DIG 4 ;
                 DROP 2 ;
                 SWAP ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 TRANSFER_TOKENS ;
                 SWAP ;
                 NIL operation ;
                 DIG 2 ;
                 CONS }
               { DIG 2 ;
                 DIG 3 ;
                 DROP 2 ;
                 IF_LEFT
                   { DIG 3 ;
                     DROP ;
                     DUP 2 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CAR ;
                     LAMBDA
                       (pair (lambda address unit)
                             (pair (big_map (pair address address) (set nat))
                                   (or (pair address address nat) (pair address address nat))))
                       (big_map (pair address address) (set nat))
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         IF_LEFT
                           { DUP ;
                             CAR ;
                             DUP 2 ;
                             GET 3 ;
                             DUP ;
                             DUP 3 ;
                             COMPARE ;
                             EQ ;
                             IF { SWAP ; DIG 2 ; DIG 4 ; DROP 4 }
                                { DUP 2 ;
                                  DIG 5 ;
                                  SWAP ;
                                  EXEC ;
                                  DROP ;
                                  DUP 4 ;
                                  DUP 2 ;
                                  DUP 4 ;
                                  PAIR ;
                                  GET ;
                                  IF_NONE { EMPTY_SET nat } {} ;
                                  DIG 3 ;
                                  GET 4 ;
                                  PUSH bool True ;
                                  SWAP ;
                                  UPDATE ;
                                  DIG 3 ;
                                  SWAP ;
                                  SOME ;
                                  DIG 2 ;
                                  DIG 3 ;
                                  PAIR ;
                                  UPDATE } }
                           { DUP ;
                             CAR ;
                             DUP 2 ;
                             GET 3 ;
                             DUP ;
                             DUP 3 ;
                             COMPARE ;
                             EQ ;
                             IF { SWAP ; DIG 2 ; DIG 4 ; DROP 4 }
                                { DUP 2 ;
                                  DIG 5 ;
                                  SWAP ;
                                  EXEC ;
                                  DROP ;
                                  DUP 4 ;
                                  DIG 4 ;
                                  DUP 3 ;
                                  DUP 5 ;
                                  PAIR ;
                                  GET ;
                                  IF_NONE
                                    { DIG 3 ; DROP ; NONE (set nat) }
                                    { DIG 4 ;
                                      GET 4 ;
                                      PUSH bool False ;
                                      SWAP ;
                                      UPDATE ;
                                      PUSH nat 0 ;
                                      DUP 2 ;
                                      SIZE ;
                                      COMPARE ;
                                      EQ ;
                                      IF { DROP ; NONE (set nat) } { SOME } } ;
                                  DIG 2 ;
                                  DIG 3 ;
                                  PAIR ;
                                  UPDATE } } } ;
                     DUP 5 ;
                     APPLY ;
                     DIG 4 ;
                     DROP ;
                     SWAP ;
                     DIG 2 ;
                     ITER { SWAP ; PAIR ; DUP 2 ; SWAP ; EXEC } ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     CAR ;
                     DUP ;
                     CDR ;
                     DUP ;
                     CAR ;
                     DIG 4 ;
                     UPDATE 1 ;
                     UPDATE 1 ;
                     UPDATE 2 ;
                     UPDATE 1 ;
                     NIL operation }
                   { DIG 2 ;
                     DROP ;
                     AMOUNT ;
                     SELF_ADDRESS ;
                     DUP 3 ;
                     GET 11 ;
                     DUP 4 ;
                     GET 9 ;
                     DUP 5 ;
                     GET 5 ;
                     DUP 5 ;
                     DUP 7 ;
                     GET 3 ;
                     DUP 8 ;
                     CAR ;
                     DIG 6 ;
                     PAIR 7 ;
                     PACK ;
                     PUSH string "INVALID_ADMIN_SIGNATURE" ;
                     SWAP ;
                     DUP 4 ;
                     GET 12 ;
                     DUP 6 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CHECK_SIGNATURE ;
                     IF { DROP } { FAILWITH } ;
                     PUSH string "VOUCHER_EXPIRED" ;
                     NOW ;
                     DUP 4 ;
                     GET 11 ;
                     COMPARE ;
                     GT ;
                     IF { DROP } { FAILWITH } ;
                     PUSH nat 10000 ;
                     DUP 4 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     CAR ;
                     DUP 3 ;
                     MUL ;
                     EDIV ;
                     IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                     CAR ;
                     NIL operation ;
                     PUSH mutez 0 ;
                     DUP 3 ;
                     COMPARE ;
                     GT ;
                     IF { DUP 5 ;
                          CAR ;
                          CDR ;
                          CAR ;
                          CDR ;
                          CONTRACT unit ;
                          IF_NONE { DUP 6 ; FAILWITH } {} ;
                          DUP 3 ;
                          UNIT ;
                          TRANSFER_TOKENS ;
                          CONS }
                        {} ;
                     SWAP ;
                     DIG 2 ;
                     SUB_MUTEZ ;
                     IF_NONE { PUSH mutez 0 } {} ;
                     PUSH mutez 0 ;
                     DUP 2 ;
                     COMPARE ;
                     GT ;
                     IF { DUP 3 ;
                          GET 5 ;
                          CONTRACT unit ;
                          IF_NONE { DIG 4 ; FAILWITH } { DIG 5 ; DROP } ;
                          DIG 2 ;
                          SWAP ;
                          DIG 2 ;
                          UNIT ;
                          TRANSFER_TOKENS ;
                          CONS }
                        { DIG 4 ; DROP 2 } ;
                     DUP 2 ;
                     CAR ;
                     DUP 3 ;
                     GET 9 ;
                     PUSH nat 0 ;
                     DUP 2 ;
                     COMPARE ;
                     EQ ;
                     IF { PUSH string "MAX_EDITIONS_MUST_BE_GREATER_THAN_ZERO" ; FAILWITH } {} ;
                     DUP 2 ;
                     PACK ;
                     BLAKE2B ;
                     DUP 6 ;
                     CDR ;
                     DUP 2 ;
                     GET ;
                     IF_NONE
                       { DUP 6 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         DUP 7 ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CDR ;
                         DIG 4 ;
                         DUP 3 ;
                         PAIR ;
                         DIG 2 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         DUP 6 ;
                         CDR ;
                         DUP 7 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         DIG 4 ;
                         PUSH nat 1 ;
                         PAIR ;
                         PAIR ;
                         DIG 3 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         PAIR ;
                         PUSH nat 1 ;
                         DUP 5 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         ADD ;
                         DUP 5 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         PAIR ;
                         PAIR }
                       { DIG 2 ;
                         DIG 3 ;
                         DROP 2 ;
                         DUP ;
                         CAR ;
                         CAR ;
                         DUP 2 ;
                         CAR ;
                         CDR ;
                         COMPARE ;
                         EQ ;
                         IF { DROP 2 ; PUSH string "NO_REMAINING_MINTS" ; FAILWITH }
                            { DUP ;
                              CDR ;
                              DUP 2 ;
                              CAR ;
                              CDR ;
                              PUSH nat 1 ;
                              DUP 4 ;
                              CAR ;
                              CAR ;
                              ADD ;
                              PAIR ;
                              PAIR ;
                              DUP 6 ;
                              CAR ;
                              CDR ;
                              CDR ;
                              CDR ;
                              DUP 7 ;
                              CDR ;
                              DIG 2 ;
                              DIG 4 ;
                              SWAP ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              PAIR ;
                              DUP 5 ;
                              CAR ;
                              CAR ;
                              CDR ;
                              CDR ;
                              DIG 2 ;
                              CDR ;
                              PAIR ;
                              PAIR } } ;
                     UNPAIR ;
                     UNPAIR ;
                     DIG 2 ;
                     UNPAIR ;
                     DUP 7 ;
                     DUP 8 ;
                     CAR ;
                     DUP ;
                     CAR ;
                     DUP ;
                     CDR ;
                     DIG 7 ;
                     UPDATE 2 ;
                     UPDATE 2 ;
                     UPDATE 1 ;
                     UPDATE 1 ;
                     DUP ;
                     CAR ;
                     DUP ;
                     CDR ;
                     DUP ;
                     CDR ;
                     DIG 5 ;
                     UPDATE 2 ;
                     UPDATE 2 ;
                     UPDATE 2 ;
                     UPDATE 1 ;
                     DUP ;
                     CAR ;
                     DUP ;
                     CAR ;
                     DUP ;
                     CAR ;
                     DIG 8 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     PUSH nat 1 ;
                     DIG 7 ;
                     DIG 9 ;
                     GET 7 ;
                     PAIR ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     UPDATE 2 ;
                     UPDATE 1 ;
                     UPDATE 1 ;
                     UPDATE 1 ;
                     SWAP ;
                     UPDATE 2 ;
                     SWAP } } } ;
         PAIR } ;
  view "get_balance"
       (pair address nat)
       nat
       { UNPAIR ;
         UNPAIR ;
         DIG 2 ;
         CAR ;
         CAR ;
         CAR ;
         CDR ;
         DUG 2 ;
         PAIR ;
         GET ;
         IF_NONE { PUSH nat 0 } {} } }
