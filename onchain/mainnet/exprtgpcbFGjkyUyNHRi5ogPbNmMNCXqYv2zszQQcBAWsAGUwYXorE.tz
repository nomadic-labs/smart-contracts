{ parameter
    (or (or (pair %addLiquidity
               (address %dexAddress)
               (pair (nat %tokenAmount) (mutez %xtzAmountToLP)))
            (pair %launchDexAndAddLiquidity
               (address %dexAddress)
               (pair (nat %tokenAmount) (mutez %xtzAmountToLP))))
        (or (address %setTokenPoolAddress)
            (or (nat %withdrawEarningAndLeftover)
                (pair %withdrawLPTokens
                   (address %tokenAddress)
                   (pair (nat %tokenAmount) (nat %tokenId)))))) ;
  storage
    (pair (pair (timestamp %LPLockupTime) (address %admin))
          (pair (pair %contracts (address %adminFactory) (option %pool address))
                (pair (bool %isLiquidityProvided) (pair %token (nat %ID) (address %address))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DUP 2 ;
                 GET 3 ;
                 CDR ;
                 IF_NONE { PUSH int 611 ; FAILWITH } {} ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_TOKEN_POOL" ; FAILWITH } ;
                 NIL operation ;
                 DUP 3 ;
                 GET 8 ;
                 CONTRACT %update_operators
                   (list (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                             (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))) ;
                 IF_NONE { PUSH int 621 ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 NIL (or (pair address (pair address nat)) (pair address (pair address nat))) ;
                 DUP 6 ;
                 GET 7 ;
                 DUP 6 ;
                 CAR ;
                 SELF_ADDRESS ;
                 PAIR 3 ;
                 LEFT (pair address (pair address nat)) ;
                 CONS ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DUP 2 ;
                 CAR ;
                 CONTRACT %investLiquidity nat ;
                 IF_NONE { PUSH int 638 ; FAILWITH } {} ;
                 DIG 2 ;
                 DUP ;
                 GET 4 ;
                 SWAP ;
                 GET 3 ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 SWAP ;
                 PUSH bool True ;
                 UPDATE 5 ;
                 SWAP }
               { DUP 2 ;
                 GET 3 ;
                 CDR ;
                 IF_NONE { PUSH int 567 ; FAILWITH } {} ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_TOKEN_POOL" ; FAILWITH } ;
                 NIL operation ;
                 DUP 3 ;
                 GET 8 ;
                 CONTRACT %update_operators
                   (list (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                             (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))) ;
                 IF_NONE { PUSH int 579 ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 NIL (or (pair address (pair address nat)) (pair address (pair address nat))) ;
                 DUP 6 ;
                 GET 7 ;
                 DUP 6 ;
                 CAR ;
                 SELF_ADDRESS ;
                 PAIR 3 ;
                 LEFT (pair address (pair address nat)) ;
                 CONS ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DUP 2 ;
                 CAR ;
                 CONTRACT %launchExchange (pair (pair %token address nat) (nat %token_amount)) ;
                 IF_NONE { PUSH int 599 ; FAILWITH } {} ;
                 DIG 2 ;
                 DUP ;
                 GET 4 ;
                 SWAP ;
                 GET 3 ;
                 DUP 5 ;
                 GET 7 ;
                 DUP 6 ;
                 GET 8 ;
                 PAIR ;
                 PAIR ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 SWAP ;
                 PUSH bool True ;
                 UPDATE 5 ;
                 SWAP } }
           { IF_LEFT
               { DUP 2 ;
                 GET 3 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_FACTORY" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 GET 3 ;
                 DIG 2 ;
                 SOME ;
                 UPDATE 2 ;
                 UPDATE 3 ;
                 NIL operation }
               { IF_LEFT
                   { SENDER ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     PUSH bool True ;
                     DUP 3 ;
                     GET 5 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "LIQUIDITY_NOT_PROVIDED" ; FAILWITH } ;
                     NIL operation ;
                     SENDER ;
                     CONTRACT unit ;
                     IF_NONE { PUSH string "Not enough Funds / Transfer Error" ; FAILWITH } {} ;
                     BALANCE ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     DUP 3 ;
                     GET 8 ;
                     CONTRACT %transfer
                       (list (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                     IF_NONE { PUSH int 688 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     NIL (pair address (pair nat nat)) ;
                     DIG 5 ;
                     DUP 7 ;
                     GET 7 ;
                     SENDER ;
                     PAIR 3 ;
                     CONS ;
                     SELF_ADDRESS ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SENDER ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     PUSH bool True ;
                     DUP 3 ;
                     GET 5 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "LIQUIDITY_NOT_PROVIDED" ; FAILWITH } ;
                     DUP 2 ;
                     CAR ;
                     CAR ;
                     NOW ;
                     COMPARE ;
                     GT ;
                     IF {} { PUSH string "NOT_UNLOCKED" ; FAILWITH } ;
                     NIL operation ;
                     DUP 2 ;
                     CAR ;
                     CONTRACT %transfer
                       (list (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                     IF_NONE { PUSH int 688 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     NIL (pair address (pair nat nat)) ;
                     DIG 5 ;
                     DUP ;
                     GET 3 ;
                     SWAP ;
                     GET 4 ;
                     SENDER ;
                     PAIR 3 ;
                     CONS ;
                     SELF_ADDRESS ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS ;
                     CONS } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
