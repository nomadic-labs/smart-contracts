{ parameter
    (or (or (or (nat %add_funds)
                (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin)))
            (or (list %record_members (pair (address %address) (string %username)))
                (list %record_votes (pair (address %voter) (string %asset_id)))))
        (or (or (nat %update_member_reward) (nat %update_vote_reward)) (nat %withdraw_funds))) ;
  storage
    (pair (option %admin
             (pair (pair (address %admin) (bool %paused)) (option %pending_admin address)))
          (nat %funds)
          (nat %vote_reward)
          (nat %member_reward)
          (list %recorded_votes (pair (address %voter) (string %asset_id)))
          (list %recorded_members (pair (address %address) (string %username)))
          (address %token_address)
          (nat %token_id)) ;
  code { LAMBDA
           (pair (pair (pair address nat) (pair nat address)) address)
           operation
           { UNPAIR ;
             UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DIG 2 ;
             CONTRACT %transfer
               (list (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))) ;
             IF_NONE
               { DROP 4 ; PUSH string "CANNOT_INVOKE_FA2_TRANSFER" ; FAILWITH }
               { PUSH mutez 0 ;
                 NIL (pair address (list (pair address nat nat))) ;
                 NIL (pair address nat nat) ;
                 DIG 4 ;
                 DIG 6 ;
                 DIG 7 ;
                 PAIR 3 ;
                 CONS ;
                 DIG 4 ;
                 PAIR ;
                 CONS ;
                 TRANSFER_TOKENS } } ;
         LAMBDA
           (option (pair (pair address bool) (option address)))
           unit
           { IF_NONE
               { UNIT }
               { CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } } ;
         LAMBDA
           (option (pair (pair address bool) (option address)))
           unit
           { IF_NONE
               { UNIT }
               { CAR ; CDR ; IF { PUSH string "PAUSED" ; FAILWITH } { UNIT } } } ;
         DIG 3 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DIG 2 ;
                 DROP ;
                 IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SELF_ADDRESS ;
                     SENDER ;
                     DUP 3 ;
                     PAIR ;
                     DUP 4 ;
                     GET 14 ;
                     DUP 5 ;
                     GET 13 ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DUP 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     GET 3 ;
                     ADD ;
                     UPDATE 3 ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { DIG 3 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SWAP ;
                     IF_LEFT
                       { IF_LEFT
                           { DIG 3 ;
                             DROP 2 ;
                             IF_NONE
                               { PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { DUP ;
                                 CDR ;
                                 IF_NONE
                                   { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                                   { SENDER ;
                                     COMPARE ;
                                     EQ ;
                                     IF { NONE address ; SWAP ; CAR ; CDR ; SENDER ; PAIR ; PAIR ; SOME }
                                        { DROP ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             DIG 4 ;
                             SWAP ;
                             EXEC ;
                             DROP ;
                             SWAP ;
                             IF_NONE
                               { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { DUP ; CDR ; DUG 2 ; CAR ; CAR ; PAIR ; PAIR ; SOME } ;
                             NIL operation ;
                             PAIR } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         IF_NONE
                           { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                           { SWAP ; SOME ; SWAP ; CAR ; PAIR ; SOME } ;
                         NIL operation ;
                         PAIR } ;
                     UNPAIR ;
                     DUG 2 ;
                     UPDATE 1 ;
                     SWAP ;
                     PAIR } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH nat 0 ;
                     NIL operation ;
                     DUP 4 ;
                     GET 14 ;
                     DUP 5 ;
                     GET 13 ;
                     DUP 6 ;
                     GET 7 ;
                     DUP 7 ;
                     GET 11 ;
                     PAIR 6 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     ITER { DUP ;
                            DUG 2 ;
                            PUSH bool False ;
                            PAIR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            ITER { SWAP ;
                                   UNPAIR ;
                                   DUG 2 ;
                                   DUP ;
                                   DUG 3 ;
                                   COMPARE ;
                                   EQ ;
                                   OR ;
                                   IF { PUSH bool True ; PAIR } { PUSH bool False ; PAIR } } ;
                            CAR ;
                            NOT ;
                            IF { DUP ;
                                 GET 3 ;
                                 SWAP ;
                                 DUP ;
                                 DUP ;
                                 DUG 3 ;
                                 GET 9 ;
                                 DIG 4 ;
                                 CAR ;
                                 SELF_ADDRESS ;
                                 DUP 5 ;
                                 PAIR ;
                                 DUP 6 ;
                                 GET 7 ;
                                 DUP 7 ;
                                 GET 5 ;
                                 PAIR ;
                                 PAIR ;
                                 PAIR ;
                                 DUP 8 ;
                                 SWAP ;
                                 EXEC ;
                                 CONS ;
                                 UPDATE 9 ;
                                 SWAP ;
                                 DIG 2 ;
                                 GET 10 ;
                                 ADD ;
                                 UPDATE 10 }
                               { SWAP ; DROP } } ;
                     DIG 3 ;
                     DROP ;
                     DUP 3 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 10 ;
                     COMPARE ;
                     LE ;
                     IF { DUP 3 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 10 ;
                          DUP 5 ;
                          GET 3 ;
                          SUB ;
                          ABS ;
                          UPDATE 3 ;
                          DIG 2 ;
                          DIG 3 ;
                          GET 11 ;
                          ITER { CONS } ;
                          UPDATE 11 ;
                          SWAP ;
                          GET 9 ;
                          PAIR }
                        { DROP 3 ; PUSH string "NO_FUNDS" ; FAILWITH } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH nat 0 ;
                     NIL operation ;
                     DUP 4 ;
                     GET 14 ;
                     DUP 5 ;
                     GET 13 ;
                     DUP 6 ;
                     GET 5 ;
                     DUP 7 ;
                     GET 9 ;
                     PAIR 6 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     ITER { DUP ;
                            DUG 2 ;
                            PUSH bool False ;
                            PAIR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            ITER { SWAP ;
                                   UNPAIR ;
                                   DUG 2 ;
                                   DUP ;
                                   DUG 3 ;
                                   COMPARE ;
                                   EQ ;
                                   OR ;
                                   IF { PUSH bool True ; PAIR } { PUSH bool False ; PAIR } } ;
                            CAR ;
                            NOT ;
                            IF { DUP ;
                                 GET 3 ;
                                 SWAP ;
                                 DUP ;
                                 DUP ;
                                 DUG 3 ;
                                 GET 9 ;
                                 DIG 4 ;
                                 CAR ;
                                 SELF_ADDRESS ;
                                 DUP 5 ;
                                 PAIR ;
                                 DUP 6 ;
                                 GET 7 ;
                                 DUP 7 ;
                                 GET 5 ;
                                 PAIR ;
                                 PAIR ;
                                 PAIR ;
                                 DUP 8 ;
                                 SWAP ;
                                 EXEC ;
                                 CONS ;
                                 UPDATE 9 ;
                                 SWAP ;
                                 DIG 2 ;
                                 GET 10 ;
                                 ADD ;
                                 UPDATE 10 }
                               { SWAP ; DROP } } ;
                     DIG 3 ;
                     DROP ;
                     DUP 3 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 10 ;
                     COMPARE ;
                     LE ;
                     IF { DUP 3 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 10 ;
                          DUP 5 ;
                          GET 3 ;
                          SUB ;
                          ABS ;
                          UPDATE 3 ;
                          DIG 2 ;
                          DIG 3 ;
                          GET 9 ;
                          ITER { CONS } ;
                          UPDATE 9 ;
                          SWAP ;
                          GET 9 ;
                          PAIR }
                        { DROP 3 ; PUSH string "NO_FUNDS" ; FAILWITH } } } }
           { DIG 2 ;
             DROP ;
             IF_LEFT
               { DIG 3 ;
                 DROP ;
                 IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     UPDATE 7 ;
                     NIL operation ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     UPDATE 5 ;
                     NIL operation ;
                     PAIR } }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SENDER ;
                 SELF_ADDRESS ;
                 DUP 3 ;
                 PAIR ;
                 DUP 4 ;
                 GET 14 ;
                 DUP 5 ;
                 GET 13 ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DUP 3 ;
                 DIG 2 ;
                 DIG 3 ;
                 GET 3 ;
                 SUB ;
                 ABS ;
                 UPDATE 3 ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR } } } }
