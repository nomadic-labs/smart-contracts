{ storage
    (pair (address %admin)
          (pair (nat %channelId)
                (pair (big_map %channels
                         nat
                         (pair (string %ipfsHash)
                               (pair (set %managers address) (pair (address %owner) (nat %totalSubscribers)))))
                      (pair (big_map %metadata string bytes) (big_map %subscribers address (set nat)))))) ;
  parameter
    (or (or (or (pair %addManager (nat %channelId) (address %manager))
                (pair %createChannel (string %ipfsHash) (set %managers address)))
            (or (nat %deleteChannel) (pair %removeManager (nat %channelId) (address %manager))))
        (or (or (pair %selectiveNotifications
                   (nat %channelId)
                   (pair (string %description)
                         (pair (string %ipfsHash)
                               (pair (list %receivers address) (pair (string %title) (string %type))))))
                (pair %sendNotifications
                   (nat %channelId)
                   (pair (string %description)
                         (pair (string %ipfsHash) (pair (string %title) (string %type))))))
            (or (address %setAdmin)
                (or (pair %setOwner (nat %channelId) (address %owner))
                    (list %subscribeOrUnsubscribe (pair (nat %channelId) (bool %subscribedOrNot))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH int 82 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { PUSH bool True }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 5 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          GET ;
                          IF_NONE { PUSH int 82 ; FAILWITH } {} ;
                          GET 3 ;
                          SENDER ;
                          MEM } ;
                     IF {} { PUSH string "NOT_OWNER_OR_MANAGER" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 5 ;
                     DUP ;
                     DUP 4 ;
                     CAR ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 86 ; FAILWITH } {} ;
                     DUP ;
                     GET 3 ;
                     PUSH bool True ;
                     DIG 6 ;
                     CDR ;
                     UPDATE ;
                     UPDATE 3 ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     UPDATE 5 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     DUP ;
                     GET 5 ;
                     PUSH nat 0 ;
                     SENDER ;
                     DIG 4 ;
                     DUP ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 6 ;
                     CAR ;
                     PAIR 4 ;
                     DIG 3 ;
                     DROP ;
                     SOME ;
                     DIG 3 ;
                     GET 3 ;
                     UPDATE ;
                     UPDATE 5 ;
                     DUP ;
                     GET 3 ;
                     PUSH nat 1 ;
                     ADD ;
                     UPDATE 3 } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 105 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 5 ;
                     NONE (pair string (pair (set address) (pair address nat))) ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 5 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH int 95 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { PUSH bool True }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 5 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          GET ;
                          IF_NONE { PUSH int 95 ; FAILWITH } {} ;
                          GET 3 ;
                          SENDER ;
                          MEM } ;
                     IF {} { PUSH string "NOT_OWNER_OR_MANAGER" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 5 ;
                     DUP ;
                     DUP 4 ;
                     CAR ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 99 ; FAILWITH } {} ;
                     DUP ;
                     GET 3 ;
                     PUSH bool False ;
                     DIG 6 ;
                     CDR ;
                     UPDATE ;
                     UPDATE 3 ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     UPDATE 5 } } ;
             NIL operation }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH int 165 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { PUSH bool True }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 5 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          GET ;
                          IF_NONE { PUSH int 165 ; FAILWITH } {} ;
                          GET 3 ;
                          SENDER ;
                          MEM } ;
                     IF {} { PUSH string "NOT_OWNER_OR_MANAGER" ; FAILWITH } ;
                     DUP ;
                     GET 7 ;
                     ITER { DUP 3 ;
                            GET 8 ;
                            SWAP ;
                            GET ;
                            IF_NONE { PUSH int 171 ; FAILWITH } {} ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            MEM ;
                            IF {} { PUSH string "NOT_SUBSCRIBER" ; FAILWITH } } ;
                     DROP ;
                     NIL operation }
                   { PUSH mutez 1000000 ;
                     AMOUNT ;
                     COMPARE ;
                     GE ;
                     IF {} { PUSH string "MINIMUM_AMOUNT_TO_SEND_1_TEZ" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH int 140 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { DROP ; PUSH bool True }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 5 ;
                          SWAP ;
                          CAR ;
                          GET ;
                          IF_NONE { PUSH int 140 ; FAILWITH } {} ;
                          GET 3 ;
                          SENDER ;
                          MEM } ;
                     IF {} { PUSH string "NOT_OWNER_OR_MANAGER" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     CONTRACT unit ;
                     IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                     NIL operation ;
                     SWAP ;
                     AMOUNT ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     CONS } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 1 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 72 ; FAILWITH } {} ;
                         GET 5 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 5 ;
                         DUP ;
                         DUP 4 ;
                         CAR ;
                         DUP ;
                         DUG 2 ;
                         GET ;
                         IF_NONE { PUSH int 73 ; FAILWITH } {} ;
                         DIG 4 ;
                         CDR ;
                         UPDATE 5 ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 5 }
                       { DUP ;
                         ITER { DUP ;
                                CDR ;
                                IF { DUP 3 ;
                                     GET 8 ;
                                     SENDER ;
                                     MEM ;
                                     IF {}
                                        { DIG 2 ;
                                          DUP ;
                                          GET 8 ;
                                          PUSH (option (set nat)) (Some {}) ;
                                          SENDER ;
                                          UPDATE ;
                                          UPDATE 8 ;
                                          DUG 2 } ;
                                     DUP 3 ;
                                     GET 8 ;
                                     SENDER ;
                                     GET ;
                                     IF_NONE { PUSH int 120 ; FAILWITH } {} ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     CAR ;
                                     MEM ;
                                     IF { DROP }
                                        { DIG 2 ;
                                          DUP ;
                                          GET 5 ;
                                          DUP ;
                                          DUP 4 ;
                                          CAR ;
                                          DUP ;
                                          DUG 2 ;
                                          GET ;
                                          IF_NONE { PUSH int 121 ; FAILWITH } {} ;
                                          DUP ;
                                          GET 6 ;
                                          PUSH nat 1 ;
                                          ADD ;
                                          UPDATE 6 ;
                                          SOME ;
                                          SWAP ;
                                          UPDATE ;
                                          UPDATE 5 ;
                                          DUP ;
                                          GET 8 ;
                                          DUP ;
                                          SENDER ;
                                          DUP ;
                                          DUG 2 ;
                                          GET ;
                                          IF_NONE { PUSH int 122 ; FAILWITH } {} ;
                                          PUSH bool True ;
                                          DIG 5 ;
                                          CAR ;
                                          UPDATE ;
                                          SOME ;
                                          SWAP ;
                                          UPDATE ;
                                          UPDATE 8 ;
                                          SWAP } }
                                   { DIG 2 ;
                                     DUP ;
                                     GET 8 ;
                                     DUP ;
                                     SENDER ;
                                     DUP ;
                                     DUG 2 ;
                                     GET ;
                                     IF_NONE { PUSH int 125 ; FAILWITH } {} ;
                                     PUSH bool False ;
                                     DUP 6 ;
                                     CAR ;
                                     UPDATE ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     UPDATE 8 ;
                                     DUP ;
                                     DUG 3 ;
                                     DUP ;
                                     GET 5 ;
                                     DUP ;
                                     DUP 4 ;
                                     CAR ;
                                     DUP ;
                                     DUG 2 ;
                                     GET ;
                                     IF_NONE { PUSH int 126 ; FAILWITH } {} ;
                                     PUSH nat 1 ;
                                     DIG 7 ;
                                     GET 5 ;
                                     DIG 6 ;
                                     CAR ;
                                     GET ;
                                     IF_NONE { PUSH int 126 ; FAILWITH } {} ;
                                     GET 6 ;
                                     SUB ;
                                     ISNAT ;
                                     IF_NONE { PUSH int 126 ; FAILWITH } {} ;
                                     UPDATE 6 ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     UPDATE 5 ;
                                     SWAP } } ;
                         DROP } } ;
                 NIL operation } } ;
         PAIR } }
