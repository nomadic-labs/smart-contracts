{ parameter
    (or (or (or (or (or (pair %balance_of
                           (list %requests (pair (address %owner) (nat %token_id)))
                           (contract %callback
                              (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                        (address %declare_ownership))
                    (or (unit %claim_ownership) (address %update_minter)))
                (or (or (unit %remove_minter) (unit %pause))
                    (or (unit %unpause) (pair %set_metadata (string %k) (option %d bytes)))))
            (or (or (or (pair %set_token_metadata (nat %tid) (map %tdata string bytes))
                        (address %set_permits))
                    (or (list %update_operators
                           (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                               (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                        (list %update_operators_for_all
                           (or (address %add_for_all) (address %remove_for_all)))))
                (or (or (list %do_transfer
                           (pair (address %from_)
                                 (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                        (list %transfer_gasless
                           (pair (list %transfer_params
                                    (pair (address %from_)
                                          (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                                 (pair (key %user_pk) (signature %user_sig)))))
                    (or (list %transfer
                           (pair (address %from_)
                                 (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                        (pair %mint (address %tow) (pair (nat %tid) (nat %nbt)))))))
        (pair %burn (nat %tid) (nat %nbt))) ;
  storage
    (pair (address %owner)
          (pair (address %minter)
                (pair (address %permits)
                      (pair (option %owner_candidate address)
                            (pair (bool %paused)
                                  (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                                        (pair (big_map %ledger (pair address nat) nat)
                                              (pair (big_map %operator (pair address (pair nat address)) unit)
                                                    (pair (big_map %operator_for_all (pair address address) unit)
                                                          (big_map %metadata string bytes)))))))))) ;
  code { LAMBDA
           bool
           bool
           { PUSH unit Unit ;
             DUP 2 ;
             IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
             PUSH bool True ;
             SWAP ;
             DROP ;
             DUG 1 ;
             DROP } ;
         LAMBDA
           (pair string
                 (list (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
           (option address)
           { UNPAIR 2 ;
             PUSH unit Unit ;
             DUP 3 ;
             IF_CONS
               { DUP ;
                 CAR ;
                 DUP 3 ;
                 ITER { DUP ;
                        CAR ;
                        DUP 3 ;
                        COMPARE ;
                        EQ ;
                        NOT ;
                        IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} ;
                        DROP } ;
                 DUP ;
                 SOME ;
                 DIP { DIG 3 ; DROP } ;
                 DUG 3 ;
                 DROP 3 }
               { NONE address ; SWAP ; DROP } ;
             DUG 2 ;
             DROP 2 } ;
         LAMBDA
           (pair (big_map (pair address address) unit)
                 (pair (big_map (pair address (pair nat address)) unit)
                       (list (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))))
           bool
           { UNPAIR 3 ;
             PUSH unit Unit ;
             PUSH bool True ;
             DUP 5 ;
             ITER { DUP ;
                    CAR ;
                    DUP 2 ;
                    CDR ;
                    DUP ;
                    ITER { DUP 5 ;
                           IF { DUP 3 ;
                                SENDER ;
                                COMPARE ;
                                NEQ ;
                                IF { DUP 8 ;
                                     DUP 4 ;
                                     DUP 3 ;
                                     CDR ;
                                     CAR ;
                                     PAIR ;
                                     SENDER ;
                                     PAIR ;
                                     MEM ;
                                     IF { PUSH bool True }
                                        { DUP 7 ;
                                          DUP 4 ;
                                          SENDER ;
                                          PAIR ;
                                          MEM ;
                                          IF { PUSH bool True } { PUSH bool False } } }
                                   { PUSH bool True } ;
                                IF { PUSH bool True } { PUSH bool False } }
                              { PUSH bool False } ;
                           DIP { DIG 4 ; DROP } ;
                           DUG 4 ;
                           DROP } ;
                    DROP 3 } ;
             DUP ;
             DIP { DIG 1 ; DROP } ;
             DUG 1 ;
             DROP ;
             DUG 3 ;
             DROP 3 } ;
         LAMBDA
           (pair address
                 (list (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
           bool
           { UNPAIR 2 ;
             PUSH unit Unit ;
             PUSH bool True ;
             DUP 4 ;
             ITER { DUP 2 ;
                    IF { DUP ;
                         CAR ;
                         DUP 5 ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH bool True } { PUSH bool False } }
                       { PUSH bool False } ;
                    DIP { DIG 1 ; DROP } ;
                    DUG 1 ;
                    DROP } ;
             DUP ;
             DIP { DIG 1 ; DROP } ;
             DUG 1 ;
             DROP ;
             DUG 2 ;
             DROP 2 } ;
         NIL operation ;
         DIG 5 ;
         UNPAIR ;
         DIP { UNPAIR 10 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { UNPAIR ;
                             NIL operation ;
                             NIL operation ;
                             DUP 15 ;
                             ITER { CONS } ;
                             DUP 4 ;
                             AMOUNT ;
                             DUP 5 ;
                             MAP { PUSH nat 0 ;
                                   DUP 2 ;
                                   CDR ;
                                   COMPARE ;
                                   NEQ ;
                                   IF { PUSH nat 0 }
                                      { DUP 14 ;
                                        DUP 2 ;
                                        CDR ;
                                        DUP 3 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} } ;
                                   DUP ;
                                   DUP 3 ;
                                   PAIR ;
                                   DIP { DROP } ;
                                   SWAP ;
                                   DROP } ;
                             TRANSFER_TOKENS ;
                             CONS ;
                             ITER { CONS } ;
                             DIP { DIG 12 ; DROP } ;
                             DUG 12 ;
                             DROP 2 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { DUP 2 ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP ;
                             SOME ;
                             DIP { DIG 4 ; DROP } ;
                             DUG 4 ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } }
                       { IF_LEFT
                           { DROP ;
                             DUP 4 ;
                             IF_NONE
                               { PUSH bool False }
                               { SENDER ; DUP 2 ; COMPARE ; EQ ; SWAP ; DROP } ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             SENDER ;
                             SWAP ;
                             DROP ;
                             NONE address ;
                             DIP { DIG 3 ; DROP } ;
                             DUG 3 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { DUP 2 ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 16 ;
                             DUP 7 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "r2" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                             DUP ;
                             DIP { DIG 2 ; DROP } ;
                             DUG 2 ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } } }
                   { IF_LEFT
                       { IF_LEFT
                           { DROP ;
                             DUP ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 15 ;
                             DUP 6 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "r3" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH } {} ;
                             DUP ;
                             DIP { DIG 1 ; DROP } ;
                             DUG 1 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { DROP ;
                             DUP ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 15 ;
                             DUP 6 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "pausable_r1" ;
                                  PUSH string "INVALID_CONDITION" ;
                                  PAIR ;
                                  FAILWITH }
                                {} ;
                             PUSH bool True ;
                             DIP { DIG 4 ; DROP } ;
                             DUG 4 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } }
                       { IF_LEFT
                           { DROP ;
                             DUP ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 5 ;
                             NOT ;
                             IF { PUSH string "CONTRACT_NOT_PAUSED" ; FAILWITH } {} ;
                             PUSH bool False ;
                             DIP { DIG 4 ; DROP } ;
                             DUG 4 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { UNPAIR ;
                             SWAP ;
                             DUP 3 ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 17 ;
                             DUP 8 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "md_r1" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP 12 ;
                             DUP 2 ;
                             DUP 4 ;
                             UPDATE ;
                             DIP { DIG 11 ; DROP } ;
                             DUG 11 ;
                             DROP 2 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } } } }
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { UNPAIR ;
                             SWAP ;
                             DUP 3 ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 17 ;
                             DUP 8 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "tmd_r1" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP 8 ;
                             DUP 2 ;
                             DUP 4 ;
                             PAIR ;
                             SOME ;
                             DUP 4 ;
                             UPDATE ;
                             DIP { DIG 7 ; DROP } ;
                             DUG 7 ;
                             DROP 2 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { DUP 2 ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 16 ;
                             DUP 7 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "p_r1" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP ;
                             DIP { DIG 3 ; DROP } ;
                             DUG 3 ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } }
                       { IF_LEFT
                           { DUP 16 ;
                             DUP 7 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "fa2_r1" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP ;
                             ITER { DUP ;
                                    IF_LEFT
                                      { SENDER ;
                                        DUP 2 ;
                                        CAR ;
                                        COMPARE ;
                                        EQ ;
                                        NOT ;
                                        IF { PUSH string "CALLER_NOT_OWNER" ; FAILWITH } {} ;
                                        DUP 11 ;
                                        PUSH unit Unit ;
                                        SOME ;
                                        DUP 3 ;
                                        CAR ;
                                        DUP 4 ;
                                        CDR ;
                                        CDR ;
                                        PAIR ;
                                        DUP 4 ;
                                        CDR ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 10 ; DROP } ;
                                        DUG 10 ;
                                        DROP }
                                      { SENDER ;
                                        DUP 2 ;
                                        CAR ;
                                        COMPARE ;
                                        EQ ;
                                        NOT ;
                                        IF { PUSH string "CALLER_NOT_OWNER" ; FAILWITH } {} ;
                                        DUP 11 ;
                                        NONE unit ;
                                        DUP 3 ;
                                        CAR ;
                                        DUP 4 ;
                                        CDR ;
                                        CDR ;
                                        PAIR ;
                                        DUP 4 ;
                                        CDR ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 10 ; DROP } ;
                                        DUG 10 ;
                                        DROP } ;
                                    DROP } ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { DUP 16 ;
                             DUP 7 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "fa2_r2" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP ;
                             ITER { DUP ;
                                    IF_LEFT
                                      { DUP 12 ;
                                        PUSH unit Unit ;
                                        SOME ;
                                        SENDER ;
                                        DUP 4 ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 11 ; DROP } ;
                                        DUG 11 ;
                                        DROP }
                                      { DUP 12 ;
                                        NONE unit ;
                                        SENDER ;
                                        DUP 4 ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 11 ; DROP } ;
                                        DUG 11 ;
                                        DROP } ;
                                    DROP } ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } } }
                   { IF_LEFT
                       { IF_LEFT
                           { SELF_ADDRESS ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP ;
                             ITER { DUP ;
                                    CAR ;
                                    DUP 2 ;
                                    CDR ;
                                    DUP ;
                                    ITER { DUP ;
                                           CDR ;
                                           CAR ;
                                           DUP 13 ;
                                           DUP 2 ;
                                           DUP 6 ;
                                           PAIR ;
                                           GET ;
                                           IF_NONE { NONE nat } { DUP ; SOME ; SWAP ; DROP } ;
                                           IF_NONE { PUSH nat 0 } {} ;
                                           DUP 3 ;
                                           CDR ;
                                           CDR ;
                                           INT ;
                                           DUP 2 ;
                                           INT ;
                                           SUB ;
                                           ISNAT ;
                                           IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                           PUSH nat 0 ;
                                           DUP 2 ;
                                           COMPARE ;
                                           EQ ;
                                           IF { DUP 15 ;
                                                NONE nat ;
                                                DUP 5 ;
                                                DUP 9 ;
                                                PAIR ;
                                                UPDATE ;
                                                DIP { DIG 14 ; DROP } ;
                                                DUG 14 }
                                              { DUP 15 ;
                                                DUP 2 ;
                                                SOME ;
                                                DUP 5 ;
                                                DUP 9 ;
                                                PAIR ;
                                                UPDATE ;
                                                DIP { DIG 14 ; DROP } ;
                                                DUG 14 } ;
                                           DUP 15 ;
                                           DUP 4 ;
                                           DUP 6 ;
                                           CAR ;
                                           PAIR ;
                                           MEM ;
                                           IF { DUP 15 ;
                                                DUP 4 ;
                                                DUP 6 ;
                                                CAR ;
                                                PAIR ;
                                                GET ;
                                                IF_NONE
                                                  { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                                                  {} ;
                                                DUP 16 ;
                                                DUP 6 ;
                                                CDR ;
                                                CDR ;
                                                DUP 3 ;
                                                ADD ;
                                                SOME ;
                                                DUP 6 ;
                                                DUP 8 ;
                                                CAR ;
                                                PAIR ;
                                                UPDATE ;
                                                DIP { DIG 15 ; DROP } ;
                                                DUG 15 ;
                                                DROP }
                                              { DUP 15 ;
                                                DUP 5 ;
                                                CDR ;
                                                CDR ;
                                                PUSH nat 0 ;
                                                ADD ;
                                                SOME ;
                                                DUP 5 ;
                                                DUP 7 ;
                                                CAR ;
                                                PAIR ;
                                                UPDATE ;
                                                DIP { DIG 14 ; DROP } ;
                                                DUG 14 } ;
                                           DROP 4 } ;
                                    DROP 3 } ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { DUP 16 ;
                             DUP 7 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "fa2_r3" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP ;
                             ITER { DUP ;
                                    CAR ;
                                    DUP 2 ;
                                    CDR ;
                                    CAR ;
                                    DUP 3 ;
                                    CDR ;
                                    CDR ;
                                    DUP 2 ;
                                    HASH_KEY ;
                                    IMPLICIT_ACCOUNT ;
                                    ADDRESS ;
                                    DUP 18 ;
                                    DUP 5 ;
                                    DUP 3 ;
                                    PAIR ;
                                    EXEC ;
                                    NOT ;
                                    IF { PUSH string "SIGNER_NOT_FROM" ; FAILWITH } {} ;
                                    NIL operation ;
                                    NIL operation ;
                                    DUP 19 ;
                                    ITER { CONS } ;
                                    DUP 11 ;
                                    CONTRACT %check (pair key (pair signature bytes)) ;
                                    IF_NONE
                                      { PUSH string "check" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                                      {} ;
                                    PUSH mutez 0 ;
                                    DUP 8 ;
                                    PACK ;
                                    DUP 7 ;
                                    PAIR ;
                                    DUP 8 ;
                                    PAIR ;
                                    TRANSFER_TOKENS ;
                                    CONS ;
                                    ITER { CONS } ;
                                    DIP { DIG 16 ; DROP } ;
                                    DUG 16 ;
                                    NIL operation ;
                                    NIL operation ;
                                    DUP 19 ;
                                    ITER { CONS } ;
                                    SELF_ADDRESS ;
                                    CONTRACT %do_transfer (list (pair address (list (pair address (pair nat nat))))) ;
                                    IF_NONE
                                      { PUSH string "do_transfer" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                                      {} ;
                                    PUSH mutez 0 ;
                                    DUP 8 ;
                                    TRANSFER_TOKENS ;
                                    CONS ;
                                    ITER { CONS } ;
                                    DIP { DIG 16 ; DROP } ;
                                    DUG 16 ;
                                    DROP 5 } ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } }
                       { IF_LEFT
                           { DUP 16 ;
                             DUP 7 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "fa2_r4" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP 14 ;
                             DUP 2 ;
                             DUP 11 ;
                             PAIR ;
                             DUP 12 ;
                             PAIR ;
                             EXEC ;
                             NOT ;
                             IF { DUP 15 ;
                                  DUP 2 ;
                                  PUSH string "FA2_NOT_OPERATOR" ;
                                  PAIR ;
                                  EXEC ;
                                  IF_NONE
                                    {}
                                    { NIL operation ;
                                      NIL operation ;
                                      DUP 15 ;
                                      ITER { CONS } ;
                                      DUP 7 ;
                                      CONTRACT %consume (pair address (pair bytes string)) ;
                                      IF_NONE
                                        { PUSH string "consume" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                                        {} ;
                                      PUSH mutez 0 ;
                                      PUSH string "FA2_NOT_OPERATOR" ;
                                      DUP 7 ;
                                      PACK ;
                                      PAIR ;
                                      DUP 6 ;
                                      PAIR ;
                                      TRANSFER_TOKENS ;
                                      CONS ;
                                      ITER { CONS } ;
                                      DIP { DIG 12 ; DROP } ;
                                      DUG 12 ;
                                      DROP } }
                                {} ;
                             NIL operation ;
                             NIL operation ;
                             DUP 14 ;
                             ITER { CONS } ;
                             SELF_ADDRESS ;
                             CONTRACT %do_transfer (list (pair address (list (pair address (pair nat nat))))) ;
                             IF_NONE
                               { PUSH string "do_transfer" ; PUSH string "ENTRY_NOT_FOUND" ; PAIR ; FAILWITH }
                               {} ;
                             PUSH mutez 0 ;
                             DUP 5 ;
                             TRANSFER_TOKENS ;
                             CONS ;
                             ITER { CONS } ;
                             DIP { DIG 11 ; DROP } ;
                             DUG 11 ;
                             DROP ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR }
                           { UNPAIR ;
                             SWAP ;
                             UNPAIR ;
                             SWAP ;
                             DUP 5 ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "INVALID_CALLER" ; FAILWITH } {} ;
                             DUP 18 ;
                             DUP 9 ;
                             EXEC ;
                             NOT ;
                             IF { PUSH string "fa2_r5" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                                {} ;
                             DUP 10 ;
                             DUP 3 ;
                             DUP 5 ;
                             PAIR ;
                             MEM ;
                             IF { DUP 10 ;
                                  DUP 3 ;
                                  DUP 5 ;
                                  PAIR ;
                                  GET ;
                                  IF_NONE
                                    { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                                    {} ;
                                  DUP 11 ;
                                  DUP 3 ;
                                  DUP 3 ;
                                  ADD ;
                                  SOME ;
                                  DUP 5 ;
                                  DUP 7 ;
                                  PAIR ;
                                  UPDATE ;
                                  DIP { DIG 10 ; DROP } ;
                                  DUG 10 ;
                                  DROP }
                                { DUP 10 ;
                                  DUP 2 ;
                                  PUSH nat 0 ;
                                  ADD ;
                                  SOME ;
                                  DUP 4 ;
                                  DUP 6 ;
                                  PAIR ;
                                  UPDATE ;
                                  DIP { DIG 9 ; DROP } ;
                                  DUG 9 } ;
                             DROP 3 ;
                             PAIR 10 ;
                             DIG 1 ;
                             PAIR } } } } }
           { UNPAIR ;
             SWAP ;
             DUP 9 ;
             DUP 3 ;
             SENDER ;
             PAIR ;
             GET ;
             IF_NONE { NONE nat } { DUP ; SOME ; SWAP ; DROP } ;
             IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
             DUP 18 ;
             DUP 9 ;
             EXEC ;
             NOT ;
             IF { PUSH string "fa2_r6" ; PUSH string "INVALID_CONDITION" ; PAIR ; FAILWITH }
                {} ;
             DUP 2 ;
             DUP 2 ;
             COMPARE ;
             GE ;
             NOT ;
             IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
             DUP 2 ;
             DUP 2 ;
             COMPARE ;
             GT ;
             IF { DUP 10 ;
                  DUP 4 ;
                  SENDER ;
                  PAIR ;
                  GET ;
                  IF_NONE
                    { PUSH string "ledger" ; PUSH string "ASSET_NOT_FOUND" ; PAIR ; FAILWITH }
                    {} ;
                  DUP 11 ;
                  PUSH int 0 ;
                  DUP 5 ;
                  INT ;
                  DUP 4 ;
                  SUB ;
                  COMPARE ;
                  GE ;
                  IF { DUP 4 ; INT ; DUP 3 ; SUB ; ABS }
                     { PUSH string "NAT_NEG_ASSIGN" ; FAILWITH } ;
                  SOME ;
                  DUP 6 ;
                  SENDER ;
                  PAIR ;
                  UPDATE ;
                  DIP { DIG 10 ; DROP } ;
                  DUG 10 ;
                  DROP }
                { DUP 10 ;
                  NONE nat ;
                  DUP 5 ;
                  SENDER ;
                  PAIR ;
                  UPDATE ;
                  DIP { DIG 9 ; DROP } ;
                  DUG 9 } ;
             DROP 3 ;
             PAIR 10 ;
             DIG 1 ;
             PAIR } ;
         DIP { DROP 4 } } }
