{ view "get_balance"
       (pair address nat)
       nat
       { UNPAIR ;
         PUSH nat 0 ;
         DIG 2 ;
         GET 7 ;
         DIG 2 ;
         GET ;
         IF_NONE {} { SWAP ; DROP } } ;
  view "get_total_supply"
       nat
       nat
       { UNPAIR ;
         PUSH nat 0 ;
         DIG 2 ;
         GET 11 ;
         DIG 2 ;
         GET ;
         IF_NONE {} { SWAP ; DROP } } ;
  parameter
    (or (or (or (or (pair %balance_of
                       (list %requests (pair (address %owner) (nat %token_id)))
                       (contract %callback
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                    (pair %burn (nat %token_id) (address %account) (nat %amount)))
                (or (address %change_bridge) (address %change_owner)))
            (or (or (unit %confirm_owner)
                    (pair %create_token
                       (pair %token (bytes %chain_id) (bytes %native_token_address))
                       (map %metadata string bytes)))
                (or (pair %mint (nat %token_id) (address %account) (nat %amount))
                    (unit %toggle_pause))))
        (or (list %transfer
               (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))
            (list %update_operators
               (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                   (pair %remove_operator (address %owner) (address %operator) (nat %token_id)))))) ;
  storage
    (pair (address %owner)
          (option %pending_owner address)
          (address %bridge)
          (big_map %ledger (pair address nat) nat)
          (big_map %allowances (pair (pair address nat) address) unit)
          (big_map %tokens_supply nat nat)
          (nat %token_count)
          (big_map %token_infos nat (pair (bytes %chain_id) (bytes %native_token_address)))
          (big_map %token_ids (pair (bytes %chain_id) (bytes %native_token_address)) nat)
          (big_map %metadata string bytes)
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
          (bool %paused)) ;
  code { NIL operation ;
         PUSH string "NOT_ADMIN" ;
         PUSH string "NOT_PENDING_ADMIN" ;
         PUSH string "Wrapped-token/not-bridge" ;
         PUSH string "Wrapped-token/contract-paused" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         PUSH string "FA2_NOT_OWNER" ;
         PUSH string "FA2_TOKEN_UNDEFINED" ;
         DIG 8 ;
         UNPAIR ;
         PUSH string "Wrapped-token/unexpected-xtz-amount" ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         EQ ;
         IF { DROP } { FAILWITH } ;
         IF_LEFT
           { DIG 3 ;
             DROP ;
             IF_LEFT
               { DIG 6 ;
                 DROP ;
                 IF_LEFT
                   { DIG 6 ;
                     DROP ;
                     IF_LEFT
                       { DIG 3 ;
                         DIG 4 ;
                         DIG 5 ;
                         DIG 6 ;
                         DROP 4 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SWAP ;
                         DIG 2 ;
                         NIL (pair (pair address nat) nat) ;
                         DUP 3 ;
                         CAR ;
                         ITER { SWAP ;
                                DUP 6 ;
                                DUP 4 ;
                                GET 13 ;
                                DUP 4 ;
                                CDR ;
                                COMPARE ;
                                LT ;
                                IF { DROP } { FAILWITH } ;
                                PUSH nat 0 ;
                                DUP 4 ;
                                GET 7 ;
                                DUP 4 ;
                                CDR ;
                                DUP 5 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE {} { SWAP ; DROP } ;
                                DIG 2 ;
                                PAIR ;
                                CONS } ;
                         SWAP ;
                         DIG 4 ;
                         DROP 2 ;
                         NIL operation ;
                         DIG 2 ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         PAIR }
                       { DIG 2 ;
                         DROP ;
                         DIG 4 ;
                         DUP 3 ;
                         GET 5 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         DIG 3 ;
                         DUP 3 ;
                         GET 22 ;
                         NOT ;
                         IF { DROP } { FAILWITH } ;
                         PUSH string "Wrapped-token/token-undefined" ;
                         DUP 3 ;
                         GET 11 ;
                         DUP 3 ;
                         CAR ;
                         GET ;
                         IF_NONE { FAILWITH } { SWAP ; DROP } ;
                         DIG 2 ;
                         DUP ;
                         GET 11 ;
                         PUSH string "Wrapped-token/not-nat" ;
                         DUP 5 ;
                         GET 4 ;
                         DIG 4 ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { FAILWITH } { SWAP ; DROP } ;
                         DUP 4 ;
                         CAR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 11 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         DUP 3 ;
                         GET 3 ;
                         PAIR ;
                         SWAP ;
                         DUP ;
                         DUP ;
                         DUG 3 ;
                         GET 7 ;
                         DIG 5 ;
                         DIG 5 ;
                         GET 4 ;
                         PUSH nat 0 ;
                         DIG 6 ;
                         GET 7 ;
                         DUP 7 ;
                         GET ;
                         IF_NONE {} { SWAP ; DROP } ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { FAILWITH } { SWAP ; DROP } ;
                         DIG 3 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 7 ;
                         SWAP ;
                         PAIR } }
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DIG 5 ;
                     DROP 4 ;
                     IF_LEFT
                       { DUG 2 ;
                         DUP ;
                         DUG 3 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         UPDATE 5 ;
                         SWAP ;
                         PAIR }
                       { DUG 2 ;
                         DUP ;
                         DUG 3 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         SOME ;
                         UPDATE 3 ;
                         SWAP ;
                         PAIR } } }
               { DIG 3 ;
                 DROP ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DROP 3 ;
                     IF_LEFT
                       { DIG 3 ;
                         DROP 2 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         IF_NONE { FAILWITH } { SWAP ; DROP } ;
                         DIG 2 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         UPDATE 1 ;
                         NONE address ;
                         UPDATE 3 ;
                         SWAP ;
                         PAIR }
                       { DIG 2 ;
                         DROP ;
                         DUG 2 ;
                         DUP ;
                         DUG 3 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUP ;
                         DUG 3 ;
                         GET 21 ;
                         DUP 3 ;
                         CDR ;
                         DUP 5 ;
                         GET 13 ;
                         PAIR ;
                         DIG 4 ;
                         GET 13 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 21 ;
                         DUP ;
                         DUP ;
                         DUG 2 ;
                         GET 17 ;
                         DIG 2 ;
                         GET 13 ;
                         DUP 4 ;
                         CAR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 17 ;
                         DUP ;
                         DUP ;
                         DUG 2 ;
                         GET 15 ;
                         DIG 3 ;
                         CAR ;
                         DIG 3 ;
                         GET 13 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 15 ;
                         DUP ;
                         PUSH nat 1 ;
                         DIG 2 ;
                         GET 13 ;
                         ADD ;
                         UPDATE 13 ;
                         SWAP ;
                         PAIR } }
                   { DIG 5 ;
                     DROP ;
                     IF_LEFT
                       { DIG 5 ;
                         DROP ;
                         DIG 4 ;
                         DUP 3 ;
                         GET 5 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         DUG 2 ;
                         DUP ;
                         DUG 3 ;
                         GET 13 ;
                         DUP 3 ;
                         CAR ;
                         COMPARE ;
                         LT ;
                         IF { DROP } { FAILWITH } ;
                         DUG 2 ;
                         DUP ;
                         DUG 3 ;
                         GET 22 ;
                         NOT ;
                         IF { DROP } { FAILWITH } ;
                         DUP ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         PAIR ;
                         DIG 2 ;
                         DUP ;
                         DUP ;
                         DUG 4 ;
                         GET 7 ;
                         DUP 4 ;
                         GET 4 ;
                         PUSH nat 0 ;
                         DIG 6 ;
                         GET 7 ;
                         DUP 6 ;
                         GET ;
                         IF_NONE {} { SWAP ; DROP } ;
                         ADD ;
                         DIG 3 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 7 ;
                         DUP ;
                         DUP ;
                         DUG 2 ;
                         GET 11 ;
                         DUP 4 ;
                         GET 4 ;
                         PUSH nat 0 ;
                         DIG 4 ;
                         GET 11 ;
                         DUP 6 ;
                         CAR ;
                         GET ;
                         IF_NONE {} { SWAP ; DROP } ;
                         ADD ;
                         DIG 3 ;
                         CAR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 11 ;
                         SWAP ;
                         PAIR }
                       { DIG 2 ;
                         DIG 3 ;
                         DIG 4 ;
                         DROP 4 ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DROP } { FAILWITH } ;
                         DUP ;
                         GET 22 ;
                         NOT ;
                         UPDATE 22 ;
                         SWAP ;
                         PAIR } } } }
           { DIG 6 ;
             DIG 7 ;
             DIG 8 ;
             DROP 3 ;
             IF_LEFT
               { DIG 2 ;
                 DIG 3 ;
                 DROP 2 ;
                 ITER { SWAP ;
                        DUP 4 ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        GET 22 ;
                        NOT ;
                        IF { DROP } { FAILWITH } ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        CDR ;
                        ITER { DUP ;
                               DUG 2 ;
                               GET 3 ;
                               DUP 4 ;
                               CAR ;
                               PAIR ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               GET 9 ;
                               SENDER ;
                               DUP 3 ;
                               PAIR ;
                               GET ;
                               IF_NONE
                                 { PUSH string "FA2_NOT_OPERATOR" ;
                                   SENDER ;
                                   DUP 6 ;
                                   CAR ;
                                   COMPARE ;
                                   EQ ;
                                   IF { DROP ; UNIT } { FAILWITH } }
                                 { DROP ; UNIT } ;
                               DROP ;
                               SWAP ;
                               DUP ;
                               DUP ;
                               DUG 3 ;
                               GET 7 ;
                               DUP 7 ;
                               DUP 6 ;
                               GET 4 ;
                               PUSH nat 0 ;
                               DIG 6 ;
                               GET 7 ;
                               DUP 7 ;
                               GET ;
                               IF_NONE {} { SWAP ; DROP } ;
                               SUB ;
                               ISNAT ;
                               IF_NONE { FAILWITH } { SWAP ; DROP } ;
                               DIG 3 ;
                               SWAP ;
                               SOME ;
                               SWAP ;
                               UPDATE ;
                               UPDATE 7 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               GET 3 ;
                               DUP 3 ;
                               CAR ;
                               PAIR ;
                               SWAP ;
                               DUP ;
                               DUP ;
                               DUG 3 ;
                               GET 7 ;
                               DIG 4 ;
                               GET 4 ;
                               PUSH nat 0 ;
                               DIG 5 ;
                               GET 7 ;
                               DUP 6 ;
                               GET ;
                               IF_NONE {} { SWAP ; DROP } ;
                               ADD ;
                               DIG 3 ;
                               SWAP ;
                               SOME ;
                               SWAP ;
                               UPDATE ;
                               UPDATE 7 } ;
                        SWAP ;
                        DROP } ;
                 SWAP ;
                 DIG 2 ;
                 DROP 2 ;
                 SWAP ;
                 PAIR }
               { DIG 4 ;
                 DIG 5 ;
                 DROP 2 ;
                 ITER { IF_LEFT
                          { DUP 3 ;
                            DUP 3 ;
                            GET 13 ;
                            DUP 3 ;
                            GET 4 ;
                            COMPARE ;
                            LT ;
                            IF { DROP } { FAILWITH } ;
                            DUP 4 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            SENDER ;
                            COMPARE ;
                            EQ ;
                            IF { DROP } { FAILWITH } ;
                            SWAP ;
                            DUP ;
                            GET 9 ;
                            UNIT ;
                            DUP 4 ;
                            GET 3 ;
                            DUP 5 ;
                            GET 4 ;
                            DIG 5 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            SWAP ;
                            SOME ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 9 }
                          { DUP 3 ;
                            DUP 3 ;
                            GET 13 ;
                            DUP 3 ;
                            GET 4 ;
                            COMPARE ;
                            LT ;
                            IF { DROP } { FAILWITH } ;
                            DUP 4 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            SENDER ;
                            COMPARE ;
                            EQ ;
                            IF { DROP } { FAILWITH } ;
                            SWAP ;
                            DUP ;
                            GET 9 ;
                            DUP 3 ;
                            GET 3 ;
                            DUP 4 ;
                            GET 4 ;
                            DIG 4 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            NONE unit ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 9 } } ;
                 SWAP ;
                 DIG 2 ;
                 DROP 2 ;
                 SWAP ;
                 PAIR } } } }
