{ parameter
    (or (or (or (list %assert_balances (pair (address %owner) (nat %token_id) (nat %balance)))
                (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance))))))
            (or (list %burn (pair (address %owner) (nat %token_id) (nat %amount)))
                (list %transfer
                   (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))))
        (list %update_operators
           (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
               (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))) ;
  storage
    (pair (big_map %metadata string bytes)
          (big_map %token_total_supply nat nat)
          (big_map %ledger (pair address nat) nat)
          (big_map %operators (pair address address nat) unit)
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))) ;
  code { LAMBDA
           (pair (pair address nat)
                 (big_map string bytes)
                 (big_map nat nat)
                 (big_map (pair address nat) nat)
                 (big_map (pair address address nat) unit)
                 (big_map nat (pair nat (map string bytes))))
           nat
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             GET 5 ;
             DUG 2 ;
             PAIR ;
             GET ;
             IF_NONE { PUSH nat 0 } {} } ;
         LAMBDA
           (pair (pair address address)
                 nat
                 (big_map string bytes)
                 (big_map nat nat)
                 (big_map (pair address nat) nat)
                 (big_map (pair address address nat) unit)
                 (big_map nat (pair nat (map string bytes))))
           unit
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             PUSH bool False ;
             DIG 2 ;
             GET 7 ;
             DIG 2 ;
             DUP 5 ;
             PAIR ;
             DUP 4 ;
             PAIR ;
             MEM ;
             DIG 3 ;
             DIG 3 ;
             COMPARE ;
             EQ ;
             OR ;
             COMPARE ;
             EQ ;
             IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } { UNIT } } ;
         LAMBDA
           address
           unit
           { SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "FA2_NOT_OWNER" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair nat
                 (big_map string bytes)
                 (big_map nat nat)
                 (big_map (pair address nat) nat)
                 (big_map (pair address address nat) unit)
                 (big_map nat (pair nat (map string bytes))))
           unit
           { CAR ;
             PUSH nat 0 ;
             SWAP ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } { UNIT } } ;
         DIG 4 ;
         UNPAIR ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         NEQ ;
         IF { PUSH string "FA2_DONT_SEND_TEZ" ; FAILWITH } {} ;
         IF_LEFT
           { DIG 3 ;
             DROP ;
             IF_LEFT
               { DIG 3 ;
                 DROP ;
                 IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     ITER { SWAP ;
                            DUP 2 ;
                            GET 4 ;
                            DUP 2 ;
                            DUP 4 ;
                            GET 3 ;
                            DIG 4 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            DUP 4 ;
                            SWAP ;
                            EXEC ;
                            COMPARE ;
                            LT ;
                            IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} } ;
                     SWAP ;
                     DROP ;
                     NIL operation }
                   { DUP 2 ;
                     NIL (pair (pair address nat) nat) ;
                     DUP 3 ;
                     CAR ;
                     ITER { SWAP ;
                            DUP 5 ;
                            DUP 3 ;
                            CDR ;
                            PAIR ;
                            DUP 7 ;
                            SWAP ;
                            EXEC ;
                            DROP ;
                            DUP 5 ;
                            DUP 3 ;
                            CDR ;
                            DUP 4 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            DUP 8 ;
                            SWAP ;
                            EXEC ;
                            DIG 2 ;
                            PAIR ;
                            CONS } ;
                     DIG 3 ;
                     DIG 4 ;
                     DIG 5 ;
                     DROP 3 ;
                     NIL operation ;
                     DIG 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS } }
               { IF_LEFT
                   { ITER { SWAP ;
                            DUP ;
                            DUP 3 ;
                            GET 3 ;
                            PAIR ;
                            SENDER ;
                            DUP 4 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            DUP 5 ;
                            SWAP ;
                            EXEC ;
                            DROP ;
                            DUP ;
                            DUP 3 ;
                            GET 3 ;
                            PAIR ;
                            DUP 4 ;
                            SWAP ;
                            EXEC ;
                            DROP ;
                            DUP ;
                            DUP 3 ;
                            GET 3 ;
                            DUP 4 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            DUP 6 ;
                            SWAP ;
                            EXEC ;
                            DUP 3 ;
                            GET 4 ;
                            DUP 2 ;
                            COMPARE ;
                            LT ;
                            IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                            DUP 2 ;
                            DIG 2 ;
                            GET 5 ;
                            DUP 4 ;
                            GET 4 ;
                            DIG 3 ;
                            SUB ;
                            ABS ;
                            DUP 4 ;
                            GET 3 ;
                            DUP 5 ;
                            CAR ;
                            PAIR ;
                            SWAP ;
                            SOME ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 5 ;
                            DUP ;
                            DUP 2 ;
                            GET 3 ;
                            DUP 4 ;
                            GET 4 ;
                            DIG 3 ;
                            DUP 5 ;
                            GET 3 ;
                            SWAP ;
                            GET 3 ;
                            SWAP ;
                            GET ;
                            IF_NONE { PUSH nat 0 } {} ;
                            SUB ;
                            ABS ;
                            DIG 3 ;
                            GET 3 ;
                            SWAP ;
                            SOME ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 3 } ;
                     SWAP ;
                     DIG 2 ;
                     DIG 3 ;
                     DROP 3 }
                   { ITER { SWAP ;
                            DUP 2 ;
                            CDR ;
                            ITER { SWAP ;
                                   DUP ;
                                   DUP 3 ;
                                   GET 3 ;
                                   PAIR ;
                                   SENDER ;
                                   DUP 5 ;
                                   CAR ;
                                   PAIR ;
                                   PAIR ;
                                   DUP 6 ;
                                   SWAP ;
                                   EXEC ;
                                   DROP ;
                                   DUP ;
                                   DUP 3 ;
                                   GET 3 ;
                                   PAIR ;
                                   DUP 5 ;
                                   SWAP ;
                                   EXEC ;
                                   DROP ;
                                   DUP ;
                                   DUP 3 ;
                                   GET 3 ;
                                   DUP 5 ;
                                   CAR ;
                                   PAIR ;
                                   PAIR ;
                                   DUP 7 ;
                                   SWAP ;
                                   EXEC ;
                                   DUP 3 ;
                                   GET 4 ;
                                   DUP 2 ;
                                   COMPARE ;
                                   LT ;
                                   IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                   DUP 2 ;
                                   DIG 2 ;
                                   GET 5 ;
                                   DUP 4 ;
                                   GET 4 ;
                                   DIG 3 ;
                                   SUB ;
                                   ABS ;
                                   DUP 4 ;
                                   GET 3 ;
                                   DUP 6 ;
                                   CAR ;
                                   PAIR ;
                                   SWAP ;
                                   SOME ;
                                   SWAP ;
                                   UPDATE ;
                                   UPDATE 5 ;
                                   DUP ;
                                   DUP 3 ;
                                   GET 3 ;
                                   DUP 4 ;
                                   CAR ;
                                   PAIR ;
                                   PAIR ;
                                   DUP 7 ;
                                   SWAP ;
                                   EXEC ;
                                   DUP 2 ;
                                   DIG 2 ;
                                   GET 5 ;
                                   DUP 4 ;
                                   GET 4 ;
                                   DIG 3 ;
                                   ADD ;
                                   DUP 4 ;
                                   GET 3 ;
                                   DIG 4 ;
                                   CAR ;
                                   PAIR ;
                                   SWAP ;
                                   SOME ;
                                   SWAP ;
                                   UPDATE ;
                                   UPDATE 5 } ;
                            SWAP ;
                            DROP } ;
                     SWAP ;
                     DIG 2 ;
                     DIG 3 ;
                     DROP 3 } ;
                 NIL operation } }
           { DIG 4 ;
             DIG 5 ;
             DROP 2 ;
             ITER { IF_LEFT
                      { DUP 2 ;
                        DUP 2 ;
                        GET 4 ;
                        PAIR ;
                        DUP 4 ;
                        SWAP ;
                        EXEC ;
                        DROP ;
                        DUP ;
                        CAR ;
                        DUP 5 ;
                        SWAP ;
                        EXEC ;
                        DROP ;
                        DUP 2 ;
                        DIG 2 ;
                        GET 7 ;
                        UNIT ;
                        SOME ;
                        DUP 4 ;
                        GET 4 ;
                        DUP 5 ;
                        GET 3 ;
                        PAIR ;
                        DIG 4 ;
                        CAR ;
                        PAIR ;
                        UPDATE ;
                        UPDATE 7 }
                      { DUP 2 ;
                        DUP 2 ;
                        GET 4 ;
                        PAIR ;
                        DUP 4 ;
                        SWAP ;
                        EXEC ;
                        DROP ;
                        DUP ;
                        CAR ;
                        DUP 5 ;
                        SWAP ;
                        EXEC ;
                        DROP ;
                        DUP 2 ;
                        DIG 2 ;
                        GET 7 ;
                        DUP 3 ;
                        GET 4 ;
                        DUP 4 ;
                        GET 3 ;
                        PAIR ;
                        DIG 3 ;
                        CAR ;
                        PAIR ;
                        NONE unit ;
                        SWAP ;
                        UPDATE ;
                        UPDATE 7 } } ;
             SWAP ;
             DIG 2 ;
             DROP 2 ;
             NIL operation } ;
         PAIR } ;
  view "get_balance"
       (pair (address %owner) (nat %token_id))
       nat
       { UNPAIR ;
         SWAP ;
         GET 5 ;
         DUP 2 ;
         CDR ;
         DIG 2 ;
         CAR ;
         PAIR ;
         GET ;
         IF_NONE { PUSH nat 0 } {} } ;
  view "total_supply"
       nat
       nat
       { UNPAIR ; SWAP ; GET 3 ; SWAP ; GET ; IF_NONE { PUSH nat 0 } {} } ;
  view "all_tokens" unit (list nat) { DROP ; NIL nat ; PUSH nat 0 ; CONS } ;
  view "is_operator"
       (pair (address %owner) (address %operator) (nat %token_id))
       bool
       { UNPAIR ;
         DUP ;
         GET 3 ;
         DUP 2 ;
         CAR ;
         DIG 3 ;
         GET 7 ;
         DIG 3 ;
         GET 4 ;
         DUP 4 ;
         PAIR ;
         DUP 3 ;
         PAIR ;
         MEM ;
         DUG 2 ;
         COMPARE ;
         EQ ;
         OR } }
