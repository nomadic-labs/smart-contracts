{ parameter
    (or (or (pair %deposit (address %address) (ticket %ticket bytes))
            (pair %update_root_hash
               (pair (pair (bytes %block_hash) (int %block_height))
                     (pair (bytes %block_payload_hash) (bytes %handles_hash)))
               (pair (pair (list %signatures (option signature)) (bytes %state_hash))
                     (list %validators key))))
        (pair %withdraw
           (pair (contract %callback (ticket bytes))
                 (pair %handle
                    (pair (pair (nat %amount) (bytes %data)) (pair (nat %id) (address %owner)))
                    (address %ticketer)))
           (pair (bytes %handles_hash) (list %proofs (pair (bytes %left) (bytes %right)))))) ;
  storage
    (pair (pair %root_hash
             (pair (pair (bytes %current_block_hash) (int %current_block_height))
                   (pair (bytes %current_handles_hash) (bytes %current_state_hash)))
             (list %current_validators key))
          (pair %vault
             (pair (big_map %known_handles_hash bytes unit) (big_map %used_handles nat unit))
             (big_map %vault (pair address bytes) (ticket bytes)))) ;
  code { LAMBDA
           (pair string bool)
           unit
           { UNPAIR ; SWAP ; NOT ; IF { FAILWITH } { DROP ; PUSH unit Unit } } ;
         SWAP ;
         UNPAIR ;
         SWAP ;
         UNPAIR ;
         DIG 2 ;
         IF_LEFT
           { IF_LEFT
               { DIG 3 ;
                 DROP ;
                 DIG 2 ;
                 UNPAIR ;
                 UNPAIR ;
                 DIG 3 ;
                 CDR ;
                 READ_TICKET ;
                 UNPAIR ;
                 SWAP ;
                 CAR ;
                 DIG 5 ;
                 NONE (ticket bytes) ;
                 DUP 3 ;
                 DUP 5 ;
                 PAIR ;
                 GET_AND_UPDATE ;
                 IF_NONE
                   { DIG 3 ; PAIR }
                   { DIG 4 ;
                     SWAP ;
                     PAIR ;
                     JOIN_TICKETS ;
                     IF_NONE { DROP ; PUSH string "unreachable" ; FAILWITH } { PAIR } } ;
                 UNPAIR ;
                 DIG 2 ;
                 DIG 3 ;
                 PAIR ;
                 SWAP ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 DUG 2 ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 NIL operation ;
                 PAIR }
               { DUP ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DUP 4 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 COMPARE ;
                 GT ;
                 PUSH string "old block height" ;
                 PAIR ;
                 DUP 7 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP 3 ;
                 CDR ;
                 CDR ;
                 PACK ;
                 BLAKE2B ;
                 DUP 4 ;
                 CDR ;
                 CAR ;
                 CDR ;
                 DUP 5 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 PAIR ;
                 DUP 5 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 DUP 6 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 PACK ;
                 BLAKE2B ;
                 DUP 4 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 PUSH string "invalid block hash" ;
                 PAIR ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 PUSH int 3 ;
                 PUSH int 2 ;
                 DUP 6 ;
                 CDR ;
                 SIZE ;
                 INT ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DUP 3 ;
                 PAIR ;
                 DUP 4 ;
                 CDR ;
                 CAR ;
                 CAR ;
                 DIG 5 ;
                 CDR ;
                 PAIR ;
                 PAIR ;
                 LEFT unit ;
                 LOOP_LEFT
                   { UNPAIR ;
                     UNPAIR ;
                     DIG 2 ;
                     UNPAIR ;
                     DIG 3 ;
                     DIG 3 ;
                     IF_CONS
                       { DIG 2 ;
                         IF_CONS
                           { IF_NONE
                               { SWAP ;
                                 DROP ;
                                 DIG 3 ;
                                 DIG 3 ;
                                 PAIR ;
                                 SWAP ;
                                 DIG 2 ;
                                 PAIR ;
                                 PAIR ;
                                 LEFT unit }
                               { DUP 5 ;
                                 SWAP ;
                                 DIG 3 ;
                                 CHECK_SIGNATURE ;
                                 IF { PUSH int 1 ;
                                      DIG 4 ;
                                      SUB ;
                                      DIG 3 ;
                                      PAIR ;
                                      SWAP ;
                                      DIG 2 ;
                                      PAIR ;
                                      PAIR ;
                                      LEFT unit }
                                    { DROP 4 ; PUSH string "bad signature" ; FAILWITH } } }
                           { DROP 4 ;
                             PUSH string "validators and signatures have different size" ;
                             FAILWITH } }
                       { SWAP ;
                         DROP ;
                         IF_CONS
                           { DROP 3 ;
                             PUSH string "validators and signatures have different size" ;
                             FAILWITH }
                           { PUSH int 0 ;
                             SWAP ;
                             COMPARE ;
                             GT ;
                             IF { PUSH string "not enough signatures" ; FAILWITH }
                                { PUSH unit Unit ;
                                  RIGHT (pair (pair (list key) (list (option signature))) (pair bytes int)) } } } } ;
                 DROP ;
                 DUP 3 ;
                 CDR ;
                 CDR ;
                 DUP 4 ;
                 CDR ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 PAIR ;
                 DIG 2 ;
                 DIG 3 ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 UNPAIR ;
                 UNPAIR ;
                 PUSH unit Unit ;
                 DUP 5 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 SWAP ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 NIL operation ;
                 PAIR } }
           { DUP ;
             CDR ;
             CAR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CDR ;
             DIG 4 ;
             UNPAIR ;
             UNPAIR ;
             DUP ;
             DUP 6 ;
             MEM ;
             PUSH string "unknown handles hash" ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             SWAP ;
             DUP ;
             DUG 2 ;
             DUP 5 ;
             CAR ;
             CDR ;
             CAR ;
             MEM ;
             NOT ;
             PUSH string "already used handle" ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             DUP 6 ;
             CDR ;
             CDR ;
             DIG 5 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             PUSH int 1 ;
             DIG 3 ;
             SIZE ;
             INT ;
             SUB ;
             PAIR ;
             PAIR ;
             LEFT unit ;
             LOOP_LEFT
               { UNPAIR ;
                 UNPAIR ;
                 SWAP ;
                 IF_CONS
                   { DUP ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CONCAT ;
                     BLAKE2B ;
                     DIG 4 ;
                     COMPARE ;
                     EQ ;
                     PUSH string "invalid proof hash" ;
                     PAIR ;
                     DUP 11 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH nat 0 ;
                     DUP 8 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     DUP 5 ;
                     ABS ;
                     PUSH nat 1 ;
                     LSL ;
                     AND ;
                     COMPARE ;
                     NEQ ;
                     IF { CDR } { CAR } ;
                     SWAP ;
                     PUSH int 1 ;
                     DIG 3 ;
                     SUB ;
                     PAIR ;
                     PAIR ;
                     LEFT unit }
                   { DROP ;
                     DUP 5 ;
                     PACK ;
                     BLAKE2B ;
                     SWAP ;
                     COMPARE ;
                     EQ ;
                     PUSH string "invalid handle data" ;
                     PAIR ;
                     DUP 8 ;
                     SWAP ;
                     EXEC ;
                     RIGHT (pair (pair int (list (pair bytes bytes))) bytes) } } ;
             DIG 7 ;
             DROP 2 ;
             DIG 2 ;
             NONE (ticket bytes) ;
             DUP 5 ;
             CAR ;
             CAR ;
             CDR ;
             DUP 6 ;
             CDR ;
             PAIR ;
             GET_AND_UPDATE ;
             IF_NONE { DROP ; PUSH string "unreachable" ; FAILWITH } { PAIR } ;
             UNPAIR ;
             READ_TICKET ;
             CDR ;
             CDR ;
             DUP 6 ;
             CAR ;
             CAR ;
             CAR ;
             SWAP ;
             SUB ;
             ABS ;
             DUP 6 ;
             CAR ;
             CAR ;
             CAR ;
             PAIR ;
             SWAP ;
             SPLIT_TICKET ;
             IF_NONE { PUSH string "unreachable" ; FAILWITH } {} ;
             UNPAIR ;
             DUG 2 ;
             DUP 6 ;
             CAR ;
             CAR ;
             CDR ;
             DUP 7 ;
             CDR ;
             PAIR ;
             SWAP ;
             SOME ;
             SWAP ;
             UPDATE ;
             SWAP ;
             DIG 5 ;
             CAR ;
             CAR ;
             PUSH mutez 0 ;
             DIG 2 ;
             TRANSFER_TOKENS ;
             SWAP ;
             DIG 3 ;
             PUSH unit Unit ;
             DIG 5 ;
             CAR ;
             CDR ;
             CAR ;
             SWAP ;
             SOME ;
             SWAP ;
             UPDATE ;
             DIG 3 ;
             PAIR ;
             PAIR ;
             NIL operation ;
             DIG 2 ;
             CONS ;
             SWAP ;
             DIG 2 ;
             PAIR ;
             SWAP ;
             PAIR } } }
