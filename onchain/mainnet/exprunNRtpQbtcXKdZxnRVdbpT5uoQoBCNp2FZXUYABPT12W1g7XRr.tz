{ parameter
    (or (or (or (address %add_admin)
                (or (address %add_minter)
                    (pair %balance_of
                       (list %requests (pair (address %owner) (nat %token_id)))
                       (contract %callback
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))))
            (or (or (list %burn nat)
                    (list %burn_range (pair (nat %token_id_start) (nat %token_id_end))))
                (or (unit %destroy)
                    (pair %mint
                       (address %owner)
                       (list %requests (pair (nat %token_id) (bytes %metadata)))))))
        (or (or (or (bool %pause)
                    (pair %power_mint
                       (address %owner)
                       (list %requests
                          (pair (bytes %metadata) (pair (nat %token_id_start) (nat %token_id_end))))))
                (or (address %remove_admin) (address %remove_minter)))
            (or (or (list %select
                       (pair (nat %token_id)
                             (pair (address %recipient) (pair (nat %token_id_start) (nat %token_id_end)))))
                    (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
                (or (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                    (pair %update_range_owner
                       (address %new_owner)
                       (list %requests (pair (nat %token_id_start) (nat %token_id_end)))))))) ;
  storage
    (pair (pair (pair (big_map %administrators address unit)
                      (big_map %allowances
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (bool %destroyed) (nat %last_token_id)))
          (pair (pair (big_map %ledger nat address) (big_map %minters address unit))
                (pair (bool %paused)
                      (pair (big_map %power_mints
                               (pair (nat %token_id_start) (nat %token_id_end))
                               (pair (address %owner) (bytes %metadata)))
                            (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))))) ;
  code { CAST (pair (or (or (or address
                                (or address (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))))
                            (or (or (list nat) (list (pair nat nat))) (or unit (pair address (list (pair nat bytes))))))
                        (or (or (or bool (pair address (list (pair bytes (pair nat nat))))) (or address address))
                            (or (or (list (pair nat (pair address (pair nat nat))))
                                    (list (pair address (list (pair address (pair nat nat))))))
                                (or (list (or (pair address (pair address nat)) (pair address (pair address nat))))
                                    (pair address (list (pair nat nat)))))))
                    (pair (pair (pair (big_map address unit) (big_map (pair address (pair address nat)) unit))
                                (pair bool nat))
                          (pair (pair (big_map nat address) (big_map address unit))
                                (pair bool
                                      (pair (big_map (pair nat nat) (pair address bytes))
                                            (big_map nat (pair nat (map string bytes)))))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH (option unit) (Some Unit) ;
                     DIG 5 ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         PUSH (option unit) (Some Unit) ;
                         DIG 5 ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         NIL operation }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         NIL (pair (pair address nat) nat) ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         ITER { DUP 4 ;
                                GET 3 ;
                                CAR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CDR ;
                                MEM ;
                                IF { DUP ;
                                     CAR ;
                                     DUP 5 ;
                                     GET 3 ;
                                     CAR ;
                                     DUP 3 ;
                                     CDR ;
                                     GET ;
                                     IF_NONE { PUSH int 388 ; FAILWITH } {} ;
                                     COMPARE ;
                                     EQ }
                                   { PUSH bool False } ;
                                IF { SWAP ; PUSH nat 1 ; DIG 2 ; PAIR ; CONS }
                                   { SWAP ; PUSH nat 0 ; DIG 2 ; PAIR ; CONS } } ;
                         NIL operation ;
                         DIG 2 ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         ITER { DUP 3 ;
                                CAR ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                COMPARE ;
                                LE ;
                                IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                DUP 3 ;
                                GET 3 ;
                                CAR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                MEM ;
                                IF { DIG 2 ;
                                     UNPAIR ;
                                     SWAP ;
                                     UNPAIR ;
                                     UNPAIR ;
                                     NONE address ;
                                     DUP 6 ;
                                     UPDATE ;
                                     PAIR ;
                                     PAIR ;
                                     SWAP ;
                                     PAIR ;
                                     DUG 2 }
                                   {} ;
                                DIG 2 ;
                                DUP ;
                                GET 8 ;
                                PUSH (map string bytes) { Elt "" 0x } ;
                                DUP 4 ;
                                PAIR ;
                                SOME ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 8 ;
                                SWAP } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         ITER { DUP ;
                                CAR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CDR ;
                                COMPARE ;
                                GE ;
                                IF {} { PUSH string "SELFMINT_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                                DUP 3 ;
                                GET 7 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                MEM ;
                                IF {} { PUSH string "SELFMINT_TOKEN_RANGE_UNDEFINED" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 7 ;
                                NONE (pair address bytes) ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 7 ;
                                SWAP } ;
                         DROP } }
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         PUSH bool True ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CDR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_MINTER" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         ITER { DUP ;
                                CAR ;
                                PUSH nat 1 ;
                                DUP 5 ;
                                CAR ;
                                GET 4 ;
                                ADD ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "SELFMINT_TOKEN_ALREADY_EXISTS" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 8 ;
                                EMPTY_MAP string bytes ;
                                DUP 4 ;
                                CDR ;
                                SOME ;
                                PUSH string "" ;
                                UPDATE ;
                                DUP 4 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DUP 4 ;
                                CAR ;
                                UPDATE ;
                                UPDATE 8 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                DUP 6 ;
                                CAR ;
                                SOME ;
                                DUP 6 ;
                                CAR ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                UNPAIR ;
                                SWAP ;
                                CAR ;
                                DIG 3 ;
                                CAR ;
                                SWAP ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP } ;
                         DROP } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 5 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CDR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_MINTER" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         ITER { DUP ;
                                GET 3 ;
                                PUSH nat 1 ;
                                DUP 5 ;
                                CAR ;
                                GET 4 ;
                                ADD ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "SELFMINT_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                GET 4 ;
                                COMPARE ;
                                GE ;
                                IF {} { PUSH string "SELFMINT_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 7 ;
                                DUP 3 ;
                                CAR ;
                                DUP 5 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DIG 3 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                DUG 5 ;
                                GET 3 ;
                                PAIR ;
                                UPDATE ;
                                UPDATE 7 ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                CAR ;
                                DIG 3 ;
                                GET 4 ;
                                SWAP ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP } ;
                         DROP } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF {} { PUSH string "SELFMINT_CANNOT_REMOVE_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         UNPAIR ;
                         NONE unit ;
                         DIG 5 ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         MEM ;
                         IF { SWAP ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              NONE unit ;
                              DIG 5 ;
                              UPDATE ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PAIR }
                            { DROP } } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP 3 ;
                                GET 8 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                MEM ;
                                IF { PUSH string "SELFMINT_TOKEN_ALREADY_EXISTS" ; FAILWITH } {} ;
                                DUP ;
                                GET 5 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                GET 6 ;
                                COMPARE ;
                                GE ;
                                IF {} { PUSH string "SELFMINT_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                                DUP ;
                                GET 5 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                COMPARE ;
                                GE ;
                                IF { DUP ; GET 6 ; SWAP ; DUP ; DUG 2 ; CAR ; COMPARE ; LE }
                                   { PUSH bool False } ;
                                IF {} { PUSH string "SELFMINT_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                                DUP 3 ;
                                GET 7 ;
                                SWAP ;
                                DUP ;
                                GET 6 ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                GET 5 ;
                                PAIR ;
                                MEM ;
                                IF {} { PUSH string "SELFMINT_TOKEN_RANGE_UNDEFINED" ; FAILWITH } ;
                                SENDER ;
                                DUP 4 ;
                                GET 7 ;
                                DIG 2 ;
                                DUP ;
                                GET 6 ;
                                SWAP ;
                                DUP ;
                                DUG 4 ;
                                GET 5 ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 296 ; FAILWITH } {} ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; MEM } ;
                                IF { PUSH bool True } { DUP 3 ; GET 3 ; CDR ; SENDER ; MEM } ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DUP 3 ;
                                DUP ;
                                GET 8 ;
                                EMPTY_MAP string bytes ;
                                DIG 5 ;
                                GET 7 ;
                                DIG 4 ;
                                DUP ;
                                GET 6 ;
                                SWAP ;
                                DUP ;
                                DUG 6 ;
                                GET 5 ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 296 ; FAILWITH } {} ;
                                CDR ;
                                SOME ;
                                PUSH string "" ;
                                UPDATE ;
                                DUP 4 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DUP 4 ;
                                CAR ;
                                UPDATE ;
                                UPDATE 8 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                DUP 5 ;
                                GET 3 ;
                                SOME ;
                                DIG 5 ;
                                CAR ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            PUSH nat 1 ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DUP 4 ; GET 3 ; CAR ; SWAP ; DUP ; DUG 2 ; GET 3 ; MEM }
                                               { PUSH bool False } ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            IF { PUSH bool True }
                                               { DUP 4 ;
                                                 CAR ;
                                                 CAR ;
                                                 CDR ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET 3 ;
                                                 SENDER ;
                                                 DUP 5 ;
                                                 CAR ;
                                                 PAIR 3 ;
                                                 MEM } ;
                                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            DUP 5 ;
                                            GET 3 ;
                                            CAR ;
                                            DUP 3 ;
                                            GET 3 ;
                                            GET ;
                                            IF_NONE { PUSH int 376 ; FAILWITH } {} ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                            DIG 3 ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            UNPAIR ;
                                            DUP 5 ;
                                            CAR ;
                                            SOME ;
                                            DIG 5 ;
                                            GET 3 ;
                                            UPDATE ;
                                            PAIR ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            DUG 2 }
                                          { DROP } } ;
                                DROP } ;
                         DROP } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF { PUSH string "SELFMINT_CONTRACT_PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "SELFMINT_CONTRACT_DESTROYED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 5 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    NONE unit ;
                                    DIG 5 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP } } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF { PUSH bool True } { SWAP ; DUP ; DUG 2 ; GET 3 ; CDR ; SENDER ; MEM } ;
                         IF {} { PUSH string "SELFMINT_NOT_ADMIN_OR_MINTER" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         ITER { DUP ;
                                CAR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CDR ;
                                COMPARE ;
                                GE ;
                                IF {} { PUSH string "SELFMINT_INVALID_TOKEN_RANGE" ; FAILWITH } ;
                                DUP 3 ;
                                GET 7 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                MEM ;
                                IF {} { PUSH string "SELFMINT_TOKEN_RANGE_UNDEFINED" ; FAILWITH } ;
                                DUP 3 ;
                                DUP ;
                                GET 7 ;
                                DIG 4 ;
                                GET 7 ;
                                DIG 3 ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 5 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 276 ; FAILWITH } {} ;
                                CDR ;
                                DUP 5 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 7 ;
                                SWAP } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
