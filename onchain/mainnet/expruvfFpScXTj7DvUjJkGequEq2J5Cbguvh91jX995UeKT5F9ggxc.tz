{ parameter
    (or (or (unit %cancelTimelock) (or (unit %endVoting) (unit %executeTimelock)))
        (or (or (pair %propose
                   (string %title)
                   (pair (string %descriptionLink)
                         (pair (string %descriptionHash) (lambda %proposalLambda unit (list operation)))))
                (pair %setParameters
                   (nat %escrowAmount)
                   (pair (nat %voteDelayBlocks)
                         (pair (nat %voteLengthBlocks)
                               (pair (nat %minYayVotesPercentForEscrowReturn)
                                     (pair (nat %blocksInTimelockForExecution)
                                           (pair (nat %blocksInTimelockForCancellation)
                                                 (pair (nat %percentageForSuperMajority) (pair %quorumCap (nat %lower) (nat %upper))))))))))
            (or (nat %vote)
                (pair %voteCallback (address %address) (pair (nat %level) (nat %result)))))) ;
  storage
    (pair (pair (pair (address %communityFundAddress)
                      (pair (address %escrowContractAddress)
                            (pair %governanceParameters
                               (nat %escrowAmount)
                               (pair (nat %voteDelayBlocks)
                                     (pair (nat %voteLengthBlocks)
                                           (pair (nat %minYayVotesPercentForEscrowReturn)
                                                 (pair (nat %blocksInTimelockForExecution)
                                                       (pair (nat %blocksInTimelockForCancellation)
                                                             (pair (nat %percentageForSuperMajority) (pair %quorumCap (nat %lower) (nat %upper)))))))))))
                (pair (big_map %metadata string bytes)
                      (pair (nat %nextProposalId)
                            (big_map %outcomes
                               nat
                               (pair (nat %outcome)
                                     (pair %poll
                                        (nat %id)
                                        (pair (pair %proposal
                                                 (string %title)
                                                 (pair (string %descriptionLink)
                                                       (pair (string %descriptionHash) (lambda %proposalLambda unit (list operation)))))
                                              (pair (nat %votingStartBlock)
                                                    (pair (nat %votingEndBlock)
                                                          (pair (nat %yayVotes)
                                                                (pair (nat %nayVotes)
                                                                      (pair (nat %abstainVotes)
                                                                            (pair (nat %totalVotes)
                                                                                  (pair (map %voters address (pair (nat %voteValue) (pair (nat %level) (nat %votes))))
                                                                                        (pair (address %author)
                                                                                              (pair (nat %escrowAmount)
                                                                                                    (pair (nat %quorum) (pair %quorumCap (nat %lower) (nat %upper)))))))))))))))))))
          (pair (pair (option %poll
                         (pair (nat %id)
                               (pair (pair %proposal
                                        (string %title)
                                        (pair (string %descriptionLink)
                                              (pair (string %descriptionHash) (lambda %proposalLambda unit (list operation)))))
                                     (pair (nat %votingStartBlock)
                                           (pair (nat %votingEndBlock)
                                                 (pair (nat %yayVotes)
                                                       (pair (nat %nayVotes)
                                                             (pair (nat %abstainVotes)
                                                                   (pair (nat %totalVotes)
                                                                         (pair (map %voters address (pair (nat %voteValue) (pair (nat %level) (nat %votes))))
                                                                               (pair (address %author)
                                                                                     (pair (nat %escrowAmount)
                                                                                           (pair (nat %quorum) (pair %quorumCap (nat %lower) (nat %upper)))))))))))))))
                      (pair (nat %quorum) (nat %state)))
                (pair (option %timelockItem
                         (pair (nat %id)
                               (pair (pair %proposal
                                        (string %title)
                                        (pair (string %descriptionLink)
                                              (pair (string %descriptionHash) (lambda %proposalLambda unit (list operation)))))
                                     (pair (nat %endBlock) (pair (nat %cancelBlock) (address %author))))))
                      (pair (address %tokenContractAddress)
                            (option %votingState (pair (nat %voteValue) (pair (address %address) (nat %level)))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 5 ;
                 IF_NONE { PUSH string "NO_ITEM_IN_TIMELOCK" ; FAILWITH } { DROP } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 5 ;
                 IF_NONE { PUSH int 539 ; FAILWITH } {} ;
                 GET 7 ;
                 LEVEL ;
                 DIG 2 ;
                 DROP ;
                 COMPARE ;
                 GE ;
                 IF {} { PUSH string "TOO_SOON" ; FAILWITH } ;
                 DUP ;
                 GET 5 ;
                 IF_NONE { PUSH int 539 ; FAILWITH } {} ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 6 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH int 543 ; FAILWITH } {} ;
                 DIG 2 ;
                 UNPAIR ;
                 UNPAIR ;
                 SWAP ;
                 UNPAIR ;
                 SWAP ;
                 UNPAIR ;
                 SWAP ;
                 DIG 5 ;
                 CDR ;
                 PUSH nat 3 ;
                 PAIR ;
                 SOME ;
                 DIG 6 ;
                 UPDATE ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 PAIR ;
                 NONE (pair nat
                            (pair (pair string (pair string (pair string (lambda unit (list operation)))))
                                  (pair nat (pair nat address)))) ;
                 UPDATE 5 ;
                 NIL operation }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH string "NO_POLL" ; FAILWITH } { DROP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF_NONE {} { PUSH string "ITEM_IN_TIMELOCK" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     DUP ;
                     GET 7 ;
                     LEVEL ;
                     COMPARE ;
                     GT ;
                     IF {} { PUSH string "VOTING_NOT_FINISHED" ; FAILWITH } ;
                     DUP ;
                     GET 19 ;
                     PUSH nat 100 ;
                     DUP 5 ;
                     CAR ;
                     CAR ;
                     GET 11 ;
                     DIG 3 ;
                     DUP ;
                     GET 11 ;
                     SWAP ;
                     DUP ;
                     DUG 5 ;
                     GET 9 ;
                     ADD ;
                     MUL ;
                     EDIV ;
                     IF_NONE { PUSH int 298 ; FAILWITH } { CAR } ;
                     DUP 3 ;
                     GET 9 ;
                     COMPARE ;
                     LE ;
                     IF { DROP ; DUP 3 ; CAR ; CAR ; CAR } {} ;
                     NIL operation ;
                     DUP 5 ;
                     GET 7 ;
                     CONTRACT %transfer
                       (list (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                     IF_NONE { PUSH int 321 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     NIL (pair address (pair nat nat)) ;
                     DUP 9 ;
                     CAR ;
                     CAR ;
                     GET 5 ;
                     PUSH nat 1 ;
                     DUP 8 ;
                     PAIR 3 ;
                     CONS ;
                     SELF_ADDRESS ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PUSH nat 100 ;
                     DUP 6 ;
                     CAR ;
                     CAR ;
                     GET 17 ;
                     DIG 4 ;
                     DUP ;
                     GET 11 ;
                     SWAP ;
                     DUP ;
                     DUG 6 ;
                     GET 9 ;
                     ADD ;
                     MUL ;
                     EDIV ;
                     IF_NONE { PUSH int 299 ; FAILWITH } { CAR } ;
                     DUP 4 ;
                     GET 9 ;
                     COMPARE ;
                     GE ;
                     IF { DUP 5 ; GET 3 ; GET 3 ; DUP 4 ; GET 15 ; COMPARE ; GE }
                        { PUSH bool False } ;
                     IF { DUP 5 ;
                          DUP 4 ;
                          GET 19 ;
                          DUP 7 ;
                          CAR ;
                          CAR ;
                          GET 15 ;
                          LEVEL ;
                          ADD ;
                          DUP 8 ;
                          CAR ;
                          CAR ;
                          GET 13 ;
                          LEVEL ;
                          ADD ;
                          DIG 6 ;
                          DUP ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 8 ;
                          CAR ;
                          PAIR 5 ;
                          DIG 6 ;
                          DROP ;
                          SOME ;
                          UPDATE 5 ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          SWAP ;
                          DUP 8 ;
                          PUSH nat 1 ;
                          PAIR ;
                          SOME ;
                          DUP 9 ;
                          CAR ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          DUG 4 }
                        { DIG 4 ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          SWAP ;
                          DUP 8 ;
                          PUSH nat 0 ;
                          PAIR ;
                          SOME ;
                          DUP 9 ;
                          CAR ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          DUG 4 } ;
                     DIG 4 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     NONE (pair nat
                                (pair (pair string (pair string (pair string (lambda unit (list operation)))))
                                      (pair nat
                                            (pair nat
                                                  (pair nat
                                                        (pair nat
                                                              (pair nat
                                                                    (pair nat
                                                                          (pair (map address (pair nat (pair nat nat)))
                                                                                (pair address (pair nat (pair nat (pair nat nat))))))))))))) ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     DUG 4 ;
                     PUSH nat 100 ;
                     PUSH nat 80 ;
                     DUP 5 ;
                     GET 23 ;
                     MUL ;
                     EDIV ;
                     IF_NONE { PUSH int 379 ; FAILWITH } { CAR } ;
                     PUSH nat 100 ;
                     PUSH nat 20 ;
                     DUP 6 ;
                     GET 15 ;
                     MUL ;
                     EDIV ;
                     IF_NONE { PUSH int 380 ; FAILWITH } { CAR } ;
                     ADD ;
                     DUP 4 ;
                     GET 25 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     LT ;
                     IF { DROP ; DUP 3 ; GET 25 } {} ;
                     DUP 4 ;
                     GET 26 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     GT ;
                     IF { DROP ; SWAP ; DROP ; DIG 2 ; DROP ; SWAP ; GET 26 }
                        { DIG 2 ; DROP ; DIG 2 ; DROP ; DIG 2 ; DROP } ;
                     DIG 2 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CDR ;
                     DIG 4 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF_NONE { PUSH string "NO_ITEM_IN_TIMELOCK" ; FAILWITH } { DROP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF_NONE { PUSH int 539 ; FAILWITH } {} ;
                     GET 8 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_AUTHOR" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF_NONE { PUSH int 539 ; FAILWITH } {} ;
                     GET 5 ;
                     LEVEL ;
                     DIG 2 ;
                     DROP ;
                     COMPARE ;
                     GT ;
                     IF {} { PUSH string "TOO_SOON" ; FAILWITH } ;
                     DUP ;
                     GET 5 ;
                     IF_NONE { PUSH int 539 ; FAILWITH } {} ;
                     GET 3 ;
                     GET 6 ;
                     NIL operation ;
                     SWAP ;
                     UNIT ;
                     EXEC ;
                     ITER { CONS } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF_NONE { PUSH int 539 ; FAILWITH } {} ;
                     CAR ;
                     DUP 3 ;
                     CAR ;
                     GET 6 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 521 ; FAILWITH } {} ;
                     DIG 3 ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DIG 5 ;
                     CDR ;
                     PUSH nat 2 ;
                     PAIR ;
                     SOME ;
                     DIG 6 ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     NONE (pair nat
                                (pair (pair string (pair string (pair string (lambda unit (list operation)))))
                                      (pair nat (pair nat address)))) ;
                     UPDATE 5 ;
                     SWAP } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE {} { PUSH string "POLL_UNDERWAY" ; FAILWITH } ;
                     NIL operation ;
                     DUP 3 ;
                     GET 7 ;
                     CONTRACT %transfer
                       (list (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                     IF_NONE { PUSH int 227 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     NIL (pair address (list (pair address (pair nat nat)))) ;
                     NIL (pair address (pair nat nat)) ;
                     DUP 7 ;
                     CAR ;
                     CAR ;
                     GET 5 ;
                     PUSH nat 1 ;
                     SELF_ADDRESS ;
                     PAIR 3 ;
                     CONS ;
                     SENDER ;
                     PAIR ;
                     CONS ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     DUP 3 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 5 ;
                     DUP ;
                     CAR ;
                     CAR ;
                     GET 18 ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 8 ;
                     CAR ;
                     CAR ;
                     GET 5 ;
                     SENDER ;
                     EMPTY_MAP address (pair nat (pair nat nat)) ;
                     PUSH nat 0 ;
                     DUP ;
                     DUP ;
                     DUP ;
                     DIG 14 ;
                     DUP ;
                     CAR ;
                     CAR ;
                     GET 9 ;
                     SWAP ;
                     DUP ;
                     DUG 16 ;
                     CAR ;
                     CAR ;
                     GET 7 ;
                     LEVEL ;
                     ADD ;
                     ADD ;
                     DUP 16 ;
                     CAR ;
                     CAR ;
                     GET 7 ;
                     LEVEL ;
                     ADD ;
                     DUP 16 ;
                     DUP 18 ;
                     CAR ;
                     GET 5 ;
                     PAIR 13 ;
                     DIG 5 ;
                     DROP ;
                     DIG 5 ;
                     DROP ;
                     SOME ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP }
                   { SELF_ADDRESS ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_DAO" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     DIG 4 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation } }
               { IF_LEFT
                   { PUSH nat 0 ;
                     DUP 3 ;
                     GET 3 ;
                     GET 4 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "BAD_STATE" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH string "NO_POLL" ; FAILWITH } { DROP } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     PUSH nat 1 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     DUP ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     DIG 3 ;
                     PAIR 3 ;
                     SOME ;
                     UPDATE 8 ;
                     DUP ;
                     CAR ;
                     CAR ;
                     GET 3 ;
                     CONTRACT %getPriorBalance
                       (pair (pair (address %address) (nat %level))
                             (contract (pair (address %address) (pair (nat %level) (nat %result))))) ;
                     IF_NONE { PUSH int 418 ; FAILWITH } {} ;
                     NIL operation ;
                     SWAP ;
                     PUSH mutez 0 ;
                     SELF %voteCallback ;
                     DUP 5 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     GET 5 ;
                     SENDER ;
                     PAIR ;
                     PAIR ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { PUSH nat 1 ;
                     DUP 3 ;
                     GET 3 ;
                     GET 4 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "BAD_STATE" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     GET 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_TOKEN_CONTRACT" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     DUP 3 ;
                     GET 8 ;
                     IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                     GET 3 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "UNKNOWN" ; FAILWITH } ;
                     DUP ;
                     GET 3 ;
                     DUP 3 ;
                     GET 8 ;
                     IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                     GET 4 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "UNKNOWN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     GET 17 ;
                     DUP 3 ;
                     GET 8 ;
                     IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                     GET 3 ;
                     MEM ;
                     IF { PUSH string "ALREADY_VOTED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     DUP 3 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     GET 7 ;
                     LEVEL ;
                     COMPARE ;
                     LE ;
                     IF {} { PUSH string "VOTING_FINISHED" ; FAILWITH } ;
                     DUP 3 ;
                     GET 3 ;
                     CAR ;
                     IF_NONE { PUSH int 293 ; FAILWITH } {} ;
                     DUP ;
                     GET 17 ;
                     DUP 4 ;
                     GET 4 ;
                     LEVEL ;
                     DIG 4 ;
                     DROP ;
                     DUP 6 ;
                     GET 8 ;
                     IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                     CAR ;
                     PAIR 3 ;
                     SOME ;
                     DUP 5 ;
                     GET 8 ;
                     IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                     GET 3 ;
                     UPDATE ;
                     UPDATE 17 ;
                     DUP ;
                     GET 15 ;
                     DUP 3 ;
                     GET 4 ;
                     ADD ;
                     UPDATE 15 ;
                     PUSH nat 0 ;
                     DUP 4 ;
                     GET 8 ;
                     IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                     CAR ;
                     COMPARE ;
                     EQ ;
                     IF { DUP ; GET 9 ; DIG 2 ; GET 4 ; ADD ; UPDATE 9 }
                        { PUSH nat 1 ;
                          DUP 4 ;
                          GET 8 ;
                          IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                          CAR ;
                          COMPARE ;
                          EQ ;
                          IF { DUP ; GET 11 ; DIG 2 ; GET 4 ; ADD ; UPDATE 11 }
                             { PUSH nat 2 ;
                               DUP 4 ;
                               GET 8 ;
                               IF_NONE { PUSH int 455 ; FAILWITH } {} ;
                               CAR ;
                               COMPARE ;
                               EQ ;
                               IF { DUP ; GET 13 ; DIG 2 ; GET 4 ; ADD ; UPDATE 13 }
                                  { PUSH string "BAD_VOTE_VALUE" ; FAILWITH } } } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 3 ;
                     SOME ;
                     SWAP ;
                     CAR ;
                     PUSH nat 0 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     NONE (pair nat (pair address nat)) ;
                     UPDATE 8 ;
                     NIL operation } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
