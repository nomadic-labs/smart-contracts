{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %mint (pair (bytes %link_to_metadata) (address %owner))))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (address %update_admin)))
        (or (or (string %update_contract_metadata)
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))
            (address %update_whitelist))) ;
  storage
    (pair (pair (pair (address %admin) (big_map %ledger nat address))
                (pair (big_map %metadata string bytes) (nat %next_token_id)))
          (pair (pair (big_map %reverse_ledger address nat)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))
                (set %whitelisted_senders address))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           GET ;
                           IF_NONE
                             { DROP ; PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH }
                             { SWAP ;
                               DUP ;
                               CAR ;
                               DIG 2 ;
                               COMPARE ;
                               EQ ;
                               IF { PUSH nat 1 } { PUSH nat 0 } ;
                               SWAP ;
                               PAIR } } ;
                     DIG 2 ;
                     DROP ;
                     SWAP ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     DUP 3 ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     MEM ;
                     NOT ;
                     AND ;
                     IF { DROP 2 ; PUSH string "UNAUTHORIZED_TX" ; FAILWITH }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          CDR ;
                          CDR ;
                          DIG 2 ;
                          PAIR ;
                          SWAP ;
                          ITER { SWAP ;
                                 UNPAIR ;
                                 DIG 2 ;
                                 UNPAIR ;
                                 DUP 3 ;
                                 CDR ;
                                 CAR ;
                                 CAR ;
                                 DUP 3 ;
                                 GET ;
                                 IF_NONE
                                   { PUSH nat 1 ;
                                     DUP 5 ;
                                     ADD ;
                                     DUP 4 ;
                                     CDR ;
                                     DUP 5 ;
                                     CAR ;
                                     CDR ;
                                     DUP 6 ;
                                     CAR ;
                                     CAR ;
                                     CDR ;
                                     DUP 6 ;
                                     DUP 9 ;
                                     SWAP ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     DUP 7 ;
                                     CAR ;
                                     CAR ;
                                     CAR ;
                                     PAIR ;
                                     PAIR ;
                                     PAIR ;
                                     DUP ;
                                     CDR ;
                                     CDR ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     CDR ;
                                     CAR ;
                                     CDR ;
                                     DUP 7 ;
                                     CDR ;
                                     CAR ;
                                     CAR ;
                                     DUP 9 ;
                                     DIG 7 ;
                                     SWAP ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     PAIR ;
                                     PAIR ;
                                     SWAP ;
                                     CAR ;
                                     PAIR ;
                                     DUP ;
                                     CDR ;
                                     CDR ;
                                     DIG 4 ;
                                     CDR ;
                                     CAR ;
                                     CDR ;
                                     DUP 6 ;
                                     EMPTY_MAP string bytes ;
                                     DIG 6 ;
                                     SOME ;
                                     PUSH string "" ;
                                     UPDATE ;
                                     SWAP ;
                                     PAIR ;
                                     DIG 5 ;
                                     SWAP ;
                                     SOME ;
                                     SWAP ;
                                     UPDATE ;
                                     DUP 3 ;
                                     CDR ;
                                     CAR ;
                                     CAR ;
                                     PAIR ;
                                     PAIR ;
                                     SWAP ;
                                     CAR ;
                                     PAIR ;
                                     PAIR }
                                   { DIG 2 ;
                                     DROP ;
                                     DIG 3 ;
                                     DUP 4 ;
                                     CDR ;
                                     CDR ;
                                     DUP 5 ;
                                     CDR ;
                                     CAR ;
                                     CDR ;
                                     DUP 4 ;
                                     EMPTY_MAP string bytes ;
                                     DIG 6 ;
                                     SOME ;
                                     PUSH string "" ;
                                     UPDATE ;
                                     SWAP ;
                                     PAIR ;
                                     SOME ;
                                     DIG 4 ;
                                     UPDATE ;
                                     DUP 4 ;
                                     CDR ;
                                     CAR ;
                                     CAR ;
                                     PAIR ;
                                     PAIR ;
                                     DIG 2 ;
                                     CAR ;
                                     PAIR ;
                                     PAIR } } ;
                          UNPAIR ;
                          DUP ;
                          CDR ;
                          DUG 2 ;
                          DUP ;
                          DUG 3 ;
                          CAR ;
                          CDR ;
                          CAR ;
                          PAIR ;
                          DIG 2 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          PAIR } ;
                     NIL operation ;
                     PAIR } }
               { IF_LEFT
                   { DROP 2 ; PUSH string "FA2_TX_DENIED" ; FAILWITH }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          DIG 3 ;
                          CAR ;
                          CAR ;
                          CDR ;
                          DIG 3 ;
                          PAIR ;
                          PAIR ;
                          PAIR } ;
                     NIL operation ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          CDR ;
                          DUP 4 ;
                          CAR ;
                          CDR ;
                          CAR ;
                          DIG 3 ;
                          PUSH string "ipfs://" ;
                          CONCAT ;
                          PACK ;
                          SOME ;
                          PUSH string "" ;
                          UPDATE ;
                          PAIR ;
                          DIG 2 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          PAIR } ;
                     NIL operation ;
                     PAIR }
                   { DROP 2 ; PUSH string "FA2_OPERATORS_UNSUPPORTED" ; FAILWITH } }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                    { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CDR ;
                      CDR ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      MEM ;
                      IF { SWAP ; DUP ; DUG 2 ; CDR ; CDR ; SWAP ; PUSH bool False ; SWAP ; UPDATE }
                         { SWAP ; DUP ; DUG 2 ; CDR ; CDR ; SWAP ; PUSH bool True ; SWAP ; UPDATE } ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      CDR ;
                      CAR ;
                      PAIR ;
                      SWAP ;
                      CAR ;
                      PAIR } ;
                 NIL operation ;
                 PAIR } } } }
