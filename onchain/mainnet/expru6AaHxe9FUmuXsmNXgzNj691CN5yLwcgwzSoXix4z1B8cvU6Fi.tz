{ storage
    (pair (address %owner)
          (pair (set %authorized_addresses address)
                (pair (bool %is_locked)
                      (pair (big_map %royalties nat (list (pair (address %partAccount) (nat %partValue))))
                            (pair (big_map %ledger (pair nat address) nat)
                                  (pair (big_map %operators (pair address (pair nat address)) unit)
                                        (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                                              (pair (big_map %operators_for_all (pair address address) unit)
                                                    (big_map %metadata string bytes))))))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (unit %lock))
            (or (unit %unlock) (pair %set_metadata (string %ikey) (bytes %idata))))
        (or (or (or (address %set_owner) (address %authorize))
                (or (address %remove_authorization)
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))
            (or (or (list %update_operators_for_all (or address address))
                    (list %transfer
                       (pair address (list (pair (address %to) (pair (nat %token_id) (nat %amount)))))))
                (or (pair %mint
                       (nat %itokenid)
                       (pair (address %iowner)
                             (pair (nat %iamount)
                                   (pair (map %itokenMetadata string bytes)
                                         (list %iroyalties (pair (address %partAccount) (nat %partValue)))))))
                    (pair %burn (nat %itokenid) (nat %iamount)))))) ;
  code { LAMBDA
           (pair (big_map (pair nat address) nat)
                 (pair (big_map (pair address address) unit)
                       (pair (big_map (pair address (pair nat address)) unit)
                             (list (pair address (list (pair (address %to) (pair (nat %token_id) (nat %amount)))))))))
           bool
           { UNPAIR 4 ;
             PUSH unit Unit ;
             PUSH bool False ;
             DUP 6 ;
             ITER { DUP ;
                    CAR ;
                    DUP 2 ;
                    CDR ;
                    DUP ;
                    ITER { DUP 3 ;
                           SENDER ;
                           COMPARE ;
                           NEQ ;
                           IF { DUP 8 ;
                                DUP 4 ;
                                SENDER ;
                                PAIR ;
                                MEM ;
                                DUP 10 ;
                                DUP 5 ;
                                DUP 4 ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                SENDER ;
                                PAIR ;
                                MEM ;
                                OR }
                              { DUP 7 ; SENDER ; DUP 3 ; CDR ; CAR ; PAIR ; MEM } ;
                           DIP { DIG 4 ; DROP } ;
                           DUG 4 ;
                           DROP } ;
                    DROP 3 } ;
             DUP ;
             DIP { DIG 1 ; DROP } ;
             DUG 1 ;
             DROP ;
             DUG 4 ;
             DROP 4 } ;
         NIL operation ;
         DIG 2 ;
         UNPAIR ;
         DIP { UNPAIR 9 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { UNPAIR ;
                     NIL operation ;
                     NIL operation ;
                     DUP 14 ;
                     ITER { CONS } ;
                     DUP 4 ;
                     AMOUNT ;
                     DUP 5 ;
                     MAP { DUP 12 ;
                           DUP 2 ;
                           CAR ;
                           DUP 3 ;
                           CDR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 12 ;
                                DUP 2 ;
                                CAR ;
                                DUP 3 ;
                                CDR ;
                                PAIR ;
                                GET ;
                                IF_NONE
                                  { PUSH string "ledger" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                                  {} }
                              { PUSH nat 0 } ;
                           DUP 2 ;
                           PAIR ;
                           SWAP ;
                           DROP } ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     ITER { CONS } ;
                     DIP { DIG 11 ; DROP } ;
                     DUG 11 ;
                     DROP 2 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR }
                   { DROP ;
                     DUP ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     PUSH bool True ;
                     DIP { DIG 2 ; DROP } ;
                     DUG 2 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR } }
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     PUSH bool False ;
                     DIP { DIG 2 ; DROP } ;
                     DUG 2 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     DUP 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP 11 ;
                     DUP 2 ;
                     SOME ;
                     DUP 4 ;
                     UPDATE ;
                     DIP { DIG 10 ; DROP } ;
                     DUG 10 ;
                     DROP 2 ;
                     PAIR 9 ;
                     DIG 1 ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 1 ; DROP } ;
                         DUG 1 ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR }
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 3 ;
                         PUSH bool True ;
                         DUP 3 ;
                         UPDATE ;
                         DIP { DIG 2 ; DROP } ;
                         DUG 2 ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR } }
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 3 ;
                         PUSH bool False ;
                         DUP 3 ;
                         UPDATE ;
                         DIP { DIG 2 ; DROP } ;
                         DUG 2 ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR }
                       { DUP ;
                         ITER { DUP ;
                                IF_LEFT
                                  { SENDER ;
                                    DUP 2 ;
                                    CAR ;
                                    COMPARE ;
                                    EQ ;
                                    NOT ;
                                    IF { PUSH string "CALLER_NOT_OWNER" ; FAILWITH } {} ;
                                    DUP 9 ;
                                    DUP 2 ;
                                    CAR ;
                                    DUP 3 ;
                                    CDR ;
                                    CDR ;
                                    PAIR ;
                                    DUP 3 ;
                                    CDR ;
                                    CAR ;
                                    PAIR ;
                                    MEM ;
                                    IF {}
                                       { DUP 9 ;
                                         DUP 2 ;
                                         CAR ;
                                         DUP 3 ;
                                         CDR ;
                                         CDR ;
                                         PAIR ;
                                         DUP 3 ;
                                         CDR ;
                                         CAR ;
                                         PAIR ;
                                         MEM ;
                                         IF { PUSH string "operators" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                                            { DUP 9 ;
                                              PUSH unit Unit ;
                                              SOME ;
                                              DUP 3 ;
                                              CAR ;
                                              DUP 4 ;
                                              CDR ;
                                              CDR ;
                                              PAIR ;
                                              DUP 4 ;
                                              CDR ;
                                              CAR ;
                                              PAIR ;
                                              UPDATE ;
                                              DIP { DIG 8 ; DROP } ;
                                              DUG 8 } } ;
                                    DROP }
                                  { SENDER ;
                                    DUP 2 ;
                                    CAR ;
                                    COMPARE ;
                                    EQ ;
                                    NOT ;
                                    IF { PUSH string "CALLER_NOT_OWNER" ; FAILWITH } {} ;
                                    DUP 9 ;
                                    NONE unit ;
                                    DUP 3 ;
                                    CAR ;
                                    DUP 4 ;
                                    CDR ;
                                    CDR ;
                                    PAIR ;
                                    DUP 4 ;
                                    CDR ;
                                    CAR ;
                                    PAIR ;
                                    UPDATE ;
                                    DIP { DIG 8 ; DROP } ;
                                    DUG 8 ;
                                    DROP } ;
                                DROP } ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR } } }
               { IF_LEFT
                   { IF_LEFT
                       { DUP ;
                         ITER { DUP ;
                                IF_LEFT
                                  { DUP 11 ;
                                    SENDER ;
                                    DUP 3 ;
                                    PAIR ;
                                    MEM ;
                                    IF {}
                                       { DUP 11 ;
                                         SENDER ;
                                         DUP 3 ;
                                         PAIR ;
                                         MEM ;
                                         IF { PUSH string "operators_for_all" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                                            { DUP 11 ;
                                              PUSH unit Unit ;
                                              SOME ;
                                              SENDER ;
                                              DUP 4 ;
                                              PAIR ;
                                              UPDATE ;
                                              DIP { DIG 10 ; DROP } ;
                                              DUG 10 } } ;
                                    DROP }
                                  { DUP 11 ;
                                    NONE unit ;
                                    SENDER ;
                                    DUP 4 ;
                                    PAIR ;
                                    UPDATE ;
                                    DIP { DIG 10 ; DROP } ;
                                    DUG 10 ;
                                    DROP } ;
                                DROP } ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR }
                       { DUP 4 ;
                         IF { DUP 3 ;
                              SENDER ;
                              MEM ;
                              NOT ;
                              IF { PUSH string "TRANSFER_NOT_AUTHORIZED" ; FAILWITH } {} }
                            {} ;
                         DUP 12 ;
                         DUP 2 ;
                         DUP 9 ;
                         PAIR ;
                         DUP 11 ;
                         PAIR ;
                         DUP 8 ;
                         PAIR ;
                         EXEC ;
                         NOT ;
                         IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CAR ;
                                DUP 2 ;
                                CDR ;
                                DUP ;
                                ITER { DUP ;
                                       CDR ;
                                       CAR ;
                                       DUP 11 ;
                                       DUP 5 ;
                                       DUP 3 ;
                                       PAIR ;
                                       GET ;
                                       IF_NONE
                                         { PUSH string "ledger" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                                         {} ;
                                       DUP ;
                                       DUP 4 ;
                                       CDR ;
                                       CDR ;
                                       COMPARE ;
                                       GT ;
                                       IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                          { DUP ;
                                            DUP 4 ;
                                            CDR ;
                                            CDR ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DUP 12 ;
                                                 NONE nat ;
                                                 DUP 7 ;
                                                 DUP 5 ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 DIP { DIG 11 ; DROP } ;
                                                 DUG 11 }
                                               { DUP 12 ;
                                                 DUP 6 ;
                                                 DUP 4 ;
                                                 PAIR ;
                                                 GET ;
                                                 IF_NONE
                                                   { PUSH string "ledger" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                                                   {} ;
                                                 DUP 13 ;
                                                 PUSH int 0 ;
                                                 DUP 6 ;
                                                 CDR ;
                                                 CDR ;
                                                 INT ;
                                                 DUP 4 ;
                                                 SUB ;
                                                 COMPARE ;
                                                 GE ;
                                                 IF { DUP 5 ; CDR ; CDR ; INT ; DUP 3 ; SUB ; ABS }
                                                    { PUSH string "NatAssign" ; FAILWITH } ;
                                                 SOME ;
                                                 DUP 8 ;
                                                 DUP 6 ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 DIP { DIG 12 ; DROP } ;
                                                 DUG 12 ;
                                                 DROP } } ;
                                       DUP 12 ;
                                       DUP 4 ;
                                       CAR ;
                                       DUP 4 ;
                                       PAIR ;
                                       MEM ;
                                       IF { DUP 12 ;
                                            DUP 4 ;
                                            CAR ;
                                            DUP 4 ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE
                                              { PUSH string "ledger" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                                              {} ;
                                            DUP 13 ;
                                            DUP 5 ;
                                            CDR ;
                                            CDR ;
                                            DUP 3 ;
                                            ADD ;
                                            SOME ;
                                            DUP 6 ;
                                            CAR ;
                                            DUP 6 ;
                                            PAIR ;
                                            UPDATE ;
                                            DIP { DIG 12 ; DROP } ;
                                            DUG 12 ;
                                            DROP }
                                          { DUP 12 ;
                                            DUP 4 ;
                                            CAR ;
                                            DUP 4 ;
                                            PAIR ;
                                            MEM ;
                                            IF { PUSH string "ledger" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                                               { DUP 12 ;
                                                 DUP 4 ;
                                                 CDR ;
                                                 CDR ;
                                                 PUSH nat 0 ;
                                                 ADD ;
                                                 SOME ;
                                                 DUP 5 ;
                                                 CAR ;
                                                 DUP 5 ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 DIP { DIG 11 ; DROP } ;
                                                 DUG 11 } } ;
                                       DROP 3 } ;
                                DROP 3 } ;
                         DROP ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR } }
                   { IF_LEFT
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 6 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP 12 ;
                         DUP 6 ;
                         MEM ;
                         IF { PUSH string "token_metadata" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                            { DUP 12 ;
                              DUP 3 ;
                              DUP 7 ;
                              PAIR ;
                              SOME ;
                              DUP 7 ;
                              UPDATE ;
                              DIP { DIG 11 ; DROP } ;
                              DUG 11 } ;
                         DUP 9 ;
                         DUP 6 ;
                         MEM ;
                         IF { PUSH string "royalties" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                            { DUP 9 ; DUP 2 ; SOME ; DUP 7 ; UPDATE ; DIP { DIG 8 ; DROP } ; DUG 8 } ;
                         DUP 10 ;
                         DUP 5 ;
                         DUP 7 ;
                         PAIR ;
                         MEM ;
                         IF { PUSH string "ledger" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                            { DUP 10 ;
                              DUP 4 ;
                              SOME ;
                              DUP 6 ;
                              DUP 8 ;
                              PAIR ;
                              UPDATE ;
                              DIP { DIG 9 ; DROP } ;
                              DUG 9 } ;
                         DROP 5 ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR }
                       { UNPAIR ;
                         SWAP ;
                         DUP 7 ;
                         SENDER ;
                         DUP 4 ;
                         PAIR ;
                         MEM ;
                         IF { DUP 7 ;
                              SENDER ;
                              DUP 4 ;
                              PAIR ;
                              GET ;
                              IF_NONE
                                { PUSH string "ledger" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                                {} ;
                              DUP 2 ;
                              DUP 2 ;
                              COMPARE ;
                              GT ;
                              IF { DUP 8 ;
                                   SENDER ;
                                   DUP 5 ;
                                   PAIR ;
                                   GET ;
                                   IF_NONE
                                     { PUSH string "ledger" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                                     {} ;
                                   DUP 9 ;
                                   PUSH int 0 ;
                                   DUP 5 ;
                                   INT ;
                                   DUP 4 ;
                                   SUB ;
                                   COMPARE ;
                                   GE ;
                                   IF { DUP 4 ; INT ; DUP 3 ; SUB ; ABS }
                                      { PUSH string "NatAssign" ; FAILWITH } ;
                                   SOME ;
                                   SENDER ;
                                   DUP 7 ;
                                   PAIR ;
                                   UPDATE ;
                                   DIP { DIG 8 ; DROP } ;
                                   DUG 8 ;
                                   DROP }
                                 { DUP 2 ;
                                   DUP 2 ;
                                   COMPARE ;
                                   EQ ;
                                   IF { DUP 8 ;
                                        NONE nat ;
                                        SENDER ;
                                        DUP 6 ;
                                        PAIR ;
                                        UPDATE ;
                                        DIP { DIG 7 ; DROP } ;
                                        DUG 7 ;
                                        DUP 10 ;
                                        NONE (pair nat (map string bytes)) ;
                                        DUP 5 ;
                                        UPDATE ;
                                        DIP { DIG 9 ; DROP } ;
                                        DUG 9 ;
                                        DUP 7 ;
                                        NONE (list (pair address nat)) ;
                                        DUP 5 ;
                                        UPDATE ;
                                        DIP { DIG 6 ; DROP } ;
                                        DUG 6 }
                                      { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } } ;
                              DROP }
                            { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                         DROP 2 ;
                         PAIR 9 ;
                         DIG 1 ;
                         PAIR } } } } ;
         DIP { DROP } } ;
  view "get_royalties"
       nat
       (list (pair (address %partAccount) (nat %partValue)))
       { UNPAIR ;
         DIP { CDR ; CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNIT ;
         DUP 3 ;
         DUP 3 ;
         MEM ;
         IF { DUP 3 ;
              DUP 3 ;
              GET ;
              IF_NONE
                { PUSH string "royalties" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                {} }
            { NIL (pair (address %partAccount) (nat %partValue)) } ;
         SWAP ;
         DROP ;
         DIP { DROP 2 } } }
