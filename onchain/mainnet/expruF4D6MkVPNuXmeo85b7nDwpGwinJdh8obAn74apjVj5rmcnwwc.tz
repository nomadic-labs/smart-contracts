{ parameter
    (or (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
                (or %assets
                   (or (pair %balance_of
                          (list %requests (pair (address %owner) (nat %token_id)))
                          (contract %callback
                             (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                       (list %transfer
                          (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))))
                   (list %update_operators
                      (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                          (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))))
            (or (nat %bootstrap) (unit %buyIn)))
        (timestamp %startContest)) ;
  storage
    (pair (pair (pair (pair (pair %assets
                               (pair (pair (address %casino) (big_map %ledger (pair address nat) nat))
                                     (big_map %operators (pair address address nat) unit)
                                     (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))
                               (pair (big_map %token_total_supply nat nat)
                                     (big_map %topup_lockout address timestamp))
                               (int %topup_lockout_time))
                            (address %burn_address))
                      (mutez %buy_in)
                      (nat %buy_in_fee))
                (pair (pair %buy_in_fee_currency (address %fa2_address) (option %token_id nat))
                      (address %casino))
                (option %ending timestamp)
                (nat %initial_stack))
          (pair (big_map %metadata string bytes)
                (option %pauseable_admin
                   (pair (pair (address %admin) (bool %paused)) (option %pending_admin address))))
          (address %sink)) ;
  code { LAMBDA
           (option (pair (pair address bool) (option address)))
           unit
           { IF_NONE
               { UNIT }
               { CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } } ;
         PUSH string "FA2_TOKEN_UNDEFINED" ;
         LAMBDA
           (pair (pair address nat) (big_map (pair address nat) nat))
           nat
           { UNPAIR ; GET ; IF_NONE { PUSH nat 0 } {} } ;
         LAMBDA
           (pair (lambda (pair (pair address nat) (big_map (pair address nat) nat)) nat)
                 (pair (pair address nat) nat (big_map (pair address nat) nat)))
           (big_map (pair address nat) nat)
           { UNPAIR ;
             SWAP ;
             UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DIG 3 ;
             DIG 3 ;
             PAIR ;
             DUP 3 ;
             DUP 2 ;
             PAIR ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DIG 2 ;
             ADD ;
             PUSH nat 0 ;
             DUP 2 ;
             COMPARE ;
             EQ ;
             IF { DROP ; NONE nat ; SWAP ; UPDATE }
                { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
         DUP 2 ;
         APPLY ;
         LAMBDA
           (pair (pair (lambda
                          (pair (pair address nat) nat (big_map (pair address nat) nat))
                          (big_map (pair address nat) nat))
                       string)
                 (pair (list (pair address nat nat))
                       (pair (pair address (big_map (pair address nat) nat))
                             (big_map (pair address address nat) unit)
                             (big_map nat (pair nat (map string bytes))))
                       (pair (big_map nat nat) (big_map address timestamp))
                       int))
           (pair (pair (pair address (big_map (pair address nat) nat))
                       (big_map (pair address address nat) unit)
                       (big_map nat (pair nat (map string bytes))))
                 (pair (big_map nat nat) (big_map address timestamp))
                 int)
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DUP 2 ;
             CAR ;
             CAR ;
             CDR ;
             DUP 2 ;
             ITER { SWAP ;
                    DUP 2 ;
                    GET 4 ;
                    PAIR ;
                    DUP 2 ;
                    GET 3 ;
                    DIG 2 ;
                    CAR ;
                    PAIR ;
                    PAIR ;
                    DUP 4 ;
                    SWAP ;
                    EXEC } ;
             DIG 3 ;
             DROP ;
             DUP 3 ;
             CDR ;
             CAR ;
             CAR ;
             DIG 2 ;
             ITER { SWAP ;
                    DUP ;
                    DUP 3 ;
                    GET 3 ;
                    GET ;
                    IF_NONE
                      { DROP 2 ; DUP 3 ; FAILWITH }
                      { DUP 3 ; GET 4 ; ADD ; SOME ; DIG 2 ; GET 3 ; UPDATE } } ;
             DIG 3 ;
             DROP ;
             DUP 3 ;
             CDR ;
             DUP 4 ;
             CAR ;
             CDR ;
             DIG 3 ;
             DIG 4 ;
             CAR ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             PAIR ;
             DUP ;
             CDR ;
             CDR ;
             DUP 2 ;
             CDR ;
             CAR ;
             CDR ;
             DIG 3 ;
             PAIR ;
             PAIR ;
             SWAP ;
             CAR ;
             PAIR } ;
         DUP 4 ;
         DUP 3 ;
         PAIR ;
         APPLY ;
         LAMBDA
           (pair bool string)
           unit
           { UNPAIR ; NOT ; IF { FAILWITH } { DROP ; UNIT } } ;
         LAMBDA
           (pair (lambda (pair bool string) unit)
                 (pair (pair (pair (pair (pair (pair (pair address (big_map (pair address nat) nat))
                                                     (big_map (pair address address nat) unit)
                                                     (big_map nat (pair nat (map string bytes))))
                                               (pair (big_map nat nat) (big_map address timestamp))
                                               int)
                                         address)
                                   mutez
                                   nat)
                             (pair (pair address (option nat)) address)
                             (option timestamp)
                             nat)
                       (pair (big_map string bytes) (option (pair (pair address bool) (option address))))
                       address))
           unit
           { UNPAIR ;
             SWAP ;
             CAR ;
             CDR ;
             CDR ;
             CAR ;
             IF_NONE
               { DROP ; PUSH string "NO_CONTEST" ; FAILWITH }
               { PUSH string "CONTEST_FINISHED" ; SWAP ; NOW ; COMPARE ; LT ; PAIR ; EXEC } } ;
         DUP 2 ;
         APPLY ;
         DIG 7 ;
         UNPAIR ;
         SELF_ADDRESS ;
         SWAP ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DIG 4 ;
                 DIG 5 ;
                 DROP 3 ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DIG 5 ;
                     DROP 4 ;
                     DUP 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     IF_LEFT
                       { IF_LEFT
                           { DIG 3 ;
                             DROP 2 ;
                             IF_NONE
                               { PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { DUP ;
                                 CDR ;
                                 IF_NONE
                                   { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                                   { SENDER ;
                                     COMPARE ;
                                     EQ ;
                                     IF { NONE address ; SWAP ; CAR ; CDR ; SENDER ; PAIR ; PAIR ; SOME }
                                        { DROP ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } } }
                           { DUP 2 ;
                             DIG 4 ;
                             SWAP ;
                             EXEC ;
                             DROP ;
                             SWAP ;
                             IF_NONE
                               { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { DUP ; CDR ; DUG 2 ; CAR ; CAR ; PAIR ; PAIR ; SOME } } }
                       { DUP 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         IF_NONE
                           { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                           { SWAP ; SOME ; SWAP ; CAR ; PAIR ; SOME } } ;
                     NIL operation ;
                     DUP 3 ;
                     CDR ;
                     CDR ;
                     DIG 2 ;
                     DUP 4 ;
                     CDR ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     DIG 2 ;
                     CAR }
                   { DIG 6 ;
                     DROP ;
                     DUP 2 ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     IF_LEFT
                       { IF_LEFT
                           { DIG 3 ;
                             DROP ;
                             DUP ;
                             CAR ;
                             MAP { DUP 3 ;
                                   CAR ;
                                   CDR ;
                                   CDR ;
                                   DUP 2 ;
                                   CDR ;
                                   MEM ;
                                   NOT ;
                                   IF { DROP ; DUP 5 ; FAILWITH }
                                      { DUP 3 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        DUP 2 ;
                                        CDR ;
                                        DUP 3 ;
                                        CAR ;
                                        PAIR ;
                                        PAIR ;
                                        DUP 6 ;
                                        SWAP ;
                                        EXEC ;
                                        SWAP ;
                                        PAIR } } ;
                             DIG 4 ;
                             DIG 5 ;
                             DROP 2 ;
                             SWAP ;
                             CDR ;
                             PUSH mutez 0 ;
                             DIG 2 ;
                             TRANSFER_TOKENS ;
                             SWAP ;
                             NIL operation ;
                             DIG 2 ;
                             CONS }
                           { DUP 2 ;
                             CAR ;
                             CAR ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             EQ ;
                             IF {} { PUSH string "NOT_CASINO" ; FAILWITH } ;
                             DUP 2 ;
                             CDR ;
                             CAR ;
                             CDR ;
                             DUP 2 ;
                             ITER { SWAP ;
                                    DUP 2 ;
                                    CDR ;
                                    ITER { SWAP ;
                                           DUP 5 ;
                                           CAR ;
                                           CDR ;
                                           CDR ;
                                           DIG 2 ;
                                           GET 3 ;
                                           MEM ;
                                           NOT ;
                                           IF { DROP ; DUP 7 ; FAILWITH }
                                              { DUP 4 ; CDR ; CDR ; NOW ; ADD ; SOME ; DUP 3 ; CAR ; UPDATE } } ;
                                    SWAP ;
                                    DROP } ;
                             DUP 3 ;
                             CAR ;
                             CAR ;
                             CDR ;
                             DIG 2 ;
                             ITER { SWAP ;
                                    DUP 2 ;
                                    CDR ;
                                    ITER { SWAP ;
                                           DUP 5 ;
                                           CAR ;
                                           CDR ;
                                           CDR ;
                                           DUP 3 ;
                                           GET 3 ;
                                           MEM ;
                                           NOT ;
                                           IF { DROP 2 ; DUP 7 ; FAILWITH }
                                              { DUP 5 ;
                                                CAR ;
                                                CDR ;
                                                CAR ;
                                                DUP 3 ;
                                                GET 3 ;
                                                PAIR ;
                                                SENDER ;
                                                DUP 5 ;
                                                CAR ;
                                                DIG 2 ;
                                                UNPAIR ;
                                                DUP 4 ;
                                                DUP 4 ;
                                                COMPARE ;
                                                EQ ;
                                                IF { DROP 4 }
                                                   { DIG 3 ;
                                                     PAIR ;
                                                     DIG 2 ;
                                                     PAIR ;
                                                     MEM ;
                                                     IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } ;
                                                DUP 2 ;
                                                GET 3 ;
                                                DUP 4 ;
                                                CAR ;
                                                PAIR ;
                                                DUP 2 ;
                                                DUP 2 ;
                                                PAIR ;
                                                DUP 10 ;
                                                SWAP ;
                                                EXEC ;
                                                DUP 4 ;
                                                GET 4 ;
                                                SWAP ;
                                                SUB ;
                                                ISNAT ;
                                                IF_NONE
                                                  { DROP 2 ; PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                                  { PUSH nat 0 ;
                                                    DUP 2 ;
                                                    COMPARE ;
                                                    EQ ;
                                                    IF { DROP ; NONE nat ; SWAP ; UPDATE }
                                                       { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
                                                DUP 2 ;
                                                GET 4 ;
                                                PAIR ;
                                                DUP 2 ;
                                                GET 3 ;
                                                DIG 2 ;
                                                CAR ;
                                                PAIR ;
                                                PAIR ;
                                                DUP 6 ;
                                                SWAP ;
                                                EXEC } } ;
                                    SWAP ;
                                    DROP } ;
                             DIG 4 ;
                             DIG 5 ;
                             DIG 6 ;
                             DROP 3 ;
                             DUP 3 ;
                             CDR ;
                             DUP 4 ;
                             CAR ;
                             CDR ;
                             DIG 2 ;
                             DIG 4 ;
                             CAR ;
                             CAR ;
                             CAR ;
                             PAIR ;
                             PAIR ;
                             PAIR ;
                             DUP ;
                             CDR ;
                             CDR ;
                             DIG 2 ;
                             DUP 3 ;
                             CDR ;
                             CAR ;
                             CAR ;
                             PAIR ;
                             PAIR ;
                             SWAP ;
                             CAR ;
                             PAIR ;
                             NIL operation } }
                       { DIG 3 ;
                         DIG 4 ;
                         DIG 5 ;
                         DROP 3 ;
                         SENDER ;
                         DUP 3 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         DIG 2 ;
                         ITER { SWAP ;
                                DUP 3 ;
                                DUP 3 ;
                                IF_LEFT {} {} ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                SWAP ;
                                IF_LEFT
                                  { SWAP ;
                                    UNIT ;
                                    SOME ;
                                    DUP 3 ;
                                    GET 4 ;
                                    DUP 4 ;
                                    GET 3 ;
                                    PAIR ;
                                    DIG 3 ;
                                    CAR ;
                                    PAIR ;
                                    UPDATE }
                                  { SWAP ;
                                    DUP 2 ;
                                    GET 4 ;
                                    DUP 3 ;
                                    GET 3 ;
                                    PAIR ;
                                    DIG 2 ;
                                    CAR ;
                                    PAIR ;
                                    NONE unit ;
                                    SWAP ;
                                    UPDATE } } ;
                         SWAP ;
                         DROP ;
                         DUP 2 ;
                         CDR ;
                         DUP 3 ;
                         CAR ;
                         CDR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation } ;
                     DUP 3 ;
                     CDR ;
                     DUP 4 ;
                     CAR ;
                     CDR ;
                     DUP 5 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 5 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 5 ;
                     PAIR ;
                     PAIR ;
                     PAIR } ;
                 PAIR ;
                 SWAP }
               { DIG 6 ;
                 DIG 8 ;
                 DROP 2 ;
                 IF_LEFT
                   { DIG 3 ;
                     DIG 4 ;
                     DIG 6 ;
                     DROP 3 ;
                     DUP 3 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH nat 0 ;
                     SOME ;
                     DUP 3 ;
                     PAIR ;
                     DUP 4 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     NIL (pair address nat nat) ;
                     DUP 4 ;
                     PUSH nat 0 ;
                     DUP 7 ;
                     PAIR 3 ;
                     CONS ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     DUP 5 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CDR ;
                     PUSH nat 1 ;
                     DUP 4 ;
                     CDR ;
                     IF_NONE
                       { DIG 5 ;
                         DROP ;
                         DUP 4 ;
                         CAR ;
                         CONTRACT %approve (pair (address %spender) (nat %value)) ;
                         IF_NONE
                           { DROP 2 ; PUSH string "FA12_APPROVE_FAIL" ; FAILWITH }
                           { PUSH mutez 0 ; DIG 2 ; DIG 3 ; PAIR ; TRANSFER_TOKENS } }
                       { DROP ;
                         DUP 4 ;
                         CDR ;
                         IF_NONE { PUSH string "NO_ID" ; FAILWITH } {} ;
                         DIG 2 ;
                         DIG 6 ;
                         PAIR 3 ;
                         DUP 4 ;
                         CAR ;
                         CONTRACT %update_operators
                           (list (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                                     (pair %remove_operator (address %owner) (address %operator) (nat %token_id)))) ;
                         IF_NONE
                           { DROP 2 ; PUSH string "FA2_UPD_OP_FAIL" ; FAILWITH }
                           { PUSH mutez 0 ;
                             NIL (or (pair address address nat) (pair address address nat)) ;
                             PUSH nat 0 ;
                             DIG 5 ;
                             COMPARE ;
                             GT ;
                             IF { DIG 3 ; LEFT (pair address address nat) }
                                { DIG 3 ; RIGHT (pair address address nat) } ;
                             CONS ;
                             TRANSFER_TOKENS } } ;
                     DUP 5 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CDR ;
                     CONTRACT %invest
                       (pair (pair %bankroll_currency (address %fa2_address) (option %token_id nat))
                             (nat %amt)) ;
                     IF_NONE
                       { DIG 2 ; DIG 3 ; DROP 2 ; PUSH string "INVEST_FATAL" ; FAILWITH }
                       { PUSH mutez 0 ; DIG 5 ; DIG 5 ; PAIR ; TRANSFER_TOKENS } ;
                     DUP 4 ;
                     CDR ;
                     DUP 5 ;
                     CAR ;
                     CDR ;
                     DUP 6 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 6 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR }
                   { SWAP ;
                     DIG 7 ;
                     DROP 3 ;
                     DUP ;
                     SENDER ;
                     SWAP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     GET ;
                     IF_NONE
                       {}
                       { PUSH string "MUST_WAIT_FOR_TOPUP" ;
                         SWAP ;
                         NOW ;
                         COMPARE ;
                         GT ;
                         PAIR ;
                         DUP 4 ;
                         SWAP ;
                         EXEC ;
                         DROP } ;
                     DUP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     SWAP ;
                     PUSH nat 0 ;
                     DIG 2 ;
                     PAIR ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     PUSH string "NON_ZERO_BALANCE" ;
                     PUSH nat 0 ;
                     DIG 2 ;
                     COMPARE ;
                     EQ ;
                     PAIR ;
                     DUP 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP ;
                     DIG 2 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PUSH string "WRONG_PRICE" ;
                     DUP 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     CAR ;
                     AMOUNT ;
                     COMPARE ;
                     EQ ;
                     PAIR ;
                     DIG 2 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CAR ;
                     NIL (pair address nat nat) ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     CDR ;
                     PUSH nat 0 ;
                     SENDER ;
                     PAIR 3 ;
                     CONS ;
                     PAIR ;
                     DIG 2 ;
                     SWAP ;
                     EXEC ;
                     BALANCE ;
                     DUP 3 ;
                     CDR ;
                     CDR ;
                     CONTRACT unit ;
                     IF_NONE { PUSH string "0" ; FAILWITH } {} ;
                     SWAP ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CAR ;
                     DUP 4 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     CDR ;
                     PAIR ;
                     DUP 4 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     DIG 2 ;
                     UNPAIR ;
                     DUP 2 ;
                     CDR ;
                     IF_NONE
                       { SWAP ;
                         CAR ;
                         CONTRACT %transfer (pair (address %from) (address %to) (nat %value)) ;
                         IF_NONE { PUSH string "Invalid FA1.2 Address" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         DIG 4 ;
                         DIG 4 ;
                         PAIR 3 ;
                         TRANSFER_TOKENS }
                       { DROP ;
                         DUP 2 ;
                         CDR ;
                         IF_NONE { PUSH string "NO_ID" ; FAILWITH } {} ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR ;
                         DUP ;
                         CAR ;
                         CONTRACT %transfer
                           (list (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))) ;
                         IF_NONE { PUSH string "Invalid FA2 Address" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         NIL (pair address (list (pair address nat nat))) ;
                         NIL (pair address nat nat) ;
                         DUP 5 ;
                         CDR ;
                         CDR ;
                         DIG 5 ;
                         CDR ;
                         CAR ;
                         DIG 7 ;
                         PAIR 3 ;
                         CONS ;
                         DIG 4 ;
                         PAIR ;
                         CONS ;
                         TRANSFER_TOKENS } ;
                     DUP 4 ;
                     CDR ;
                     DUP 5 ;
                     CAR ;
                     CDR ;
                     DUP 6 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 6 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     CDR } ;
                 DIG 6 ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 DIG 2 ;
                 CONS } }
           { SWAP ;
             DIG 3 ;
             DIG 4 ;
             DIG 5 ;
             DIG 6 ;
             DIG 8 ;
             DROP 6 ;
             DUP 2 ;
             CDR ;
             CAR ;
             CDR ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DROP ;
             DUP 2 ;
             CAR ;
             CAR ;
             CAR ;
             CAR ;
             CAR ;
             CAR ;
             CDR ;
             PUSH nat 0 ;
             DUP 4 ;
             CAR ;
             CDR ;
             CAR ;
             CDR ;
             PAIR ;
             PAIR ;
             DIG 3 ;
             SWAP ;
             EXEC ;
             DUP 3 ;
             CDR ;
             DUP 4 ;
             CAR ;
             CDR ;
             DUP 5 ;
             CAR ;
             CAR ;
             CDR ;
             DUP 6 ;
             CAR ;
             CAR ;
             CAR ;
             CDR ;
             DUP 7 ;
             CAR ;
             CAR ;
             CAR ;
             CAR ;
             DUP ;
             CDR ;
             DUP 2 ;
             CAR ;
             CDR ;
             EMPTY_BIG_MAP (pair address nat) nat ;
             DUP 9 ;
             PUSH nat 0 ;
             DIG 12 ;
             CAR ;
             CDR ;
             CAR ;
             CDR ;
             PAIR ;
             SWAP ;
             SOME ;
             SWAP ;
             UPDATE ;
             DIG 3 ;
             CAR ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             PAIR ;
             DUP ;
             CDR ;
             CDR ;
             DUP 2 ;
             CDR ;
             CAR ;
             CDR ;
             EMPTY_BIG_MAP nat nat ;
             DIG 8 ;
             SOME ;
             PUSH nat 0 ;
             UPDATE ;
             PAIR ;
             PAIR ;
             SWAP ;
             CAR ;
             PAIR ;
             PAIR ;
             PAIR ;
             PAIR ;
             PAIR ;
             DUP ;
             CDR ;
             DUP 2 ;
             CAR ;
             CDR ;
             CDR ;
             CDR ;
             DIG 3 ;
             SOME ;
             PAIR ;
             DUP 3 ;
             CAR ;
             CDR ;
             CAR ;
             PAIR ;
             DIG 2 ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             NIL operation } ;
         PAIR } }
