{ parameter
    (or (or (or (address %admin_add) (address %admin_remove))
            (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (or (list %freeze_tokens nat)
                    (pair %mint
                       (address %address)
                       (pair (nat %amount) (pair (map %metadata string bytes) (nat %token_id)))))))
        (or (or (pair %set_metadata (string %k) (bytes %v))
                (or (address %set_minter) (bool %set_pause)))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (or (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                    (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes))))))) ;
  storage
    (pair (pair (pair (set %administrator address) (nat %all_tokens))
                (pair (set %frozen_tokens nat)
                      (pair (big_map %ledger (pair address nat) nat) (big_map %metadata string bytes))))
          (pair (pair (address %minter)
                      (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (bool %paused)
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (big_map %total_supply nat nat))))) ;
  code { CAST (pair (or (or (or address address)
                            (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (or (list nat) (pair address (pair nat (pair (map string bytes) nat))))))
                        (or (or (pair string bytes) (or address bool))
                            (or (list (pair address (list (pair address (pair nat nat)))))
                                (or (list (or (pair address (pair address nat)) (pair address (pair address nat))))
                                    (list (pair nat (map string bytes)))))))
                    (pair (pair (pair (set address) nat)
                                (pair (set nat) (pair (big_map (pair address nat) nat) (big_map string bytes))))
                          (pair (pair address (big_map (pair address (pair address nat)) unit))
                                (pair bool (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat)))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH bool True ;
                     DIG 5 ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     PUSH nat 1 ;
                     DUP 3 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SIZE ;
                     COMPARE ;
                     GT ;
                     IF {} { PUSH string "MUST HAVE ATLEAST ONE ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     PUSH bool False ;
                     DIG 5 ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR } ;
                 NIL operation }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 7 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           GET 5 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           GET ;
                           IF_NONE { PUSH nat 0 } {} ;
                           SWAP ;
                           PAIR } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         ITER { DUP 3 ;
                                GET 8 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                MEM ;
                                IF {} { PUSH string "NON-EXISTANT TOKEN CAN'T BE FROZEN" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                PUSH bool True ;
                                DIG 5 ;
                                UPDATE ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF { PUSH bool True }
                            { SWAP ; DUP ; DUG 2 ; GET 3 ; CAR ; SENDER ; COMPARE ; EQ } ;
                         IF {} { PUSH string "FA2_NOT_ADMIN_OR_MINTER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         MEM ;
                         IF { PUSH string "FA2_TOKEN_FROZEN" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         DUP 6 ;
                         GET 3 ;
                         DIG 7 ;
                         CAR ;
                         GET 5 ;
                         DIG 7 ;
                         DUP ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 9 ;
                         CAR ;
                         PAIR ;
                         GET ;
                         IF_NONE { PUSH nat 0 } {} ;
                         ADD ;
                         SOME ;
                         DIG 6 ;
                         DUP ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 8 ;
                         CAR ;
                         PAIR ;
                         UPDATE ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         COMPARE ;
                         LT ;
                         IF { SWAP ;
                              DUP ;
                              GET 8 ;
                              DUP ;
                              DUP 4 ;
                              GET 6 ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 209 ; FAILWITH } {} ;
                              DIG 4 ;
                              GET 3 ;
                              ADD ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              UPDATE 8 }
                            { DUP ;
                              GET 6 ;
                              DUP 3 ;
                              CAR ;
                              CAR ;
                              CDR ;
                              COMPARE ;
                              EQ ;
                              IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                              SWAP ;
                              DUP ;
                              GET 8 ;
                              DUP 3 ;
                              GET 3 ;
                              SOME ;
                              DUP 4 ;
                              GET 6 ;
                              UPDATE ;
                              UPDATE 8 ;
                              DUP ;
                              GET 7 ;
                              DIG 2 ;
                              DUP ;
                              GET 5 ;
                              SWAP ;
                              DUP ;
                              DUG 4 ;
                              GET 6 ;
                              PAIR ;
                              SOME ;
                              DIG 3 ;
                              GET 6 ;
                              UPDATE ;
                              UPDATE 7 ;
                              UNPAIR ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              PUSH nat 1 ;
                              ADD ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              PAIR } } ;
                     NIL operation } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DUP 6 ;
                     CDR ;
                     SOME ;
                     DIG 6 ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 5 } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   CAR ;
                                   CAR ;
                                   CAR ;
                                   SENDER ;
                                   MEM ;
                                   IF { PUSH bool True }
                                      { SWAP ; DUP ; DUG 2 ; CAR ; SENDER ; COMPARE ; EQ } ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP 4 ;
                                   GET 7 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        DUP 5 ;
                                        CAR ;
                                        GET 5 ;
                                        DUP 3 ;
                                        GET 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 168 ; FAILWITH } {} ;
                                        COMPARE ;
                                        GE ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        DUP 6 ;
                                        GET 4 ;
                                        DIG 9 ;
                                        CAR ;
                                        GET 5 ;
                                        DUP 8 ;
                                        GET 3 ;
                                        DUP 10 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 258 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH int 257 ; FAILWITH } {} ;
                                        SOME ;
                                        DUP 7 ;
                                        GET 3 ;
                                        DUP 9 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        DUP 6 ;
                                        GET 4 ;
                                        DIG 9 ;
                                        CAR ;
                                        GET 5 ;
                                        DIG 7 ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 9 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} ;
                                        ADD ;
                                        SOME ;
                                        DIG 6 ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { IF_LEFT
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP 3 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    SENDER ;
                                    MEM ;
                                    IF { PUSH bool True } { DUP ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 5 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP }
                                  { DUP 3 ;
                                    CAR ;
                                    CAR ;
                                    CAR ;
                                    SENDER ;
                                    MEM ;
                                    IF { PUSH bool True } { DUP ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    NONE unit ;
                                    DIG 5 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP } } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         ITER { DUP 3 ;
                                GET 7 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                MEM ;
                                IF {} { PUSH string "TOKN UNDEFINED" ; FAILWITH } ;
                                DUP 3 ;
                                CAR ;
                                GET 3 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                MEM ;
                                IF { PUSH string "FA2_TOKEN_FROZEN" ; FAILWITH } {} ;
                                DIG 2 ;
                                DUP ;
                                GET 7 ;
                                DIG 2 ;
                                DUP ;
                                SOME ;
                                SWAP ;
                                CAR ;
                                UPDATE ;
                                UPDATE 7 ;
                                SWAP } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
