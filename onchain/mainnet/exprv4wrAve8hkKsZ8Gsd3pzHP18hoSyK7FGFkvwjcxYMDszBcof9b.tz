{ parameter
    (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
            (or %assets
               (or (pair %balance_of
                      (list %requests (pair (address %owner) (nat %token_id)))
                      (contract %callback
                         (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                   (list %transfer
                      (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))))
               (list %update_operators
                  (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                      (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))))
        (or (or %postcards
               (or (list %mail_postcard
                      (pair (address %address) (nat %token_id) (bytes %token_info_uri)))
                   (list %mint_postcard (pair (address %owner) (nat %token_id))))
               (list %stamp_postcard (pair (nat %token_id) (bytes %token_info_uri))))
            (or %trustee (address %add_trustee) (address %remove_trustee)))) ;
  storage
    (pair (pair (pair (pair %admin (pair (address %admin) (bool %paused)) (option %pending_admin address))
                      (pair %assets
                         (pair (big_map %ledger (pair address nat) nat)
                               (big_map %operators (pair address address nat) unit))
                         (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                         (big_map %token_total_supply nat nat)))
                (big_map %metadata string bytes)
                (pair %postcards (bytes %default_card_token_info) (big_map %stamp_info nat bool)))
          (pair %trustee (nat %max_trustee) (set %trustees address))) ;
  code { PUSH bool True ;
         UNIT ;
         PUSH string "FA2_TOKEN_UNDEFINED" ;
         LAMBDA
           (pair unit (pair (pair address bool) (option address)))
           unit
           { UNPAIR ;
             SWAP ;
             CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { DROP ; PUSH string "NOT_AN_ADMIN" ; FAILWITH } {} } ;
         DUP 3 ;
         APPLY ;
         LAMBDA
           (pair (pair address nat) (big_map (pair address nat) nat))
           nat
           { UNPAIR ; GET ; IF_NONE { PUSH nat 0 } {} } ;
         LAMBDA
           (pair (pair nat bytes) (big_map nat (pair nat (map string bytes))))
           (big_map nat (pair nat (map string bytes)))
           { UNPAIR ;
             UNPAIR ;
             DUP 3 ;
             DUP 2 ;
             MEM ;
             NOT ;
             IF { PUSH string "TOKEN_METADATA_NOT_FOUND" ; FAILWITH } {} ;
             DIG 2 ;
             EMPTY_MAP string bytes ;
             DIG 3 ;
             SOME ;
             PUSH string "" ;
             UPDATE ;
             DUP 3 ;
             PAIR ;
             SOME ;
             DIG 2 ;
             UPDATE } ;
         DIG 6 ;
         UNPAIR ;
         IF_LEFT
           { DIG 2 ;
             DIG 7 ;
             DROP 2 ;
             IF_LEFT
               { DIG 2 ;
                 DIG 4 ;
                 DIG 5 ;
                 DROP 3 ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DIG 3 ;
                         DROP 2 ;
                         DUP ;
                         CDR ;
                         IF_NONE
                           { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             SWAP ;
                             DUP 2 ;
                             COMPARE ;
                             EQ ;
                             IF { NONE address ; DIG 2 ; CAR ; CDR ; DIG 2 ; PAIR ; PAIR }
                                { DROP 2 ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } }
                       { DUP 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP 2 ;
                         CDR ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR } }
                   { DUP 2 ; DIG 4 ; SWAP ; EXEC ; DROP ; SOME ; SWAP ; CAR ; PAIR } ;
                 NIL operation ;
                 DUP 3 ;
                 CDR ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DIG 4 }
               { DIG 3 ;
                 DROP ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CDR ;
                 IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { DIG 5 ;
                     DROP ;
                     IF_LEFT
                       { DUP ;
                         CAR ;
                         MAP { DUP 3 ;
                               CDR ;
                               CAR ;
                               DUP 2 ;
                               CDR ;
                               MEM ;
                               NOT ;
                               IF { DROP ; DUP 5 ; FAILWITH }
                                  { DUP 3 ;
                                    CAR ;
                                    CAR ;
                                    DUP 2 ;
                                    CDR ;
                                    DUP 3 ;
                                    CAR ;
                                    PAIR ;
                                    PAIR ;
                                    DUP 6 ;
                                    SWAP ;
                                    EXEC ;
                                    SWAP ;
                                    PAIR } } ;
                         DIG 4 ;
                         DIG 5 ;
                         DROP 2 ;
                         SWAP ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS }
                       { DUP 2 ;
                         CAR ;
                         CAR ;
                         SWAP ;
                         ITER { SWAP ;
                                DUP 2 ;
                                CDR ;
                                ITER { SWAP ;
                                       DUP 4 ;
                                       CDR ;
                                       CAR ;
                                       DUP 3 ;
                                       GET 3 ;
                                       MEM ;
                                       NOT ;
                                       IF { DROP 2 ; DUP 5 ; FAILWITH }
                                          { SENDER ;
                                            DUP 4 ;
                                            CAR ;
                                            DUP 2 ;
                                            DUP 2 ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DROP 2 }
                                               { DUP 6 ;
                                                 CAR ;
                                                 CDR ;
                                                 DUP 5 ;
                                                 GET 3 ;
                                                 DIG 3 ;
                                                 PAIR ;
                                                 DIG 2 ;
                                                 PAIR ;
                                                 MEM ;
                                                 IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } ;
                                            DUP 2 ;
                                            GET 3 ;
                                            DUP 4 ;
                                            CAR ;
                                            PAIR ;
                                            DUP 2 ;
                                            DUP 2 ;
                                            PAIR ;
                                            DUP 8 ;
                                            SWAP ;
                                            EXEC ;
                                            DUP 4 ;
                                            GET 4 ;
                                            SWAP ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE
                                              { DROP 2 ; PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                              { PUSH nat 0 ;
                                                DUP 2 ;
                                                COMPARE ;
                                                EQ ;
                                                IF { DROP ; NONE nat ; SWAP ; UPDATE }
                                                   { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
                                            DUP 2 ;
                                            GET 3 ;
                                            DUP 3 ;
                                            CAR ;
                                            PAIR ;
                                            DUP 2 ;
                                            DUP 2 ;
                                            PAIR ;
                                            DUP 8 ;
                                            SWAP ;
                                            EXEC ;
                                            DIG 3 ;
                                            GET 4 ;
                                            ADD ;
                                            PUSH nat 0 ;
                                            DUP 2 ;
                                            COMPARE ;
                                            EQ ;
                                            IF { DROP ; NONE nat ; SWAP ; UPDATE }
                                               { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } } ;
                                SWAP ;
                                DROP } ;
                         DIG 3 ;
                         DIG 4 ;
                         DROP 2 ;
                         DUP 2 ;
                         CDR ;
                         DIG 2 ;
                         CAR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         PAIR ;
                         NIL operation } }
                   { DIG 3 ;
                     DIG 4 ;
                     DROP 2 ;
                     SENDER ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     DIG 2 ;
                     ITER { SWAP ;
                            DUP 3 ;
                            DUP 3 ;
                            IF_LEFT {} {} ;
                            CAR ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                            SWAP ;
                            IF_LEFT
                              { SWAP ;
                                DUP 6 ;
                                SOME ;
                                DUP 3 ;
                                GET 4 ;
                                DUP 4 ;
                                GET 3 ;
                                PAIR ;
                                DIG 3 ;
                                CAR ;
                                PAIR ;
                                UPDATE }
                              { SWAP ;
                                DUP 2 ;
                                GET 4 ;
                                DUP 3 ;
                                GET 3 ;
                                PAIR ;
                                DIG 2 ;
                                CAR ;
                                PAIR ;
                                NONE unit ;
                                SWAP ;
                                UPDATE } } ;
                     SWAP ;
                     DIG 4 ;
                     DROP 2 ;
                     DUP 2 ;
                     CDR ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     NIL operation } ;
                 DUP 3 ;
                 CDR ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CAR } ;
             PAIR ;
             PAIR ;
             PAIR ;
             SWAP }
           { DIG 3 ;
             DIG 5 ;
             DIG 6 ;
             DROP 3 ;
             IF_LEFT
               { DIG 3 ;
                 DROP ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DUP 3 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 DIG 2 ;
                 IF_LEFT
                   { DIG 5 ;
                     DROP ;
                     IF_LEFT
                       { DUG 2 ;
                         PAIR ;
                         SWAP ;
                         ITER { SWAP ;
                                UNPAIR ;
                                DUP ;
                                CDR ;
                                DUP 4 ;
                                GET 3 ;
                                DUP 2 ;
                                DUP 2 ;
                                GET ;
                                IF_NONE
                                  { DROP 2 ; PUSH string "TOKEN_HAS_NOT_STAMPED_YET" ; FAILWITH }
                                  { IF { SWAP ; PUSH bool False ; SOME ; DIG 2 ; UPDATE }
                                       { DROP 2 ; PUSH string "TOKEN_HAS_NOT_STAMPED_YET" ; FAILWITH } } ;
                                DUP 3 ;
                                CAR ;
                                CAR ;
                                DUP 5 ;
                                GET 3 ;
                                DUP 6 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE
                                  { PUSH nat 1 }
                                  { PUSH nat 0 ;
                                    SWAP ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH nat 1 } { PUSH string "DUPLICATED_OWNER" ; FAILWITH } } ;
                                DUP 4 ;
                                CDR ;
                                CDR ;
                                DUP 6 ;
                                GET 3 ;
                                DUP 2 ;
                                DUP 2 ;
                                GET ;
                                IF_NONE
                                  { DROP 2 ; PUSH string "TOKEN NOT FOUND" ; FAILWITH }
                                  { PUSH nat 1 ; ADD ; DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } ;
                                DUP 5 ;
                                CDR ;
                                CAR ;
                                DUP 7 ;
                                GET 4 ;
                                DUP 8 ;
                                GET 3 ;
                                PAIR ;
                                PAIR ;
                                DUP 9 ;
                                SWAP ;
                                EXEC ;
                                DUP 6 ;
                                CDR ;
                                DUP 7 ;
                                CAR ;
                                CDR ;
                                DIG 7 ;
                                CAR ;
                                CAR ;
                                DIG 5 ;
                                SOME ;
                                DUP 9 ;
                                GET 3 ;
                                DIG 9 ;
                                CAR ;
                                PAIR ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                DUP ;
                                CDR ;
                                CDR ;
                                DIG 2 ;
                                PAIR ;
                                SWAP ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                DUP 2 ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                CAR ;
                                PAIR ;
                                SWAP ;
                                DIG 2 ;
                                CAR ;
                                PAIR ;
                                PAIR } ;
                         DIG 2 ;
                         DROP ;
                         UNPAIR ;
                         SWAP }
                       { DIG 4 ;
                         DROP ;
                         DIG 2 ;
                         SWAP ;
                         ITER { SWAP ;
                                DUP 2 ;
                                CDR ;
                                DUP 2 ;
                                CDR ;
                                CAR ;
                                DUP 2 ;
                                GET ;
                                IF_NONE
                                  { DUP 2 ;
                                    CDR ;
                                    DUP 3 ;
                                    CAR ;
                                    CDR ;
                                    DUP 4 ;
                                    CAR ;
                                    CAR ;
                                    PUSH nat 1 ;
                                    DUP 5 ;
                                    DIG 7 ;
                                    CAR ;
                                    PAIR ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR ;
                                    PAIR ;
                                    DUP ;
                                    CDR ;
                                    CDR ;
                                    DUP 4 ;
                                    CDR ;
                                    CAR ;
                                    EMPTY_MAP string bytes ;
                                    DUP 7 ;
                                    CAR ;
                                    SOME ;
                                    PUSH string "" ;
                                    UPDATE ;
                                    DUP 5 ;
                                    PAIR ;
                                    DUP 5 ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR ;
                                    SWAP ;
                                    CAR ;
                                    PAIR ;
                                    DIG 2 ;
                                    CDR ;
                                    CDR ;
                                    PUSH nat 1 ;
                                    DIG 3 ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    DUP 2 ;
                                    CDR ;
                                    CAR ;
                                    PAIR ;
                                    SWAP ;
                                    CAR ;
                                    PAIR }
                                  { DROP 4 ; PUSH string "TOKEN_HAS_ALREADY_EXISTED" ; FAILWITH } } } }
                   { DUG 2 ;
                     PAIR ;
                     SWAP ;
                     ITER { SWAP ;
                            UNPAIR ;
                            DUP ;
                            CDR ;
                            DUP 4 ;
                            CAR ;
                            DUP 2 ;
                            DUP 2 ;
                            GET ;
                            IF_NONE
                              { SWAP ; DUP 8 ; DIG 2 ; SWAP ; SOME ; SWAP ; UPDATE }
                              { IF { DROP 2 ; PUSH string "TOKEN_HAS_ALREADY_STAMPED" ; FAILWITH }
                                   { SWAP ; DUP 8 ; SOME ; DIG 2 ; UPDATE } } ;
                            DUP 3 ;
                            CDR ;
                            CAR ;
                            DUP 5 ;
                            CDR ;
                            DIG 5 ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            DUP 6 ;
                            SWAP ;
                            EXEC ;
                            DUP 4 ;
                            CDR ;
                            CDR ;
                            SWAP ;
                            PAIR ;
                            DIG 3 ;
                            CAR ;
                            PAIR ;
                            SWAP ;
                            DIG 2 ;
                            CAR ;
                            PAIR ;
                            PAIR } ;
                     DIG 2 ;
                     DIG 3 ;
                     DROP 2 ;
                     UNPAIR ;
                     SWAP } ;
                 SWAP ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP 4 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 PAIR ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 DUP ;
                 CDR ;
                 DUP 2 ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR }
               { DIG 2 ;
                 DIG 4 ;
                 DROP 2 ;
                 DUP 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP 2 ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { DUP 2 ;
                     CAR ;
                     DUP 3 ;
                     CDR ;
                     SIZE ;
                     COMPARE ;
                     GE ;
                     IF { PUSH string "OVER_MAX_TRUSTEE_AMOUNT" ; FAILWITH } {} ;
                     DUP 2 ;
                     CDR ;
                     DUP 2 ;
                     MEM ;
                     IF { PUSH string "TRUSTEE_ALREADY_EXIST" ; FAILWITH } {} ;
                     DUP 2 ;
                     CDR ;
                     SWAP ;
                     PUSH bool True ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     CAR }
                   { DUP 2 ;
                     CDR ;
                     DUP 2 ;
                     MEM ;
                     NOT ;
                     IF { PUSH string "NOT_TRUSTEE" ; FAILWITH } {} ;
                     DUP 2 ;
                     CDR ;
                     SWAP ;
                     PUSH bool False ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     CAR } ;
                 PAIR ;
                 SWAP ;
                 CAR } ;
             PAIR ;
             NIL operation } ;
         PAIR } }
