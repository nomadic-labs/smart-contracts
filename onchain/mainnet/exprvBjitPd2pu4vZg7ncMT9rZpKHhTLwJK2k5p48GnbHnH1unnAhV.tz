{ parameter
    (or (or (or (nat %cancel_swap) (pair %collect (nat %objkt_amount) (nat %swap_id)))
            (or (pair %curate (nat %hDAO_amount) (nat %objkt_id)) (unit %default)))
        (or (pair %mint_OBJKT
               (pair (address %address) (nat %amount))
               (bytes %metadata)
               (nat %royalties))
            (pair %swap (nat %objkt_amount) (nat %objkt_id) (mutez %xtz_per_objkt)))) ;
  storage
    (pair (pair (pair (address %administrator) (set %coreParticipants address))
                (address %hicetnuncMinterAddress)
                (map %shares address nat))
          (nat %totalShares)) ;
  code { LAMBDA
           (pair (pair (pair address (set address)) (pair address (map address nat))) nat)
           unit
           { CAR ;
             CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             EQ ;
             IF { UNIT }
                { PUSH string "Entrypoint can call only administrator" ; FAILWITH } } ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CONTRACT %cancel_swap nat ;
                     IF_NONE { PUSH string "No minter found" ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CONTRACT %collect (pair (nat %objkt_amount) (nat %swap_id)) ;
                     IF_NONE { PUSH string "No minter found" ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CONTRACT %curate (pair (nat %hDAO_amount) (nat %objkt_id)) ;
                     IF_NONE { PUSH string "No minter found" ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS ;
                     SWAP ;
                     NIL operation ;
                     DIG 2 ;
                     CONS ;
                     PAIR }
                   { DROP ;
                     SWAP ;
                     DROP ;
                     NIL operation ;
                     PUSH nat 0 ;
                     PUSH mutez 0 ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     ITER { SWAP ;
                            PAIR ;
                            DUP ;
                            CAR ;
                            CAR ;
                            CAR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            CDR ;
                            PUSH nat 1 ;
                            DUP 4 ;
                            CAR ;
                            CAR ;
                            CDR ;
                            ADD ;
                            DUP 5 ;
                            CAR ;
                            CDR ;
                            CDR ;
                            SIZE ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            COMPARE ;
                            EQ ;
                            IF { DUP 3 ; AMOUNT ; SUB }
                               { DUP 5 ;
                                 CDR ;
                                 DUP 5 ;
                                 CDR ;
                                 CDR ;
                                 AMOUNT ;
                                 DUG 2 ;
                                 PUSH mutez 1 ;
                                 DIG 3 ;
                                 EDIV ;
                                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                 CAR ;
                                 MUL ;
                                 EDIV ;
                                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                 CAR ;
                                 PUSH mutez 1 ;
                                 SWAP ;
                                 MUL } ;
                            DUP 5 ;
                            CDR ;
                            CAR ;
                            CONTRACT unit ;
                            IF_NONE { PUSH string "Not a contract" ; FAILWITH } {} ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            UNIT ;
                            TRANSFER_TOKENS ;
                            DUP 6 ;
                            CDR ;
                            DUP 7 ;
                            CAR ;
                            CDR ;
                            DIG 4 ;
                            DIG 7 ;
                            CAR ;
                            CAR ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            PAIR ;
                            DUP ;
                            CDR ;
                            PUSH mutez 0 ;
                            DUP 5 ;
                            COMPARE ;
                            GT ;
                            IF { DIG 4 ; DIG 3 ; CONS } { DIG 2 ; DROP ; DIG 3 } ;
                            DIG 2 ;
                            CAR ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            SWAP ;
                            DIG 2 ;
                            ADD ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CAR ;
                            CDR ;
                            DIG 2 ;
                            CAR ;
                            CAR ;
                            CDR ;
                            DIG 2 ;
                            PAIR ;
                            PAIR } ;
                     CDR ;
                     PAIR } } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 CONTRACT %mint_OBJKT
                   (pair (pair (address %address) (nat %amount)) (pair (bytes %metadata) (nat %royalties))) ;
                 IF_NONE { PUSH string "No minter found" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 TRANSFER_TOKENS ;
                 SWAP ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR }
               { SWAP ;
                 DUP ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 CAR ;
                 CONTRACT %swap
                   (pair (nat %objkt_amount) (pair (nat %objkt_id) (mutez %xtz_per_objkt))) ;
                 IF_NONE { PUSH string "No minter found" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 TRANSFER_TOKENS ;
                 SWAP ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR } } } }
