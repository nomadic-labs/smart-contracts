{ parameter
    (or (or (or (unit %default) (or (unit %enableKYC) (string %enableKYCPlatform)))
            (or (or (string %registerProof) (address %removeIdentity))
                (or (pair %removeProof (address %address) (string %prooftype))
                    (pair %send (mutez %amount) (address %receiverAddress)))))
        (or (or (address %setAdmin) (or (option %setBaker key_hash) (mutez %setCost)))
            (or (or (set %setKycPlatforms string)
                    (pair %setProofMeta
                       (pair (address %address) (string %key))
                       (pair (string %prooftype) (string %value))))
                (or (address %setStore) (pair %verifyProof (address %address) (string %prooftype)))))) ;
  storage
    (pair (pair (address %admin) (mutez %cost))
          (pair (address %idstore)
                (pair (set %kycPlatforms string) (big_map %metadata string bytes)))) ;
  code { LAMBDA
           (pair unit
                 (pair (pair address mutez) (pair address (pair (set string) (big_map string bytes)))))
           (pair unit
                 (pair (pair address mutez) (pair address (pair (set string) (big_map string bytes)))))
           { CDR ;
             DUP ;
             CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             EQ ;
             IF {} { PUSH string "Only admin can call this entrypoint" ; FAILWITH } ;
             UNIT ;
             PAIR } ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DROP ; SWAP ; DROP ; NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         PUSH string "gov" ;
                         MEM ;
                         IF {}
                            { PUSH string "Missing required proof for this entrypoint" ; FAILWITH } ;
                         PUSH bool False ;
                         NOW ;
                         EMPTY_MAP string string ;
                         PAIR 3 ;
                         DUP 3 ;
                         GET 3 ;
                         SENDER ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         PUSH string "gov" ;
                         MEM ;
                         IF { DROP 2 ;
                              SWAP ;
                              DROP ;
                              DUP ;
                              GET 3 ;
                              SENDER ;
                              VIEW "getProofsForAddress"
                                   (map string
                                        (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                              IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                              PUSH string "gov" ;
                              GET ;
                              IF_NONE { PUSH int 47 ; FAILWITH } {} }
                            { SWAP ; DROP ; DIG 2 ; DROP } ;
                         DUP ;
                         CAR ;
                         PUSH (option string) (Some "true") ;
                         PUSH string "kyc" ;
                         UPDATE ;
                         UPDATE 1 ;
                         PUSH bool False ;
                         UPDATE 4 ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %setProof
                           (pair (address %address)
                                 (pair (pair %proof
                                          (map %meta string string)
                                          (pair (timestamp %register_date) (bool %verified)))
                                       (string %prooftype))) ;
                         IF_NONE { PUSH int 111 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         PUSH string "gov" ;
                         DIG 4 ;
                         SENDER ;
                         PAIR 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         MEM ;
                         IF {} { PUSH string "KYC platform not supported" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SENDER ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         PUSH string "gov" ;
                         MEM ;
                         IF {}
                            { PUSH string "Missing required proof for this entrypoint" ; FAILWITH } ;
                         PUSH bool False ;
                         NOW ;
                         EMPTY_MAP string string ;
                         PAIR 3 ;
                         DUP 3 ;
                         GET 3 ;
                         SENDER ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         PUSH string "gov" ;
                         MEM ;
                         IF { DROP ;
                              DIG 2 ;
                              DROP ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              SENDER ;
                              VIEW "getProofsForAddress"
                                   (map string
                                        (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                              IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                              PUSH string "gov" ;
                              GET ;
                              IF_NONE { PUSH int 47 ; FAILWITH } {} }
                            { DIG 3 ; DROP } ;
                         DUP ;
                         CAR ;
                         PUSH (option string) (Some "true") ;
                         DIG 3 ;
                         UPDATE ;
                         UPDATE 1 ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %setProof
                           (pair (address %address)
                                 (pair (pair %proof
                                          (map %meta string string)
                                          (pair (timestamp %register_date) (bool %verified)))
                                       (string %prooftype))) ;
                         IF_NONE { PUSH int 121 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         PUSH string "gov" ;
                         DIG 4 ;
                         SENDER ;
                         PAIR 3 ;
                         TRANSFER_TOKENS ;
                         CONS } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         AMOUNT ;
                         COMPARE ;
                         LT ;
                         IF { PUSH string "Amount too low" ; FAILWITH } {} ;
                         PUSH bool False ;
                         NOW ;
                         EMPTY_MAP string string ;
                         PAIR 3 ;
                         DUP 3 ;
                         GET 3 ;
                         SENDER ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         DUP 3 ;
                         MEM ;
                         IF { DROP ;
                              DIG 2 ;
                              DROP ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              SENDER ;
                              VIEW "getProofsForAddress"
                                   (map string
                                        (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                              IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 47 ; FAILWITH } {} }
                            { DIG 3 ; DROP } ;
                         PUSH bool False ;
                         UPDATE 4 ;
                         NOW ;
                         UPDATE 3 ;
                         NIL operation ;
                         DUP 4 ;
                         GET 3 ;
                         CONTRACT %setProof
                           (pair (address %address)
                                 (pair (pair %proof
                                          (map %meta string string)
                                          (pair (timestamp %register_date) (bool %verified)))
                                       (string %prooftype))) ;
                         IF_NONE { PUSH int 101 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 4 ;
                         DIG 4 ;
                         SENDER ;
                         PAIR 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %removeIdentity address ;
                         IF_NONE { PUSH int 156 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %delProof (pair (address %address) (string %prooftype)) ;
                         IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 78 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS } } } }
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         SET_DELEGATE ;
                         NIL operation ;
                         SWAP ;
                         CONS }
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         UNPAIR ;
                         CAR ;
                         DIG 2 ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         NIL operation } } }
               { IF_LEFT
                   { IF_LEFT
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         UPDATE 5 ;
                         NIL operation }
                       { DUP 3 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         UNPAIR ;
                         DIG 3 ;
                         DIG 2 ;
                         DIG 3 ;
                         DIG 3 ;
                         DUP 3 ;
                         GET 3 ;
                         DUP 3 ;
                         CAR ;
                         CAR ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         DUP 3 ;
                         GET 3 ;
                         MEM ;
                         IF {}
                            { PUSH string "Missing required proof for this entrypoint" ; FAILWITH } ;
                         PUSH bool False ;
                         NOW ;
                         EMPTY_MAP string string ;
                         PAIR 3 ;
                         DUP 4 ;
                         GET 3 ;
                         DUP 4 ;
                         CAR ;
                         CAR ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         DUP 4 ;
                         GET 3 ;
                         MEM ;
                         IF { DROP 2 ;
                              DIG 2 ;
                              DROP ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CAR ;
                              CAR ;
                              VIEW "getProofsForAddress"
                                   (map string
                                        (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                              IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              GET ;
                              IF_NONE { PUSH int 47 ; FAILWITH } {} }
                            { SWAP ; DROP ; DIG 3 ; DROP } ;
                         DUP ;
                         CAR ;
                         DUP 3 ;
                         GET 4 ;
                         SOME ;
                         DUP 4 ;
                         CAR ;
                         CDR ;
                         UPDATE ;
                         UPDATE 1 ;
                         NIL operation ;
                         DUP 4 ;
                         GET 3 ;
                         CONTRACT %setProof
                           (pair (address %address)
                                 (pair (pair %proof
                                          (map %meta string string)
                                          (pair (timestamp %register_date) (bool %verified)))
                                       (string %prooftype))) ;
                         IF_NONE { PUSH int 144 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DUP 5 ;
                         GET 3 ;
                         DIG 4 ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         PAIR 3 ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         UPDATE 3 ;
                         NIL operation }
                       { DUP 3 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         UNPAIR ;
                         DIG 3 ;
                         DIG 2 ;
                         DIG 3 ;
                         DIG 3 ;
                         DUP 3 ;
                         GET 3 ;
                         DUP 3 ;
                         CAR ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         DUP 3 ;
                         CDR ;
                         MEM ;
                         IF {}
                            { PUSH string "Missing required proof for this entrypoint" ; FAILWITH } ;
                         PUSH bool False ;
                         NOW ;
                         EMPTY_MAP string string ;
                         PAIR 3 ;
                         DUP 4 ;
                         GET 3 ;
                         DUP 4 ;
                         CAR ;
                         VIEW "getProofsForAddress"
                              (map string
                                   (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                         IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                         DUP 4 ;
                         CDR ;
                         MEM ;
                         IF { DROP 2 ;
                              DIG 2 ;
                              DROP ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CAR ;
                              VIEW "getProofsForAddress"
                                   (map string
                                        (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))) ;
                              IF_NONE { PUSH string "Invalid view" ; FAILWITH } {} ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              GET ;
                              IF_NONE { PUSH int 47 ; FAILWITH } {} }
                            { SWAP ; DROP ; DIG 3 ; DROP } ;
                         PUSH bool True ;
                         UPDATE 4 ;
                         NIL operation ;
                         DUP 4 ;
                         GET 3 ;
                         CONTRACT %setProof
                           (pair (address %address)
                                 (pair (pair %proof
                                          (map %meta string string)
                                          (pair (timestamp %register_date) (bool %verified)))
                                       (string %prooftype))) ;
                         IF_NONE { PUSH int 134 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DUP 5 ;
                         CDR ;
                         DIG 4 ;
                         DIG 5 ;
                         CAR ;
                         PAIR 3 ;
                         TRANSFER_TOKENS ;
                         CONS } } } } ;
         PAIR } }
