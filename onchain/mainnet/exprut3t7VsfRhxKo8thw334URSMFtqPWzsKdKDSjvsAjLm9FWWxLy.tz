{ storage
    (pair (pair (address %administrator) (option %ballot_contract address))
          (pair (map %chapters nat bool)
                (pair (big_map %ledger (pair address nat) nat) (big_map %metadata string bytes)))) ;
  parameter
    (or (or (nat %add_chapter)
            (or (nat %del_chapter)
                (pair %register_vote (nat %chapter_id) (pair (nat %token_id) (nat %vote)))))
        (or (address %set_ballot_contract)
            (or (pair %update_chapter (nat %code) (bool %paused))
                (list %update_ledger (pair (nat %action_id) (pair (address %address) (nat %token_id))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 PACK ;
                 SENDER ;
                 PACK ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 GET 3 ;
                 PUSH (option bool) (Some True) ;
                 DIG 3 ;
                 UPDATE ;
                 UPDATE 3 ;
                 NIL operation }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     PACK ;
                     SENDER ;
                     PACK ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     NONE bool ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 3 ;
                     NIL operation }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SENDER ;
                     PAIR ;
                     MEM ;
                     IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                     PUSH nat 1 ;
                     PACK ;
                     DUP 3 ;
                     GET 5 ;
                     DUP 3 ;
                     GET 3 ;
                     SENDER ;
                     PAIR ;
                     GET ;
                     IF_NONE { PUSH int 106 ; FAILWITH } {} ;
                     PACK ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ENOUGH" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     MEM ;
                     IF {} { PUSH string "VOTING_NOT_EXISTS" ; FAILWITH } ;
                     PUSH bool False ;
                     PACK ;
                     DUP 3 ;
                     GET 3 ;
                     DUP 3 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH int 110 ; FAILWITH } {} ;
                     PACK ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "VOTING_PAUSED" ; FAILWITH } ;
                     NIL operation ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     IF_NONE { PUSH int 123 ; FAILWITH } {} ;
                     CONTRACT %cast_vote
                       (pair (pair (nat %chapter_id) (nat %token_id)) (pair (nat %vote) (address %voter))) ;
                     IF_NONE { PUSH string "InvalidInterface" ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     SENDER ;
                     DUP 5 ;
                     GET 4 ;
                     PAIR ;
                     DIG 4 ;
                     DUP ;
                     GET 3 ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     TRANSFER_TOKENS ;
                     CONS } } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 PACK ;
                 SENDER ;
                 PACK ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                 SWAP ;
                 UNPAIR ;
                 CAR ;
                 DIG 2 ;
                 SOME ;
                 SWAP ;
                 PAIR ;
                 PAIR }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     PACK ;
                     SENDER ;
                     PACK ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     DUP 3 ;
                     CDR ;
                     SOME ;
                     DIG 3 ;
                     CAR ;
                     UPDATE ;
                     UPDATE 3 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     PACK ;
                     SENDER ;
                     PACK ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP ;
                            CAR ;
                            PUSH nat 1 ;
                            COMPARE ;
                            EQ ;
                            IF { DIG 2 ;
                                 DUP ;
                                 GET 5 ;
                                 PUSH (option nat) (Some 1) ;
                                 DIG 3 ;
                                 DUP ;
                                 GET 4 ;
                                 SWAP ;
                                 DUP ;
                                 DUG 5 ;
                                 GET 3 ;
                                 PAIR ;
                                 UPDATE ;
                                 UPDATE 5 ;
                                 DUG 2 }
                               {} ;
                            DUP ;
                            CAR ;
                            PUSH nat 2 ;
                            COMPARE ;
                            EQ ;
                            IF { DUP 3 ;
                                 GET 5 ;
                                 SWAP ;
                                 DUP ;
                                 GET 4 ;
                                 SWAP ;
                                 DUP ;
                                 DUG 3 ;
                                 GET 3 ;
                                 PAIR ;
                                 MEM ;
                                 IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                                 DIG 2 ;
                                 DUP ;
                                 GET 5 ;
                                 PUSH (option nat) (Some 0) ;
                                 DIG 3 ;
                                 DUP ;
                                 GET 4 ;
                                 SWAP ;
                                 GET 3 ;
                                 PAIR ;
                                 UPDATE ;
                                 UPDATE 5 ;
                                 SWAP }
                               { DROP } } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
