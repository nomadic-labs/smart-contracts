{ parameter
    (or (or (or (pair %approve (address %spender) (nat %value))
                (pair %getAllowance
                   (pair %request (address %owner) (address %spender))
                   (contract %callback nat)))
            (or (pair %getBalance (address %owner) (contract %callback nat))
                (pair %getTotalSupply (unit %request) (contract %callback nat))))
        (pair %transfer (address %from) (pair (address %to) (nat %value)))) ;
  storage
    (pair (big_map %ledger address nat)
          (pair (big_map %allowances (pair (address %owner) (address %spender)) nat)
                (pair (address %admin)
                      (pair (address %reserve)
                            (pair (address %burn_address)
                                  (pair (nat %initial_supply)
                                        (pair (nat %total_supply)
                                              (pair (nat %burned_supply)
                                                    (pair (big_map %metadata string bytes)
                                                          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     PAIR ;
                     PUSH nat 0 ;
                     DUP 4 ;
                     CDR ;
                     COMPARE ;
                     GT ;
                     PUSH nat 0 ;
                     DUP 4 ;
                     DUP 4 ;
                     GET ;
                     IF_NONE { PUSH nat 0 } {} ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "UnsafeAllowanceChange" ; FAILWITH } {} ;
                     DIG 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     CDR ;
                     PUSH nat 0 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     EQ ;
                     IF { DROP ; NONE nat } { SOME } ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 3 ;
                     NIL operation ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     DUP 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 5 ;
                     GET 3 ;
                     DIG 5 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH nat 0 } {} ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     DUP 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 5 ;
                     CAR ;
                     DIG 5 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH nat 0 } {} ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 4 ;
                     GET 13 ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR } } }
           { DUP ;
             GET 3 ;
             DUP 3 ;
             GET 3 ;
             DUP 4 ;
             CAR ;
             DUP 4 ;
             CAR ;
             SENDER ;
             COMPARE ;
             EQ ;
             IF { SWAP }
                { SENDER ;
                  DUP 5 ;
                  CAR ;
                  PAIR ;
                  DUP 5 ;
                  GET 4 ;
                  DUP 4 ;
                  DUP 3 ;
                  GET ;
                  IF_NONE { PUSH nat 0 } {} ;
                  SUB ;
                  ISNAT ;
                  IF_NONE { PUSH string "NotEnoughAllowance" ; FAILWITH } {} ;
                  DIG 3 ;
                  PUSH nat 0 ;
                  DUP 3 ;
                  COMPARE ;
                  EQ ;
                  IF { SWAP ; DROP ; NONE nat } { SWAP ; SOME } ;
                  DIG 2 ;
                  UPDATE } ;
             DUP 4 ;
             GET 4 ;
             DUP 3 ;
             DUP 6 ;
             CAR ;
             GET ;
             IF_NONE { PUSH nat 0 } {} ;
             SUB ;
             ISNAT ;
             IF_NONE { PUSH string "NotEnoughBalance" ; FAILWITH } {} ;
             DIG 2 ;
             PUSH nat 0 ;
             DUP 3 ;
             COMPARE ;
             EQ ;
             IF { SWAP ; DROP ; NONE nat } { SWAP ; SOME } ;
             DUP 5 ;
             CAR ;
             UPDATE ;
             DUP 3 ;
             CONTRACT %setBaker (pair (option %baker key_hash) (bool %freezeBaker)) ;
             DUP 4 ;
             CONTRACT %set_baker (option key_hash) ;
             DUP 5 ;
             CONTRACT %baker key_hash ;
             DUP 6 ;
             CONTRACT %setAdmin address ;
             DUP 7 ;
             CONTRACT %set_admin address ;
             DIG 7 ;
             CONTRACT %set_administrator address ;
             DIG 3 ;
             DIG 4 ;
             DIG 5 ;
             IF_NONE
               { IF_NONE
                   { IF_NONE { PUSH bool False } { DROP ; PUSH bool True } }
                   { DROP 2 ; PUSH bool True } }
               { DROP 3 ; PUSH bool True } ;
             SWAP ;
             DIG 2 ;
             DIG 3 ;
             IF_NONE
               { IF_NONE
                   { IF_NONE { PUSH bool False } { DROP ; PUSH bool True } }
                   { DROP 2 ; PUSH bool True } }
               { DROP 3 ; PUSH bool True } ;
             OR ;
             IF { DUP 3 ;
                  GET 4 ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  DUP 5 ;
                  GET 3 ;
                  GET ;
                  IF_NONE { PUSH nat 0 } {} ;
                  ADD ;
                  DIG 4 ;
                  DUG 2 ;
                  SOME ;
                  DIG 4 ;
                  GET 3 ;
                  UPDATE ;
                  UPDATE 1 ;
                  SWAP ;
                  UPDATE 3 ;
                  NIL operation ;
                  PAIR }
                { DUP 4 ;
                  GET 9 ;
                  DUP 5 ;
                  GET 7 ;
                  PUSH int 7 ;
                  DUP 6 ;
                  GET 4 ;
                  MUL ;
                  ABS ;
                  PUSH nat 100 ;
                  SWAP ;
                  EDIV ;
                  IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                  CAR ;
                  DUP ;
                  DUP 8 ;
                  GET 15 ;
                  ADD ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  DUP 9 ;
                  GET 13 ;
                  SUB ;
                  ABS ;
                  DIG 2 ;
                  DUP 6 ;
                  DUP 6 ;
                  GET ;
                  IF_NONE { PUSH nat 0 } {} ;
                  ADD ;
                  DIG 5 ;
                  SWAP ;
                  SOME ;
                  DIG 5 ;
                  UPDATE ;
                  PUSH int 1 ;
                  DUP 7 ;
                  GET 4 ;
                  MUL ;
                  ABS ;
                  PUSH nat 100 ;
                  SWAP ;
                  EDIV ;
                  IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                  CAR ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  DUP 6 ;
                  GET ;
                  IF_NONE { PUSH nat 0 } {} ;
                  ADD ;
                  SOME ;
                  DIG 4 ;
                  UPDATE ;
                  PUSH int 92 ;
                  DUP 6 ;
                  GET 4 ;
                  MUL ;
                  ABS ;
                  PUSH nat 100 ;
                  SWAP ;
                  EDIV ;
                  IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                  CAR ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  DUP 7 ;
                  GET 3 ;
                  GET ;
                  IF_NONE { PUSH nat 0 } {} ;
                  ADD ;
                  DIG 6 ;
                  DUG 2 ;
                  SOME ;
                  DIG 6 ;
                  GET 3 ;
                  UPDATE ;
                  UPDATE 1 ;
                  DIG 3 ;
                  UPDATE 3 ;
                  DIG 2 ;
                  UPDATE 15 ;
                  SWAP ;
                  UPDATE 13 ;
                  NIL operation ;
                  PAIR } } } }
