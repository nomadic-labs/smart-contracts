{ storage
    (pair (pair (address %admin) (big_map %ledger (pair address nat) nat))
          (pair (big_map %metadata string bytes)
                (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (big_map %states nat (pair (nat %prop_1) (nat %prop_2)))))) ;
  parameter
    (or (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback
                  (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
            (pair %change_state (pair %state (nat %prop_1) (nat %prop_2)) (nat %token_id)))
        (or (pair %mint
               (address %address)
               (pair (pair %state (nat %prop_1) (nat %prop_2)) (nat %token_id)))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { NIL (pair (pair address nat) nat) ;
                 DUP 2 ;
                 CAR ;
                 ITER { SWAP ; PUSH nat 0 ; DIG 2 ; PAIR ; CONS } ;
                 NIL operation ;
                 DIG 2 ;
                 CDR ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS }
               { DUP 2 ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_AUTHORISED" ; FAILWITH } ;
                 DUP 2 ;
                 GET 6 ;
                 DUP 2 ;
                 CDR ;
                 MEM ;
                 IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 GET 6 ;
                 DUP 3 ;
                 CAR ;
                 SOME ;
                 DIG 3 ;
                 CDR ;
                 UPDATE ;
                 UPDATE 6 ;
                 NIL operation } }
           { IF_LEFT
               { DUP 2 ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_AUTHORISED" ; FAILWITH } ;
                 DUP 2 ;
                 GET 6 ;
                 DUP 2 ;
                 GET 4 ;
                 MEM ;
                 IF { PUSH string "TOKEN_ID_ALREADY_EXISTS" ; FAILWITH } {} ;
                 SWAP ;
                 UNPAIR ;
                 UNPAIR ;
                 SWAP ;
                 PUSH (option nat) (Some 1) ;
                 DUP 5 ;
                 GET 4 ;
                 DUP 6 ;
                 CAR ;
                 PAIR ;
                 UPDATE ;
                 SWAP ;
                 PAIR ;
                 PAIR ;
                 DUP ;
                 GET 6 ;
                 DUP 3 ;
                 GET 3 ;
                 SOME ;
                 DIG 3 ;
                 GET 4 ;
                 UPDATE ;
                 UPDATE 6 }
               { IF_LEFT
                   { DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 2 ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { DUP 4 ; GET 5 ; DUP 2 ; GET 3 ; SENDER ; DUP 5 ; CAR ; PAIR 3 ; MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   PUSH nat 0 ;
                                   DUP 2 ;
                                   GET 4 ;
                                   COMPARE ;
                                   GT ;
                                   IF { DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 4 ;
                                        GET 4 ;
                                        DIG 7 ;
                                        CAR ;
                                        CDR ;
                                        DUP 6 ;
                                        GET 3 ;
                                        DUP 8 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 94 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                        SOME ;
                                        DUP 5 ;
                                        GET 3 ;
                                        DUP 7 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 3 ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 4 ;
                                        GET 4 ;
                                        DIG 7 ;
                                        CAR ;
                                        CDR ;
                                        DUP 6 ;
                                        GET 3 ;
                                        DUP 7 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} ;
                                        ADD ;
                                        SOME ;
                                        DUP 5 ;
                                        GET 3 ;
                                        DIG 5 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { DUP ;
                     ITER { IF_LEFT
                              { SENDER ;
                                DUP 2 ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 5 ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 5 ;
                                SWAP }
                              { SENDER ;
                                DUP 2 ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 5 ;
                                NONE unit ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 5 ;
                                SWAP } } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
