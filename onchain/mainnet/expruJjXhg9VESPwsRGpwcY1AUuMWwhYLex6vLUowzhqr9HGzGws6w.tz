{ parameter
    (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
            (list %admin_distribute_packs nat))
        (or (pair %offchain_redeem_booster
               (nat %pack_token_id)
               (pair %permit (key %signerKey) (signature %signature)))
            (nat %redeem_booster))) ;
  storage
    (pair (address %pack_fa2)
          (pair (address %burn_address)
                (pair (big_map %packs nat (or (address %distributed) (address %redeemed)))
                      (pair %admin (pair (address %admin) (bool %paused)) (option %pending_admin address))))) ;
  code { LAMBDA
           (pair bool string)
           unit
           { UNPAIR ; NOT ; IF { FAILWITH } { DROP ; UNIT } } ;
         LAMBDA
           (pair (pair address bool) (option address))
           unit
           { CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         PUSH string "XTZ_TRANSFER" ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         EQ ;
         PAIR ;
         DUP 3 ;
         SWAP ;
         EXEC ;
         DROP ;
         SWAP ;
         LAMBDA
           (pair (lambda (pair bool string) unit)
                 (pair (pair nat address)
                       (pair address
                             (pair address
                                   (pair (big_map nat (or address address)) (pair (pair address bool) (option address)))))))
           (pair (list operation)
                 (pair address
                       (pair address
                             (pair (big_map nat (or address address)) (pair (pair address bool) (option address))))))
           { UNPAIR ;
             SWAP ;
             UNPAIR ;
             UNPAIR ;
             PUSH string "BOOSTER_ALREADY_REDEEMED" ;
             DUP 4 ;
             CDR ;
             CDR ;
             CAR ;
             DUP 3 ;
             MEM ;
             NOT ;
             PAIR ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DROP ;
             DUP 3 ;
             CDR ;
             CAR ;
             DUP 3 ;
             PUSH nat 1 ;
             PAIR ;
             DUP 3 ;
             DUP 6 ;
             CAR ;
             DIG 2 ;
             UNPAIR ;
             DIG 2 ;
             CONTRACT %transfer
               (list (pair (address %from_)
                           (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
             IF_NONE
               { DROP 4 ; PUSH string "CANNOT_INVOKE_FA2_TRANSFER" ; FAILWITH }
               { PUSH mutez 0 ;
                 NIL (pair address (list (pair address (pair nat nat)))) ;
                 NIL (pair address (pair nat nat)) ;
                 DIG 4 ;
                 DIG 6 ;
                 PAIR ;
                 DIG 6 ;
                 PAIR ;
                 CONS ;
                 DIG 4 ;
                 PAIR ;
                 CONS ;
                 TRANSFER_TOKENS } ;
             DUP 4 ;
             CDR ;
             CDR ;
             CDR ;
             DUP 5 ;
             CDR ;
             CDR ;
             CAR ;
             DIG 4 ;
             RIGHT address ;
             DIG 4 ;
             SWAP ;
             SOME ;
             SWAP ;
             UPDATE ;
             PAIR ;
             DUP 3 ;
             CDR ;
             CAR ;
             PAIR ;
             DIG 2 ;
             CAR ;
             PAIR ;
             NIL operation ;
             DIG 2 ;
             CONS ;
             PAIR } ;
         SWAP ;
         APPLY ;
         DIG 2 ;
         UNPAIR ;
         IF_LEFT
           { DIG 2 ;
             DROP ;
             IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CDR ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         DIG 2 ;
                         DROP ;
                         DUP ;
                         CDR ;
                         IF_NONE
                           { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             COMPARE ;
                             EQ ;
                             IF { NONE address ; SWAP ; CAR ; CDR ; SENDER ; PAIR ; PAIR }
                                { DROP ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } ;
                         NIL operation ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SOME ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } ;
                 UNPAIR ;
                 SWAP ;
                 DUP 3 ;
                 CDR ;
                 CDR ;
                 CAR ;
                 PAIR ;
                 DUP 3 ;
                 CDR ;
                 CAR ;
                 PAIR ;
                 DIG 2 ;
                 CAR ;
                 PAIR ;
                 SWAP ;
                 PAIR }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CDR ;
                 CDR ;
                 DUP 4 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 ITER { SWAP ;
                        DUP ;
                        CDR ;
                        CDR ;
                        CDR ;
                        DUP 4 ;
                        SWAP ;
                        EXEC ;
                        DROP ;
                        DUP ;
                        DUP 3 ;
                        SWAP ;
                        CDR ;
                        CDR ;
                        CAR ;
                        SWAP ;
                        GET ;
                        IF_NONE { PUSH string "PACK_NOT_EXIST" ; FAILWITH } {} ;
                        IF_LEFT { DROP ; PUSH string "PACK_ALREADY_DISTRIBUTED" ; FAILWITH } {} ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        CDR ;
                        CDR ;
                        CDR ;
                        DUP 3 ;
                        CDR ;
                        CDR ;
                        CAR ;
                        DIG 2 ;
                        LEFT address ;
                        SOME ;
                        DIG 4 ;
                        UPDATE ;
                        PAIR ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        CDR ;
                        CAR ;
                        PAIR ;
                        SWAP ;
                        CAR ;
                        PAIR } ;
                 SWAP ;
                 DROP ;
                 NIL operation ;
                 PAIR } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CDR ;
                 CDR ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP ;
                 UNPAIR ;
                 DUP ;
                 PACK ;
                 BLAKE2B ;
                 PUSH nat 0 ;
                 DIG 3 ;
                 DUG 2 ;
                 PAIR ;
                 SELF_ADDRESS ;
                 CHAIN_ID ;
                 PAIR ;
                 PAIR ;
                 PACK ;
                 DUP ;
                 DUP 3 ;
                 CDR ;
                 DIG 3 ;
                 CAR ;
                 CHECK_SIGNATURE ;
                 NOT ;
                 IF { PUSH string "MISSIGNED" ; PAIR ; FAILWITH } { DROP } ;
                 SWAP ;
                 CDR ;
                 CAR ;
                 HASH_KEY ;
                 IMPLICIT_ACCOUNT ;
                 ADDRESS ;
                 DIG 2 ;
                 SWAP ;
                 DIG 2 ;
                 PAIR ;
                 PAIR ;
                 EXEC }
               { DIG 3 ; DROP ; SWAP ; SENDER ; DIG 2 ; PAIR ; PAIR ; EXEC } } } }
