{ storage
    (pair (pair (pair (address %administrator) (big_map %ledger (pair address nat) nat))
                (pair (bool %lock_update) (big_map %metadata string bytes)))
          (pair (pair (big_map %minters address unit)
                      (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (big_map %supply nat nat)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (unit %lock_update))
            (or (list %mint
                   (pair (pair (address %address) (nat %amount))
                         (pair (nat %token_id) (map %token_info string bytes))))
                (or (address %set_administrator) (big_map %set_metadata string bytes))))
        (or (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_minters (or (address %add_address) (address %remove_address))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (or (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes)))
                    (pair %withdraw_mutez (mutez %amount) (address %destination)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { NIL operation ;
                     DUP ;
                     DUP 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DUP 5 ;
                     CAR ;
                     MAP { DUP 7 ;
                           GET 6 ;
                           DUP 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 7 ;
                           CAR ;
                           CAR ;
                           CDR ;
                           DUP 2 ;
                           CDR ;
                           DUP 3 ;
                           CAR ;
                           PAIR ;
                           GET ;
                           IF_NONE { PUSH nat 0 } {} ;
                           SWAP ;
                           PAIR } ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { DROP ;
                     DUP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CDR ;
                     PUSH bool True ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     NIL operation } }
               { IF_LEFT
                   { DUP 2 ;
                     GET 3 ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP 3 ;
                            GET 5 ;
                            DUP 2 ;
                            GET 3 ;
                            MEM ;
                            IF { PUSH string "NFT-asset: cannot mint twice same token" ; FAILWITH } {} ;
                            DIG 2 ;
                            DUP ;
                            GET 6 ;
                            DUP 3 ;
                            GET 4 ;
                            DUP 4 ;
                            GET 3 ;
                            PAIR ;
                            SOME ;
                            DUP 4 ;
                            GET 3 ;
                            UPDATE ;
                            UPDATE 6 ;
                            DUP ;
                            GET 5 ;
                            DUP 3 ;
                            CAR ;
                            CDR ;
                            SOME ;
                            DUP 4 ;
                            GET 3 ;
                            UPDATE ;
                            UPDATE 5 ;
                            UNPAIR ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            DUP 5 ;
                            CAR ;
                            CDR ;
                            SOME ;
                            DIG 5 ;
                            DUP ;
                            GET 3 ;
                            SWAP ;
                            CAR ;
                            CAR ;
                            PAIR ;
                            UPDATE ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            PAIR ;
                            SWAP } ;
                     DROP }
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { DUP 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CAR ;
                         DIG 3 ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   GET 6 ;
                                   DUP 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP 2 ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CDR ;
                                        DUP 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   PUSH nat 0 ;
                                   DUP 2 ;
                                   GET 4 ;
                                   COMPARE ;
                                   GT ;
                                   IF { DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 5 ;
                                        GET 4 ;
                                        DIG 8 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        DUP 7 ;
                                        GET 3 ;
                                        DUP 9 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                        SOME ;
                                        DUP 6 ;
                                        GET 3 ;
                                        DUP 8 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 3 ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 5 ;
                                        GET 4 ;
                                        DIG 8 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        DUP 7 ;
                                        GET 3 ;
                                        DUP 8 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH nat 0 } {} ;
                                        ADD ;
                                        SOME ;
                                        DUP 6 ;
                                        GET 3 ;
                                        DIG 6 ;
                                        CAR ;
                                        PAIR ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { DUP 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { IF_LEFT
                              { DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                NONE unit ;
                                DIG 5 ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP } ;
                 NIL operation }
               { IF_LEFT
                   { DUP ;
                     ITER { IF_LEFT
                              { SENDER ;
                                DUP 2 ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { SENDER ;
                                DUP 2 ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                NONE unit ;
                                DIG 5 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP ;
                     NIL operation }
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         GET 3 ;
                         IF { PUSH string "UPDATE_LOCKED" ; FAILWITH } {} ;
                         DUP 2 ;
                         GET 3 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH string "FA2_NOT_MINTER" ; FAILWITH } ;
                         DUP ;
                         ITER { DIG 2 ;
                                DUP ;
                                GET 6 ;
                                DUP 3 ;
                                CDR ;
                                DUP 4 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DIG 3 ;
                                CAR ;
                                UPDATE ;
                                UPDATE 6 ;
                                SWAP } ;
                         DROP ;
                         NIL operation }
                       { DUP 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         NIL operation ;
                         DUP 2 ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 440 ; FAILWITH } {} ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS } } } } ;
         PAIR } ;
  view "get_balance_of"
       (list (pair (address %owner) (nat %token_id)))
       (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))
       { UNPAIR ;
         DUP ;
         MAP { DUP 3 ;
               GET 6 ;
               DUP 2 ;
               CDR ;
               MEM ;
               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
               DUP 3 ;
               CAR ;
               CAR ;
               CDR ;
               DUP 2 ;
               CDR ;
               DUP 3 ;
               CAR ;
               PAIR ;
               GET ;
               IF_NONE { PUSH nat 0 } {} ;
               SWAP ;
               PAIR } ;
         SWAP ;
         DROP ;
         SWAP ;
         DROP } }
