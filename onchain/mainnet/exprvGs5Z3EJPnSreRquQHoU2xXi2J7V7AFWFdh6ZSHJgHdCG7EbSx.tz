{ parameter
    (or (or (or (or (address %add_admin)
                    (list %add_artist (pair (address %address) (nat %mint_count))))
                (or (address %add_manager)
                    (or (map %airdrop nat (list (pair (nat %amount) (address %recipient))))
                        (nat %cancel_listing))))
            (or (or (pair %create_claim
                       (nat %airdrop_capacity)
                       (pair (list %burn_recipe (map address (map (option nat) nat)))
                             (pair (option %condition (pair (address %address) (bytes %id)))
                                   (pair (timestamp %end_time)
                                         (pair (nat %max_per_wallet)
                                               (pair (mutez %price)
                                                     (pair (pair %split (map %shares address nat) (nat %total))
                                                           (pair (timestamp %start_time) (nat %token_id)))))))))
                    (or (pair %create_listing (nat %amount) (pair (nat %editions) (nat %token_id)))
                        (map %create_token string bytes)))
                (or (pair %lock (bool %metadata) (pair (bool %mint) (nat %token_id)))
                    (or (address %remove_admin) (list %remove_artist address)))))
        (or (or (or (address %remove_manager) (address %set_administrator))
                (or (bool %set_curated)
                    (or (address %set_marketplace_address) (big_map %set_metadata string bytes))))
            (or (or (bool %set_paused)
                    (or (pair %set_platform_fee (nat %marketplace_share) (pair (nat %share) (nat %total)))
                        (address %set_platform_fee_address)))
                (or (address %set_saoe_address)
                    (or (list %transfer (pair (address %to_) (pair (nat %token_id) (nat %amount))))
                        (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes)))))))) ;
  storage
    (pair (set %admin address)
          (pair (big_map %artists address nat)
                (pair (bool %curated)
                      (pair (big_map %listed nat unit)
                            (pair (address %marketplace_address)
                                  (pair (bool %paused)
                                        (pair (pair %platform_fee (nat %marketplace_share) (pair (nat %share) (nat %total)))
                                              (pair (address %platform_fee_address)
                                                    (pair (address %saoe_address)
                                                          (pair (big_map %token_creators nat address) (nat %token_id))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         CAR ;
                         PUSH bool True ;
                         DIG 3 ;
                         UPDATE ;
                         UPDATE 1 }
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         DUP ;
                         ITER { DIG 2 ;
                                DUP ;
                                GET 3 ;
                                DUP 3 ;
                                CDR ;
                                SOME ;
                                DIG 3 ;
                                CAR ;
                                UPDATE ;
                                UPDATE 3 ;
                                SWAP } ;
                         DROP } ;
                     NIL operation }
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 17 ;
                         CONTRACT %add_manager address ;
                         IF_NONE { PUSH int 116 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { IF_LEFT
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             DUP ;
                             ITER { DUP 3 ;
                                    GET 20 ;
                                    SWAP ;
                                    CAR ;
                                    COMPARE ;
                                    LT ;
                                    IF {}
                                       { PUSH string "WrongCondition: token.key < self.data.token_id" ; FAILWITH } } ;
                             DUP 2 ;
                             GET 17 ;
                             CONTRACT %airdrop (map nat (list (pair (nat %amount) (address %recipient)))) ;
                             IF_NONE { PUSH int 155 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             TRANSFER_TOKENS ;
                             CONS }
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             DUP 2 ;
                             GET 9 ;
                             CONTRACT %retract_ask nat ;
                             IF_NONE { PUSH int 141 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             TRANSFER_TOKENS ;
                             CONS } } } }
               { IF_LEFT
                   { IF_LEFT
                       { PUSH bool False ;
                         DUP 3 ;
                         GET 11 ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } ;
                         IF {}
                            { PUSH string
                                   "WrongCondition: (self.data.paused == False) or (self.data.admin.contains(sp.sender))" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 20 ;
                         DUP 2 ;
                         GET 16 ;
                         COMPARE ;
                         LT ;
                         IF { SENDER ;
                              DUP 3 ;
                              GET 19 ;
                              DUP 3 ;
                              GET 16 ;
                              GET ;
                              IF_NONE { PUSH int 243 ; FAILWITH } {} ;
                              COMPARE ;
                              EQ ;
                              IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } }
                            { PUSH bool False } ;
                         IF {}
                            { PUSH string
                                   "WrongCondition: (params.token_id < self.data.token_id) and ((self.data.token_creators[params.token_id] == sp.sender) or (self.data.admin.contains(sp.sender)))" ;
                              FAILWITH } ;
                         DUP ;
                         GET 13 ;
                         CAR ;
                         DUP 3 ;
                         GET 15 ;
                         MEM ;
                         IF {}
                            { PUSH string
                                   "WrongCondition: params.split.shares.contains(self.data.platform_fee_address)" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 13 ;
                         GET 3 ;
                         DUP 2 ;
                         GET 13 ;
                         CAR ;
                         DUP 4 ;
                         GET 15 ;
                         GET ;
                         IF_NONE { PUSH int 247 ; FAILWITH } {} ;
                         COMPARE ;
                         EQ ;
                         IF {}
                            { PUSH string
                                   "WrongCondition: params.split.shares[self.data.platform_fee_address] == self.data.platform_fee.share" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 13 ;
                         GET 4 ;
                         DUP 2 ;
                         GET 13 ;
                         CDR ;
                         COMPARE ;
                         EQ ;
                         IF {}
                            { PUSH string "WrongCondition: params.split.total == self.data.platform_fee.total" ;
                              FAILWITH } ;
                         PUSH bool False ;
                         DUP 3 ;
                         GET 7 ;
                         DUP 3 ;
                         GET 16 ;
                         MEM ;
                         COMPARE ;
                         EQ ;
                         IF {}
                            { PUSH string
                                   "WrongCondition: (self.data.listed.contains(params.token_id)) == False" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 7 ;
                         PUSH (option unit) (Some Unit) ;
                         DUP 4 ;
                         GET 16 ;
                         UPDATE ;
                         UPDATE 7 ;
                         SWAP ;
                         DUP ;
                         GET 5 ;
                         DUP 2 ;
                         GET 13 ;
                         DUP 3 ;
                         CAR ;
                         DUP 4 ;
                         GET 9 ;
                         DUP 5 ;
                         GET 3 ;
                         DUP 6 ;
                         GET 11 ;
                         DUP 7 ;
                         GET 7 ;
                         DUP 8 ;
                         GET 15 ;
                         DUP 9 ;
                         GET 16 ;
                         PAIR 9 ;
                         SWAP ;
                         DROP ;
                         DUP 2 ;
                         GET 17 ;
                         CONTRACT %create_claim
                           (pair (nat %token_id)
                                 (pair (timestamp %start_time)
                                       (pair (timestamp %end_time)
                                             (pair (mutez %price)
                                                   (pair (list %burn_recipe (map address (map (option nat) nat)))
                                                         (pair (nat %max_per_wallet)
                                                               (pair (nat %airdrop_capacity)
                                                                     (pair (pair %split (map %shares address nat) (nat %total))
                                                                           (option %condition (pair (address %address) (bytes %id))))))))))) ;
                         IF_NONE { PUSH int 265 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { IF_LEFT
                           { PUSH bool False ;
                             DUP 3 ;
                             GET 11 ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } ;
                             IF {}
                                { PUSH string
                                       "WrongCondition: (self.data.paused == False) or (self.data.admin.contains(sp.sender))" ;
                                  FAILWITH } ;
                             DUP 2 ;
                             GET 20 ;
                             DUP 2 ;
                             GET 4 ;
                             COMPARE ;
                             LT ;
                             IF { SENDER ;
                                  DUP 3 ;
                                  GET 19 ;
                                  DUP 3 ;
                                  GET 4 ;
                                  GET ;
                                  IF_NONE { PUSH int 166 ; FAILWITH } {} ;
                                  COMPARE ;
                                  EQ ;
                                  IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } }
                                { PUSH bool False } ;
                             IF {}
                                { PUSH string
                                       "WrongCondition: (params.token_id < self.data.token_id) and ((self.data.token_creators[params.token_id] == sp.sender) or (self.data.admin.contains(sp.sender)))" ;
                                  FAILWITH } ;
                             PUSH nat 0 ;
                             DUP 2 ;
                             GET 3 ;
                             COMPARE ;
                             GT ;
                             IF {} { PUSH string "WrongCondition: params.editions > 0" ; FAILWITH } ;
                             PUSH bool False ;
                             DUP 3 ;
                             GET 7 ;
                             DUP 3 ;
                             GET 4 ;
                             MEM ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } ;
                             IF {}
                                { PUSH string
                                       "WrongCondition: ((self.data.listed.contains(params.token_id)) == False) or (self.data.admin.contains(sp.sender))" ;
                                  FAILWITH } ;
                             SWAP ;
                             DUP ;
                             GET 7 ;
                             PUSH (option unit) (Some Unit) ;
                             DUP 4 ;
                             GET 4 ;
                             UPDATE ;
                             UPDATE 7 ;
                             SWAP ;
                             DUP ;
                             GET 4 ;
                             NIL (pair nat address) ;
                             SELF_ADDRESS ;
                             DUP 4 ;
                             GET 3 ;
                             PAIR ;
                             CONS ;
                             PAIR ;
                             DUP 3 ;
                             GET 17 ;
                             CONTRACT %mint
                               (pair (list %mint_items (pair (nat %amount) (address %to_))) (nat %token_id)) ;
                             IF_NONE { PUSH int 177 ; FAILWITH } {} ;
                             NIL operation ;
                             DUP 2 ;
                             PUSH mutez 0 ;
                             DUP 5 ;
                             TRANSFER_TOKENS ;
                             CONS ;
                             NIL (or (pair address (pair address nat)) (pair address (pair address nat))) ;
                             DUP 5 ;
                             GET 4 ;
                             DUP 7 ;
                             GET 9 ;
                             SELF_ADDRESS ;
                             PAIR 3 ;
                             LEFT (pair address (pair address nat)) ;
                             CONS ;
                             DUP 6 ;
                             GET 17 ;
                             CONTRACT %update_operators
                               (list (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                                         (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))) ;
                             IF_NONE { PUSH int 185 ; FAILWITH } {} ;
                             DIG 2 ;
                             DUP 2 ;
                             PUSH mutez 0 ;
                             DUP 5 ;
                             TRANSFER_TOKENS ;
                             CONS ;
                             DUG 2 ;
                             NONE address ;
                             NONE timestamp ;
                             NIL (pair nat address) ;
                             SENDER ;
                             DUP 11 ;
                             GET 13 ;
                             GET 3 ;
                             DUP 12 ;
                             GET 13 ;
                             CAR ;
                             DUP 13 ;
                             GET 13 ;
                             GET 4 ;
                             SUB ;
                             ISNAT ;
                             IF_NONE { PUSH int 214 ; FAILWITH } {} ;
                             SUB ;
                             ISNAT ;
                             IF_NONE { PUSH int 214 ; FAILWITH } {} ;
                             PAIR ;
                             CONS ;
                             DUP 10 ;
                             GET 15 ;
                             DUP 11 ;
                             GET 13 ;
                             GET 3 ;
                             PAIR ;
                             CONS ;
                             DUP 9 ;
                             GET 3 ;
                             DUP 10 ;
                             CAR ;
                             PUSH (or address (or (pair address nat) unit)) (Right (Right Unit)) ;
                             DUP 12 ;
                             GET 4 ;
                             DUP 14 ;
                             GET 17 ;
                             PAIR ;
                             PAIR 7 ;
                             SWAP ;
                             DROP ;
                             SWAP ;
                             DROP ;
                             DIG 2 ;
                             DROP ;
                             DIG 2 ;
                             DROP ;
                             DIG 2 ;
                             DROP ;
                             DUP 3 ;
                             GET 9 ;
                             CONTRACT %ask
                               (pair (pair %token (address %address) (nat %token_id))
                                     (pair (or %currency
                                              (address %fa12)
                                              (or (pair %fa2 (address %address) (nat %token_id)) (unit %tez)))
                                           (pair (nat %amount)
                                                 (pair (nat %editions)
                                                       (pair (list %shares (pair (nat %amount) (address %recipient)))
                                                             (pair (option %expiry_time timestamp) (option %target address))))))) ;
                             IF_NONE { PUSH int 222 ; FAILWITH } {} ;
                             DIG 2 ;
                             SWAP ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             TRANSFER_TOKENS ;
                             CONS }
                           { PUSH bool False ;
                             DUP 3 ;
                             GET 11 ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } ;
                             IF {}
                                { PUSH string
                                       "WrongCondition: (self.data.paused == False) or (self.data.admin.contains(sp.sender))" ;
                                  FAILWITH } ;
                             DUP 2 ;
                             GET 5 ;
                             IF { DUP 2 ; CAR ; SENDER ; MEM ; NOT } { PUSH bool False } ;
                             IF { DUP 2 ;
                                  GET 3 ;
                                  SENDER ;
                                  MEM ;
                                  IF {}
                                     { PUSH string "WrongCondition: self.data.artists.contains(sp.sender)" ;
                                       FAILWITH } ;
                                  PUSH nat 0 ;
                                  DUP 3 ;
                                  GET 3 ;
                                  SENDER ;
                                  GET ;
                                  IF_NONE { PUSH int 284 ; FAILWITH } {} ;
                                  COMPARE ;
                                  GT ;
                                  IF {}
                                     { PUSH string "WrongCondition: self.data.artists[sp.sender] > 0" ; FAILWITH } ;
                                  DUP 2 ;
                                  DUP ;
                                  GET 3 ;
                                  PUSH nat 1 ;
                                  DIG 4 ;
                                  GET 3 ;
                                  SENDER ;
                                  GET ;
                                  IF_NONE { PUSH int 285 ; FAILWITH } {} ;
                                  SUB ;
                                  ISNAT ;
                                  IF_NONE { PUSH int 285 ; FAILWITH } {} ;
                                  SOME ;
                                  SENDER ;
                                  UPDATE ;
                                  UPDATE 3 ;
                                  SWAP }
                                {} ;
                             DUP 2 ;
                             DUP ;
                             GET 19 ;
                             SENDER ;
                             SOME ;
                             DIG 4 ;
                             GET 20 ;
                             UPDATE ;
                             UPDATE 19 ;
                             DUP ;
                             GET 20 ;
                             PUSH nat 1 ;
                             ADD ;
                             UPDATE 20 ;
                             SWAP ;
                             DUP 2 ;
                             GET 17 ;
                             CONTRACT %create_token (map string bytes) ;
                             IF_NONE { PUSH int 290 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             TRANSFER_TOKENS ;
                             CONS } } }
                   { IF_LEFT
                       { DUP 2 ;
                         GET 20 ;
                         DUP 2 ;
                         GET 4 ;
                         COMPARE ;
                         LT ;
                         IF { SENDER ;
                              DUP 3 ;
                              GET 19 ;
                              DUP 3 ;
                              GET 4 ;
                              GET ;
                              IF_NONE { PUSH int 295 ; FAILWITH } {} ;
                              COMPARE ;
                              EQ ;
                              IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } }
                            { PUSH bool False } ;
                         IF {}
                            { PUSH string
                                   "WrongCondition: (params.token_id < self.data.token_id) and ((self.data.token_creators[params.token_id] == sp.sender) or (self.data.admin.contains(sp.sender)))" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 17 ;
                         CONTRACT %lock (pair (bool %metadata) (pair (bool %mint) (nat %token_id))) ;
                         IF_NONE { PUSH int 297 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { IF_LEFT
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             SWAP ;
                             DUP ;
                             CAR ;
                             PUSH bool False ;
                             DIG 3 ;
                             UPDATE ;
                             UPDATE 1 }
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             DUP ;
                             ITER { DIG 2 ; DUP ; GET 3 ; NONE nat ; DIG 3 ; UPDATE ; UPDATE 3 ; SWAP } ;
                             DROP } ;
                         NIL operation } } } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 17 ;
                         CONTRACT %remove_manager address ;
                         IF_NONE { PUSH int 122 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         DUP 2 ;
                         GET 17 ;
                         CONTRACT %set_administrator address ;
                         IF_NONE { PUSH int 103 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         UPDATE 5 ;
                         NIL operation }
                       { IF_LEFT
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             UPDATE 9 ;
                             NIL operation }
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             DUP 2 ;
                             GET 17 ;
                             CONTRACT %set_metadata (big_map string bytes) ;
                             IF_NONE { PUSH int 109 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             TRANSFER_TOKENS ;
                             CONS } } } }
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         UPDATE 11 }
                       { IF_LEFT
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             DUP ;
                             GET 4 ;
                             DUP 2 ;
                             GET 3 ;
                             COMPARE ;
                             LT ;
                             IF {}
                                { PUSH string "WrongCondition: params.share < params.total" ; FAILWITH } ;
                             UPDATE 13 }
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             UPDATE 15 } } ;
                     NIL operation }
                   { IF_LEFT
                       { DUP 2 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                              FAILWITH } ;
                         UPDATE 17 ;
                         NIL operation }
                       { IF_LEFT
                           { DUP 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             IF {}
                                { PUSH string "WrongCondition: self.data.admin.contains(sp.sender)" ;
                                  FAILWITH } ;
                             DUP 2 ;
                             GET 17 ;
                             CONTRACT %transfer
                               (list (pair (address %from_)
                                           (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                             IF_NONE { PUSH int 131 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             PUSH mutez 0 ;
                             NIL (pair address (list (pair address (pair nat nat)))) ;
                             DIG 4 ;
                             SELF_ADDRESS ;
                             PAIR ;
                             CONS ;
                             TRANSFER_TOKENS ;
                             CONS }
                           { DUP ;
                             ITER { DUP 3 ;
                                    GET 20 ;
                                    DUP 2 ;
                                    CAR ;
                                    COMPARE ;
                                    LT ;
                                    IF { SENDER ;
                                         DUP 4 ;
                                         GET 19 ;
                                         DIG 2 ;
                                         CAR ;
                                         GET ;
                                         IF_NONE { PUSH int 303 ; FAILWITH } {} ;
                                         COMPARE ;
                                         EQ ;
                                         IF { PUSH bool True } { DUP 2 ; CAR ; SENDER ; MEM } }
                                       { DROP ; PUSH bool False } ;
                                    IF {}
                                       { PUSH string
                                              "WrongCondition: (token.token_id < self.data.token_id) and ((self.data.token_creators[token.token_id] == sp.sender) or (self.data.admin.contains(sp.sender)))" ;
                                         FAILWITH } } ;
                             DUP 2 ;
                             GET 17 ;
                             CONTRACT %update_token_metadata
                               (list (pair (nat %token_id) (map %token_info string bytes))) ;
                             IF_NONE { PUSH int 305 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             TRANSFER_TOKENS ;
                             CONS } } } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
