{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %mint
                   (pair (pair (address %address) (nat %amount))
                         (pair (pair %item_attributes
                                  (map %attr string nat)
                                  (pair (string %cons_type) (nat %duration)))
                               (map %metadata string bytes)))))
            (or (address %set_administrator) (pair %set_metadata (string %k) (bytes %v))))
        (or (or (bool %set_pause)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
            (or (pair %update_item_attributes
                   (pair %attributes (map %attr string nat) (pair (string %cons_type) (nat %duration)))
                   (nat %item_id))
                (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))) ;
  storage
    (pair (pair (pair (address %administrator) (nat %all_tokens))
                (pair (big_map %item_attributes
                         nat
                         (pair (map %attr string nat) (pair (string %cons_type) (nat %duration))))
                      (pair (nat %last_token_id) (big_map %ledger (pair address nat) nat))))
          (pair (pair (big_map %metadata string bytes)
                      (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (bool %paused)
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (big_map %total_supply nat nat))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (list (pair (pair address nat) (pair (pair (map string nat) (pair string nat)) (map string bytes)))))
                            (or address (pair string bytes)))
                        (or (or bool (list (pair address (list (pair address (pair nat nat))))))
                            (or (pair (pair (map string nat) (pair string nat)) nat)
                                (list (or (pair address (pair address nat)) (pair address (pair address nat)))))))
                    (pair (pair (pair address nat)
                                (pair (big_map nat (pair (map string nat) (pair string nat)))
                                      (pair nat (big_map (pair address nat) nat))))
                          (pair (pair (big_map string bytes) (big_map (pair address (pair address nat)) unit))
                                (pair bool (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat)))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 7 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           GET 6 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 3 ;
                                CAR ;
                                GET 6 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 429 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP 3 ;
                            CAR ;
                            GET 5 ;
                            DUP ;
                            DUP 5 ;
                            CAR ;
                            CAR ;
                            CDR ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                            DIG 3 ;
                            UNPAIR ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            PUSH nat 1 ;
                            DUP 6 ;
                            ADD ;
                            SWAP ;
                            DUP ;
                            DUP 3 ;
                            COMPARE ;
                            LE ;
                            IF { SWAP ; DROP } { DROP } ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            PAIR ;
                            DUP ;
                            DUG 4 ;
                            CAR ;
                            GET 6 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            DUP 4 ;
                            CAR ;
                            CAR ;
                            PAIR ;
                            MEM ;
                            IF { DIG 3 ;
                                 UNPAIR ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 SWAP ;
                                 DUP ;
                                 DUP 7 ;
                                 DUP 9 ;
                                 CAR ;
                                 CAR ;
                                 PAIR ;
                                 DUP ;
                                 DUG 2 ;
                                 GET ;
                                 IF_NONE { PUSH int 61 ; FAILWITH } {} ;
                                 DUP 9 ;
                                 CAR ;
                                 CDR ;
                                 ADD ;
                                 SOME ;
                                 SWAP ;
                                 UPDATE ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 PAIR ;
                                 DUG 3 }
                               { DIG 3 ;
                                 UNPAIR ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 SWAP ;
                                 DUP 7 ;
                                 CAR ;
                                 CDR ;
                                 SOME ;
                                 DUP 7 ;
                                 DUP 9 ;
                                 CAR ;
                                 CAR ;
                                 PAIR ;
                                 UPDATE ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 PAIR ;
                                 DUG 3 } ;
                            DUP 4 ;
                            GET 7 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            MEM ;
                            IF { DIG 3 ;
                                 DUP ;
                                 GET 8 ;
                                 DUP 4 ;
                                 CAR ;
                                 CDR ;
                                 SOME ;
                                 DUP 4 ;
                                 UPDATE ;
                                 UPDATE 8 ;
                                 DUG 3 }
                               { DIG 3 ;
                                 DUP ;
                                 GET 7 ;
                                 DUP 4 ;
                                 GET 4 ;
                                 DUP 4 ;
                                 PAIR ;
                                 SOME ;
                                 DUP 4 ;
                                 UPDATE ;
                                 UPDATE 7 ;
                                 DUP ;
                                 GET 8 ;
                                 DUP 4 ;
                                 CAR ;
                                 CDR ;
                                 SOME ;
                                 DUP 4 ;
                                 UPDATE ;
                                 UPDATE 8 ;
                                 DUG 3 } ;
                            DUP 4 ;
                            CAR ;
                            GET 3 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            MEM ;
                            IF { DROP 2 }
                               { DIG 3 ;
                                 UNPAIR ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 DIG 5 ;
                                 GET 3 ;
                                 SOME ;
                                 DIG 5 ;
                                 UPDATE ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 PAIR ;
                                 SWAP } ;
                            SWAP ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            PUSH nat 1 ;
                            ADD ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            SWAP } ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     DUP 5 ;
                     CDR ;
                     SOME ;
                     DIG 5 ;
                     CAR ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 5 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   CAR ;
                                   CAR ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP 4 ;
                                   GET 7 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        DUP 5 ;
                                        CAR ;
                                        GET 6 ;
                                        DUP 3 ;
                                        GET 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 408 ; FAILWITH } {} ;
                                        COMPARE ;
                                        GE ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP ;
                                        DUP 7 ;
                                        GET 3 ;
                                        DUP 9 ;
                                        CAR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 2 ;
                                        GET ;
                                        IF_NONE { PUSH int 411 ; FAILWITH } { DROP } ;
                                        DUP 7 ;
                                        GET 4 ;
                                        DIG 10 ;
                                        CAR ;
                                        GET 6 ;
                                        DUP 9 ;
                                        GET 3 ;
                                        DUP 11 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 412 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH int 411 ; FAILWITH } {} ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 4 ;
                                        CAR ;
                                        GET 6 ;
                                        SWAP ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 3 ;
                                        CAR ;
                                        PAIR ;
                                        MEM ;
                                        IF { DIG 3 ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             SWAP ;
                                             DUP ;
                                             DIG 6 ;
                                             DUP ;
                                             GET 3 ;
                                             SWAP ;
                                             DUP ;
                                             DUG 8 ;
                                             CAR ;
                                             PAIR ;
                                             DUP ;
                                             DUG 2 ;
                                             GET ;
                                             IF_NONE { PUSH int 414 ; FAILWITH } {} ;
                                             DIG 7 ;
                                             GET 4 ;
                                             ADD ;
                                             SOME ;
                                             SWAP ;
                                             UPDATE ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 }
                                           { DIG 3 ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             SWAP ;
                                             DUP 6 ;
                                             GET 4 ;
                                             SOME ;
                                             DIG 6 ;
                                             DUP ;
                                             GET 3 ;
                                             SWAP ;
                                             CAR ;
                                             PAIR ;
                                             UPDATE ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 } }
                                      { DROP } } ;
                            DROP } ;
                     DROP } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     MEM ;
                     IF {} { PUSH string "TC-Token : ERROR_CAN_NOT_FIND_TOKEN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     DUP 5 ;
                     CAR ;
                     SOME ;
                     DIG 5 ;
                     CDR ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                NONE unit ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } ;
  view "get_balance_of"
       (list (pair (address %owner) (nat %token_id)))
       (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))
       { UNPAIR ;
         DUP ;
         MAP { DUP 3 ;
               GET 7 ;
               SWAP ;
               DUP ;
               DUG 2 ;
               CDR ;
               MEM ;
               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
               DUP 3 ;
               CAR ;
               GET 6 ;
               SWAP ;
               DUP ;
               CDR ;
               SWAP ;
               DUP ;
               DUG 3 ;
               CAR ;
               PAIR ;
               GET ;
               IF_NONE { PUSH nat 0 } {} ;
               SWAP ;
               PAIR } ;
         SWAP ;
         DROP ;
         SWAP ;
         DROP } ;
  view "get_item_attributes"
       nat
       (pair (map %attr string nat) (pair (string %cons_type) (nat %duration)))
       { UNPAIR ;
         SWAP ;
         DUP ;
         DUG 2 ;
         CAR ;
         GET 3 ;
         SWAP ;
         DUP ;
         DUG 2 ;
         MEM ;
         IF {} { PUSH string "TC-Token : ERROR_CAN_NOT_FIND_TOKEN" ; FAILWITH } ;
         SWAP ;
         CAR ;
         GET 3 ;
         SWAP ;
         GET ;
         IF_NONE { PUSH int 92 ; FAILWITH } {} } }
