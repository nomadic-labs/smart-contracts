{ parameter
    (or (or (or (or (address %add_manager) (set %lock nat))
                (or (list %lock_callback
                       (pair (string %category)
                             (pair (map %ints string int)
                                   (pair (bool %on_sale)
                                         (pair (address %owner)
                                               (pair (map %sets string (set nat))
                                                     (pair (map %strings string string) (pair (nat %token_id) (nat %uid)))))))))
                    (address %remove_manager)))
            (or (or (address %set_admin) (address %set_manager_proxy))
                (or (address %set_minter) (bool %set_pause))))
        (or (or (or (address %set_resource_treasury) (key %set_signer))
                (or (pair %unlock
                       (address %address)
                       (pair (map %attr nat (map string int))
                             (pair (list %custom_nfts
                                      (pair (pair %attribute
                                               (string %category)
                                               (pair (map %ints string int)
                                                     (pair (map %sets string (set nat)) (map %strings string string))))
                                            (bytes %metadata)))
                                   (pair (list %nfts bytes) (pair (map %resources nat nat) (signature %signature))))))
                    (list %unlock_callback
                       (pair (string %category)
                             (pair (map %ints string int)
                                   (pair (bool %on_sale)
                                         (pair (address %owner)
                                               (pair (map %sets string (set nat))
                                                     (pair (map %strings string string) (pair (nat %token_id) (nat %uid)))))))))))
            (or (or (set %update_allowed_attrs string) (set %update_custom_categories string))
                (or (pair %update_nfts
                       (list %add_
                          (pair (pair %attribute
                                   (string %category)
                                   (pair (map %ints string int)
                                         (pair (map %sets string (set nat)) (map %strings string string))))
                                (bytes %metadata)))
                       (list %remove_ bytes))
                    (pair %update_unit_limits (nat %max) (nat %min)))))) ;
  storage
    (pair (address %admin)
          (pair (set %allowed_attrs string)
                (pair (option %current_lock (pair (set %token_ids nat) (address %user)))
                      (pair (option %current_unlock
                               (pair (map %attr nat (map string int))
                                     (pair (list %custom_nfts
                                              (pair (pair %attribute
                                                       (string %category)
                                                       (pair (map %ints string int)
                                                             (pair (map %sets string (set nat)) (map %strings string string))))
                                                    (bytes %metadata)))
                                           (pair (list %nfts bytes) (pair (map %resources nat nat) (address %user))))))
                            (pair (set %custom_categories string)
                                  (pair (address %manager_proxy)
                                        (pair (set %managers address)
                                              (pair (nat %max_units)
                                                    (pair (big_map %metadata string bytes)
                                                          (pair (nat %min_units)
                                                                (pair (address %minter)
                                                                      (pair (address %nft_registry)
                                                                            (pair (big_map %nfts
                                                                                     bytes
                                                                                     (pair (string %category)
                                                                                           (pair (map %ints string int)
                                                                                                 (pair (map %sets string (set nat)) (map %strings string string)))))
                                                                                  (pair (bool %paused)
                                                                                        (pair (address %resource_treasury)
                                                                                              (pair (key %signer) (big_map %unit_map address (pair (nat %level) (set %units nat))))))))))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 13 ;
                         PUSH bool True ;
                         DIG 3 ;
                         UPDATE ;
                         UPDATE 13 ;
                         NIL operation }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 27 ;
                         IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 32 ;
                         SENDER ;
                         MEM ;
                         IF { PUSH string "ALREADY_LOCKED_UNITS" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 19 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SIZE ;
                         COMPARE ;
                         GE ;
                         IF { SWAP ; DUP ; DUG 2 ; GET 15 ; SWAP ; DUP ; DUG 2 ; SIZE ; COMPARE ; LE }
                            { PUSH bool False } ;
                         IF {} { PUSH string "INVALID_NUMBER_OF_UNITS" ; FAILWITH } ;
                         SWAP ;
                         SENDER ;
                         DUP 3 ;
                         PAIR ;
                         SOME ;
                         UPDATE 5 ;
                         SWAP ;
                         NIL operation ;
                         DUP 3 ;
                         GET 23 ;
                         CONTRACT %get_attributes
                           (pair (list nat)
                                 (contract
                                    (list (pair (string %category)
                                                (pair (map %ints string int)
                                                      (pair (bool %on_sale)
                                                            (pair (address %owner)
                                                                  (pair (map %sets string (set nat))
                                                                        (pair (map %strings string string) (pair (nat %token_id) (nat %uid))))))))))) ;
                         IF_NONE { PUSH int 168 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         SELF %lock_callback ;
                         NIL nat ;
                         DIG 5 ;
                         ITER { CONS } ;
                         NIL nat ;
                         SWAP ;
                         ITER { CONS } ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 23 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_NFT_REGISTRY" ; FAILWITH } ;
                         NIL (pair address (pair nat nat)) ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         ITER { DUP ;
                                CAR ;
                                PUSH string "unit" ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "NOT_A_UNIT" ; FAILWITH } ;
                                DUP 4 ;
                                GET 5 ;
                                IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                GET 7 ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "NOT_OWNED_BY_USER" ; FAILWITH } ;
                                SWAP ;
                                PUSH nat 1 ;
                                DIG 2 ;
                                GET 13 ;
                                SELF_ADDRESS ;
                                PAIR 3 ;
                                CONS } ;
                         NIL operation ;
                         DUP 4 ;
                         GET 23 ;
                         CONTRACT %transfer
                           (list (pair (address %from_)
                                       (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                         IF_NONE { PUSH int 199 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         NIL (pair address (list (pair address (pair nat nat)))) ;
                         DUP 5 ;
                         DUP 8 ;
                         GET 5 ;
                         IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                         CDR ;
                         PAIR ;
                         CONS ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         DUP 4 ;
                         DUP ;
                         GET 32 ;
                         DUP 6 ;
                         GET 5 ;
                         IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                         CAR ;
                         LEVEL ;
                         DIG 5 ;
                         DROP ;
                         DIG 5 ;
                         DROP ;
                         PAIR ;
                         SOME ;
                         DIG 4 ;
                         GET 5 ;
                         IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                         CDR ;
                         UPDATE ;
                         UPDATE 32 ;
                         NONE (pair (set nat) address) ;
                         UPDATE 5 ;
                         SWAP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 13 ;
                         PUSH bool False ;
                         DIG 3 ;
                         UPDATE ;
                         UPDATE 13 ;
                         NIL operation } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 1 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 11 } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 21 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 27 } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 29 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 31 } ;
                     NIL operation }
                   { IF_LEFT
                       { DUP ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH bool True } { SWAP ; DUP ; DUG 2 ; GET 13 ; SENDER ; MEM } ;
                         IF {} { PUSH string "INVALID_USER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 27 ;
                         IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 32 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         MEM ;
                         IF {} { PUSH string "NO_LOCKED_UNITS" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 32 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 228 ; FAILWITH } {} ;
                         CAR ;
                         LEVEL ;
                         COMPARE ;
                         GT ;
                         IF {} { PUSH string "CANNOT_LOCK_AND_UNLOCK_IN_SAME_LEVEL" ; FAILWITH } ;
                         DUP ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         GET 9 ;
                         SWAP ;
                         DUP ;
                         GET 7 ;
                         SWAP ;
                         DUP ;
                         GET 5 ;
                         SWAP ;
                         DUP ;
                         DUG 6 ;
                         GET 3 ;
                         PAIR 5 ;
                         SOME ;
                         UPDATE 7 ;
                         SWAP ;
                         DUP ;
                         GET 9 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         DUP 4 ;
                         GET 32 ;
                         DUP 4 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 241 ; FAILWITH } {} ;
                         CAR ;
                         DUP 4 ;
                         GET 5 ;
                         SELF_ADDRESS ;
                         DIG 5 ;
                         DUP ;
                         GET 3 ;
                         SWAP ;
                         DUP ;
                         DUG 7 ;
                         CAR ;
                         PAIR 7 ;
                         PACK ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 10 ;
                         DUP 4 ;
                         GET 31 ;
                         CHECK_SIGNATURE ;
                         IF {} { PUSH string "INVALID_SIGN" ; FAILWITH } ;
                         NIL operation ;
                         DUP 3 ;
                         GET 23 ;
                         CONTRACT %get_attributes
                           (pair (list nat)
                                 (contract
                                    (list (pair (string %category)
                                                (pair (map %ints string int)
                                                      (pair (bool %on_sale)
                                                            (pair (address %owner)
                                                                  (pair (map %sets string (set nat))
                                                                        (pair (map %strings string string) (pair (nat %token_id) (nat %uid))))))))))) ;
                         IF_NONE { PUSH int 257 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         SELF %unlock_callback ;
                         NIL nat ;
                         DUP 7 ;
                         GET 32 ;
                         DIG 6 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 260 ; FAILWITH } {} ;
                         CDR ;
                         ITER { CONS } ;
                         NIL nat ;
                         SWAP ;
                         ITER { CONS } ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 23 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_NFT_REGISTRY" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         IF_NONE { PUSH string "NO_DATA_SET" ; FAILWITH } {} ;
                         NIL (pair (map string int) (pair (map string (set nat)) (pair (map string string) nat))) ;
                         NIL (pair address (pair nat nat)) ;
                         DUP 4 ;
                         ITER { DUP ;
                                CAR ;
                                PUSH string "unit" ;
                                COMPARE ;
                                EQ ;
                                IF {} { PUSH string "NOT_A_UNIT" ; FAILWITH } ;
                                DUP ;
                                DIG 2 ;
                                PUSH nat 1 ;
                                DUP 4 ;
                                GET 13 ;
                                DUP 7 ;
                                GET 8 ;
                                PAIR 3 ;
                                CONS ;
                                DUG 2 ;
                                DUP ;
                                GET 3 ;
                                PUSH timestamp "1970-01-01T00:00:00Z" ;
                                NOW ;
                                SUB ;
                                ABS ;
                                INT ;
                                SOME ;
                                PUSH string "damage_time" ;
                                UPDATE ;
                                UPDATE 3 ;
                                DUP 5 ;
                                CAR ;
                                DUP 3 ;
                                GET 13 ;
                                MEM ;
                                IF { DUP 5 ;
                                     CAR ;
                                     DUP 3 ;
                                     GET 13 ;
                                     GET ;
                                     IF_NONE { PUSH int 292 ; FAILWITH } {} ;
                                     ITER { CAR ;
                                            DUP 8 ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            MEM ;
                                            IF {} { PUSH string "INVALID_ATTRIBUTE" ; FAILWITH } ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            DUP 7 ;
                                            CAR ;
                                            DUP 5 ;
                                            GET 13 ;
                                            GET ;
                                            IF_NONE { PUSH int 294 ; FAILWITH } {} ;
                                            DUP 4 ;
                                            GET ;
                                            IF_NONE { PUSH int 294 ; FAILWITH } {} ;
                                            SOME ;
                                            DIG 3 ;
                                            UPDATE ;
                                            UPDATE 3 } }
                                   {} ;
                                DUP 4 ;
                                DUP 3 ;
                                GET 13 ;
                                DIG 2 ;
                                DUP ;
                                GET 11 ;
                                SWAP ;
                                DUP ;
                                GET 9 ;
                                SWAP ;
                                DUP ;
                                DUG 5 ;
                                GET 3 ;
                                PAIR 4 ;
                                DIG 2 ;
                                DROP ;
                                DIG 2 ;
                                DROP ;
                                DIG 3 ;
                                DROP ;
                                CONS ;
                                SWAP } ;
                         NIL operation ;
                         DUP 6 ;
                         GET 23 ;
                         CONTRACT %transfer
                           (list (pair (address %from_)
                                       (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                         IF_NONE { PUSH int 302 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         NIL (pair address (list (pair address (pair nat nat)))) ;
                         DUP 5 ;
                         SELF_ADDRESS ;
                         PAIR ;
                         CONS ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         DUP 6 ;
                         GET 11 ;
                         CONTRACT %update_attributes
                           (list (pair (map %ints string int)
                                       (pair (map %sets string (set nat)) (pair (map %strings string string) (nat %token_id))))) ;
                         IF_NONE { PUSH int 308 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DUP 5 ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         PUSH nat 0 ;
                         DUP 5 ;
                         GET 5 ;
                         SIZE ;
                         COMPARE ;
                         GT ;
                         IF { DUP 4 ;
                              GET 5 ;
                              ITER { DUP 7 ;
                                     GET 25 ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     MEM ;
                                     IF {} { PUSH string "NFT_DOES_NOT_EXIST" ; FAILWITH } ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     DUP 8 ;
                                     GET 21 ;
                                     CONTRACT %mint
                                       (pair (address %address)
                                             (pair (pair %attribute
                                                      (string %category)
                                                      (pair (map %ints string int)
                                                            (pair (map %sets string (set nat)) (map %strings string string))))
                                                   (pair (nat %editions) (bytes %metadataUri)))) ;
                                     IF_NONE { PUSH int 329 ; FAILWITH } {} ;
                                     PUSH mutez 0 ;
                                     DUP 4 ;
                                     PUSH nat 1 ;
                                     DUP 12 ;
                                     GET 25 ;
                                     DUP 7 ;
                                     GET ;
                                     IF_NONE { PUSH int 334 ; FAILWITH } {} ;
                                     DUP 11 ;
                                     GET 8 ;
                                     PAIR 4 ;
                                     DIG 4 ;
                                     DROP ;
                                     DIG 4 ;
                                     DROP ;
                                     TRANSFER_TOKENS ;
                                     CONS } }
                            {} ;
                         PUSH nat 0 ;
                         DUP 5 ;
                         GET 3 ;
                         SIZE ;
                         COMPARE ;
                         GT ;
                         IF { DUP 4 ;
                              GET 3 ;
                              ITER { DUP 7 ;
                                     GET 9 ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     CAR ;
                                     CAR ;
                                     MEM ;
                                     IF {} { PUSH string "CATEGORY_NOT_ALLOWED" ; FAILWITH } ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     DUP 8 ;
                                     GET 21 ;
                                     CONTRACT %mint
                                       (pair (address %address)
                                             (pair (pair %attribute
                                                      (string %category)
                                                      (pair (map %ints string int)
                                                            (pair (map %sets string (set nat)) (map %strings string string))))
                                                   (pair (nat %editions) (bytes %metadataUri)))) ;
                                     IF_NONE { PUSH int 351 ; FAILWITH } {} ;
                                     PUSH mutez 0 ;
                                     DUP 4 ;
                                     CDR ;
                                     PUSH nat 1 ;
                                     DUP 6 ;
                                     CAR ;
                                     DUP 11 ;
                                     GET 8 ;
                                     PAIR 4 ;
                                     DIG 4 ;
                                     DROP ;
                                     DIG 4 ;
                                     DROP ;
                                     TRANSFER_TOKENS ;
                                     CONS } }
                            {} ;
                         PUSH nat 0 ;
                         DUP 5 ;
                         GET 7 ;
                         SIZE ;
                         COMPARE ;
                         GT ;
                         IF { EMPTY_MAP nat nat ;
                              DUP 5 ;
                              GET 7 ;
                              ITER { CAR ;
                                     SWAP ;
                                     DUP 6 ;
                                     GET 7 ;
                                     DUP 3 ;
                                     GET ;
                                     IF_NONE { PUSH int 373 ; FAILWITH } {} ;
                                     SOME ;
                                     DIG 2 ;
                                     UPDATE } ;
                              DIG 2 ;
                              DROP ;
                              DIG 2 ;
                              DROP ;
                              DIG 3 ;
                              DROP ;
                              SWAP ;
                              DUP 4 ;
                              GET 29 ;
                              CONTRACT %resource_transfer (pair (address %address) (map %resources nat nat)) ;
                              IF_NONE { PUSH int 369 ; FAILWITH } {} ;
                              PUSH mutez 0 ;
                              DIG 3 ;
                              DUP 5 ;
                              GET 8 ;
                              PAIR ;
                              TRANSFER_TOKENS ;
                              CONS }
                            { SWAP ; DROP ; SWAP ; DROP ; DIG 2 ; DROP } ;
                         DIG 2 ;
                         DUP ;
                         GET 32 ;
                         NONE (pair nat (set nat)) ;
                         DIG 4 ;
                         GET 8 ;
                         UPDATE ;
                         UPDATE 32 ;
                         NONE (pair (map nat (map string int))
                                    (pair (list (pair (pair string (pair (map string int) (pair (map string (set nat)) (map string string))))
                                                      bytes))
                                          (pair (list bytes) (pair (map nat nat) address)))) ;
                         UPDATE 7 ;
                         SWAP } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 3 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 9 } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         CAR ;
                         ITER { DIG 2 ;
                                DUP ;
                                GET 25 ;
                                DUP 3 ;
                                CAR ;
                                SOME ;
                                DIG 3 ;
                                CDR ;
                                UPDATE ;
                                UPDATE 25 ;
                                SWAP } ;
                         DUP ;
                         CDR ;
                         ITER { DUP 3 ;
                                GET 25 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                MEM ;
                                IF {} { PUSH string "DOES_NOT_EXIST" ; FAILWITH } ;
                                DIG 2 ;
                                DUP ;
                                GET 25 ;
                                NONE (pair string (pair (map string int) (pair (map string (set nat)) (map string string)))) ;
                                DIG 3 ;
                                UPDATE ;
                                UPDATE 25 ;
                                SWAP } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         UPDATE 15 ;
                         SWAP ;
                         CDR ;
                         UPDATE 19 } } ;
                 NIL operation } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
