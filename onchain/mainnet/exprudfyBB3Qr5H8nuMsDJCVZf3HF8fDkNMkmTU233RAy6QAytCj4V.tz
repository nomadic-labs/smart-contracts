{ view "is_paused" unit bool { CDR ; CDR ; CAR ; CDR } ;
  view "balance_of"
       (pair address nat)
       (option nat)
       { UNPAIR ; UNPAIR ; DIG 2 ; CAR ; CDR ; CAR ; DUG 2 ; PAIR ; GET } ;
  parameter
    (or (or (or (or (pair %add_token_metadata nat bytes)
                    (pair %balance_of
                       (list %requests (pair (address %owner) (nat %token_id)))
                       (contract %callback
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance))))))
                (or (unit %mint) (unit %set_pause)))
            (or (or (list %transfer
                       (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))
                    (address %update_admin))
                (or (unit %update_current_token_id) (bytes %update_metadata))))
        (or (list %update_operators
               (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                   (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))
            (pair %update_token_metadata nat bytes))) ;
  storage
    (pair (pair (pair (address %admin) (nat %current_token_id))
                (pair (big_map %ledger (pair address nat) nat) (big_map %metadata string bytes)))
          (pair (pair (big_map %operators (pair (address %owner) (address %operator) (nat %token_id)) unit)
                      (bool %paused))
                (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                      (nat %total_nfts)))) ;
  code { LAMBDA
           (pair address
                 (pair (pair (pair address nat) (pair (big_map (pair address nat) nat) (big_map string bytes)))
                       (pair (pair (big_map (pair address address nat) unit) bool)
                             (pair (big_map nat (pair nat (map string bytes))) nat))))
           unit
           { UNPAIR ;
             SWAP ;
             CAR ;
             CAR ;
             CAR ;
             SWAP ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair (pair (pair address nat) (pair (big_map (pair address nat) nat) (big_map string bytes)))
                 (pair (pair (big_map (pair address address nat) unit) bool)
                       (pair (big_map nat (pair nat (map string bytes))) nat)))
           unit
           { PUSH bool True ;
             SWAP ;
             CDR ;
             CAR ;
             CDR ;
             COMPARE ;
             EQ ;
             IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } { UNIT } } ;
         DIG 2 ;
         UNPAIR ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         NEQ ;
         IF { DROP 4 ; PUSH string "NO_XTZ_AMOUNT" ; FAILWITH }
            { IF_LEFT
                { IF_LEFT
                    { IF_LEFT
                        { DIG 2 ;
                          DROP ;
                          IF_LEFT
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              SENDER ;
                              PAIR ;
                              DIG 3 ;
                              SWAP ;
                              EXEC ;
                              DROP ;
                              UNPAIR ;
                              DUP 3 ;
                              CDR ;
                              CDR ;
                              CAR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              MEM ;
                              IF { DROP 3 ; PUSH string "TOKEN_ALREADY_EXISTS" ; FAILWITH }
                                 { DUP 3 ;
                                   CDR ;
                                   CDR ;
                                   CDR ;
                                   DUP 4 ;
                                   CDR ;
                                   CDR ;
                                   CAR ;
                                   EMPTY_MAP string bytes ;
                                   DIG 4 ;
                                   SOME ;
                                   PUSH string "" ;
                                   UPDATE ;
                                   DUP 4 ;
                                   PAIR ;
                                   DIG 3 ;
                                   SWAP ;
                                   SOME ;
                                   SWAP ;
                                   UPDATE ;
                                   PAIR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   CAR ;
                                   PAIR ;
                                   SWAP ;
                                   CAR ;
                                   PAIR } ;
                              NIL operation ;
                              PAIR }
                            { DIG 2 ;
                              DROP ;
                              DUP ;
                              CAR ;
                              MAP { DUP 3 ;
                                    CDR ;
                                    CDR ;
                                    CAR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    CDR ;
                                    MEM ;
                                    NOT ;
                                    IF { DROP ; PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH }
                                       { DUP 3 ;
                                         CAR ;
                                         CDR ;
                                         CAR ;
                                         SWAP ;
                                         DUP ;
                                         DUG 2 ;
                                         CDR ;
                                         DUP 3 ;
                                         CAR ;
                                         PAIR ;
                                         GET ;
                                         IF_NONE { PUSH nat 0 ; SWAP ; PAIR } { SWAP ; PAIR } } } ;
                              DIG 2 ;
                              NIL operation ;
                              DIG 3 ;
                              CDR ;
                              PUSH mutez 0 ;
                              DIG 4 ;
                              TRANSFER_TOKENS ;
                              CONS ;
                              PAIR } }
                        { IF_LEFT
                            { DIG 3 ;
                              DROP 2 ;
                              DUP ;
                              DIG 2 ;
                              SWAP ;
                              EXEC ;
                              DROP ;
                              DUP ;
                              CAR ;
                              CDR ;
                              CAR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CAR ;
                              CAR ;
                              CDR ;
                              SENDER ;
                              PAIR ;
                              MEM ;
                              IF { DROP ; PUSH string "ALREADY_CLAIMED" ; FAILWITH }
                                 { DUP ;
                                   CDR ;
                                   CDR ;
                                   CAR ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CAR ;
                                   CAR ;
                                   CDR ;
                                   MEM ;
                                   NOT ;
                                   IF { DROP ; PUSH string "NO_TOKEN_METADATA" ; FAILWITH }
                                      { DUP ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        CDR ;
                                        CDR ;
                                        DUP 3 ;
                                        CAR ;
                                        CDR ;
                                        CAR ;
                                        PUSH nat 1 ;
                                        DUP 5 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        SENDER ;
                                        PAIR ;
                                        SWAP ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        PAIR ;
                                        DUP 3 ;
                                        CAR ;
                                        CAR ;
                                        PAIR ;
                                        PAIR ;
                                        PUSH nat 1 ;
                                        DIG 2 ;
                                        CDR ;
                                        CDR ;
                                        CDR ;
                                        ADD ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CDR ;
                                        CDR ;
                                        CAR ;
                                        PAIR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CDR ;
                                        CAR ;
                                        PAIR ;
                                        SWAP ;
                                        CAR ;
                                        PAIR } } ;
                              NIL operation ;
                              PAIR }
                            { DIG 2 ;
                              DROP 2 ;
                              DUP ;
                              SENDER ;
                              PAIR ;
                              DIG 2 ;
                              SWAP ;
                              EXEC ;
                              DROP ;
                              DUP ;
                              CDR ;
                              CDR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              CAR ;
                              CDR ;
                              NOT ;
                              DUP 3 ;
                              CDR ;
                              CAR ;
                              CAR ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              CAR ;
                              PAIR ;
                              NIL operation ;
                              PAIR } } }
                    { DIG 2 ;
                      DROP ;
                      IF_LEFT
                        { IF_LEFT
                            { DIG 2 ;
                              DROP ;
                              ITER { UNPAIR ;
                                     DIG 2 ;
                                     SWAP ;
                                     PAIR ;
                                     SWAP ;
                                     ITER { SWAP ;
                                            UNPAIR ;
                                            DIG 2 ;
                                            UNPAIR 3 ;
                                            DUP 5 ;
                                            CDR ;
                                            CDR ;
                                            CAR ;
                                            DUP 3 ;
                                            MEM ;
                                            NOT ;
                                            IF { DROP 5 ; PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH }
                                               { SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 SENDER ;
                                                 DUP 6 ;
                                                 PAIR 3 ;
                                                 DUP 6 ;
                                                 CDR ;
                                                 CAR ;
                                                 CAR ;
                                                 SWAP ;
                                                 MEM ;
                                                 NOT ;
                                                 DUP 5 ;
                                                 SENDER ;
                                                 COMPARE ;
                                                 NEQ ;
                                                 AND ;
                                                 IF { DROP 5 ; PUSH string "FA2_NOT_OPERATOR" ; FAILWITH }
                                                    { DUP 5 ;
                                                      CAR ;
                                                      CDR ;
                                                      CAR ;
                                                      DUP 3 ;
                                                      DUP 6 ;
                                                      PAIR ;
                                                      GET ;
                                                      IF_NONE
                                                        { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                                        { DUP 4 ;
                                                          SWAP ;
                                                          DUP ;
                                                          DUG 2 ;
                                                          COMPARE ;
                                                          LT ;
                                                          IF { DROP ; PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH }
                                                             { DUP 6 ;
                                                               CAR ;
                                                               CDR ;
                                                               CAR ;
                                                               DUP 5 ;
                                                               DIG 2 ;
                                                               SUB ;
                                                               ABS ;
                                                               SOME ;
                                                               DUP 4 ;
                                                               DUP 7 ;
                                                               PAIR ;
                                                               UPDATE } } ;
                                                      DUP ;
                                                      DUP 4 ;
                                                      DUP 4 ;
                                                      PAIR ;
                                                      GET ;
                                                      IF_NONE
                                                        { DUG 3 ; PAIR ; SWAP ; SOME ; SWAP ; UPDATE }
                                                        { SWAP ; DIG 4 ; DIG 2 ; ADD ; SOME ; DIG 3 ; DIG 3 ; PAIR ; UPDATE } ;
                                                      DUP 3 ;
                                                      CDR ;
                                                      DUP 4 ;
                                                      CAR ;
                                                      CDR ;
                                                      CDR ;
                                                      DIG 2 ;
                                                      PAIR ;
                                                      DIG 3 ;
                                                      CAR ;
                                                      CAR ;
                                                      PAIR ;
                                                      PAIR ;
                                                      SWAP ;
                                                      PAIR } } } ;
                                     CDR } ;
                              NIL operation ;
                              PAIR }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              SENDER ;
                              PAIR ;
                              DIG 3 ;
                              SWAP ;
                              EXEC ;
                              DROP ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              DUP 3 ;
                              CAR ;
                              CDR ;
                              DIG 3 ;
                              CAR ;
                              CAR ;
                              CDR ;
                              DIG 3 ;
                              PAIR ;
                              PAIR ;
                              PAIR ;
                              NIL operation ;
                              PAIR } }
                        { IF_LEFT
                            { DROP ;
                              DUP ;
                              SENDER ;
                              PAIR ;
                              DIG 2 ;
                              SWAP ;
                              EXEC ;
                              DROP ;
                              PUSH nat 1 ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CAR ;
                              CAR ;
                              CDR ;
                              ADD ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              CDR ;
                              CAR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              MEM ;
                              NOT ;
                              IF { DROP 2 ; PUSH string "NO_MATCHING_TOKEN_METADATA" ; FAILWITH }
                                 { SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CDR ;
                                   DUP 3 ;
                                   CAR ;
                                   CDR ;
                                   DIG 2 ;
                                   DIG 3 ;
                                   CAR ;
                                   CAR ;
                                   CAR ;
                                   PAIR ;
                                   PAIR ;
                                   PAIR } ;
                              NIL operation ;
                              PAIR }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              SENDER ;
                              PAIR ;
                              DIG 3 ;
                              SWAP ;
                              EXEC ;
                              DROP ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CDR ;
                              DUP 3 ;
                              CAR ;
                              CDR ;
                              CDR ;
                              DIG 2 ;
                              SOME ;
                              PUSH string "contents" ;
                              UPDATE ;
                              DUP 3 ;
                              CAR ;
                              CDR ;
                              CAR ;
                              PAIR ;
                              DIG 2 ;
                              CAR ;
                              CAR ;
                              PAIR ;
                              PAIR ;
                              NIL operation ;
                              PAIR } } } }
                { IF_LEFT
                    { DIG 3 ;
                      DROP ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      DIG 3 ;
                      SWAP ;
                      EXEC ;
                      DROP ;
                      ITER { IF_LEFT
                               { DUP ;
                                 CAR ;
                                 SENDER ;
                                 COMPARE ;
                                 NEQ ;
                                 IF { DROP 2 ; PUSH string "FA2_NOT_OWNER" ; FAILWITH }
                                    { SWAP ;
                                      DUP ;
                                      DUG 2 ;
                                      CDR ;
                                      CDR ;
                                      DUP 3 ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      DUP 4 ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      UNIT ;
                                      DIG 4 ;
                                      SWAP ;
                                      SOME ;
                                      SWAP ;
                                      UPDATE ;
                                      PAIR ;
                                      PAIR ;
                                      SWAP ;
                                      CAR ;
                                      PAIR } }
                               { DUP ;
                                 CAR ;
                                 SENDER ;
                                 COMPARE ;
                                 NEQ ;
                                 IF { DROP 2 ; PUSH string "FA2_NOT_OWNER" ; FAILWITH }
                                    { SWAP ;
                                      DUP ;
                                      DUG 2 ;
                                      CDR ;
                                      CDR ;
                                      DUP 3 ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      DUP 4 ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      DIG 3 ;
                                      NONE unit ;
                                      SWAP ;
                                      UPDATE ;
                                      PAIR ;
                                      PAIR ;
                                      SWAP ;
                                      CAR ;
                                      PAIR } } } ;
                      NIL operation ;
                      PAIR }
                    { DIG 2 ;
                      DROP ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      SENDER ;
                      PAIR ;
                      DIG 3 ;
                      SWAP ;
                      EXEC ;
                      DROP ;
                      UNPAIR ;
                      DUP 3 ;
                      CDR ;
                      CDR ;
                      CAR ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      MEM ;
                      NOT ;
                      IF { DROP 3 ; PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH }
                         { DUP 3 ;
                           CDR ;
                           CDR ;
                           CDR ;
                           DUP 4 ;
                           CDR ;
                           CDR ;
                           CAR ;
                           EMPTY_MAP string bytes ;
                           DIG 4 ;
                           SOME ;
                           PUSH string "" ;
                           UPDATE ;
                           DUP 4 ;
                           PAIR ;
                           SOME ;
                           DIG 3 ;
                           UPDATE ;
                           PAIR ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           CAR ;
                           PAIR ;
                           SWAP ;
                           CAR ;
                           PAIR } ;
                      NIL operation ;
                      PAIR } } } } }
