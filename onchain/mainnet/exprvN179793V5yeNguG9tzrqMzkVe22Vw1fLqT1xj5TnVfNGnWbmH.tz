{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %burn (pair (address %from_) (pair (nat %token_id) (nat %amount)))))
            (or (map %create_token string bytes)
                (or (nat %lock) (pair %mint (address %to_) (nat %token_id)))))
        (or (or (address %set_administrator) (big_map %set_metadata string bytes))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (or (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                    (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes))))))) ;
  storage
    (pair (pair (address %administrator)
                (pair (nat %last_token_id) (big_map %ledger nat address)))
          (pair (pair (big_map %locked nat unit) (big_map %metadata string bytes))
                (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { NIL operation ;
                     DUP ;
                     DUP 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DUP 5 ;
                     CAR ;
                     MAP { DUP 7 ;
                           GET 6 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP ;
                           CAR ;
                           DUP 8 ;
                           CAR ;
                           GET 4 ;
                           DUP 3 ;
                           CDR ;
                           GET ;
                           IF_NONE { PUSH int 369 ; FAILWITH } {} ;
                           COMPARE ;
                           EQ ;
                           IF { PUSH nat 1 } { PUSH nat 0 } ;
                           SWAP ;
                           PAIR } ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { DUP ;
                     ITER { DUP 3 ;
                            GET 6 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 3 ;
                            MEM ;
                            IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                            DUP ;
                            CAR ;
                            SENDER ;
                            COMPARE ;
                            EQ ;
                            IF { PUSH bool True }
                               { DUP 3 ;
                                 GET 5 ;
                                 SWAP ;
                                 DUP ;
                                 DUG 2 ;
                                 GET 3 ;
                                 SENDER ;
                                 DUP 4 ;
                                 CAR ;
                                 PAIR 3 ;
                                 MEM } ;
                            IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                            DUP ;
                            GET 4 ;
                            PUSH nat 0 ;
                            COMPARE ;
                            LT ;
                            IF { DUP ;
                                 GET 4 ;
                                 PUSH nat 1 ;
                                 COMPARE ;
                                 EQ ;
                                 IF { DUP 3 ;
                                      CAR ;
                                      CAR ;
                                      SENDER ;
                                      COMPARE ;
                                      EQ ;
                                      IF { PUSH bool True }
                                         { DUP ;
                                           CAR ;
                                           DUP 4 ;
                                           CAR ;
                                           GET 4 ;
                                           DUP 3 ;
                                           GET 3 ;
                                           GET ;
                                           IF_NONE { PUSH int 73 ; FAILWITH } {} ;
                                           COMPARE ;
                                           EQ } }
                                    { PUSH bool False } ;
                                 IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                 DIG 2 ;
                                 UNPAIR ;
                                 UNPAIR ;
                                 SWAP ;
                                 UNPAIR ;
                                 SWAP ;
                                 NONE address ;
                                 DUP 6 ;
                                 GET 3 ;
                                 UPDATE ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 PAIR ;
                                 DUP ;
                                 GET 6 ;
                                 NONE (pair nat (map string bytes)) ;
                                 DIG 3 ;
                                 GET 3 ;
                                 UPDATE ;
                                 UPDATE 6 ;
                                 SWAP }
                               { DROP } } ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     DIG 2 ;
                     DUP ;
                     GET 6 ;
                     DIG 3 ;
                     DUP 4 ;
                     PAIR ;
                     SOME ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 6 ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 4 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.ledger.contains(params)" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         PUSH (option unit) (Some Unit) ;
                         DIG 5 ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         MEM ;
                         IF {}
                            { PUSH string "WrongCondition: self.data.token_metadata.contains(params.token_id)" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 4 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         MEM ;
                         IF { PUSH string "WrongCondition: ~ (self.data.ledger.contains(params.token_id))" ;
                              FAILWITH }
                            {} ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 5 ;
                         CAR ;
                         SOME ;
                         DIG 5 ;
                         CDR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } }
               { IF_LEFT
                   { DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   GET 6 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 5 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        PUSH nat 1 ;
                                        COMPARE ;
                                        EQ ;
                                        IF { SWAP ;
                                             DUP ;
                                             DUG 2 ;
                                             CAR ;
                                             DUP 5 ;
                                             CAR ;
                                             GET 4 ;
                                             DUP 3 ;
                                             GET 3 ;
                                             GET ;
                                             IF_NONE { PUSH int 377 ; FAILWITH } {} ;
                                             COMPARE ;
                                             EQ }
                                           { PUSH bool False } ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DIG 3 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP 5 ;
                                        CAR ;
                                        SOME ;
                                        DIG 5 ;
                                        GET 3 ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { IF_LEFT
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 5 ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 3 ;
                                    UPDATE ;
                                    UPDATE 5 ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 5 ;
                                    NONE unit ;
                                    DIG 3 ;
                                    UPDATE ;
                                    UPDATE 5 ;
                                    SWAP } } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         ITER { DUP 3 ;
                                GET 3 ;
                                CAR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                MEM ;
                                IF { PUSH string "WrongCondition: ~ (self.data.locked.contains(metadata.token_id))" ;
                                     FAILWITH }
                                   {} ;
                                DIG 2 ;
                                DUP ;
                                GET 6 ;
                                DIG 2 ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 4 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DIG 3 ;
                                CAR ;
                                UPDATE ;
                                UPDATE 6 ;
                                SWAP } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } ;
  view "get_next_token_id" unit nat { CDR ; CAR ; GET 3 } }
