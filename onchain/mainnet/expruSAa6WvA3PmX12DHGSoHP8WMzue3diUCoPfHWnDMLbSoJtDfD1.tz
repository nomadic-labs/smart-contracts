{ parameter
    (or (or (or (pair %addLiquidity
                   (address %owner)
                   (pair (nat %minLqtMinted) (pair (nat %maxTokensDeposited) (timestamp %deadline))))
                (pair %cashToToken (address %to) (pair (nat %minTokensBought) (timestamp %deadline))))
            (or (pair %removeLiquidity
                   (address %to)
                   (pair (nat %lqtBurned)
                         (pair (nat %minCashWithdrawn) (pair (nat %minTokensWithdrawn) (timestamp %deadline)))))
                (address %setLqtAddress)))
        (or (or (pair %tokenToCash
                   (address %to)
                   (pair (nat %tokensSold) (pair (nat %minCashBought) (timestamp %deadline))))
                (pair %tokenToToken
                   (address %outputCfmmContract)
                   (pair (nat %minTokensBought)
                         (pair (address %to) (pair (nat %tokensSold) (timestamp %deadline))))))
            (or (unit %updatePools) (nat %updateTokenPoolInternal)))) ;
  storage
    (pair (nat %tokenPool)
          (pair (nat %cashPool)
                (pair (nat %lqtTotal)
                      (pair (nat %pendingPoolUpdates)
                            (pair (address %tokenAddress)
                                  (pair (address %lqtAddress)
                                        (pair (timestamp %lastOracleUpdate) (address %consumerEntrypoint)))))))) ;
  code { LAMBDA
           (pair (list operation)
                 (pair nat
                       (pair nat (pair nat (pair nat (pair address (pair address (pair timestamp address))))))))
           (pair (list operation)
                 (pair nat
                       (pair nat (pair nat (pair nat (pair address (pair address (pair timestamp address))))))))
           { UNPAIR ;
             NOW ;
             DUP 3 ;
             GET 13 ;
             COMPARE ;
             EQ ;
             IF { PAIR }
                { SWAP ;
                  DUP ;
                  DUG 2 ;
                  GET 14 ;
                  CONTRACT (pair nat nat) ;
                  IF_NONE { PUSH nat 35 ; FAILWITH } {} ;
                  DUP 3 ;
                  NOW ;
                  UPDATE 13 ;
                  DUG 2 ;
                  PUSH mutez 0 ;
                  DUP 5 ;
                  CAR ;
                  DIG 5 ;
                  GET 3 ;
                  PAIR ;
                  TRANSFER_TOKENS ;
                  CONS ;
                  PAIR } } ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     UNPAIR 4 ;
                     AMOUNT ;
                     PUSH mutez 1 ;
                     SWAP ;
                     EDIV ;
                     IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                     CAR ;
                     PUSH nat 0 ;
                     DUP 7 ;
                     GET 7 ;
                     COMPARE ;
                     GT ;
                     IF { DROP 6 ; PUSH nat 2 ; FAILWITH }
                        { DIG 4 ;
                          NOW ;
                          COMPARE ;
                          GE ;
                          IF { DROP 5 ; PUSH nat 3 ; FAILWITH }
                             { DUP 5 ;
                               GET 3 ;
                               DUP ;
                               DUP 7 ;
                               GET 5 ;
                               DUP 4 ;
                               MUL ;
                               EDIV ;
                               IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                               CAR ;
                               DUP 7 ;
                               CAR ;
                               DUP 4 ;
                               MUL ;
                               DIG 2 ;
                               INT ;
                               SWAP ;
                               NEG ;
                               EDIV ;
                               IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                               CAR ;
                               ABS ;
                               DIG 5 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               COMPARE ;
                               GT ;
                               IF { DROP 6 ; PUSH nat 4 ; FAILWITH }
                                  { DIG 4 ;
                                    DUP 3 ;
                                    COMPARE ;
                                    LT ;
                                    IF { DROP 5 ; PUSH nat 5 ; FAILWITH }
                                       { DUP 5 ;
                                         DUP 3 ;
                                         DUP 7 ;
                                         GET 5 ;
                                         ADD ;
                                         UPDATE 5 ;
                                         SWAP ;
                                         DUP ;
                                         DUG 2 ;
                                         DUP 7 ;
                                         CAR ;
                                         ADD ;
                                         UPDATE 1 ;
                                         DIG 3 ;
                                         DIG 5 ;
                                         GET 3 ;
                                         ADD ;
                                         UPDATE 3 ;
                                         DUP ;
                                         SENDER ;
                                         SELF_ADDRESS ;
                                         DIG 4 ;
                                         SWAP ;
                                         PAIR ;
                                         SWAP ;
                                         PAIR ;
                                         SWAP ;
                                         PAIR ;
                                         UNPAIR 4 ;
                                         GET 9 ;
                                         CONTRACT %transfer (pair address (pair address nat)) ;
                                         IF_NONE { PUSH nat 0 ; FAILWITH } {} ;
                                         PUSH mutez 0 ;
                                         DIG 4 ;
                                         DIG 4 ;
                                         PAIR ;
                                         DIG 3 ;
                                         PAIR ;
                                         TRANSFER_TOKENS ;
                                         SWAP ;
                                         DUP ;
                                         DUG 2 ;
                                         GET 11 ;
                                         CONTRACT %mintOrBurn (pair (int %quantity) (address %target)) ;
                                         IF_NONE { PUSH nat 12 ; FAILWITH } {} ;
                                         PUSH mutez 0 ;
                                         DIG 4 ;
                                         INT ;
                                         DIG 5 ;
                                         SWAP ;
                                         PAIR ;
                                         TRANSFER_TOKENS ;
                                         DIG 2 ;
                                         NIL operation ;
                                         DIG 2 ;
                                         CONS ;
                                         DIG 2 ;
                                         CONS ;
                                         PAIR } } } } }
                   { UNPAIR 3 ;
                     AMOUNT ;
                     PUSH mutez 1 ;
                     SWAP ;
                     EDIV ;
                     IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                     CAR ;
                     PUSH nat 0 ;
                     DUP 6 ;
                     GET 7 ;
                     COMPARE ;
                     GT ;
                     IF { DROP 5 ; PUSH nat 2 ; FAILWITH }
                        { DIG 3 ;
                          NOW ;
                          COMPARE ;
                          GE ;
                          IF { DROP 4 ; PUSH nat 3 ; FAILWITH }
                             { PUSH nat 9995 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               MUL ;
                               PUSH nat 10000 ;
                               DUP 6 ;
                               GET 3 ;
                               MUL ;
                               ADD ;
                               DUP 5 ;
                               CAR ;
                               PUSH nat 9995 ;
                               DUP 4 ;
                               MUL ;
                               MUL ;
                               EDIV ;
                               IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                               CAR ;
                               DIG 3 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               COMPARE ;
                               LT ;
                               IF { DROP ; PUSH nat 18 ; FAILWITH } {} ;
                               DUP ;
                               DUP 5 ;
                               CAR ;
                               SUB ;
                               ISNAT ;
                               IF_NONE { PUSH nat 19 ; FAILWITH } {} ;
                               DUP 5 ;
                               DIG 3 ;
                               DIG 5 ;
                               GET 3 ;
                               ADD ;
                               UPDATE 3 ;
                               SWAP ;
                               UPDATE 1 ;
                               DUP ;
                               SELF_ADDRESS ;
                               DIG 4 ;
                               DIG 4 ;
                               SWAP ;
                               PAIR ;
                               SWAP ;
                               PAIR ;
                               SWAP ;
                               PAIR ;
                               UNPAIR 4 ;
                               GET 9 ;
                               CONTRACT %transfer (pair address (pair address nat)) ;
                               IF_NONE { PUSH nat 0 ; FAILWITH } {} ;
                               PUSH mutez 0 ;
                               DIG 4 ;
                               DIG 4 ;
                               PAIR ;
                               DIG 3 ;
                               PAIR ;
                               TRANSFER_TOKENS ;
                               SWAP ;
                               NIL operation ;
                               DIG 2 ;
                               CONS ;
                               PAIR } } ;
                     EXEC } }
               { DIG 2 ;
                 DROP ;
                 IF_LEFT
                   { UNPAIR 5 ;
                     PUSH nat 0 ;
                     DUP 7 ;
                     GET 7 ;
                     COMPARE ;
                     GT ;
                     IF { DROP 6 ; PUSH nat 2 ; FAILWITH }
                        { DIG 4 ;
                          NOW ;
                          COMPARE ;
                          GE ;
                          IF { DROP 5 ; PUSH nat 3 ; FAILWITH }
                             { PUSH mutez 0 ;
                               AMOUNT ;
                               COMPARE ;
                               GT ;
                               IF { DROP 5 ; PUSH nat 10 ; FAILWITH }
                                  { DUP 5 ;
                                    GET 5 ;
                                    DUP 6 ;
                                    GET 3 ;
                                    DUP 4 ;
                                    MUL ;
                                    EDIV ;
                                    IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                    CAR ;
                                    DUP 6 ;
                                    GET 5 ;
                                    DUP 7 ;
                                    CAR ;
                                    DUP 5 ;
                                    MUL ;
                                    EDIV ;
                                    IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                    CAR ;
                                    DIG 4 ;
                                    DUP 3 ;
                                    COMPARE ;
                                    LT ;
                                    IF { DROP 6 ; PUSH nat 11 ; FAILWITH }
                                       { DIG 4 ;
                                         SWAP ;
                                         DUP ;
                                         DUG 2 ;
                                         COMPARE ;
                                         LT ;
                                         IF { DROP 5 ; PUSH nat 13 ; FAILWITH }
                                            { DUP 4 ;
                                              DUP 6 ;
                                              GET 5 ;
                                              SUB ;
                                              ISNAT ;
                                              IF_NONE { PUSH nat 14 ; FAILWITH } {} ;
                                              SWAP ;
                                              DUP ;
                                              DUG 2 ;
                                              DUP 7 ;
                                              CAR ;
                                              SUB ;
                                              ISNAT ;
                                              IF_NONE { PUSH nat 15 ; FAILWITH } {} ;
                                              DUP 4 ;
                                              DUP 8 ;
                                              GET 3 ;
                                              SUB ;
                                              ISNAT ;
                                              IF_NONE { PUSH nat 16 ; FAILWITH } {} ;
                                              DUP 8 ;
                                              SENDER ;
                                              DIG 8 ;
                                              PUSH int 0 ;
                                              SUB ;
                                              SWAP ;
                                              PAIR ;
                                              SWAP ;
                                              PAIR ;
                                              UNPAIR 3 ;
                                              GET 11 ;
                                              CONTRACT %mintOrBurn (pair (int %quantity) (address %target)) ;
                                              IF_NONE { PUSH nat 12 ; FAILWITH } {} ;
                                              PUSH mutez 0 ;
                                              DIG 3 ;
                                              DIG 3 ;
                                              SWAP ;
                                              PAIR ;
                                              TRANSFER_TOKENS ;
                                              DUP 8 ;
                                              SELF_ADDRESS ;
                                              SENDER ;
                                              DIG 7 ;
                                              SWAP ;
                                              PAIR ;
                                              SWAP ;
                                              PAIR ;
                                              SWAP ;
                                              PAIR ;
                                              UNPAIR 4 ;
                                              GET 9 ;
                                              CONTRACT %transfer (pair address (pair address nat)) ;
                                              IF_NONE { PUSH nat 0 ; FAILWITH } {} ;
                                              PUSH mutez 0 ;
                                              DIG 4 ;
                                              DIG 4 ;
                                              PAIR ;
                                              DIG 3 ;
                                              PAIR ;
                                              TRANSFER_TOKENS ;
                                              DIG 6 ;
                                              CONTRACT unit ;
                                              IF_NONE { PUSH nat 9 ; FAILWITH } {} ;
                                              PUSH mutez 1 ;
                                              DIG 7 ;
                                              MUL ;
                                              PUSH unit Unit ;
                                              TRANSFER_TOKENS ;
                                              DIG 6 ;
                                              DIG 4 ;
                                              UPDATE 3 ;
                                              DIG 5 ;
                                              UPDATE 5 ;
                                              DIG 4 ;
                                              UPDATE 1 ;
                                              NIL operation ;
                                              DIG 2 ;
                                              CONS ;
                                              DIG 2 ;
                                              CONS ;
                                              DIG 2 ;
                                              CONS ;
                                              PAIR } } } } } }
                   { PUSH nat 0 ;
                     DUP 3 ;
                     GET 7 ;
                     COMPARE ;
                     GT ;
                     IF { DROP 2 ; PUSH nat 2 ; FAILWITH }
                        { PUSH mutez 0 ;
                          AMOUNT ;
                          COMPARE ;
                          GT ;
                          IF { DROP 2 ; PUSH nat 10 ; FAILWITH }
                             { PUSH address "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU" ;
                               DUP 3 ;
                               GET 11 ;
                               COMPARE ;
                               NEQ ;
                               IF { DROP 2 ; PUSH nat 24 ; FAILWITH }
                                  { UPDATE 11 ; NIL operation ; PAIR } } } } } }
           { IF_LEFT
               { IF_LEFT
                   { UNPAIR 4 ;
                     PUSH nat 0 ;
                     DUP 6 ;
                     GET 7 ;
                     COMPARE ;
                     GT ;
                     IF { DROP 5 ; PUSH nat 2 ; FAILWITH }
                        { DIG 3 ;
                          NOW ;
                          COMPARE ;
                          GE ;
                          IF { DROP 4 ; PUSH nat 3 ; FAILWITH }
                             { PUSH mutez 0 ;
                               AMOUNT ;
                               COMPARE ;
                               GT ;
                               IF { DROP 4 ; PUSH nat 10 ; FAILWITH }
                                  { PUSH nat 9995 ;
                                    DUP 3 ;
                                    MUL ;
                                    PUSH nat 10000 ;
                                    DUP 6 ;
                                    CAR ;
                                    MUL ;
                                    ADD ;
                                    DUP 5 ;
                                    GET 3 ;
                                    PUSH nat 9995 ;
                                    DUP 5 ;
                                    MUL ;
                                    MUL ;
                                    EDIV ;
                                    IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                    CAR ;
                                    DIG 3 ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    COMPARE ;
                                    LT ;
                                    IF { DROP ; PUSH nat 8 ; FAILWITH } {} ;
                                    DUP 4 ;
                                    SENDER ;
                                    SELF_ADDRESS ;
                                    DUP 6 ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    UNPAIR 4 ;
                                    GET 9 ;
                                    CONTRACT %transfer (pair address (pair address nat)) ;
                                    IF_NONE { PUSH nat 0 ; FAILWITH } {} ;
                                    PUSH mutez 0 ;
                                    DIG 4 ;
                                    DIG 4 ;
                                    PAIR ;
                                    DIG 3 ;
                                    PAIR ;
                                    TRANSFER_TOKENS ;
                                    DIG 2 ;
                                    CONTRACT unit ;
                                    IF_NONE { PUSH nat 9 ; FAILWITH } {} ;
                                    PUSH mutez 1 ;
                                    DUP 4 ;
                                    MUL ;
                                    PUSH unit Unit ;
                                    TRANSFER_TOKENS ;
                                    DIG 2 ;
                                    DUP 5 ;
                                    GET 3 ;
                                    SUB ;
                                    ISNAT ;
                                    IF_NONE { PUSH nat 1 ; FAILWITH } {} ;
                                    DUP 5 ;
                                    DIG 4 ;
                                    DIG 5 ;
                                    CAR ;
                                    ADD ;
                                    UPDATE 1 ;
                                    SWAP ;
                                    UPDATE 3 ;
                                    NIL operation ;
                                    DIG 2 ;
                                    CONS ;
                                    DIG 2 ;
                                    CONS ;
                                    PAIR } } } ;
                     EXEC }
                   { UNPAIR 5 ;
                     CONTRACT %cashToToken
                       (pair (address %to) (pair (nat %minTokensBought) (timestamp %deadline))) ;
                     IF_NONE { PUSH nat 31 ; FAILWITH } {} ;
                     PUSH nat 0 ;
                     DUP 7 ;
                     GET 7 ;
                     COMPARE ;
                     GT ;
                     IF { DROP 6 ; PUSH nat 2 ; FAILWITH }
                        { PUSH mutez 0 ;
                          AMOUNT ;
                          COMPARE ;
                          GT ;
                          IF { DROP 6 ; PUSH nat 10 ; FAILWITH }
                             { DUP 5 ;
                               NOW ;
                               COMPARE ;
                               GE ;
                               IF { DROP 6 ; PUSH nat 3 ; FAILWITH }
                                  { PUSH nat 9995 ;
                                    DUP 5 ;
                                    MUL ;
                                    PUSH nat 10000 ;
                                    DUP 8 ;
                                    CAR ;
                                    MUL ;
                                    ADD ;
                                    DUP 7 ;
                                    GET 3 ;
                                    PUSH nat 9995 ;
                                    DUP 7 ;
                                    MUL ;
                                    MUL ;
                                    EDIV ;
                                    IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                    CAR ;
                                    DUP ;
                                    DUP 8 ;
                                    GET 3 ;
                                    SUB ;
                                    ISNAT ;
                                    IF_NONE { PUSH nat 17 ; FAILWITH } {} ;
                                    DUP 8 ;
                                    DUP 7 ;
                                    DIG 9 ;
                                    CAR ;
                                    ADD ;
                                    UPDATE 1 ;
                                    SWAP ;
                                    UPDATE 3 ;
                                    DIG 2 ;
                                    PUSH mutez 1 ;
                                    DIG 3 ;
                                    MUL ;
                                    DIG 4 ;
                                    DIG 4 ;
                                    DIG 6 ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    TRANSFER_TOKENS ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    SENDER ;
                                    SELF_ADDRESS ;
                                    DIG 5 ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    UNPAIR 4 ;
                                    GET 9 ;
                                    CONTRACT %transfer (pair address (pair address nat)) ;
                                    IF_NONE { PUSH nat 0 ; FAILWITH } {} ;
                                    PUSH mutez 0 ;
                                    DIG 4 ;
                                    DIG 4 ;
                                    PAIR ;
                                    DIG 3 ;
                                    PAIR ;
                                    TRANSFER_TOKENS ;
                                    DIG 2 ;
                                    NIL operation ;
                                    DIG 2 ;
                                    CONS ;
                                    DIG 2 ;
                                    CONS ;
                                    PAIR } } } ;
                     EXEC } }
               { DIG 2 ;
                 DROP ;
                 IF_LEFT
                   { DROP ;
                     SOURCE ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP ; PUSH nat 25 ; FAILWITH }
                        { PUSH mutez 0 ;
                          AMOUNT ;
                          COMPARE ;
                          GT ;
                          IF { DROP ; PUSH nat 10 ; FAILWITH }
                             { SELF %updateTokenPoolInternal ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               GET 9 ;
                               CONTRACT %getBalance (pair address (contract nat)) ;
                               IF_NONE { PUSH nat 28 ; FAILWITH } {} ;
                               PUSH mutez 0 ;
                               DIG 2 ;
                               SELF_ADDRESS ;
                               PAIR ;
                               TRANSFER_TOKENS ;
                               SWAP ;
                               PUSH nat 1 ;
                               UPDATE 7 ;
                               NIL operation ;
                               DIG 2 ;
                               CONS ;
                               PAIR } } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 9 ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     PUSH nat 0 ;
                     DUP 4 ;
                     GET 7 ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     IF { DROP 2 ; PUSH nat 29 ; FAILWITH }
                        { PUSH nat 1 ;
                          DUP 3 ;
                          GET 7 ;
                          SUB ;
                          ABS ;
                          DUG 2 ;
                          UPDATE 1 ;
                          SWAP ;
                          UPDATE 7 ;
                          NIL operation ;
                          PAIR } } } } } }
