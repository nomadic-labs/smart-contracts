{ storage
    (pair (pair (address %administrator) (nat %last_token_id))
          (pair (big_map %ledger nat address)
                (pair (big_map %metadata string bytes)
                      (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (list %burn (pair (address %from_) (pair (nat %token_id) (nat %amount)))))
            (or (list %mint (pair (address %to_) (map %metadata string bytes)))
                (address %set_administrator)))
        (or (or (big_map %set_metadata string bytes)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
            (or (map %updateTokenMetadata nat bytes)
                (or (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                    (pair %withdraw_mutez (mutez %amount) (address %destination)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { NIL operation ;
                     DUP ;
                     DUP 3 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DUP 5 ;
                     CAR ;
                     MAP { DUP 7 ;
                           GET 6 ;
                           DUP 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP ;
                           CAR ;
                           DUP 8 ;
                           GET 3 ;
                           DUP 3 ;
                           CDR ;
                           GET ;
                           IF_NONE { PUSH int 370 ; FAILWITH } {} ;
                           COMPARE ;
                           EQ ;
                           IF { PUSH nat 1 } { PUSH nat 0 } ;
                           SWAP ;
                           PAIR } ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { DUP ;
                     ITER { DUP 3 ;
                            GET 6 ;
                            DUP 2 ;
                            GET 3 ;
                            MEM ;
                            IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                            PUSH nat 0 ;
                            DUP 2 ;
                            GET 4 ;
                            COMPARE ;
                            GT ;
                            IF { PUSH nat 1 ;
                                 DUP 2 ;
                                 GET 4 ;
                                 COMPARE ;
                                 EQ ;
                                 IF { DUP ;
                                      CAR ;
                                      DUP 4 ;
                                      GET 3 ;
                                      DUP 3 ;
                                      GET 3 ;
                                      GET ;
                                      IF_NONE { PUSH int 53 ; FAILWITH } {} ;
                                      COMPARE ;
                                      EQ }
                                    { PUSH bool False } ;
                                 IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                 DIG 2 ;
                                 DUP ;
                                 GET 3 ;
                                 NONE address ;
                                 DUP 4 ;
                                 GET 3 ;
                                 UPDATE ;
                                 UPDATE 3 ;
                                 DUP ;
                                 GET 6 ;
                                 NONE (pair nat (map string bytes)) ;
                                 DIG 3 ;
                                 GET 3 ;
                                 UPDATE ;
                                 UPDATE 6 ;
                                 SWAP }
                               { DROP } } ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { DUP 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP 3 ;
                            CAR ;
                            CDR ;
                            DIG 3 ;
                            DUP ;
                            GET 6 ;
                            DUP 4 ;
                            CDR ;
                            DUP 4 ;
                            PAIR ;
                            SOME ;
                            DUP 4 ;
                            UPDATE ;
                            UPDATE 6 ;
                            DUP ;
                            GET 3 ;
                            DIG 3 ;
                            CAR ;
                            SOME ;
                            DIG 3 ;
                            UPDATE ;
                            UPDATE 3 ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            PUSH nat 1 ;
                            ADD ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            SWAP } ;
                     DROP }
                   { DUP 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 5 }
                   { PUSH string "FA2_TX_DENIED" ; FAILWITH } ;
                 NIL operation }
               { IF_LEFT
                   { DUP 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { CAR ;
                            DIG 2 ;
                            DUP ;
                            GET 6 ;
                            EMPTY_MAP string bytes ;
                            DUP 5 ;
                            DUP 5 ;
                            GET ;
                            IF_NONE { PUSH int 25 ; FAILWITH } {} ;
                            SOME ;
                            PUSH string "" ;
                            UPDATE ;
                            DUP 4 ;
                            PAIR ;
                            SOME ;
                            DIG 3 ;
                            UPDATE ;
                            UPDATE 6 ;
                            SWAP } ;
                     DROP ;
                     NIL operation }
                   { IF_LEFT
                       { PUSH string "FA2_OPERATORS_UNSUPPORTED" ; FAILWITH }
                       { DUP 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         NIL operation ;
                         DUP 2 ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 546 ; FAILWITH } {} ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS } } } } ;
         PAIR } ;
  view "get_balance_of"
       (list (pair (address %owner) (nat %token_id)))
       (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))
       { UNPAIR ;
         DUP ;
         MAP { DUP 3 ;
               GET 6 ;
               DUP 2 ;
               CDR ;
               MEM ;
               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
               DUP ;
               CAR ;
               DUP 4 ;
               GET 3 ;
               DUP 3 ;
               CDR ;
               GET ;
               IF_NONE { PUSH int 370 ; FAILWITH } {} ;
               COMPARE ;
               EQ ;
               IF { PUSH nat 1 } { PUSH nat 0 } ;
               SWAP ;
               PAIR } ;
         SWAP ;
         DROP ;
         SWAP ;
         DROP } }
