{ parameter
    (or (or (or (list %add_artifacts
                   (pair (map %ints string int)
                         (pair (bytes %ipfs_uri) (pair (map %price_resources nat nat) (mutez %price_xtz)))))
                (nat %mint))
            (or (list %remove_artifacts nat) (or (address %set_admin) (address %set_minter))))
        (or (or (list %update_artifacts
                   (pair (nat %artifact_id)
                         (pair (map %ints string int) (pair (map %price_resources nat nat) (mutez %price_xtz)))))
                (pair %update_collector (address %resources) (address %xtz)))
            (or (address %update_resource_registry)
                (or (address %update_revenue_pool) (nat %update_revenue_share))))) ;
  storage
    (pair (address %admin)
          (pair (nat %artifact_id)
                (pair (big_map %artifacts
                         nat
                         (pair (map %ints string int)
                               (pair (bytes %ipfs_uri) (pair (map %price_resources nat nat) (mutez %price_xtz)))))
                      (pair (address %collector_resources)
                            (pair (address %collector_xtz)
                                  (pair (big_map %metadata string bytes)
                                        (pair (address %minter)
                                              (pair (address %resource_registry)
                                                    (pair (address %revenue_pool) (nat %revenue_share)))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DUP 3 ;
                            DUP ;
                            GET 5 ;
                            DIG 2 ;
                            DUP ;
                            GET 6 ;
                            SWAP ;
                            DUP ;
                            GET 5 ;
                            SWAP ;
                            DUP ;
                            GET 3 ;
                            SWAP ;
                            DUP ;
                            DUG 6 ;
                            CAR ;
                            PAIR 4 ;
                            DIG 3 ;
                            DROP ;
                            SOME ;
                            DIG 4 ;
                            GET 3 ;
                            UPDATE ;
                            UPDATE 5 ;
                            DUP ;
                            GET 3 ;
                            PUSH nat 1 ;
                            ADD ;
                            UPDATE 3 ;
                            SWAP } ;
                     DROP ;
                     NIL operation }
                   { AMOUNT ;
                     DUP 3 ;
                     GET 5 ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 130 ; FAILWITH } {} ;
                     GET 6 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "ARTIFACT_XTZ_PRICE_MISMATCH" ; FAILWITH } ;
                     NIL operation ;
                     DUP ;
                     DUP 4 ;
                     GET 13 ;
                     CONTRACT %mint
                       (pair (address %address)
                             (pair (pair %attribute
                                      (string %category)
                                      (pair (map %ints string int)
                                            (pair (map %sets string (set nat)) (map %strings string string))))
                                   (pair (nat %editions) (bytes %metadataUri)))) ;
                     IF_NONE { PUSH int 149 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     DUP 6 ;
                     GET 5 ;
                     DUP 6 ;
                     GET ;
                     IF_NONE { PUSH int 141 ; FAILWITH } {} ;
                     GET 3 ;
                     PUSH nat 1 ;
                     EMPTY_MAP string string ;
                     EMPTY_MAP string (set nat) ;
                     DUP 10 ;
                     GET 5 ;
                     DUP 10 ;
                     GET ;
                     IF_NONE { PUSH int 144 ; FAILWITH } {} ;
                     CAR ;
                     PUSH string "artifact" ;
                     PAIR 4 ;
                     SENDER ;
                     PAIR 4 ;
                     DIG 4 ;
                     DROP ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PUSH mutez 0 ;
                     AMOUNT ;
                     COMPARE ;
                     GT ;
                     IF { PUSH nat 10000 ;
                          DUP 4 ;
                          GET 18 ;
                          AMOUNT ;
                          MUL ;
                          EDIV ;
                          IF_NONE { PUSH int 154 ; FAILWITH } {} ;
                          CAR ;
                          DUP ;
                          PUSH mutez 0 ;
                          COMPARE ;
                          LT ;
                          IF { SWAP ;
                               DUP 4 ;
                               GET 17 ;
                               CONTRACT unit ;
                               IF_NONE { PUSH int 161 ; FAILWITH } {} ;
                               DUP 3 ;
                               UNIT ;
                               TRANSFER_TOKENS ;
                               CONS ;
                               SWAP }
                             {} ;
                          SWAP ;
                          DUP 4 ;
                          GET 9 ;
                          CONTRACT unit ;
                          IF_NONE { PUSH int 163 ; FAILWITH } {} ;
                          DIG 2 ;
                          AMOUNT ;
                          SUB_MUTEZ ;
                          IF_NONE { PUSH int 163 ; FAILWITH } {} ;
                          UNIT ;
                          TRANSFER_TOKENS ;
                          CONS }
                        {} ;
                     NIL (pair address (pair nat nat)) ;
                     DUP 4 ;
                     GET 5 ;
                     DUP 4 ;
                     GET ;
                     IF_NONE { PUSH int 167 ; FAILWITH } {} ;
                     GET 5 ;
                     ITER { CAR ;
                            SWAP ;
                            DUP 5 ;
                            GET 5 ;
                            DUP 5 ;
                            GET ;
                            IF_NONE { PUSH int 171 ; FAILWITH } {} ;
                            GET 5 ;
                            DUP 3 ;
                            GET ;
                            IF_NONE { PUSH int 171 ; FAILWITH } {} ;
                            DIG 2 ;
                            DUP 6 ;
                            GET 7 ;
                            PAIR 3 ;
                            CONS } ;
                     DUP ;
                     SIZE ;
                     PUSH nat 0 ;
                     COMPARE ;
                     LT ;
                     IF { DIG 2 ;
                          DROP ;
                          SWAP ;
                          DUP 3 ;
                          GET 15 ;
                          CONTRACT %transfer
                            (list (pair (address %from_)
                                        (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                          IF_NONE { PUSH int 175 ; FAILWITH } {} ;
                          PUSH mutez 0 ;
                          NIL (pair address (list (pair address (pair nat nat)))) ;
                          DIG 4 ;
                          SENDER ;
                          PAIR ;
                          CONS ;
                          TRANSFER_TOKENS ;
                          CONS }
                        { DROP ; SWAP ; DROP } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DIG 2 ;
                            DUP ;
                            GET 5 ;
                            NONE (pair (map string int) (pair bytes (pair (map nat nat) mutez))) ;
                            DIG 3 ;
                            UPDATE ;
                            UPDATE 5 ;
                            SWAP } ;
                     DROP }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 1 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 13 } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DIG 2 ;
                            DUP ;
                            GET 5 ;
                            DUP ;
                            DUP 4 ;
                            CAR ;
                            DUP ;
                            DUG 2 ;
                            GET ;
                            IF_NONE { PUSH int 111 ; FAILWITH } {} ;
                            DUP 5 ;
                            GET 6 ;
                            UPDATE 6 ;
                            SOME ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 5 ;
                            DUP ;
                            GET 5 ;
                            DUP ;
                            DUP 4 ;
                            CAR ;
                            DUP ;
                            DUG 2 ;
                            GET ;
                            IF_NONE { PUSH int 112 ; FAILWITH } {} ;
                            DUP 5 ;
                            GET 3 ;
                            UPDATE 1 ;
                            SOME ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 5 ;
                            DUP ;
                            GET 5 ;
                            DUP ;
                            DUP 4 ;
                            CAR ;
                            DUP ;
                            DUG 2 ;
                            GET ;
                            IF_NONE { PUSH int 113 ; FAILWITH } {} ;
                            DIG 4 ;
                            GET 5 ;
                            UPDATE 5 ;
                            SOME ;
                            SWAP ;
                            UPDATE ;
                            UPDATE 5 ;
                            SWAP } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     UPDATE 9 ;
                     SWAP ;
                     CAR ;
                     UPDATE 7 } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 15 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 17 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         PUSH nat 10000 ;
                         SWAP ;
                         COMPARE ;
                         LE ;
                         IF {} { PUSH string "INVALID_SHARE" ; FAILWITH } ;
                         UPDATE 18 } } } ;
             NIL operation } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
