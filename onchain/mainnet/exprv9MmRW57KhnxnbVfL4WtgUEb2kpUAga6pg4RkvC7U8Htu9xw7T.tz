{ parameter
    (or (or (or (unit %deposit) (unit %harvest))
            (or (pair %rescue address nat) (address %updateAdmin)))
        (or (pair %updateIfo
               (pair %offeringToken
                  (address %address)
                  (nat %tokenId)
                  (or %tokenType (unit %fa1) (unit %fa2)))
               (mutez %raisingGoal)
               (nat %offeringSupply)
               (mutez %minAmount)
               (mutez %maxAmount)
               (timestamp %startTime)
               (timestamp %endTime)
               (timestamp %harvestTime)
               (int %harvestDuration))
            (address %withdraw))) ;
  storage
    (pair (big_map %metadata string bytes)
          (pair %ifo
             (pair %offeringToken
                (address %address)
                (nat %tokenId)
                (or %tokenType (unit %fa1) (unit %fa2)))
             (mutez %raisingGoal)
             (mutez %totalRaised)
             (nat %offeringSupply)
             (mutez %minAmount)
             (mutez %maxAmount)
             (timestamp %startTime)
             (timestamp %endTime)
             (timestamp %harvestTime)
             (int %harvestDuration))
          (big_map %ledger
             address
             (pair (mutez %amount) (mutez %amountHarvested) (timestamp %lastHarvest)))
          (address %admin)) ;
  code { LAMBDA
           (pair (pair address address) (pair address nat (or unit unit)) nat)
           operation
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DUP ;
             GET 4 ;
             IF_LEFT
               { DROP ;
                 CAR ;
                 CONTRACT %transfer (pair (address %from) (address %to) (nat %value)) ;
                 IF_NONE { PUSH string "CANNOT_INVOKE_FA1_TRANSFER" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 DIG 4 ;
                 DIG 4 ;
                 PAIR 3 ;
                 TRANSFER_TOKENS }
               { DROP ;
                 DUP ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))) ;
                 IF_NONE { PUSH string "CANNOT_INVOKE_FA2_TRANSFER" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 NIL (pair address (list (pair address nat nat))) ;
                 NIL (pair address nat nat) ;
                 DIG 5 ;
                 DIG 5 ;
                 GET 3 ;
                 DIG 7 ;
                 PAIR 3 ;
                 CONS ;
                 DIG 4 ;
                 PAIR ;
                 CONS ;
                 TRANSFER_TOKENS } } ;
         LAMBDA
           mutez
           nat
           { PUSH mutez 1 ;
             SWAP ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR } ;
         LAMBDA
           (pair address mutez)
           operation
           { UNPAIR ;
             CONTRACT unit ;
             IF_NONE { PUSH string "INVALID_TEZ_DESTINATION" ; FAILWITH } {} ;
             SWAP ;
             UNIT ;
             TRANSFER_TOKENS } ;
         NIL operation ;
         PUSH nat 1000000000000000000 ;
         LAMBDA
           (pair (big_map string bytes)
                 (pair (pair address nat (or unit unit))
                       mutez
                       mutez
                       nat
                       mutez
                       mutez
                       timestamp
                       timestamp
                       timestamp
                       int)
                 (big_map address (pair mutez mutez timestamp))
                 address)
           unit
           { GET 6 ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "INVALID_ACCESS" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair address
                 (big_map string bytes)
                 (pair (pair address nat (or unit unit))
                       mutez
                       mutez
                       nat
                       mutez
                       mutez
                       timestamp
                       timestamp
                       timestamp
                       int)
                 (big_map address (pair mutez mutez timestamp))
                 address)
           (pair mutez mutez timestamp)
           { UNPAIR ;
             DUP 2 ;
             GET 3 ;
             GET 17 ;
             PUSH mutez 0 ;
             PUSH mutez 0 ;
             PAIR 3 ;
             DIG 2 ;
             GET 5 ;
             DIG 2 ;
             GET ;
             IF_NONE {} { SWAP ; DROP } } ;
         LAMBDA
           (pair (pair nat (lambda mutez nat))
                 (pair mutez
                       (big_map string bytes)
                       (pair (pair address nat (or unit unit))
                             mutez
                             mutez
                             nat
                             mutez
                             mutez
                             timestamp
                             timestamp
                             timestamp
                             int)
                       (big_map address (pair mutez mutez timestamp))
                       address))
           nat
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DUP 2 ;
             GET 3 ;
             GET 3 ;
             DIG 2 ;
             GET 3 ;
             GET 5 ;
             DUP 2 ;
             DUP 2 ;
             COMPARE ;
             GT ;
             IF { SWAP ; DROP } { DROP } ;
             DUP 4 ;
             SWAP ;
             EXEC ;
             DUG 2 ;
             DIG 3 ;
             SWAP ;
             EXEC ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR } ;
         DUP 7 ;
         DUP 5 ;
         PAIR ;
         APPLY ;
         LAMBDA
           (pair (pair (lambda
                          (pair mutez
                                (big_map string bytes)
                                (pair (pair address nat (or unit unit))
                                      mutez
                                      mutez
                                      nat
                                      mutez
                                      mutez
                                      timestamp
                                      timestamp
                                      timestamp
                                      int)
                                (big_map address (pair mutez mutez timestamp))
                                address)
                          nat)
                       nat)
                 (pair mutez
                       (big_map string bytes)
                       (pair (pair address nat (or unit unit))
                             mutez
                             mutez
                             nat
                             mutez
                             mutez
                             timestamp
                             timestamp
                             timestamp
                             int)
                       (big_map address (pair mutez mutez timestamp))
                       address))
           nat
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DUP 2 ;
             SWAP ;
             PAIR ;
             DIG 2 ;
             SWAP ;
             EXEC ;
             DUG 2 ;
             GET 3 ;
             GET 7 ;
             DIG 2 ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR } ;
         DUP 5 ;
         DUP 3 ;
         PAIR ;
         APPLY ;
         DIG 9 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DIG 5 ;
                 DROP ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 5 ;
                     DIG 7 ;
                     DIG 8 ;
                     DIG 9 ;
                     DROP 7 ;
                     DUP ;
                     GET 3 ;
                     GET 13 ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "IFO_NOT_STARTED" ; FAILWITH } {} ;
                     DUP ;
                     GET 3 ;
                     GET 15 ;
                     NOW ;
                     COMPARE ;
                     GT ;
                     IF { PUSH string "IFO_ENDED" ; FAILWITH } {} ;
                     PUSH mutez 1 ;
                     AMOUNT ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "INVALID_AMOUNT" ; FAILWITH } {} ;
                     DUP ;
                     SENDER ;
                     PAIR ;
                     DIG 2 ;
                     SWAP ;
                     EXEC ;
                     DUP 2 ;
                     GET 3 ;
                     GET 9 ;
                     AMOUNT ;
                     DUP 3 ;
                     CAR ;
                     ADD ;
                     COMPARE ;
                     LT ;
                     PUSH mutez 1 ;
                     DUP 4 ;
                     GET 3 ;
                     GET 9 ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "MIN_AMOUNT_NOT_MET" ; FAILWITH } {} ;
                     DUP 2 ;
                     GET 3 ;
                     GET 11 ;
                     AMOUNT ;
                     DUP 3 ;
                     CAR ;
                     ADD ;
                     COMPARE ;
                     GT ;
                     PUSH mutez 1 ;
                     DUP 4 ;
                     GET 3 ;
                     GET 11 ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "MAX_AMOUNT_EXCEEDED" ; FAILWITH } {} ;
                     DUP ;
                     AMOUNT ;
                     DIG 2 ;
                     CAR ;
                     ADD ;
                     UPDATE 1 ;
                     DUP 2 ;
                     DUP 3 ;
                     GET 3 ;
                     AMOUNT ;
                     DIG 4 ;
                     GET 3 ;
                     GET 5 ;
                     ADD ;
                     UPDATE 5 ;
                     UPDATE 3 ;
                     SWAP ;
                     DUP 2 ;
                     DIG 2 ;
                     GET 5 ;
                     DIG 2 ;
                     SENDER ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     UPDATE 5 }
                   { DIG 6 ;
                     DROP 2 ;
                     DUP ;
                     GET 3 ;
                     GET 17 ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     DUP 2 ;
                     GET 3 ;
                     GET 15 ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     OR ;
                     IF { PUSH string "NOT_HARVEST_TIME" ; FAILWITH } {} ;
                     DUP ;
                     SENDER ;
                     PAIR ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     PUSH mutez 1 ;
                     DUP 2 ;
                     CAR ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "NOTHING_TO_HARVEST" ; FAILWITH } {} ;
                     DUP 2 ;
                     GET 3 ;
                     GET 18 ;
                     DUP 3 ;
                     GET 3 ;
                     GET 17 ;
                     ADD ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     IF { DUP 2 ;
                          GET 3 ;
                          GET 18 ;
                          ABS ;
                          DUP 2 ;
                          CAR ;
                          EDIV ;
                          IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                          CAR ;
                          DUP 2 ;
                          GET 4 ;
                          NOW ;
                          SUB ;
                          ABS ;
                          SWAP ;
                          MUL ;
                          DUP 2 ;
                          CAR ;
                          DUP 2 ;
                          DUP 4 ;
                          GET 3 ;
                          ADD ;
                          COMPARE ;
                          GT ;
                          IF { DROP ;
                               DUP ;
                               GET 3 ;
                               DUP 2 ;
                               CAR ;
                               SUB_MUTEZ ;
                               IF_NONE { PUSH string "option is None" ; FAILWITH } {} }
                             {} }
                        { DUP ;
                          CAR ;
                          DUP 2 ;
                          GET 3 ;
                          COMPARE ;
                          GE ;
                          IF { PUSH mutez 0 }
                             { DUP ;
                               GET 3 ;
                               DUP 2 ;
                               CAR ;
                               SUB_MUTEZ ;
                               IF_NONE { PUSH string "option is None" ; FAILWITH } {} } } ;
                     PUSH mutez 1 ;
                     DUP 2 ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "NOTHING_TO_HARVEST" ; FAILWITH } {} ;
                     NIL operation ;
                     DUP 4 ;
                     DUP 3 ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     PUSH nat 0 ;
                     DUP 2 ;
                     COMPARE ;
                     GT ;
                     IF { DUP 5 ;
                          GET 3 ;
                          CAR ;
                          PAIR ;
                          SENDER ;
                          SELF_ADDRESS ;
                          PAIR ;
                          PAIR ;
                          DIG 9 ;
                          SWAP ;
                          EXEC ;
                          CONS }
                        { DIG 9 ; DROP 2 } ;
                     PUSH mutez 0 ;
                     DUP 4 ;
                     GET 3 ;
                     COMPARE ;
                     EQ ;
                     IF { DUP 4 ;
                          DUP 4 ;
                          CAR ;
                          PUSH mutez 0 ;
                          DUP 3 ;
                          GET 3 ;
                          GET 3 ;
                          DUP 4 ;
                          GET 3 ;
                          GET 5 ;
                          COMPARE ;
                          GT ;
                          IF { DROP ;
                               DUP 2 ;
                               DUP 2 ;
                               PAIR ;
                               DIG 7 ;
                               SWAP ;
                               EXEC ;
                               DIG 7 ;
                               DIG 3 ;
                               GET 3 ;
                               GET 3 ;
                               DIG 9 ;
                               SWAP ;
                               EXEC ;
                               DIG 2 ;
                               MUL ;
                               EDIV ;
                               IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                               CAR ;
                               PUSH mutez 1 ;
                               SWAP ;
                               MUL ;
                               SWAP ;
                               SUB_MUTEZ ;
                               IF_NONE { PUSH mutez 0 } {} }
                             { SWAP ; DIG 2 ; DIG 7 ; DIG 8 ; DIG 10 ; DROP 5 } ;
                          PUSH mutez 0 ;
                          DUP 2 ;
                          COMPARE ;
                          GT ;
                          IF { SENDER ; PAIR ; DIG 5 ; SWAP ; EXEC ; CONS } { DIG 5 ; DROP 2 } }
                        { DIG 4 ; DIG 5 ; DIG 6 ; DIG 7 ; DROP 4 } ;
                     DUP 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     GET 3 ;
                     ADD ;
                     UPDATE 3 ;
                     NOW ;
                     UPDATE 4 ;
                     SWAP ;
                     DUP 3 ;
                     DIG 3 ;
                     GET 5 ;
                     DIG 3 ;
                     SENDER ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     UPDATE 5 } }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 6 ;
                 DIG 9 ;
                 DROP 5 ;
                 IF_LEFT
                   { DIG 3 ;
                     DROP ;
                     UNPAIR ;
                     DUP 3 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     NIL operation ;
                     PUSH nat 0 ;
                     DUP 4 ;
                     COMPARE ;
                     GT ;
                     IF { DIG 2 ;
                          DUP 4 ;
                          GET 3 ;
                          CAR ;
                          PAIR ;
                          DUP 3 ;
                          SELF_ADDRESS ;
                          PAIR ;
                          PAIR ;
                          DIG 5 ;
                          SWAP ;
                          EXEC ;
                          CONS }
                        { DIG 2 ; DIG 5 ; DROP 2 } ;
                     BALANCE ;
                     DIG 2 ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     CONS ;
                     SWAP }
                   { DIG 4 ;
                     DIG 5 ;
                     DROP 2 ;
                     SWAP ;
                     DUP ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SWAP ;
                     UPDATE 6 } } }
           { DIG 3 ;
             DIG 4 ;
             DIG 6 ;
             DIG 9 ;
             DROP 4 ;
             IF_LEFT
               { DIG 2 ;
                 DIG 5 ;
                 DIG 6 ;
                 DROP 3 ;
                 SWAP ;
                 DUP ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP ;
                 GET 3 ;
                 DUP 3 ;
                 CAR ;
                 UPDATE 1 ;
                 DUP 3 ;
                 GET 3 ;
                 UPDATE 3 ;
                 DUP 3 ;
                 GET 5 ;
                 UPDATE 7 ;
                 DUP 3 ;
                 GET 7 ;
                 UPDATE 9 ;
                 DUP 3 ;
                 GET 9 ;
                 UPDATE 11 ;
                 DUP 3 ;
                 GET 11 ;
                 UPDATE 13 ;
                 DUP 3 ;
                 GET 13 ;
                 UPDATE 15 ;
                 DUP 3 ;
                 GET 15 ;
                 UPDATE 17 ;
                 DIG 2 ;
                 GET 16 ;
                 UPDATE 18 ;
                 UPDATE 3 }
               { DIG 4 ;
                 DROP ;
                 DUP 2 ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP 2 ;
                 GET 3 ;
                 GET 15 ;
                 NOW ;
                 COMPARE ;
                 LT ;
                 IF { PUSH string "IFO_NOT_ENDED" ; FAILWITH } {} ;
                 NIL operation ;
                 DUP 3 ;
                 GET 3 ;
                 GET 3 ;
                 DUP 4 ;
                 GET 3 ;
                 GET 5 ;
                 COMPARE ;
                 LT ;
                 IF { DUP 3 ;
                      DUP 4 ;
                      GET 3 ;
                      GET 5 ;
                      PAIR ;
                      DIG 4 ;
                      SWAP ;
                      EXEC ;
                      DUP 4 ;
                      GET 3 ;
                      GET 7 ;
                      SUB ;
                      ABS ;
                      PUSH nat 0 ;
                      DUP 2 ;
                      COMPARE ;
                      GT ;
                      IF { DUP 4 ;
                           GET 3 ;
                           CAR ;
                           PAIR ;
                           DUP 3 ;
                           SELF_ADDRESS ;
                           PAIR ;
                           PAIR ;
                           DIG 5 ;
                           SWAP ;
                           EXEC ;
                           CONS }
                         { DIG 5 ; DROP 2 } }
                    { DIG 3 ; DIG 5 ; DROP 2 } ;
                 DUP 3 ;
                 GET 3 ;
                 GET 3 ;
                 DUP 4 ;
                 GET 3 ;
                 GET 5 ;
                 DUP 2 ;
                 DUP 2 ;
                 COMPARE ;
                 LT ;
                 IF { SWAP ; DROP } { DROP } ;
                 DIG 2 ;
                 PAIR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 CONS ;
                 SWAP } } ;
         SWAP ;
         PAIR } }
