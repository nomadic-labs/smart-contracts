{ storage
    (pair (address %admin)
          (pair (set %managers address) (pair (big_map %metadata string bytes) (bool %paused)))) ;
  parameter
    (or (or (pair %create_collection
               (address %admin)
               (pair (set %managers address) (big_map %metadata string bytes)))
            (address %set_admin))
        (or (bool %set_pause) (list %update_managers (or (address %add) (address %remove))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF { PUSH bool True } { SWAP ; DUP ; DUG 2 ; GET 3 ; SENDER ; MEM } ;
                 IF {} { PUSH string "NOT_ADMIN_OR_MANAGER" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 6 ;
                 IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                 NIL operation ;
                 EMPTY_BIG_MAP nat (pair nat (map string bytes)) ;
                 EMPTY_BIG_MAP nat nat ;
                 PUSH bool False ;
                 EMPTY_BIG_MAP (pair address (pair address nat)) unit ;
                 DIG 5 ;
                 DUP ;
                 GET 4 ;
                 SWAP ;
                 DUP ;
                 DUG 7 ;
                 GET 3 ;
                 EMPTY_BIG_MAP (pair address nat) nat ;
                 PUSH nat 0 ;
                 DUP 10 ;
                 CAR ;
                 PAIR 9 ;
                 PUSH mutez 0 ;
                 NONE key_hash ;
                 CREATE_CONTRACT
                   { parameter
                       (or (or (or (pair %balance_of
                                      (list %requests (pair (address %owner) (nat %token_id)))
                                      (contract %callback
                                         (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                                   (list %burn (pair (nat %amount) (pair (address %from_) (nat %token_id)))))
                               (or (list %mint
                                      (pair (nat %amount)
                                            (pair (address %to_) (or %token (nat %existing) (map %new string bytes)))))
                                   (or (address %set_administrator) (big_map %set_metadata string bytes))))
                           (or (or (bool %set_pause)
                                   (or (list %transfer
                                          (pair (address %from_)
                                                (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                                       (list %update_manager (or (address %add) (address %remove)))))
                               (or (list %update_operators
                                      (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                                          (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                                   (or (list %update_token_metadata
                                          (pair (pair %metadata (nat %token_id) (map %token_info string bytes))
                                                (nat %token_id)))
                                       (pair %withdraw_mutez (mutez %amount) (address %destination)))))) ;
                     storage
                       (pair (address %administrator)
                             (pair (nat %last_token_id)
                                   (pair (big_map %ledger (pair address nat) nat)
                                         (pair (set %managers address)
                                               (pair (big_map %metadata string bytes)
                                                     (pair (big_map %operators
                                                              (pair (address %owner) (pair (address %operator) (nat %token_id)))
                                                              unit)
                                                           (pair (bool %paused)
                                                                 (pair (big_map %supply nat nat)
                                                                       (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))))))))) ;
                     code { UNPAIR ;
                            IF_LEFT
                              { IF_LEFT
                                  { IF_LEFT
                                      { NIL operation ;
                                        DUP ;
                                        DUP 3 ;
                                        CDR ;
                                        PUSH mutez 0 ;
                                        DUP 5 ;
                                        CAR ;
                                        MAP { DUP 7 ;
                                              GET 16 ;
                                              SWAP ;
                                              DUP ;
                                              DUG 2 ;
                                              CDR ;
                                              MEM ;
                                              IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                              DUP 7 ;
                                              GET 5 ;
                                              SWAP ;
                                              DUP ;
                                              CDR ;
                                              SWAP ;
                                              DUP ;
                                              DUG 3 ;
                                              CAR ;
                                              PAIR ;
                                              GET ;
                                              IF_NONE { PUSH nat 0 } {} ;
                                              SWAP ;
                                              PAIR } ;
                                        DIG 4 ;
                                        DROP ;
                                        DIG 4 ;
                                        DROP ;
                                        TRANSFER_TOKENS ;
                                        CONS }
                                      { DUP ;
                                        ITER { DUP 3 ;
                                               GET 16 ;
                                               SWAP ;
                                               DUP ;
                                               DUG 2 ;
                                               GET 4 ;
                                               MEM ;
                                               IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                               DUP 3 ;
                                               GET 13 ;
                                               IF { PUSH (pair string string) (Pair "FA2_TX_DENIED" "FA2_PAUSED") ; FAILWITH } {} ;
                                               DUP ;
                                               GET 3 ;
                                               SENDER ;
                                               COMPARE ;
                                               EQ ;
                                               IF { PUSH bool True }
                                                  { DUP 3 ;
                                                    GET 11 ;
                                                    SWAP ;
                                                    DUP ;
                                                    DUG 2 ;
                                                    GET 4 ;
                                                    SENDER ;
                                                    DUP 4 ;
                                                    GET 3 ;
                                                    PAIR 3 ;
                                                    MEM } ;
                                               IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                               DUP 3 ;
                                               DUP ;
                                               GET 5 ;
                                               DUP 3 ;
                                               CAR ;
                                               DIG 5 ;
                                               GET 5 ;
                                               DIG 4 ;
                                               DUP ;
                                               GET 4 ;
                                               SWAP ;
                                               DUP ;
                                               DUG 6 ;
                                               GET 3 ;
                                               PAIR ;
                                               GET ;
                                               IF_NONE { PUSH nat 0 } {} ;
                                               SUB ;
                                               ISNAT ;
                                               IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                               SOME ;
                                               DIG 3 ;
                                               DUP ;
                                               GET 4 ;
                                               SWAP ;
                                               DUP ;
                                               DUG 5 ;
                                               GET 3 ;
                                               PAIR ;
                                               UPDATE ;
                                               UPDATE 5 ;
                                               DUG 2 ;
                                               DUP ;
                                               CAR ;
                                               DUP 4 ;
                                               GET 15 ;
                                               DUP 3 ;
                                               GET 4 ;
                                               GET ;
                                               IF_NONE { PUSH nat 0 } {} ;
                                               SUB ;
                                               ISNAT ;
                                               IF_NONE
                                                 { DIG 2 ;
                                                   DUP ;
                                                   GET 15 ;
                                                   PUSH (option nat) (Some 0) ;
                                                   DIG 3 ;
                                                   GET 4 ;
                                                   UPDATE ;
                                                   UPDATE 15 ;
                                                   SWAP }
                                                 { DIG 3 ;
                                                   DUP ;
                                                   GET 15 ;
                                                   DIG 2 ;
                                                   SOME ;
                                                   DIG 3 ;
                                                   GET 4 ;
                                                   UPDATE ;
                                                   UPDATE 15 ;
                                                   SWAP } } ;
                                        DROP ;
                                        NIL operation } }
                                  { IF_LEFT
                                      { SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        SENDER ;
                                        COMPARE ;
                                        EQ ;
                                        IF { PUSH bool True } { SWAP ; DUP ; DUG 2 ; GET 7 ; SENDER ; MEM } ;
                                        IF {} { PUSH string "FA2_NOT_ADMIN_OR_MANAGER" ; FAILWITH } ;
                                        DUP ;
                                        ITER { DUP ;
                                               GET 4 ;
                                               IF_LEFT
                                                 { DUP 4 ;
                                                   GET 16 ;
                                                   SWAP ;
                                                   DUP ;
                                                   DUG 2 ;
                                                   MEM ;
                                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                                   DIG 3 ;
                                                   DUP ;
                                                   GET 15 ;
                                                   DUP ;
                                                   DUP 4 ;
                                                   DUP ;
                                                   DUG 2 ;
                                                   GET ;
                                                   IF_NONE { PUSH int 666 ; FAILWITH } {} ;
                                                   DUP 6 ;
                                                   CAR ;
                                                   ADD ;
                                                   SOME ;
                                                   SWAP ;
                                                   UPDATE ;
                                                   UPDATE 15 ;
                                                   DUP ;
                                                   DUG 4 ;
                                                   DUP ;
                                                   GET 5 ;
                                                   DUP 4 ;
                                                   CAR ;
                                                   DIG 6 ;
                                                   GET 5 ;
                                                   DUP 5 ;
                                                   DUP 7 ;
                                                   GET 3 ;
                                                   PAIR ;
                                                   GET ;
                                                   IF_NONE { PUSH nat 0 } {} ;
                                                   ADD ;
                                                   SOME ;
                                                   DIG 3 ;
                                                   DIG 4 ;
                                                   GET 3 ;
                                                   PAIR ;
                                                   UPDATE ;
                                                   UPDATE 5 ;
                                                   SWAP }
                                                 { DUP 4 ;
                                                   GET 3 ;
                                                   DIG 4 ;
                                                   DUP ;
                                                   GET 16 ;
                                                   DIG 3 ;
                                                   DUP 4 ;
                                                   PAIR ;
                                                   SOME ;
                                                   DUP 4 ;
                                                   UPDATE ;
                                                   UPDATE 16 ;
                                                   DUP ;
                                                   GET 15 ;
                                                   DUP 4 ;
                                                   CAR ;
                                                   SOME ;
                                                   DUP 4 ;
                                                   UPDATE ;
                                                   UPDATE 15 ;
                                                   DUP ;
                                                   GET 5 ;
                                                   DUP 4 ;
                                                   CAR ;
                                                   SOME ;
                                                   DIG 3 ;
                                                   DIG 4 ;
                                                   GET 3 ;
                                                   PAIR ;
                                                   UPDATE ;
                                                   UPDATE 5 ;
                                                   DUP ;
                                                   GET 3 ;
                                                   PUSH nat 1 ;
                                                   ADD ;
                                                   UPDATE 3 ;
                                                   SWAP } } ;
                                        DROP }
                                      { IF_LEFT
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                                            UPDATE 1 }
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                                            UPDATE 9 } } ;
                                    NIL operation } }
                              { IF_LEFT
                                  { IF_LEFT
                                      { SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        SENDER ;
                                        COMPARE ;
                                        EQ ;
                                        IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                                        UPDATE 13 }
                                      { IF_LEFT
                                          { DUP ;
                                            ITER { DUP ;
                                                   CDR ;
                                                   ITER { DUP 4 ;
                                                          GET 16 ;
                                                          SWAP ;
                                                          DUP ;
                                                          DUG 2 ;
                                                          GET 3 ;
                                                          MEM ;
                                                          IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                                          DUP 4 ;
                                                          GET 13 ;
                                                          IF { PUSH (pair string string) (Pair "FA2_TX_DENIED" "FA2_PAUSED") ; FAILWITH } {} ;
                                                          SWAP ;
                                                          DUP ;
                                                          DUG 2 ;
                                                          CAR ;
                                                          SENDER ;
                                                          COMPARE ;
                                                          EQ ;
                                                          IF { PUSH bool True }
                                                             { DUP 4 ;
                                                               GET 11 ;
                                                               SWAP ;
                                                               DUP ;
                                                               DUG 2 ;
                                                               GET 3 ;
                                                               SENDER ;
                                                               DUP 5 ;
                                                               CAR ;
                                                               PAIR 3 ;
                                                               MEM } ;
                                                          IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                                          DUP 4 ;
                                                          DUP ;
                                                          GET 5 ;
                                                          DUP 3 ;
                                                          GET 4 ;
                                                          DIG 6 ;
                                                          GET 5 ;
                                                          DUP 5 ;
                                                          GET 3 ;
                                                          DUP 7 ;
                                                          CAR ;
                                                          PAIR ;
                                                          GET ;
                                                          IF_NONE { PUSH nat 0 } {} ;
                                                          SUB ;
                                                          ISNAT ;
                                                          IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                                          SOME ;
                                                          DUP 4 ;
                                                          GET 3 ;
                                                          DUP 6 ;
                                                          CAR ;
                                                          PAIR ;
                                                          UPDATE ;
                                                          UPDATE 5 ;
                                                          DUP ;
                                                          DUG 4 ;
                                                          DUP ;
                                                          GET 5 ;
                                                          DUP 3 ;
                                                          GET 4 ;
                                                          DIG 6 ;
                                                          GET 5 ;
                                                          DIG 4 ;
                                                          DUP ;
                                                          GET 3 ;
                                                          SWAP ;
                                                          DUP ;
                                                          DUG 6 ;
                                                          CAR ;
                                                          PAIR ;
                                                          GET ;
                                                          IF_NONE { PUSH nat 0 } {} ;
                                                          ADD ;
                                                          SOME ;
                                                          DIG 3 ;
                                                          DUP ;
                                                          GET 3 ;
                                                          SWAP ;
                                                          CAR ;
                                                          PAIR ;
                                                          UPDATE ;
                                                          UPDATE 5 ;
                                                          DUG 2 } ;
                                                   DROP } ;
                                            DROP }
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                                            DUP ;
                                            ITER { IF_LEFT
                                                     { DIG 2 ; DUP ; GET 7 ; PUSH bool True ; DIG 3 ; UPDATE ; UPDATE 7 ; SWAP }
                                                     { DIG 2 ; DUP ; GET 7 ; PUSH bool False ; DIG 3 ; UPDATE ; UPDATE 7 ; SWAP } } ;
                                            DROP } } ;
                                    NIL operation }
                                  { IF_LEFT
                                      { DUP ;
                                        ITER { IF_LEFT
                                                 { DUP 3 ;
                                                   GET 13 ;
                                                   IF { PUSH (pair string string) (Pair "FA2_OPERATORS_UNSUPPORTED" "FA2_PAUSED") ;
                                                        FAILWITH }
                                                      {} ;
                                                   DUP ;
                                                   CAR ;
                                                   SENDER ;
                                                   COMPARE ;
                                                   EQ ;
                                                   IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                                   DIG 2 ;
                                                   DUP ;
                                                   GET 11 ;
                                                   PUSH (option unit) (Some Unit) ;
                                                   DIG 3 ;
                                                   UPDATE ;
                                                   UPDATE 11 ;
                                                   SWAP }
                                                 { DUP 3 ;
                                                   GET 13 ;
                                                   IF { PUSH (pair string string) (Pair "FA2_OPERATORS_UNSUPPORTED" "FA2_PAUSED") ;
                                                        FAILWITH }
                                                      {} ;
                                                   DUP ;
                                                   CAR ;
                                                   SENDER ;
                                                   COMPARE ;
                                                   EQ ;
                                                   IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                                   DIG 2 ;
                                                   DUP ;
                                                   GET 11 ;
                                                   NONE unit ;
                                                   DIG 3 ;
                                                   UPDATE ;
                                                   UPDATE 11 ;
                                                   SWAP } } ;
                                        DROP ;
                                        NIL operation }
                                      { IF_LEFT
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                                            DUP ;
                                            ITER { DUP 3 ;
                                                   GET 16 ;
                                                   SWAP ;
                                                   DUP ;
                                                   DUG 2 ;
                                                   CDR ;
                                                   MEM ;
                                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                                   DIG 2 ;
                                                   DUP ;
                                                   GET 16 ;
                                                   DUP 3 ;
                                                   CAR ;
                                                   SOME ;
                                                   DIG 3 ;
                                                   CDR ;
                                                   UPDATE ;
                                                   UPDATE 16 ;
                                                   SWAP } ;
                                            DROP ;
                                            NIL operation }
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                                            DUP ;
                                            CDR ;
                                            CONTRACT unit ;
                                            IF_NONE { PUSH int 581 ; FAILWITH } {} ;
                                            NIL operation ;
                                            SWAP ;
                                            DIG 2 ;
                                            CAR ;
                                            UNIT ;
                                            TRANSFER_TOKENS ;
                                            CONS } } } } ;
                            PAIR } ;
                     view "get_balance_of"
                          (list (pair (address %owner) (nat %token_id)))
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))
                          { UNPAIR ;
                            DUP ;
                            MAP { DUP 3 ;
                                  GET 16 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  CDR ;
                                  MEM ;
                                  IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                  DUP 3 ;
                                  GET 5 ;
                                  SWAP ;
                                  DUP ;
                                  CDR ;
                                  SWAP ;
                                  DUP ;
                                  DUG 3 ;
                                  CAR ;
                                  PAIR ;
                                  GET ;
                                  IF_NONE { PUSH nat 0 } {} ;
                                  SWAP ;
                                  PAIR } ;
                            SWAP ;
                            DROP ;
                            SWAP ;
                            DROP } } ;
                 SWAP ;
                 DROP ;
                 DIG 2 ;
                 DROP ;
                 CONS }
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                 UPDATE 1 ;
                 NIL operation } }
           { IF_LEFT
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                 UPDATE 6 }
               { SENDER ;
                 DUP 3 ;
                 CAR ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "NOT_ADMIN" ; FAILWITH } ;
                 DUP ;
                 ITER { IF_LEFT
                          { DIG 2 ; DUP ; GET 3 ; PUSH bool True ; DIG 3 ; UPDATE ; UPDATE 3 ; SWAP }
                          { DIG 2 ; DUP ; GET 3 ; PUSH bool False ; DIG 3 ; UPDATE ; UPDATE 3 ; SWAP } } ;
                 DROP } ;
             NIL operation } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
